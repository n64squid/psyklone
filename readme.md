# Psyklone

This is the website I made that ran from 2006 to 2011. It was initially made to host and display all the various Flash projects I was working on. It's currently non-functional since Flash has been deprecated, and the 2008 version requires php. The HTML structure for 2006 and 2007 still work fine though.

You can visit the website index here:

**[Psyklone](https://n64squid.gitlab.io/psyklone/)**

![Psyklone header](public/2007/Site Preview.jpg)

I also included the phpBB forum code. It's a phpBB v2 forum with **a lot** of mods (called hacks abck then) installed. There are also some custom content I added myself such as items and flash games. Unfortunately I didn't back up the MySQL database so all the configuration and posts are not recoverable.