<?php
/***************************************************************************
*                           admin_statistics_updates.php
*                            -------------------
*   begin                : Mon, Jan 15, 2007
*   copyright            : (C) 2007 Wicher
*   email                : ---
*
*
*
***************************************************************************/

/***************************************************************************
*
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
***************************************************************************/

define('IN_PHPBB', true);

//
// Let's set the root dir for phpBB
//
$phpbb_root_path = './../';
require($phpbb_root_path . 'extension.inc');

if (!empty($board_config))
{
	@include_once($phpbb_root_path . 'language/lang_' . $board_config['default_lang'] . '/lang_admin_statistics.' . $phpEx);
}

if( isset($HTTP_POST_VARS['mode']) || isset($HTTP_GET_VARS['mode']) )
{
	$mode = ( isset($HTTP_POST_VARS['mode']) ) ? $HTTP_POST_VARS['mode'] : $HTTP_GET_VARS['mode'];
}
else
{
	$mode = '';
}

if( !empty($setmodules) )
{
	$filename = basename(__FILE__);
	$module[$lang['StatTitle']][$lang['stat_update_title']] = $filename;
	return;
}


require('pagestart.' . $phpEx);

include($phpbb_root_path . 'stats_mod/includes/constants.'.$phpEx);

@include_once($phpbb_root_path . 'language/lang_' . $board_config['default_lang'] . '/lang_admin_statistics.' . $phpEx);


$sql = "SELECT * FROM " . STATS_CONFIG_TABLE;
if ( !($result = $db->sql_query($sql)) )
{
	message_die(GENERAL_ERROR, 'Could not query statistics config table', '', __LINE__, __FILE__, $sql);
}
$stats_config = array();
while ($row = $db->sql_fetchrow($result))
{
	$stats_config[$row['config_name']] = trim($row['config_value']);
}

$url = 'http://www.detecties.com/phpbb2018/stat_updates/statistics_updates.txt';
$allow_url_fopen = ini_get('allow_url_fopen');
if ($allow_url_fopen == 1)
{
	if (!@copy ($url, $phpbb_root_path.'modules/statistics_updates.txt')) 
	{
		message_die(GENERAL_MESSAGE, 'Cannot get remote file needed for updatescheck, it seems to be missing.<br />Please contact the MOD Author about this issue.');
	}
}
else if (CURLOPT_HTTPGET)
{
	if (substr(sprintf('%o', @fileperms($phpbb_root_path.'modules/statistics_updates.txt')), -4) == '0666')
	{
    	$fp = fopen($phpbb_root_path.'modules/statistics_updates.txt', 'wt');
    	$curl = curl_init();
    	curl_setopt ($curl, CURLOPT_URL, $url);
    	curl_setopt($curl, CURLOPT_FILE, $fp);
    	curl_exec ($curl);
    	curl_close ($curl);
		fclose($fp);
	}
	else
	{
		message_die(GENERAL_MESSAGE, 'Cannot open modules/statistics_updates.txt for writing, create an (empty) file modules/statistics_updates.txt and chmod it to 666.');
	}
}
else
{
	message_die(GENERAL_MESSAGE, 'Cannot get remote file, needed for checking for updates.');
}
$infofile = $phpbb_root_path.'modules/statistics_updates.txt';

// Get Statistics MOD version
$handle = fopen($infofile, "rt");
if ($handle) 
{
		$line = fgets($handle);
		$pos1 = strpos($line, ']');
		$pos2 = strpos($line, '@');
		$statistics = substr($line, 1, $pos1-1);
		$newversion = substr($line, $pos1+1, ($pos2-1)-$pos1);
		$download = '<a href="'.substr($line, $pos2+1, strlen($line)-($pos2-1)).'">'.$statistics.'</a>';
		if (trim($newversion) == trim($stats_config['version']))
		{
			$download = '<span style="color:green">'.$lang['no_update_available'].'</span>';
			$tr_color = '#FFBF00';
		}
		else
		{
			$tr_color = '#FF6633';
		}
		$template->assign_block_vars('statsversion', array(
			'MODULE_NAME' => trim($statistics),
			'MODULE_VERSION' => trim($stats_config['version']),
			'NEW_MODULE_NAME' => trim($download),
			'NEW_MODULE_VERSION' => trim($newversion),
			'U_NEW_MOD_VERSION' => trim($download),
			'TR_COLOR' => $tr_color
		));
}
fclose($handle);

// See wich modules are installed and get current versions and new versions
$sql = "SELECT i.*, m.short_name, m.active FROM ".MODULE_INFO_TABLE." i, ".MODULES_TABLE." m 
		WHERE i.module_id = m.module_id
		ORDER BY i.long_name ASC";
if (!($result = $db->sql_query($sql)) )
{
	message_die(GENERAL_ERROR, 'Unable to get Module Informations', '', __LINE__, __FILE__, $sql);
}
if ($db->sql_numrows($result) == 0)
{
	message_die(GENERAL_MESSAGE, 'No modules found on your board.');
}

$i = 1;
$installedmodules = array();
$active = array();
$installedversion = array();
while ($row = $db->sql_fetchrow($result))
{
	$installedmodules[] = trim($row['long_name']);
	$active[] = $row['active'];
	$installedversion[] = $row['version'];
	$handle = fopen($infofile, "rt");
	if ($handle) 
	{
		$line = fgets($handle);
		while (!feof($handle)) 
		{
			$line = fgets($handle);
			$pos1 = strpos($line, ']');
			$pos2 = strpos($line, '@');
			$newmodule = substr($line, 1, $pos1-1);
			if (($newmodule == trim($row['long_name'])) && ($row['active'] == 1))
			{
				$newversion = substr($line, $pos1+1, ($pos2-1)-$pos1);
				$download = '<a href="'.substr($line, $pos2+1, strlen($line)-($pos2-1)).'">'.$newmodule.'</a>';
				if (trim($newversion) == trim($row['version']))
				{
					$download = '<span style="color:green">'.$lang['no_update_available'].'</span>';
					$tr_color = '#FFBF00';
				}
				else
				{
					$tr_color = '#FF6633';
				}
				$template->assign_block_vars('latestmodulerow', array(
					'MODULE_NUM' => $i++,
					'MODULE_NAME' => trim($row['long_name']),
					'MODULE_VERSION' => trim($row['version']),
					'NEW_MODULE_NAME' => trim($download),
					'NEW_MODULE_VERSION' => trim($newversion),
					'TR_COLOR' => $tr_color
				));
			}
		}
	}
	fclose($handle);
}

// Get all available modules
$i = 1;
$handle = fopen($infofile, "rt");
if ($handle) {
	$line = fgets($handle);
	while (!feof($handle)) {
		$line = fgets($handle);
		$pos1 = strpos($line, ']');
		$pos2 = strpos($line, '@');
		$newstuff1 = substr($line, 1, $pos1-1);
		$newstuff2 = substr($line, $pos1+1, ($pos2-1)-$pos1);
		$download = '<a href="'.substr($line, $pos2+1, strlen($line)-($pos2-1)).'">'.$newstuff1.'</a>';
		$installed = 0;
		$latest_version_installed = 0;
		for($j = 0; $j < count($installedmodules); $j++)
		{
			if ($installedmodules[$j] == $newstuff1)
			{
				$installed = 1;
				$module_active = $active[$j];
				if ($installedversion[$j] == $newstuff2)
				{
					$latest_version_installed = 1;
					$old_version = $installedversion[$j+1];
				}
			}
		}
		if ($installed == 1)
		{
			$active_module = ($module_active == 1) ? '<span style="color:blue">'.$lang['yes'].'</span>' : '<span style="color:red">'.$lang['no'].'</span>';
			$not_installed = ($latest_version_installed == 1) ? '<span style="color:blue">'.$lang['yes'].'</span>' : '<span style="color:red">'.$lang['no'].', '.$old_version.' '.$lang['installed'].'</span>';
			$tr_color = ($latest_version_installed == 1) ? '#FFBF00' : '#FF6633';
		}
		else
		{
			$not_installed = $lang['not_installed'];
			$active_module = '';
			$tr_color = '#CCFFFF';
		}
		

		$template->assign_block_vars('completelistnew', array(
			'MODULE_NUM' => $i++,
			'NEW_MODULE_NAME' => trim($download),
			'NEW_MODULE_VERSION' => trim($newstuff2),
			'L_INSTALLED' => $not_installed,
			'L_ACTIVE' => $active_module,
			'TR_COLOR' => $tr_color
		));
	}
	fclose($handle);
}


//@unlink($phpbb_root_path.'modules/statistics_updates.txt');


$template->set_filenames(array(
	'body' => 'admin/stat_newupdates.tpl')
);

	$template->assign_vars(array(
	'L_STAT_UPDATES_TITLE' => $lang['stat_update_title'],
	'L_STAT_UPDATES_EXPLAIN' => $lang['stat_update_explain'],
	'L_CURRENT_ACTIVE' => $lang['current_active'],
	'L_CURRENT_VERSION' => $lang['current_version'],
	'L_MODULE_UPDATE' => $lang['module_update'],
	'L_NEWEST_VERSION' => $lang['newest_version'],
	'L_ACTIVATED_ON_BOARD' => $lang['on_board_activated'],
	'L_MODULE' => $lang['module'],
	'L_MODULEVERSION' => $lang['moduleversion'],
	'L_LATEST_INSTALLED' => $lang['latest_installed'],
	'L_ACTIVE' => $lang['active'])
	);


$template->pparse('body');


//
// Page Footer
//
include('./statistics_page_footer_admin.'.$phpEx);

?>