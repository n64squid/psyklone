<?php
/***************************************************************************
 *                            admin_user_prune.php
 *                            -------------------
 *   begin                : Tuesday, Feb 28, 2007
 *   copyright            : (C) 2007 HaLo2FrEeEk, Clan Infectionist, http://claninfectionist.com
 *   email                : halo2freeek@gmail.com

 ***************************************************************************/

/***************************************************************************
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

define('IN_PHPBB', 1);

if( !empty($setmodules) )
{
        $filename = basename(__FILE__);
        $module['Users']['U_Prune'] = $filename;

        return;
}

$phpbb_root_path = './../';
require($phpbb_root_path . 'extension.inc');
require('./pagestart.' . $phpEx);

//
// Set mode
//
if( isset( $HTTP_POST_VARS['mode'] ) || isset( $HTTP_GET_VARS['mode'] ) )
{
        $mode = ( isset( $HTTP_POST_VARS['mode']) ) ? $HTTP_POST_VARS['mode'] : $HTTP_GET_VARS['mode'];
        $mode = htmlspecialchars($mode);
}
else
{
        $mode = '';
}

//
// Begin program
//
if ($mode == 'delete')
{
        // Prune code
        $lastweek = date(strtotime('-1 week'));
        $sql = mysql_query('DELETE FROM ' . USERS_TABLE . ' WHERE user_posts = 0 AND user_id <> ' . ANONYMOUS . ' AND user_regdate < ' . $lastweek);

        $message = $lang['U_Pruned_done_explain'] . "<br /><br />" . sprintf($lang['Click_return_U_Prune'], "<a href=\"" . append_sid("") . "\">", "</a>") . "<br /><br />" . sprintf($lang['Click_return_admin_index'], "<a href=\"" . append_sid("index.$phpEx?pane=right") . "\">", "</a>");
        message_die(GENERAL_MESSAGE, $message);
}
else if ($mode == 'confirm')
{
        // Get a list of users to prune
        $lastweek = date(strtotime('-1 week'));
        $sql = mysql_query('SELECT * FROM ' . USERS_TABLE . ' WHERE `user_posts` = 0 AND `user_id` <> ' . ANONYMOUS . ' AND `user_regdate` < ' . $lastweek . ' ORDER BY `user_id`');

        $pruned = "<table width=\"75%\" cellpadding=\"4\" cellspacing=\"1\" border=\"0\" class=\"forumline\" align=\"center\">\n";
        $pruned .= "<tr>\n";
        $pruned .= "<th width=\"50%\" height=\"25\">Username</th>\n";
        $pruned .= "<th width=\"50%\" height=\"25\">User ID</th>\n";
        $pruned .= "</tr>\n";

        while($row = mysql_fetch_array($sql)) {
          $pruned .= "<tr>\n";
          $pruned .= "<td width=\"50%\" height=\"25\" class=\"row1\" align=\"center\">".$row['username']."</td>\n"; // Username
          $pruned .= "<td width=\"50%\" height=\"25\" class=\"row1\" align=\"center\">".$row['user_id']."</td>\n"; // User ID
          $pruned .= "</tr>\n";
          }

        $pruned .= "</table><br />\n";

        // Send that list to the template file, and ask for confirmation
        $template->set_filenames(array(
                'body' => 'admin/user_prune_confirm.tpl')
        );

        $template->assign_vars(array(
                'L_U_PRUNE_CONF_TITLE' => $lang['U_Prune_conf'],
                'L_U_PRUNE_CONF_EXPLAIN' => $lang['U_Prune_conf_explain'],
                'L_U_PRUNED_LIST' => $pruned,
                'U_CONF_PRUNE' => $lang['U_Conf_prune'],
                'U_CANCEL_PRUNE' => $lang['U_Cancel_prune'],

                'S_U_PRUNE_ACTION' => append_sid("admin_user_prune.$phpEx"))
        );
        $template->pparse('body');
}
else
{
        $template->set_filenames(array(
                'body' => 'admin/user_prune_body.tpl')
        );

        $template->assign_vars(array(
                'L_U_PRUNE_TITLE' => $lang['U_Prune'],
                'L_U_PRUNE_EXPLAIN' => $lang['U_Prune_explain'],
                'L_U_PRUNE' => $lang['U_Prune'],

                'S_U_PRUNE_ACTION' => append_sid("admin_user_prune.$phpEx"))
        );
        $template->pparse('body');
}

include('./page_footer_admin.'.$phpEx);

?>
