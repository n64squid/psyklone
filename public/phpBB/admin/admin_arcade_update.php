<?php
/***************************************************************************
 *
 *                           admin_arcade_update.php
 *                           -----------------------
 *   begin                : Saturday, Jan 6th, 2007
 *   copyright            : (c) 2003-2007 www.phpbb-arcade.com
 *   email                : defenders_realm@yahoo.com
 *
 *   $Id : admin_arcade_update.php, v 2.1.8 2007/01/06 dEfEndEr Exp $
 *   Support @ http://www.phpbb-arcade.com
 *
 ***************************************************************************
 *
 *   This program is copyrighted software; you can use and redistribute it
 *	 only with permission of the Author.
 *
 ***************************************************************************
 *
 *   This is a MOD for phpbb v2.0.15+. 
 *
 ***************************************************************************/

define('IN_PHPBB', 1);
$phpbb_root_path = './../';

if( !empty($setmodules) )
{
//
// No Options for this file so simply allow the parser to go over.
//
	$module['Arcade']['Version'] = $file;
	return;
}

require($phpbb_root_path . 'extension.inc');
require('pagestart.' . $phpEx);
include_once($phpbb_root_path . 'includes/functions_arcade.'.$phpEx);

if ( !defined('ARCADE_CLASSES') )
{
  @include($phpbb_root_path . 'includes/classes_arcade.'.$phpEx);
}
//
//	We need to check the user has done the basic install
//
if (!defined('iNA') || !defined('iNA_GAMES') || !defined('iNA_SCORES') || !defined('iNA_AT_SCORES') ||
 !defined('iNA_SESSIONS') || !defined('iNA_CAT') || !defined('iNA_TOUR') || !defined('iNA_TOUR_PLAY') ||
  !defined('iNA_BANNED') || !defined('iNA_FAV') || !defined('iNA_PMs_TABLE') || !defined('iNA_USER_DATA') || 
  !defined('iNA_GAMES_RATE') || !defined('iNA_GAMES_COMMENT') || !defined('iNA_LOG'))
{
	message_die(GENERAL_ERROR, $lang['arcade_install_issue']);
}

$major    = $minor = 0;
$version  = '1.0.0';
$message  = '';
$mode     = $arcade->pass_var('mode', '');
//
//  Version Checker
//
if($mode == '')
{
  $cache_update = 30; // 1 hour cache timeout. To save MY bandwidth
  $arcade_cache_file = './../cache/arcade_update.'.$phpEx; // file where to store cache 

  if(file_exists($arcade_cache_file)) 
  {  
    include($arcade_cache_file); 
  } 
  if(($arcade_last_update < (time() - $cache_update)) || ($current_version < ARCADE_VERSION))
  { 
    $host = 'www.phpbb-arcade.com';
    if ($fsock = @fsockopen($host, 80, $errno, $errstr, 5))
  	{
  		@fputs($fsock, "GET /arcade_version.txt HTTP/1.0\r\n");
  		@fputs($fsock, "HOST: $host\r\n");
  		@fputs($fsock, "User-Agent: phpbb-arcade\r\n");
  		@fputs($fsock, "Content-Type: text/html\r\n");
  		@fputs($fsock, "Connection: close\r\n\r\n");

  		$get_info = false;
  		while (!@feof($fsock))
  		{
  			if ($get_info)
  			{
  				$arcade_version_info .= @fread($fsock, 1024);
  			}
  			else
  			{
  				if (@fgets($fsock, 1024) == "\r\n")
  				{
  					$get_info = true;
  				}
  			}
  		}
  		@fclose($fsock);
  	}

 		$arcade_version_info = explode("\n", $arcade_version_info);
 		$major_temp = stristr($arcade_version_info[0], 'major ');
 		$major_temp = explode(' ', $major_temp);
 		$major = $major_temp[1];
 		unset($major_temp);
 		$check_sql = "SELECT `config_value` FROM ".$table_prefix."ina_data WHERE `config_name` = 'version'";
 		if(!$result = $db->sql_query($check_sql))
 		{
 			$message .= '<br><center>[<a href="' . append_sid("$file?mode=install") . '"><b>&raquo; INSTALL &laquo;</b></a>]</center><br>';
 		}
 		$ina_info = $db->sql_fetchrow($result);
 		$major_version = $ina_info['config_value'];
 		if($major_version < $major)
 		{
      $message .= '<br>' . sprintf($lang['activitiy_mod__newinfo'], $major) . '<br>';
 		}
  }
	if($current_version < $major)
	{
  	$message .= '<br><center>[<a href="' . append_sid("$file?mode=update") . '"><b>&raquo; UPDATE to '.$major.'&laquo;</b></a>]</center><br>';
	}
  $message .= '<br>' . sprintf($lang['activitiy_mod_info'], $current_version) . '<br><br />';
}
//
//  See if we are doing a NEW install
//
else if ($mode == 'install')
{
  $sql = array();
  
  $sql[] = "CREATE TABLE ". iNA ." (`config_name` varchar(255) default NULL,`config_value` varchar(255) default NULL, UNIQUE KEY `config_name` (`config_name`))";

  $sql[] = "CREATE TABLE " . iNA_CAT . " (
    `cat_id` mediumint(8) NOT NULL auto_increment,
    `cat_name` varchar(100) default NULL,
    `cat_desc` text,
    `cat_icon` varchar(255) default NULL,
    `mod_id` mediumint(8) default NULL,
    `special_play` smallint(5) NOT NULL default '0',
    `last_game` varchar(50) default NULL,
    `last_player` mediumint(8) default NULL,
    `last_time` int(11) default NULL,
    `group_required` mediumint(8) NOT NULL default '0',
    `cat_type` char(1) NOT NULL default 'p',
    `cat_parent` mediumint(8) default NULL,
    `cat_order` mediumint(8) unsigned NOT NULL default '1',
    `total_games` int(8) unsigned NOT NULL default '0',
    `total_played` bigint(12) unsigned NOT NULL default '0',
    PRIMARY KEY  (`cat_id`))";

  $sql[] = "CREATE TABLE " . iNA_GAMES . " (
    `game_id` mediumint(9) NOT NULL auto_increment,
    `cat_id` mediumint(8) NOT NULL default '-1',
    `game_name` varchar(50) default NULL,
    `game_path` varchar(255) default NULL,
    `image_path` varchar(255) default NULL,
    `game_desc` varchar(255) default NULL,
    `game_charge` int(11) unsigned default '0',
    `game_reward` int(11) unsigned NOT NULL default '0',
    `game_bonus` smallint(5) unsigned default '0',
    `at_game_bonus` smallint(5) default '0',
    `game_use_gl` tinyint(3) unsigned default '0',
    `game_flash` tinyint(1) unsigned NOT NULL default '0',
    `game_show_score` tinyint(1) NOT NULL default '1',
    `game_avail` tinyint(1) unsigned default '1',
    `allow_guest` tinyint(1) unsigned default '0',
    `win_width` smallint(6) NOT NULL default '0',
    `win_height` smallint(6) NOT NULL default '0',
    `highscore_limit` int(11) default NULL,
    `at_highscore_limit` int(10) default '0',
    `reverse_list` tinyint(1) NOT NULL default '0',
    `played` int(10) unsigned NOT NULL default '0',
    `instructions` text,
    `score_type` smallint(1) default NULL,
    `game_autosize` smallint(1) default NULL,
    `date_added` int(11) default '0',
    `rank_required` int(11) NOT NULL default '0',
    `level_required` tinyint(4) NOT NULL default '0',
    `group_required` mediumint(8) NOT NULL default '0',
    `game_control` int(1) default '0',
    `highscore_id` mediumint(8) NOT NULL default '0',
    `at_highscore_id` mediumint(8) NOT NULL default '0',
    PRIMARY KEY  (`game_id`),
    KEY `game_name` (`game_name`))";

  $sql[] = "CREATE TABLE " . iNA_FAV . " (
    `user_id` mediumint(8) NOT NULL default '0',
    `fav_game_name` varchar(50) NOT NULL default '',
    PRIMARY KEY  (`fav_game_name`,`user_id`))";

  $sql[] = "CREATE TABLE " . iNA_SESSIONS . " (
    `arcade_hash` varchar(33) NOT NULL default '',
    `user_id` mediumint(8) NOT NULL default '0',
    `start_time` int(11) NOT NULL default '0',
    `session_ip` varchar(8) NOT NULL default '',
    `page` int(1) NOT NULL default '0',
    `game_name` varchar(50) default NULL,
    `tour_id` mediumint(5) default NULL,
    `user_ip` varchar(16) default '0',
    `ip_name` varchar(255) default NULL,
    `user_win` varchar(25) NOT NULL default 'NORM',
    `randchar1` int(4) default NULL,
    `randchar2` int(4) default NULL,
    PRIMARY KEY  (`arcade_hash`),
    KEY `user_id` (`user_id`))";

  $sql[] = "CREATE TABLE " . iNA_SCORES . " (
    `game_name` varchar(50) default NULL,
    `gameData` text,
    `player_id` mediumint(8) default NULL,
    `player_ip` varchar(16) default NULL,
    `score` double(14,4) NOT NULL default '0.0000',
    `date` int(11) default NULL,
    `time_taken` int(11) default NULL,
    PRIMARY KEY  (`game_name`,`player_id`))";

  $sql[] = "CREATE TABLE " . iNA_AT_SCORES . " (
    `game_name` varchar(50) NOT NULL default '',
    `player_id` mediumint(8) NOT NULL default '0',
    `player_name` varchar(25) NOT NULL default '',
    `player_ip` varchar(16) default NULL,
    `score` double(14,4) NOT NULL default '0.0000',
    `date` int(11) default NULL,
    `time_taken` int(11) default NULL,
    PRIMARY KEY  (`game_name`,`player_id`))";

  $sql[] = "CREATE TABLE " . iNA_GAMES_COMMENT . " (
    `comment_id` int(11) unsigned NOT NULL auto_increment,
    `comment_game_name` varchar(50) default NULL,
    `comment_user_id` mediumint(8) NOT NULL default '0',
    `comment_username` varchar(32) default NULL,
    `comment_user_ip` varchar(8) NOT NULL default '',
    `comment_time` int(11) unsigned NOT NULL default '0',
    `comment_text` text,
    `comment_edit_time` int(11) unsigned default NULL,
    `comment_edit_count` smallint(5) unsigned NOT NULL default '0',
    `comment_edit_user_id` mediumint(8) default NULL,
    PRIMARY KEY  (`comment_id`)
    KEY `comment_game_name` (`comment_game_name`))";
    
  $sql[] = "CREATE TABLE " . iNA_GAMES_RATE . " (
    `rate_game_name` varchar(50) NOT NULL default '',
    `rate_user_id` mediumint(8) NOT NULL default '0',
    `rate_user_ip` varchar(8) NOT NULL default '',
    `rate_point` tinyint(3) unsigned NOT NULL default '0',
    PRIMARY KEY  (`rate_game_name`,`rate_user_id`)) ";

  $sql[] = "CREATE TABLE " . iNA_USER_DATA . " (
    `user_id` mediumint(8) NOT NULL default '0',
    `last_played` varchar(50) default NULL,
    `last_played_date` int(11) default NULL,
    `first_places` mediumint(9) default '0',
    `first_list` text,
    `second_places` mediumint(8) default '0',
    `second_list` text,
    `third_places` mediumint(9) default NULL,
    `third_list` text,
    `last_won_date` int(11) default NULL,
    `at_first_places` mediumint(9) default '0',
    `at_first_list` text,
    `at_second_places` mediumint(9) default NULL,
    `at_second_list` text,
    `at_third_places` mediumint(9) default NULL,
    `at_third_list` text,
    `user_game_time` int(11) default '0',
    PRIMARY KEY  (`user_id`))";

  $sql[] = "CREATE TABLE ". iNA_LOG . " (
    `record_no` mediumint(8) unsigned NOT NULL auto_increment,
    `user_id` mediumint(8) NOT NULL default '-1',
    `name` text,
    `value` text,
    `date` int(11) NOT NULL default '0',
    PRIMARY KEY  (`record_no`))";

  $sql[] = "CREATE TABLE " . iNA_TOUR . " ( 
    `tour_id` mediumint(5) NOT NULL auto_increment, 
    `tour_name` varchar(25) default NULL, 
    `tour_desc` text, 
    `tour_max_players` int(4) NOT NULL default '0', 
    `tour_player_turns` int(4) NOT NULL default '0', 
    `block_plays` int(1) default NULL, 
    `tour_active` int(1) NOT NULL default '0', 
    `start_id` mediumint(8) NOT NULL default '0', 
    `start_date` int(11) NOT NULL default '0', 
    `end_date` int(11) default NULL, 
    `length` int(11) default NULL, 
    `end_type` int(1) default NULL, 
    `champion` mediumint(8) default NULL, 
     PRIMARY KEY  (`tour_id`) )"; 
 
  $sql[] = "CREATE TABLE " . iNA_TOUR_DATA . " ( 
    `tour_id` mediumint(5) NOT NULL default '0', 
    `game_name` varchar(50) NOT NULL default '', 
    `top_score` double(12,4) default NULL, 
    `top_player` mediumint(11) default NULL, 
     PRIMARY KEY  (`tour_id`,`game_name`) )"; 
 
  $sql[] = "CREATE TABLE " . iNA_TOUR_PLAY . " ( 
    `tour_id` mediumint(5) NOT NULL default '0', 
    `user_id` mediumint(8) NOT NULL default '0', 
    `last_played_time` int(11) NOT NULL default '0', 
    `last_played_game` varchar(50) default NULL, 
    `gamedata` text,
     PRIMARY KEY  (`tour_id`,`user_id`) )"; 

  $sql[] = "CREATE TABLE " . iNA_PMs_TABLE . " (
    `to_id` mediumint(8) NOT NULL default '0',
    `from_id` mediumint(8) NOT NULL default '0',
    `last_sent` int(11) NOT NULL default '0',
    `total_sent` mediumint(8) default NULL,
    `code` tinyint(4) default NULL,
    KEY `to_id` (`to_id`))";

  $sql[] = "CREATE TABLE " . iNA_BANNED . " (
    `username` varchar(25) default NULL,
    `user_id` mediumint(8) default NULL,
    `player_ip` varchar(16) default NULL,
    `game` varchar(25) default NULL,
    `score` mediumint(8) default NULL,
    `date` varchar(16) default NULL,
    KEY `user_id` (`user_id`)) ";

  $sql[] = "CREATE TABLE " . iNA_HIGHSCORE . "(
    `highscore_id` MEDIUMINT( 8 ) UNSIGNED NOT NULL AUTO_INCREMENT ,
    `highscore_year` YEAR UNSIGNED NOT NULL ,
    `highscore_mon` TINYINT( 2 ) UNSIGNED NOT NULL ,
    `highscore_game` VARCHAR( 50 ) NOT NULL ,
    `highscore_player` VARCHAR( 40 ) NOT NULL ,
    `highscore_score` DOUBLE( 12, 4 ) UNSIGNED NOT NULL ,
    `highscore_date` INT( 11 ) UNSIGNED NOT NULL ,
    PRIMARY KEY `highscore_id` ( `highscore_id` ) )";
//
//  Default Data Required to start the Arcade
//
  $sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`)
    VALUES  ('version', '2.1.8'),
            ('default_reward_dbfield', 'user_points'),
            ('default_cash', 'user_cash'),
            ('use_rewards_mod', '0'),
            ('use_allowance_system', '0'),
            ('use_point_system', '0'),
            ('use_gk_shop', '0'),
            ('use_cash_system', '0'),
            ('report_cheater', '0'),
            ('warn_cheater', '0'),
            ('use_gamelib', '0'),
            ('games_path', 'games/'),
            ('gamelib_path', '0'),
            ('games_show_fav', '1'),
            ('games_per_page', '20'),
            ('games_default_img', 'templates/subSilver/images/games.gif'),
            ('games_default_txt', 'More games available to Registered Members - No Fee - No Catch.<br />'),
            ('games_default_id', '0'),
            ('games_tournament_mode', '0'),
            ('games_offline', '0'),
            ('games_cheat_mode', '1'),
            ('games_guest_highscore', '1'),
            ('games_auto_size', '1'),
            ('games_at_highscore', '1'),
            ('games_show_stats', '1'),
            ('games_image_width', '50'),
            ('games_image_height', '50'),
            ('games_per_admin_page', '20'),
            ('games_cat_image_width', '80'),
            ('games_cat_image_height', '80'),
            ('games_tournament_max', '10'),
            ('games_tournament_games', '6'),
            ('games_tournament_players', '36'),
            ('games_moderators_mode', '0'),
            ('games_posts_required', '0'),
            ('games_use_pms', '1'),
            ('games_total_top', '10'),
            ('games_new_games', '1'),
            ('games_cat_zero', 'Show all the Games'),
            ('games_use_comments', '0'),
            ('games_use_rating', '0'),
            ('games_show_played', '1'),
            ('games_show_all', '1'),
            ('games_no_guests', '0'),
            ('games_comments', '1'),
            ('games_mod_ban_users', '0'),
            ('games_comment_size', '256'),
            ('games_rate', '1'),
            ('games_rank_required', '0'),
            ('games_pm_highscore', '1'),
            ('games_pm_at_highscore', '1'),
            ('games_pm_comment', '1'),
            ('games_pm_new', '0'),
            ('highscore_start_year', '" . date(Y) . "'),
            ('highscore_start_mon', '" . date(m) . "'),
            ('default_sort', 'default'),
            ('default_sort_order', 'DESC'),
            ('games_group_required', '0'),
            ('games_new_for', '1209600'),
            ('use_cache', '1'),
            ('config_cache', '172800'),
            ('categories_cache', '60'),
            ('games_cache', '86400'),
            ('highscore_cache', '300'),
            ('games_show_mhm', '1'),
            ('games_tournament_user', '0'),
            ('games_default_rate', '5'),
            ('at_highscore_cache', '300'),
            ('games_level_required', '0'),
            ('games_use_log', '1')";
//
//  Default Cat ID
//
  $sql[] = "INSERT INTO " . iNA_CAT . " (`cat_id`, `cat_type`, `total_games`) 
      VALUES (-1, 'p', 1)";
//
//  Donkey Kong Game
//
  $sql[] = "INSERT INTO " . iNA_GAMES . " (`game_id`, `cat_id`, `game_name`, `game_path`, `game_desc`, `game_flash`, `game_show_score`, `game_avail`, `allow_guest`, `win_width`, `win_height`, `highscore_limit`, `at_highscore_limit`, `reverse_list`, `played`, `instructions`, `score_type`, `game_autosize`, `date_added`, `rank_required`, `level_required`, `group_required`, `game_control`) 
    VALUES (1, -1, 'donkeykong', 'games/donkeykong/', 'The Classic - Donkey Kong', 1, 1, 0, 1, 448, 600, 50, 100, 0, 0, 'CLICK where it says ''Press Space Bar To Start'' to start the game :(', 3, 0, " . date() . ", 0, 0, 0, 1)";
}
//
//  Upgrade
//
else if($mode = 'update')
{
//
//  Try and get the NEW style version 1st
//
  $sql = "SELECT `config_value` FROM " . iNA . " WHERE `config_name` = 'version'";
  $result = $db->sql_query($sql);
  if($result)
  {
    $ina_info = $db->sql_fetchrow($result);
    if($ina_info)
    {
     $version = $ina_info['config_value'];
    } 
  }
//
//  Check to see if that failed
//
  if($version == '1.0.0')
  {
    $sql = "SELECT version FROM " . iNA;
    $result = $db->sql_query($sql);
    if($result)
    {
    	$ina_info = $db->sql_fetchrow($result);
    	if($ina_info)
    	{
        $version = $ina_info['version'];
      }
  	}
  }
  else if(empty($version))
  {
    die('Unable to Read DATABASE.');
  }
  echo ("<center>Current Version: $version<br><br></center>");
  
  $sql = array();
//
//  New lets see what we have to do
//
  switch ($version)
  {
	case 'v1.0.0':
	case '1.0.0':
		$sql[] = "CREATE TABLE " . iNA . " (
        `version` VARCHAR(25) DEFAULT '2.0.0' )";
		$sql[] = "CREATE TABLE " . iNA_GAMES . " (
        `game_id` mediumint(9) NOT NULL auto_increment, 
				`game_name` varchar(25) default NULL, 
				`game_path` varchar(255) default NULL, 
				`game_desc` varchar(255) default NULL, 
				`game_charge` int(11) unsigned default '0', 
				`game_reward` int(11) unsigned NOT NULL default '0', 
				`game_bonus` smallint(5) unsigned default '0', 
				`game_use_gl` tinyint(3) unsigned default '0', 
				`game_flash` tinyint(1) unsigned NOT NULL default '0', 
				`game_show_score` tinyint(1) NOT NULL default '1', 
				`win_width` smallint(6) NOT NULL default '0', 
				`win_height` smallint(6) NOT NULL default '0', 
				`highscore_limit` varchar(255) default NULL, 
				`reverse_list` tinyint(1) NOT NULL default '0', 
				`played` int(10) unsigned NOT NULL default '0', 
				`instructions` text, 
				PRIMARY KEY `game_id` (`game_id`) )";

		$sql[] = "CREATE TABLE " . iNA_SCORES . " (
        `game_name` varchar(255) default NULL, 
				`player` varchar(40) default NULL, 
				`score` int(10) unsigned NOT NULL default '0', 
				`date` int(11) default NULL,
        KEY `game_name` (`game_name`))";

		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('default_reward_dbfield','')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('default_cash','')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('use_rewards_mod','0')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('use_cash_system','0')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('report_cheater','0')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('warn_cheater','0')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('use_point_system','0')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('use_gamelib','0')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_path','')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('gamelib_path','')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('use_gk_shop','0')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('use_allowance_system','0')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_per_page','20')";

    $sql[] = "INSERT INTO " . iNA . "  ( `version` ) VALUES ('2.0.0')";
	
	case 'v2.0.0':
	case '2.0.0':
		$sql[] = "ALTER TABLE " . iNA_GAMES . " ADD `game_avail` TINYINT(1) default 1";
		$sql[] = "ALTER TABLE " . iNA_GAMES . " ADD `allow_guest` TINYINT(1) default 0";
		$sql[] = "ALTER TABLE " . iNA_GAMES . " ADD `image_path` VARCHAR(255) default '.gif'";
		$sql[] = "ALTER TABLE " . iNA_GAMES . " ADD `cat_id` MEDIUMINT(8) default NULL";

		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_default_img','templates/subSilver/images/games.gif')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_default_txt','Over 120 games available to Registered Members - No Fee - No Catch.<br />')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_default_id','0')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_tournament_mode','0')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_offline','0')";

		$sql[] = "CREATE TABLE " . iNA_AT_SCORES . " (
        `game_name` varchar(255) default NULL, 
				`player` varchar(40) default NULL, 
				`score` int(10) unsigned NOT NULL default '0', 
				`date` int(11) default NULL,
        KEY `game_name` (`game_name`))";

	case 'v2.0.1':
	case '2.0.1':
		$sql[] = "ALTER TABLE " . iNA_SCORES . " ADD `player_id` MEDIUMINT(8) AFTER `player`";
		$sql[] = "ALTER TABLE " . iNA_AT_SCORES . " ADD `player_id` MEDIUMINT(8) AFTER `player`";

		$sql[] = "CREATE TABLE " . iNA_SESSIONS . " (
        `session_id` varchar(32) default NULL,
				`user_id` mediumint(8) NOT NULL default '0',
				`start_time` int(11) NOT NULL default '0',
				`session_ip` varchar(8) NOT NULL default '',
				`page` int(1) NOT NULL default '0',
				`game_name` varchar(25) default NULL,
				`user_ip` varchar(16) default '0',
				`ip_name` varchar(255) default NULL,
        KEY `session_id` (`session_id`))";
 
		$sql[] = "UPDATE " . iNA_SCORES . " AS s, " . USERS_TABLE . " AS u SET `s.player_id` = `u.user_id` WHERE `s.player` = `u.username`";

		$sql[] = "UPDATE " . iNA_AT_SCORES . " AS s, " . USERS_TABLE . " AS u SET `s.player_id` = `u.user_id` WHERE `s.player` = `u.username`";

		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_cheat_mode','1')";
	
	case 'v2.0.4':
	case '2.0.4':
	case '2.0.2':
	case 'v2.0.2':
		$sql[] = "ALTER TABLE " . iNA_SCORES . " ADD `player_ip` VARCHAR(16) default 0";
		$sql[] = "ALTER TABLE " . iNA_AT_SCORES . " ADD `player_ip` VARCHAR(16) default 0";

		$sql[] = "ALTER TABLE " . iNA_GAMES . " ADD `cat_id` mediumint(8) default 0";
		$sql[] = "ALTER TABLE " . iNA_GAMES . " ADD `at_highscore_limit` smallint(5) default 0";
		$sql[] = "ALTER TABLE " . iNA_GAMES . " ADD `at_game_bonus` smallint(5) default 0";

    $sql[] = "INSERT INTO " . iNA_GAMES . " (`game_id`, `game_name`, `game_path`, `image_path`, `game_desc`, `game_charge`, `game_reward`, `game_bonus`, `game_use_gl`, `game_flash`, `game_show_score`, `win_width`, `win_height`, `highscore_limit`, `reverse_list`, `played`, `instructions`) VALUES (1, 'donkeykong', 'games/donkeykong/', '', 'The Classic - Donkey Kong', 0, 0, 0, 0, 1, 1, 448, 600, 0, 0, 0, 'CLICK where it says \'Press Space Bar To Start\' to start the game :(')";

		$sql[] = "CREATE TABLE " . iNA_CAT . " (
			`cat_id` mediumint(8) NOT NULL auto_increment,
			`mod_id` mediumint(8) default NULL,
			`name` varchar(100) default NULL,
			`desc` text,
			`icon` varchar(255) default NULL,
			`special_play` smallint(5) NOT NULL default '0',
			`last_game` varchar(25) default NULL,
			`last_player` mediumint(8) default NULL,
			`last_time` int(11) default NULL,
			PRIMARY KEY  (`cat_id`),
			KEY `cat_id` (`cat_id`))";
	
	case 'v2.0.5';
	case '2.0.5';
		$sql[] = "ALTER TABLE " . iNA_SESSIONS . " ADD `arcade_hash` varchar(33) default NULL";
		$sql[] = "ALTER TABLE " . iNA_SESSIONS . " ADD KEY `arcade_hash` (`arcade_hash`)";

		$sql[] = "ALTER TABLE " . iNA_GAMES . " ADD `score_type` smallint(1) default NULL";
		$sql[] = "ALTER TABLE " . iNA_GAMES . " ADD `cat_id` mediumint(8) default 0";
		$sql[] = "ALTER TABLE " . iNA_GAMES . " ADD `at_highscore_limit` smallint(5) default 0";
		$sql[] = "ALTER TABLE " . iNA_GAMES . " ADD `at_game_bonus` smallint(5) default 0";
	
		$sql[] = "ALTER TABLE " . iNA_SCORES . " CHANGE `score` `score` DOUBLE( 12, 4 ) UNSIGNED DEFAULT '0' NOT NULL";
		$sql[] = "ALTER TABLE " . iNA_SCORES . " ADD `player_ip` VARCHAR(16) default 0";
		$sql[] = "ALTER TABLE " . iNA_AT_SCORES . " ADD `player_ip` VARCHAR(16) default 0";
		$sql[] = "ALTER TABLE " . iNA_AT_SCORES . " CHANGE `score` `score` DOUBLE( 12, 4 ) UNSIGNED DEFAULT '0' NOT NULL";

		$sql[] = "CREATE TABLE " . iNA_BANNED . " (
			`username` varchar(25) default NULL,
			`user_id` mediumint(8) default NULL,
			`player_ip` varchar(16) default NULL,
			`game` varchar(25) default NULL,
			`score` mediumint(8) default NULL,
			`date` varchar(16) default NULL,
      KEY `user_id` (`user_id`))";

		$sql[] = "CREATE TABLE " . iNA_CAT . " (
			`cat_id` mediumint(8) NOT NULL auto_increment,
			`mod_id` mediumint(8) default NULL,
			`name` varchar(100) default NULL,
			`desc` text,
			`icon` varchar(255) default NULL,
			`special_play` smallint(5) NOT NULL default '0',
			`last_game` varchar(25) default NULL,
			`last_player` mediumint(8) default NULL,
			`last_time` int(11) default NULL,
			PRIMARY KEY `cat_id` (`cat_id`) )";

		$sql[] = "CREATE TABLE " . iNA_FAV . " (
			`user_id` mediumint(8) NOT NULL default '0',
			`game_id` mediumint(9) NOT NULL default '0',
      KEY `game_id` (`game_id`))";

		$sql[] = "CREATE TABLE " . iNA_TOUR . " (
			`tour_id` mediumint(5) NOT NULL auto_increment,
			`tour_name` varchar(25) default NULL,
			`tour_desc` text,
			PRIMARY KEY `tour_id` (`tour_id`))";

		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_guest_highscore', '1')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_auto_size', '1')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_at_highscore', '1')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_show_stats', '1')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_image_width', '50')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_image_height', '50')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_per_admin_page', '')";

	case 'v2.0.6':
	case '2.0.6':
		$sql[] = "CREATE TABLE " . iNA_PMs_TABLE . " (
				`to_id` mediumint(8) NOT NULL default '0',
				`from_id` mediumint(8) NOT NULL default '0',
				`last_sent` int(11) NOT NULL default '0',
				`total_sent` mediumint(8) default NULL,
				`code` tinyint(4) default NULL,
        KEY `to_id` (`to_id`))";

    $sql[] = "CREATE TABLE  " . iNA_USER_DATA . " (
      `user_id` mediumint(8) NOT NULL default '0',
      `last_played` varchar(25) default NULL,
      `last_played_date` int(11) default NULL,
      `first_places` mediumint(9) default '0',
      `last_won_date` int(11) default NULL,
      `at_first_places` mediumint(9) default '0',
      `user_game_time` int(11) default '0',
      KEY `user_id` (`user_id`))";

		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_cat_image_width', '80')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_cat_image_height', '80')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_tournament_max', '10')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_tournament_games', '6')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_tournament_players', '12')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_moderators_mode', '0')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_posts_required', '1')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_use_pms', '0')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_total_top', '10')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_new_games', '1')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_cat_zero', 'Show all the Games')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_use_comments', '0')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_use_rating', '0')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_show_played', '1')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_show_all', '1')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_no_guests', '0')";

		$sql[] = "ALTER TABLE " . iNA_GAMES . " ADD `game_autosize` smallint(1) default NULL";
		$sql[] = "ALTER TABLE " . iNA_GAMES . " ADD `date_added` int(11) default '0'";
		$sql[] = "ALTER TABLE " . iNA_GAMES . " ADD `rank_required` int(11) default NULL";
		$sql[] = "ALTER TABLE " . iNA_GAMES . " ADD `level_required` tinyint(4) default NULL";

		$sql[] = "ALTER TABLE " . iNA_GAMES . " CHANGE `game_name` `game_name` VARCHAR( 50 ) DEFAULT NULL";
		$sql[] = "ALTER TABLE " . iNA_SESSIONS . " CHANGE `game_name` `game_name` VARCHAR( 50 ) DEFAULT NULL";
		$sql[] = "ALTER TABLE " . iNA_SCORES . " CHANGE `game_name` `game_name` VARCHAR( 50 ) DEFAULT NULL";
		$sql[] = "ALTER TABLE " . iNA_AT_SCORES . " CHANGE `game_name` `game_name` VARCHAR( 50 ) DEFAULT NULL";

		$sql[] = "ALTER TABLE " . iNA_CAT . " CHANGE `name` `cat_name` VARCHAR( 100 ) DEFAULT NULL";
		$sql[] = "ALTER TABLE " . iNA_CAT . " CHANGE `desc` `cat_desc` TEXT DEFAULT NULL";
		$sql[] = "ALTER TABLE " . iNA_CAT . " CHANGE `icon` `cat_icon` VARCHAR( 255 ) DEFAULT NULL";

	case 'v2.0.7':
	case '2.0.7':
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_cat_image_width', '80')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_cat_image_height', '80')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_tournament_max', '10')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_tournament_games', '6')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_tournament_players', '12')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_moderators_mode', '0')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_posts_required', '1')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_use_pms', '1')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_total_top', '10')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_new_games', '1')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_cat_zero', 'Show all the Games')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_use_comments', '0')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_use_rating', '0')";

		$sql[] = "ALTER TABLE " . iNA_GAMES . " ADD `game_autosize` smallint(1) default NULL";
		$sql[] = "ALTER TABLE " . iNA_GAMES . " ADD `date_added` int(11) default '0'";
		$sql[] = "ALTER TABLE " . iNA_GAMES . " ADD `rank_required` int(11) default NULL";
		$sql[] = "ALTER TABLE " . iNA_GAMES . " ADD `level_required` tinyint(4) default NULL";

		$sql[] = "ALTER TABLE " . iNA_SESSIONS . " CHANGE `game_name` `game_name` VARCHAR( 50 ) DEFAULT NULL";
		$sql[] = "ALTER TABLE " . iNA_GAMES . " CHANGE `game_name` `game_name` VARCHAR( 50 ) DEFAULT NULL";
		$sql[] = "ALTER TABLE " . iNA_SCORES . " ADD `time_taken` INT( 11 ) DEFAULT NULL";
		$sql[] = "ALTER TABLE " . iNA_SCORES . " CHANGE `game_name` `game_name` VARCHAR( 50 ) DEFAULT NULL";
		$sql[] = "ALTER TABLE " . iNA_AT_SCORES . " CHANGE `game_name` `game_name` VARCHAR( 50 ) DEFAULT NULL";
		$sql[] = "ALTER TABLE " . iNA_AT_SCORES . " ADD `time_taken` INT( 11 ) DEFAULT NULL";

		$sql[] = "ALTER TABLE " . USERS_TABLE . " ADD `games_block_pm` tinyint(1) NOT NULL default 1";
		
	case 'v2.0.8':
	case '2.0.8':
			$sql[] = "ALTER TABLE " . iNA . " ADD `minor` TINYINT(3) default '0'";
      $sql[] = "ALTER TABLE " . USERS_TABLE . " CHANGE `user_rank` `user_rank` INT( 11 ) NOT NULL DEFAULT '0'";
      $sql[] = "ALTER TABLE " . USERS_TABLE . " CHANGE `user_level` `user_level` TINYINT( 4 ) NOT NULL DEFAULT '0'";
			$sql[] = "ALTER TABLE " . iNA_SESSIONS . " ADD `user_win` VARCHAR(25) default 'NORM'";
      $sql[] = "ALTER TABLE " . iNA_USER_DATA . " ADD `user_game_time` INT( 11 ) DEFAULT '0'";
      $sql[] = "ALTER TABLE " . iNA_GAMES . " ADD `rank_required` INT( 11 ) DEFAULT '0' NOT NULL";
      $sql[] = "ALTER TABLE " . iNA_GAMES . " ADD `level_required` TINYINT( 4 ) DEFAULT '0' NOT NULL";
      $sql[] = "ALTER TABLE " . iNA_GAMES . " ADD `group_required` MEDIUMINT( 8 ) DEFAULT '0' NOT NULL";
      $sql[] = "ALTER TABLE " . iNA_GAMES . " CHANGE `level_required` `level_required` TINYINT( 4 ) NOT NULL DEFAULT '0'";
      $sql[] = "ALTER TABLE " . iNA_GAMES . " CHANGE `group_required` `group_required` MEDIUMINT( 8 ) DEFAULT '0' NOT NULL";
      $sql[] = "ALTER TABLE " . iNA_GAMES . " CHANGE `rank_required` `rank_required` INT( 11 ) DEFAULT '0' NOT NULL";
      $sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_rate', '1')";
		  $sql[] = "ALTER TABLE " . iNA_FAV . " CHANGE `game_id` `fav_game_id` MEDIUMINT( 9 ) DEFAULT NULL";
      $sql[] = "CREATE TABLE " . iNA_GAMES_RATE . " (
        `rate_game_name` varchar(50) default NULL,
        `rate_user_id` mediumint(8) NOT NULL default '0',
        `rate_user_ip` varchar(8) NOT NULL default '',
        `rate_point` tinyint(3) unsigned NOT NULL default '0',
        KEY `rate_game_name` (`rate_game_name`))";

    $sql[] = "CREATE TABLE " . iNA_GAMES_COMMENT ."  (
          `comment_id` int(11) unsigned NOT NULL auto_increment,
          `comment_game_name` varchar(50) default NULL,
          `comment_user_id` mediumint(8) NOT NULL default '0',
          `comment_username` varchar(32) default NULL,
          `comment_user_ip` varchar(8) NOT NULL default '',
          `comment_time` int(11) unsigned NOT NULL default '0',
          `comment_text` text,
          `comment_edit_time` int(11) unsigned default NULL,
          `comment_edit_count` smallint(5) unsigned NOT NULL default '0',
          `comment_edit_user_id` mediumint(8) default NULL,
          `game_name` varchar(50) default NULL,
        	PRIMARY KEY `comment_id` (`comment_id`),
        	KEY `comment_game_name` (`comment_game_name`),
        	KEY `comment_user_id` (`comment_user_id`))";
        	
    $sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_comments', '1')";
    $sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_mod_ban_users', '0')";
    $sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_comment_size', '256')";
    $sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_rate', '1')";
 		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_rank_required', '0')";
 		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_pm_highscore', '0')";
 		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_pm_at_highscore', '0')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_pm_comment', '0')";
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_pm_new', '0')";

	  $sql[] = "ALTER TABLE " . iNA_FAV . " ADD `fav_game_name` varchar( 50 ) DEFAULT NULL";
	  $sql[] = "ALTER TABLE " . iNA_GAMES_RATE . " ADD `rate_game_name` varchar( 50 ) DEFAULT NULL";
	  $sql[] = "ALTER TABLE " . iNA_GAMES_COMMENT . " ADD `comment_game_name` varchar( 50 ) DEFAULT NULL";
    $sql[] = "UPDATE " . iNA_FAV . " f, " . iNA_GAMES . " g SET `f.fav_game_name` = `g.game_name` WHERE `g.game_id` = `f.fav_game_id`";
    $sql[] = "UPDATE " . iNA_GAMES_RATE . " r, " . iNA_GAMES . " g SET `r.rate_game_name` = `g.game_name` WHERE `r.rate_game_id` = `g.game_id`";
    $sql[] = "UPDATE " . iNA_GAMES_COMMENT . " c, " . iNA_GAMES . " g SET `c.comment_game_name` = `g.game_name` WHERE `c.comment_game_id` = `g.game_id`";

    $sql[] = "ALTER TABLE " . iNA_SCORES . " ADD `gameData` TEXT AFTER `game_name`";

 		$sql[] = "UPDATE " . iNA . " SET version = '2.1.0', minor = '0'";

  case '2.1.0':
    $sql[] = "ALTER TABLE " . iNA_USER_DATA . " ADD `second_places` MEDIUMINT (9) AFTER `first_places`";
    $sql[] = "ALTER TABLE " . iNA_USER_DATA . " ADD `third_places` MEDIUMINT (9) AFTER `second_places`";
    $sql[] = "ALTER TABLE " . iNA_USER_DATA . " ADD `at_second_places` MEDIUMINT (9) AFTER `at_first_places`";
    $sql[] = "ALTER TABLE " . iNA_USER_DATA . " ADD `at_third_places` MEDIUMINT (9) AFTER `at_second_places`";

    $sql[] = "CREATE TABLE `" . $table_prefix . "ina_highscore` (
        `highscore_id` MEDIUMINT( 8 ) UNSIGNED NOT NULL AUTO_INCREMENT ,
        `highscore_year` YEAR UNSIGNED NOT NULL ,
        `highscore_mon` TINYINT( 2 ) UNSIGNED NOT NULL ,
        `highscore_game` VARCHAR( 25 ) NOT NULL ,
        `highscore_player` VARCHAR( 40 ) NOT NULL ,
        `highscore_score` DOUBLE( 12, 4 ) UNSIGNED NOT NULL ,
        `highscore_date` INT( 11 ) UNSIGNED NOT NULL ,
        PRIMARY KEY `highscore_id` ( `highscore_id` ) )";
        
    $sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('highscore_start_year', '".date(Y)."')";
    $sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('highscore_start_mon', '".date(m)."')";

 		$sql[] = "UPDATE " . iNA . " SET version = '2.1.1'";
	  $sql[] = "ALTER TABLE " . iNA_FAV . " DROP `game_id`";
  
  case '2.1.1':
		$sql[] = "INSERT INTO " . CONFIG_TABLE . " (`config_name`, `config_value`) VALUES ('games_show_fav', '1')";
 
 		$sql[] = "UPDATE " . iNA . " SET version = '2.1.2'";
 
  case '2.1.2':
    $sql[] = "ALTER TABLE " . USERS_TABLE . " ADD `arcade_banned` INT DEFAULT '0'";

    $sql[] = "ALTER TABLE " . iNA . " ADD `config_name` VARCHAR( 255 ) NOT NULL DEFAULT '0'";
    $sql[] = "ALTER TABLE " . iNA . " ADD `config_value` VARCHAR( 255 ) NOT NULL DEFAULT '0'";
    $sql[] = "ALTER TABLE " . iNA . " DROP `version`";
    $sql[] = "ALTER TABLE " . iNA . " DROP `minor`";
    $sql[] = "ALTER TABLE " . iNA . " ADD UNIQUE (`config_name`)";
 
		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('default_reward_dbfield','user_points')";
		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('default_cash','user_cash')";
		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('use_rewards_mod','0')";
		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('use_cash_system','0')";
		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('report_cheater','0')";
		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('warn_cheater','0')";
		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('use_point_system','0')";
		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('use_gamelib','0')";
		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_path','games/')";
		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('gamelib_path','0')";
		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('use_gk_shop','0')";
		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('use_allowance_system','0')";
		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_per_page','20')";
		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_default_img','templates/subSilver/images/games.gif')";
		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_default_txt','Over 120 games available to Registered Members - No Fee - No Catch.<br />')";
		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_default_id','0')";
		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_tournament_mode','0')";
		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_offline','0')";
		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_cheat_mode','1')";
		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_guest_highscore', '1')";
		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_auto_size', '1')";
		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_at_highscore', '1')";
		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_show_stats', '1')";
		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_image_width', '50')";
		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_image_height', '50')";
		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_per_admin_page', '20')";
		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_cat_image_width', '80')";
		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_cat_image_height', '80')";
		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_tournament_max', '10')";
		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_tournament_games', '6')";
		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_tournament_players', '12')";
		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_moderators_mode', '0')";
		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_posts_required', '1')";
		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_use_pms', '0')";
		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_total_top', '10')";
		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_new_games', '1')";
		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_cat_zero', 'Show all the Games')";
		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_use_comments', '0')";
		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_use_rating', '0')";
		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_show_played', '1')";
		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_show_all', '1')";
		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_no_guests', '0')";
    $sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_rate', '1')";
    $sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_comments', '1')";
    $sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_mod_ban_users', '0')";
    $sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_comment_size', '256')";
 		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_rank_required', '0')";
 		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_level_required', '0')";
 		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_pm_highscore', '0')";
 		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_pm_at_highscore', '0')";
		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_pm_comment', '0')";
		$sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_pm_new', '0')";
    $sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('highscore_start_year', '".date(Y)."')";
    $sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('highscore_start_mon', '".date(m)."')";

		$sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'use_rewards_mod'";
		$sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'use_cash_system'";
		$sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'report_cheater'";
		$sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'warn_cheater'";
		$sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'use_point_system'";
		$sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'use_gamelib'";
		$sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'games_path'";
		$sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'gamelib_path'";
		$sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'use_gk_shop'";
		$sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'use_allowance_system'";
		$sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'games_per_page'";
		$sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'games_default_img'";
		$sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'games_default_txt'";
		$sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'games_default_id'";
		$sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'games_tournament_mode'";
		$sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'games_offline'";
		$sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'games_cheat_mode'";
		$sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'games_guest_highscore'";
		$sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'games_auto_size'";
		$sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'games_at_highscore'";
		$sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'games_show_stats'";
		$sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'games_image_width'";
		$sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'games_image_height'";
		$sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'games_per_admin_page'";
		$sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'games_cat_image_width'";
		$sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'games_cat_image_height'";
		$sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'games_tournament_max'";
		$sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'games_tournament_games'";
		$sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'games_tournament_players'";
		$sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'games_moderators_mode'";
		$sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'games_posts_required'";
		$sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'games_use_pms'";
		$sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'games_total_top'";
		$sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'games_new_games'";
		$sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'games_cat_zero'";
		$sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'games_use_comments'";
		$sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'games_use_rating'";
		$sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'games_show_played'";
		$sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'games_show_all'";
		$sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'games_no_guests'";
    $sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'games_rate'";
    $sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'games_comments'";
    $sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'games_mod_ban_users'";
    $sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'games_comment_size'";
    $sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'games_rate'";
 		$sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'games_rank_required'";
 		$sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'games_pm_highscore'";
 		$sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'games_pm_at_highscore'";
		$sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'games_pm_comment'";
		$sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'games_pm_new'";
    $sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'highscore_start_year'";
    $sql[] = "DELETE FROM " . CONFIG_TABLE . " WHERE `config_name` = 'highscore_start_mon'";

    $sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('default_sort', 'game_id')";
    $sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('default_sort_order', 'DESC')";
    $sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_show_fav', '1')";
    $sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_new_for', '1209600')";
    $sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('use_cache', '1')";
    $sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('config_cache', '86400')";
    $sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('categories_cache', '300')";
    $sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_cache', '86400')";
    $sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_show_mhm', '1')";
    $sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_tournament_user', '1')";

    $sql[] = "ALTER TABLE " . iNA_GAMES . " ADD `game_control` INT( 1 ) DEFAULT '0' NOT NULL";

 		$sql[] = "INSERT INTO " . iNA . " (`config_name`,`config_value`) VALUES ('version', '2.1.3')";

    $sql[] = "INSERT INTO " . iNA_CAT . " (`cat_id`) VALUES ('-1')";
    $sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_default_rate', '10')";

  case '2.1.3':
    $sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('highscore_cache', '180')";
    $sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('at_highscore_cache', '1440')";

    $sql[] = "ALTER TABLE " . iNA_SESSIONS . " ADD `tour_id` MEDIUMINT( 5 )";
    $sql[] = "ALTER TABLE " . iNA_SESSIONS . " ADD `randchar1` INT( 4 )";
    $sql[] = "ALTER TABLE " . iNA_SESSIONS . " ADD `randchar2` INT( 4 )";

    $sql[] = "DROP TABLE " . iNA_TOUR;
    $sql[] = "DROP TABLE " . iNA_TOUR_DATA;
    $sql[] = "DROP TABLE " . iNA_TOUR_PLAY;

    $sql[] = "CREATE TABLE " . iNA_TOUR . " ( 
      `tour_id` mediumint(5) NOT NULL auto_increment, 
      `tour_name` varchar(25) default NULL, 
      `tour_desc` text, 
      `tour_max_players` int(4) NOT NULL default '0', 
      `tour_player_turns` int(4) NOT NULL default '0', 
      `block_plays` int(1) default NULL, 
      `tour_active` int(1) NOT NULL default '0', 
      `start_id` mediumint(8) NOT NULL default '0', 
      `start_date` int(11) NOT NULL default '0', 
      `end_date` int(11) default NULL, 
      `length` int(11) default NULL, 
      `end_type` int(1) default NULL, 
      `champion` mediumint(8) default NULL, 
        PRIMARY KEY  (`tour_id`) )"; 
    $sql[] = "CREATE TABLE " . iNA_TOUR_DATA . " ( 
      `tour_id` mediumint(5) NOT NULL default '0', 
      `game_name` varchar(50) NOT NULL default '', 
      `top_score` double(12,4) default NULL, 
      `top_player` mediumint(11) default NULL, 
        PRIMARY KEY  (`tour_id`,`game_name`) )"; 
    $sql[] = "CREATE TABLE " . iNA_TOUR_PLAY . " ( 
      `tour_id` mediumint(5) NOT NULL default '0', 
      `user_id` mediumint(8) NOT NULL default '0', 
      `last_played_time` int(11) NOT NULL default '0', 
      `last_played_game` varchar(50) default NULL, 
      `gamedata` text,
        PRIMARY KEY  (`tour_id`,`user_id`) )"; 

  case '2.1.4':
    $sql[] = "ALTER TABLE ". iNA_CAT ." ADD `group_required` MEDIUMINT( 8 ) NOT NULL DEFAULT '0'";
    $sql[] = "ALTER TABLE ". iNA_CAT ." ADD `cat_type` CHAR( 1 ) NOT NULL DEFAULT 'p'"; 
    $sql[] = "ALTER TABLE ". iNA_CAT ." ADD `cat_parent` MEDIUMINT( 8 ) NULL "; 
    $sql[] = "ALTER TABLE ". iNA_CAT ." CHANGE `last_game` `last_game` VARCHAR( 50 ) DEFAULT NULL";   
    $sql[] = "ALTER TABLE ". iNA_CAT ." ADD `total_games` INT( 8 ) UNSIGNED NOT NULL DEFAULT '0'"; 
    $sql[] = "ALTER TABLE ". iNA_CAT ." ADD `total_played` BIGINT( 12 ) UNSIGNED NOT NULL DEFAULT '0'";

    $sql[] = "ALTER TABLE ". iNA_GAMES ." CHANGE `cat_id` `cat_id` MEDIUMINT( 8 ) NULL DEFAULT '-1'";
    $sql[] = "ALTER TABLE ". iNA_GAMES ." ADD `highscore_id` MEDIUMINT( 8 ) NOT NULL DEFAULT '0'"; 
    $sql[] = "ALTER TABLE ". iNA_GAMES ." ADD `at_highscore_id` MEDIUMINT( 8 ) NOT NULL DEFAULT '0'"; 
    $sql[] = "ALTER TABLE ". iNA_GAMES ." ADD INDEX ( `game_name` )"; 
    $sql[] = "UPDATE ". iNA_GAMES ." set cat_id = -1 WHERE cat_id = 0";
    $sql[] = "UPDATE ". iNA_GAMES ." set cat_id = -1 WHERE cat_id IS NULL";

    $sql[] = "ALTER TABLE ". iNA_SCORES ." ADD INDEX ( `game_name`, `player_id` )"; 
    $sql[] = "ALTER TABLE ". iNA_SCORES ." ADD INDEX ( `date` )"; 

    $sql[] = "ALTER TABLE ". iNA_HIGHSCORE  ." ADD INDEX ( `highscore_game` ) "; 

    $sql[] = "ALTER TABLE ". iNA_USER_DATA ." CHANGE `last_played` `last_played` VARCHAR( 50 ) DEFAULT NULL"; 

    $sql[] = "ALTER TABLE ". iNA_AT_SCORES ." ADD `player_name` VARCHAR( 25 ) NOT NULL AFTER `player_id`"; 
    $sql[] = "ALTER TABLE ". iNA_AT_SCORES ." ADD INDEX ( `game_name`, `player_id` )"; 
    $sql[] = "ALTER TABLE ". iNA_AT_SCORES ." ADD INDEX ( `date` )"; 

    $sql[] = "ALTER TABLE ". iNA_FAV ." ADD INDEX ( `fav_game_name` )"; 

  case '2.1.6':
    $sql[] = "ALTER TABLE ". iNA_HIGHSCORE ." CHANGE `highscore_game` `highscore_game` VARCHAR( 50 ) NOT NULL"; 

    $sql[] = "ALTER TABLE ". iNA_USER_DATA ." ADD `first_list` TEXT NULL AFTER `first_places`"; 
    $sql[] = "ALTER TABLE ". iNA_USER_DATA ." ADD `second_list` TEXT NULL AFTER `second_places`"; 
    $sql[] = "ALTER TABLE ". iNA_USER_DATA ." ADD `third_list` TEXT NULL AFTER `third_places`"; 
    $sql[] = "ALTER TABLE ". iNA_USER_DATA ." ADD `at_first_list` TEXT NULL AFTER `at_first_places`"; 
    $sql[] = "ALTER TABLE ". iNA_USER_DATA ." ADD `at_second_list` TEXT NULL AFTER `at_second_places`"; 
    $sql[] = "ALTER TABLE ". iNA_USER_DATA ." ADD `at_third_list` TEXT NULL AFTER `at_third_places`"; 
  

  case '2.1.7':
  case '2.1.8':
    $sql[] = "CREATE TABLE ". iNA_LOG . " (
      `record_no` mediumint(8) unsigned NOT NULL auto_increment,
      `user_id` mediumint(8) NOT NULL default '-1',
      `name` text,
      `value` text,
      `date` int(11) NOT NULL default '0',
      PRIMARY KEY  (`record_no`))";

    $sql[] = "ALTER TABLE " . iNA_SCORES . " CHANGE `score` `score` DOUBLE( 14, 4 ) NOT NULL DEFAULT '0.0000'";
    $sql[] = "ALTER TABLE " . iNA_AT_SCORES . " CHANGE `score` `score` DOUBLE( 14, 4 ) NOT NULL DEFAULT '0.0000'";

    $sql[] = "ALTER TABLE " . iNA_CAT . " ADD `cat_order` MEDIUMINT( 8 ) UNSIGNED NOT NULL DEFAULT '1' AFTER `cat_parent`";	

    $sql[] = "INSERT INTO " . iNA . " (`config_name`, `config_value`) VALUES ('games_use_log', '1')";


    $sql[] = "UPDATE " . iNA . " SET `config_value` = '2.1.8' WHERE `config_name` = 'version'";
		break;

	default:
		require 'http://www.phpbb-arcade.com/arcade_update.php';
		exit;
		break;
  }
}
//
//  Now lets get Building..!
//
for($i = 0; $i < count($sql); $i++)
{
	if (!($result = $db->sql_query($sql[$i])))
	{
    $error = $db->sql_error($result);
    if ((strpos($error['message'], 'already') == FALSE))
    {
      echo $sql[$i] . ' <b><i>NOT REQUIRED?</i>!<br /><span style="color:red">' . $error['message'] . '</span></b><br />';
		}
		else
		{
			echo $sql[$i] . ' <b><u>DONE.!</b></u><br />';
    }
	}
  $arcade->version('./../');
  $arcade->clear_cache('version');
  $arcade->clear_cache('config');
	$message = '<br /><br /><b>!..ALL DONE..!</b>';
	flush();
}

$arcade->clear_cache('config', './../');
$arcade->clear_cache('update', './../');
$arcade->clear_cache('version', './../');

message_die(GENERAL_MESSAGE, $message);

?>
