<?php
// Easy Announcement Hack
// Author: Richard Fitzgerald (richardfitzgerald85@hotmail.com)
// Version: 1.0.0
//

define('IN_PHPBB',true);

if( !empty($setmodules) )
{
	$file = basename(__FILE__);
	$module['General']['Events'] = $file;
	return;
}

$phpbb_root_path = "../"; // set path to phpBB files
include($phpbb_root_path . 'extension.inc');
//include($phpbb_root_path . 'common.php');
require('./pagestart.' . $phpEx);

//pagination
$start = ( isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;
$end = '10';//$board_config['topics_per_page']; 
$pagination = '';
$total_pag_items = 1; 
		
$content = $_POST['content'];
$edate = $_POST['edate'];
$location = $_POST['location'];
$title = $_POST['title'];
$doors = $_POST['doors'];
$flyer = $_POST['flyer'];
$sdate = time();

//date functions - Change to local timezone
 putenv('TZ=GMT'); 
 
        
//display previous announcements
$sql = "Select id, content, edate, sdate, flyer, doors, location, title from " . EVENTS . "";
if( !($result = $db->sql_query($sql)) )
{
	message_die(GENERAL_ERROR, 'Could not pull news', '', __LINE__, __FILE__, $sql);
}
$announce = array();
while( $announce[] = $db->sql_fetchrow($result) );
$db->sql_freeresult($result);
$total_announce = count($announce);


for ($t = 0; $t < $total_announce; $t++)
{
	if ($announce[$t]['content'] <> ''){
	
	$template->assign_block_vars('list', array(
	'event_id' => $announce[$t]['id'],
	'event_content' => $announce[$t]['content'],
	'event_edate' => $announce[$t]['edate'],
	'event_title' => $announce[$t]['title'],
	'event_flyer' => $announce[$t]['flyer'],
	'event_doors' => $announce[$t]['doors'],
	'event_location' => $announce[$t]['location'],
	'U_delete' => append_sid('admin_easy_events.php?delete=' . $announce[$t]['id']),
	'event_sdate' => date("d/m/y - H:i T",$announce[$t]['date'])));
}
}


//delete

$remove =  $HTTP_GET_VARS['delete'];
if ($remove <> ''){

$sql = "DELETE FROM " . EVENTS . " WHERE id = $remove";

if( !($result = $db->sql_query($sql)) )
{
	message_die(GENERAL_ERROR, 'Could not delete Event', '', __LINE__, __FILE__, $sql);
}
$link1 = append_sid("admin_easy_events.$phpEx");
message_die(SUCCESS, 'Deleted Event <br/> <a href="' . $link1 .'">Return to Events</a>', '', '','','' );
}




//enter
if ($content <> '') {
	
$sql = "INSERT INTO " . EVENTS . " (title, edate, location, doors, flyer, content, sdate) VALUES ('$title','$edate','$location','$doors','$flyer','$content', '$sdate')";
if( !($result = $db->sql_query($sql)) )
{
	message_die(GENERAL_ERROR, 'Could not insert events', '', __LINE__, __FILE__, $sql);
}
$link1 = append_sid("admin_easy_events.$phpEx");
message_die(SUCCESS, 'Inserted Event <br/> <a href="' . $link1 .'">Return to Events</a>', '', '','','' );
}

 $template->set_filenames(array(
         'body' => 'admin/admin_easy_events.tpl')
      );  

     $template->assign_vars(array(
        "S_ACTION" => append_sid("admin_easy_events.$phpEx"),
       ));

        
        //pagination
$sql = 'SELECT count(*) AS total
   FROM ' . EVENTS .'';

if ( !($result = $db->sql_query($sql)) )
{
   message_die(GENERAL_ERROR, 'Error getting total', '', __LINE__, __FILE__, $sql);
}

if ( $total = $db->sql_fetchrow($result) )
{
   $total_pag_items = $total['total'];
   $pagination = generate_pagination("admin_easy_events.php?$mode=$mode&order=$sort_order", $total_pag_items, $end, $start). '';
}

$template->assign_vars(array(
   'PAGINATION' => $pagination,
   'PAGE_NUMBER' => sprintf($lang['Page_of'], ( floor( $start / $board_config['topics_per_page'] ) + 1 ), ceil( $total_pag_items / $board_config['topics_per_page'] ))
));

      $template->pparse('body');
      
     
?>

