<?php
/***************************************************************************
 *                              admin_rabbitoshi_general.php
 *                            ------------------
 *   begin                : 30/11/2003
 *
 *
 ***************************************************************************/

if( !empty($setmodules) )
{
	$file = basename(__FILE__);
	$module['Rabbitoshi']['Rabbitoshi_settings'] = $file;
	return;
}

define('IN_PHPBB', true);

$phpbb_root_path = '../';
require($phpbb_root_path . 'extension.inc');
require("pagestart.$phpEx");

include($phpbb_root_path . 'language/lang_' . $board_config['default_lang'] . '/lang_rabbitoshi.'.$phpEx);
include($phpbb_root_path . 'includes/functions_rabbitoshi.'.$phpEx);

$template->set_filenames(array(
	'body' => 'admin/config_rabbitoshi_general_body.tpl')
);

$submit = isset($HTTP_POST_VARS['submit']); 

$sql = "SELECT *
FROM " . RABBITOSHI_GENERAL_TABLE ;
if(!$result = $db->sql_query($sql))
{
	message_die(CRITICAL_ERROR, "Could not query config information in admin_board", "", __LINE__, __FILE__, $sql);
}
while( $row = $db->sql_fetchrow($result) )
{
	$rabitoshi[$row['config_name']] = $row['config_value'];
}

$template->assign_vars(array(
	'RABBITOSHI_NAME' => $board_config['rabbitoshi_name'],
	'RABBITOSHI_USE_CHECKED' => ( $board_config['rabbitoshi_enable'] ? 'CHECKED' :'' ),
	'RABBITOSHI_USE_CRON_CHECKED' => ( $board_config['rabbitoshi_enable_cron'] ? 'CHECKED' :'' ),
	'RABBITOSHI_CRON_TIME' => $board_config['rabbitoshi_cron_time'],
	'RABBITOSHI_CRON_TIME_EXPLAIN' => rabbitoshi_make_time($board_config['rabbitoshi_cron_time']),
	'RABBITOSHI_THIRST_TIME' => $rabitoshi['thirst_time'],
	'RABBITOSHI_THIRST_TIME_EXPLAIN' => rabbitoshi_make_time($rabitoshi['thirst_time']),
	'RABBITOSHI_THIRST_VALUE' => $rabitoshi['thirst_value'],
	'RABBITOSHI_HUNGER_TIME' => $rabitoshi['hunger_time'],
	'RABBITOSHI_HUNGER_TIME_EXPLAIN' => rabbitoshi_make_time($rabitoshi['hunger_time']),
	'RABBITOSHI_HUNGER_VALUE' => $rabitoshi['hunger_value'],
	'RABBITOSHI_HEALTH_TIME' => $rabitoshi['health_time'],
	'RABBITOSHI_HEALTH_TIME_EXPLAIN' => rabbitoshi_make_time($rabitoshi['health_time']),
	'RABBITOSHI_HEALTH_VALUE' => $rabitoshi['health_value'],
	'RABBITOSHI_HYGIENE_TIME' => $rabitoshi['hygiene_time'],
	'RABBITOSHI_HYGIENE_TIME_EXPLAIN' => rabbitoshi_make_time($rabitoshi['hygiene_time']),
	'RABBITOSHI_HYGIENE_VALUE' => $rabitoshi['hygiene_value'],
	'RABBITOSHI_REBIRTH_CHECKED' => ( $rabitoshi['rebirth_enable'] ? 'CHECKED' :'' ),
	'RABBITOSHI_REBIRTH_PRICE' => $rabitoshi['rebirth_price'],
	'RABBITOSHI_VET_CHECKED' => ( $rabitoshi['vet_enable'] ? 'CHECKED' :'' ),
	'RABBITOSHI_VET_PRICE' => $rabitoshi['vet_price'],
	'RABBITOSHI_HOTEL_CHECKED' => ( $rabitoshi['hotel_enable'] ? 'CHECKED' :'' ),
	'RABBITOSHI_HOTEL_PRICE' => $rabitoshi['hotel_cost'],
	'RABBITOSHI_EVOLUTION_CHECKED' => ( $rabitoshi['evolution_enable'] ? 'CHECKED' :'' ),
	'RABBITOSHI_EVOLUTION_PRICE' => $rabitoshi['evolution_cost'],
	'RABBITOSHI_EVOLUTION_TIME' => $rabitoshi['evolution_time'],
));

if ( $submit )
{
	$use = intval ( $HTTP_POST_VARS['use']);
	$rebirth = intval ( $HTTP_POST_VARS['rebirth']);
	$vet = intval ( $HTTP_POST_VARS['vet']);
	$hotel = intval ( $HTTP_POST_VARS['hotel']);
	$evolution = intval ( $HTTP_POST_VARS['evolution']);
	$name = $HTTP_POST_VARS['name'];
	$hunger_time = $HTTP_POST_VARS['hunger_time'];
	$hunger_value = $HTTP_POST_VARS['hunger_value'];
	$thirst_time = $HTTP_POST_VARS['thirst_time'];
	$thirst_value = $HTTP_POST_VARS['thirst_value'];
	$health_time = $HTTP_POST_VARS['health_time'];
	$health_value = $HTTP_POST_VARS['health_value'];
	$hygiene_time = $HTTP_POST_VARS['hygiene_time'];
	$hygiene_value = $HTTP_POST_VARS['hygiene_value'];
	$rebirth_price = $HTTP_POST_VARS['rebirth_price'];
	$vet_price = $HTTP_POST_VARS['vet_price'];
	$hotel_price = $HTTP_POST_VARS['hotel_price'];
	$evolution_price = $HTTP_POST_VARS['evolution_price'];
	$evolution_time = $HTTP_POST_VARS['evolution_time'];
	$use_cron = intval ( $HTTP_POST_VARS['use_cron']);
	$cron_time = $HTTP_POST_VARS['cron_time'];

	$lsql= "UPDATE ". CONFIG_TABLE . " SET config_value = '$use' WHERE config_name = 'rabbitoshi_enable' ";
	if ( !($lresult = $db->sql_query($lsql)) ) 
	{ 
		message_die(GENERAL_ERROR, $lang['Rabbitoshi_update_error'] , "", __LINE__, __FILE__, $lsql); 
	} 
	$lsql= "UPDATE ". CONFIG_TABLE . " SET config_value = '$name' WHERE config_name = 'rabbitoshi_name' ";
	if ( !($lresult = $db->sql_query($lsql)) ) 
	{ 
		message_die(GENERAL_ERROR, $lang['Rabbitoshi_update_error'] , "", __LINE__, __FILE__, $lsql); 
	} 
	$lsql= "UPDATE ". CONFIG_TABLE . " SET config_value = '$use_cron' WHERE config_name = 'rabbitoshi_enable_cron' ";
	if ( !($lresult = $db->sql_query($lsql)) ) 
	{ 
		message_die(GENERAL_ERROR, $lang['Rabbitoshi_update_error'] , "", __LINE__, __FILE__, $lsql); 
	} 
	$lsql= "UPDATE ". CONFIG_TABLE . " SET config_value = '$cron_time' WHERE config_name = 'rabbitoshi_cron_time' ";
	if ( !($lresult = $db->sql_query($lsql)) ) 
	{ 
		message_die(GENERAL_ERROR, $lang['Rabbitoshi_update_error'] , "", __LINE__, __FILE__, $lsql); 
	} 
	$lsql= "UPDATE ". RABBITOSHI_GENERAL_TABLE . " SET config_value = '$hunger_time' WHERE config_name = 'hunger_time' ";
	if ( !($lresult = $db->sql_query($lsql)) ) 
	{ 
		message_die(GENERAL_ERROR, $lang['Rabbitoshi_update_error'] , "", __LINE__, __FILE__, $lsql); 
	} 
	$lsql= "UPDATE ". RABBITOSHI_GENERAL_TABLE . " SET config_value = '$hunger_value' WHERE config_name = 'hunger_value' ";
	if ( !($lresult = $db->sql_query($lsql)) ) 
	{ 
		message_die(GENERAL_ERROR, $lang['Rabbitoshi_update_error'] , "", __LINE__, __FILE__, $lsql); 
	} 
	$lsql= "UPDATE ". RABBITOSHI_GENERAL_TABLE . " SET config_value = '$thirst_time' WHERE config_name = 'thirst_time' ";
	if ( !($lresult = $db->sql_query($lsql)) ) 
	{ 
		message_die(GENERAL_ERROR, $lang['Rabbitoshi_update_error'] , "", __LINE__, __FILE__, $lsql); 
	} 
	$lsql= "UPDATE ". RABBITOSHI_GENERAL_TABLE . " SET config_value = '$thirst_value' WHERE config_name = 'thirst_value' ";
	if ( !($lresult = $db->sql_query($lsql)) ) 
	{ 
		message_die(GENERAL_ERROR, $lang['Rabbitoshi_update_error'] , "", __LINE__, __FILE__, $lsql); 
	} 
	$lsql= "UPDATE ". RABBITOSHI_GENERAL_TABLE . " SET config_value = '$health_time' WHERE config_name = 'health_time' ";
	if ( !($lresult = $db->sql_query($lsql)) ) 
	{ 
		message_die(GENERAL_ERROR, $lang['Rabbitoshi_update_error'] , "", __LINE__, __FILE__, $lsql); 
	} 
	$lsql= "UPDATE ". RABBITOSHI_GENERAL_TABLE . " SET config_value = '$health_value' WHERE config_name = 'health_value' ";
	if ( !($lresult = $db->sql_query($lsql)) ) 
	{ 
		message_die(GENERAL_ERROR, $lang['Rabbitoshi_update_error'] , "", __LINE__, __FILE__, $lsql); 
	} 
	$lsql= "UPDATE ". RABBITOSHI_GENERAL_TABLE . " SET config_value = '$hygiene_time' WHERE config_name = 'hygiene_time' ";
	if ( !($lresult = $db->sql_query($lsql)) ) 
	{ 
		message_die(GENERAL_ERROR, $lang['Rabbitoshi_update_error'] , "", __LINE__, __FILE__, $lsql); 
	} 	
	$lsql= "UPDATE ". RABBITOSHI_GENERAL_TABLE . " SET config_value = '$hygiene_value' WHERE config_name = 'hygiene_value' ";
	if ( !($lresult = $db->sql_query($lsql)) ) 
	{ 
		message_die(GENERAL_ERROR, $lang['Rabbitoshi_update_error'] , "", __LINE__, __FILE__, $lsql); 
	} 
	$lsql= "UPDATE ". RABBITOSHI_GENERAL_TABLE . " SET config_value = '$rebirth' WHERE config_name = 'rebirth_enable' ";
	if ( !($lresult = $db->sql_query($lsql)) ) 
	{ 
		message_die(GENERAL_ERROR, $lang['Rabbitoshi_update_error'] , "", __LINE__, __FILE__, $lsql); 
	} 	
	$lsql= "UPDATE ". RABBITOSHI_GENERAL_TABLE . " SET config_value = '$rebirth_price' WHERE config_name = 'rebirth_price' ";
	if ( !($lresult = $db->sql_query($lsql)) ) 
	{ 
		message_die(GENERAL_ERROR, $lang['Rabbitoshi_update_error'] , "", __LINE__, __FILE__, $lsql); 
	} 
	$lsql= "UPDATE ". RABBITOSHI_GENERAL_TABLE . " SET config_value = '$vet' WHERE config_name = 'vet_enable' ";
	if ( !($lresult = $db->sql_query($lsql)) ) 
	{ 
		message_die(GENERAL_ERROR, $lang['Rabbitoshi_update_error'] , "", __LINE__, __FILE__, $lsql); 
	} 	
	$lsql= "UPDATE ". RABBITOSHI_GENERAL_TABLE . " SET config_value = '$vet_price' WHERE config_name = 'vet_price' ";
	if ( !($lresult = $db->sql_query($lsql)) ) 
	{ 
		message_die(GENERAL_ERROR, $lang['Rabbitoshi_update_error'] , "", __LINE__, __FILE__, $lsql); 
	} 
	$lsql= "UPDATE ". RABBITOSHI_GENERAL_TABLE . " SET config_value = '$hotel' WHERE config_name = 'hotel_enable' ";
	if ( !($lresult = $db->sql_query($lsql)) ) 
	{ 
		message_die(GENERAL_ERROR, $lang['Rabbitoshi_update_error'] , "", __LINE__, __FILE__, $lsql); 
	} 
	$lsql= "UPDATE ". RABBITOSHI_GENERAL_TABLE . " SET config_value = '$hotel_price' WHERE config_name = 'hotel_cost' ";
	if ( !($lresult = $db->sql_query($lsql)) ) 
	{ 
		message_die(GENERAL_ERROR, $lang['Rabbitoshi_update_error'] , "", __LINE__, __FILE__, $lsql); 
	} 
	$lsql= "UPDATE ". RABBITOSHI_GENERAL_TABLE . " SET config_value = '$evolution' WHERE config_name = 'evolution_enable' ";
	if ( !($lresult = $db->sql_query($lsql)) ) 
	{ 
		message_die(GENERAL_ERROR, $lang['Rabbitoshi_update_error'] , "", __LINE__, __FILE__, $lsql); 
	} 
	$lsql= "UPDATE ". RABBITOSHI_GENERAL_TABLE . " SET config_value = '$evolution_price' WHERE config_name = 'evolution_cost' ";
	if ( !($lresult = $db->sql_query($lsql)) ) 
	{ 
		message_die(GENERAL_ERROR, $lang['Rabbitoshi_update_error'] , "", __LINE__, __FILE__, $lsql); 
	} 
	$lsql= "UPDATE ". RABBITOSHI_GENERAL_TABLE . " SET config_value = '$evolution_time' WHERE config_name = 'evolution_time' ";
	if ( !($lresult = $db->sql_query($lsql)) ) 
	{ 
		message_die(GENERAL_ERROR, $lang['Rabbitoshi_update_error'] , "", __LINE__, __FILE__, $lsql); 
	} 
	message_die(GENERAL_MESSAGE, sprintf($lang['Rabbitoshi_updated_return_settings'], '<a href="' . append_sid(basename(__FILE__)) . '">', '</a>'), $lang['Rabbitoshi_settings']);
}

$template->assign_vars(array(
	'L_RABBITOSHI_SETTINGS' => $lang['Rabbitoshi_settings'],
	'L_RABBITOSHI_SETTINGS_EXPLAIN' => $lang['Rabbitoshi_settings_explain'],
	'L_RABBITOSHI_USE' => $lang['Rabbitoshi_use'],
	'L_RABBITOSHI_NAME' => $lang['Rabbitoshi_settings_name'],
	'L_RABBITOSHI_REBIRTH' => $lang['Rabbitoshi_rebirth_enable'],
	'L_RABBITOSHI_REBIRTH_PRICE' => $lang['Rabbitoshi_rebirth_price'],
	'L_RABBITOSHI_REBIRTH_EXPLAIN' => $lang['Rabbitoshi_rebirth_enable_explain'],
	'L_RABBITOSHI_VET' => $lang['Rabbitoshi_vet_enable'],
	'L_RABBITOSHI_VET_PRICE' => $lang['Rabbitoshi_vet_price'],
	'L_RABBITOSHI_VET_EXPLAIN' => $lang['Rabbitoshi_rebirth_vet_explain'],
	'L_RABBITOSHI_HOTEL_USE' => $lang['Rabbitoshi_hotel_use'],
	'L_RABBITOSHI_HOTEL_USE_EXPLAIN' => $lang['Rabbitoshi_hotel_use_explain'],
	'L_RABBITOSHI_HOTEL_PRICE' => $lang['Rabbitoshi_hotel_price'],
	'L_RABBITOSHI_HOTEL_PRICE_EXPLAIN' => $lang['Rabbitoshi_hotel_price_explain'],
	'L_RABBITOSHI_EVOLUTION_USE' => $lang['Rabbitoshi_evolution_use'],
	'L_RABBITOSHI_EVOLUTION_USE_EXPLAIN' => $lang['Rabbitoshi_evolution_use_explain'],
	'L_RABBITOSHI_EVOLUTION_PRICE' => $lang['Rabbitoshi_evolution_price'],
	'L_RABBITOSHI_EVOLUTION_PRICE_EXPLAIN' => $lang['Rabbitoshi_evolution_price_explain'],
	'L_RABBITOSHI_EVOLUTION_TIME' => $lang['Rabbitoshi_evolution_time'],
	'L_RABBITOSHI_EVOLUTION_TIME_EXPLAIN' => $lang['Rabbitoshi_evolution_time_explain'],
	'L_RABBITOSHI_HUNGER_TIME' => $lang['Rabbitoshi_hunger_time'],
	'L_RABBITOSHI_HUNGER_VALUE' => $lang['Rabbitoshi_hunger_value'],
	'L_RABBITOSHI_THIRST_TIME' => $lang['Rabbitoshi_thirst_time'],
	'L_RABBITOSHI_THIRST_VALUE' => $lang['Rabbitoshi_thirst_value'],
	'L_RABBITOSHI_HEALTH_TIME' => $lang['Rabbitoshi_health_time'],
	'L_RABBITOSHI_HEALTH_EXPLAIN' => $lang['Rabbitoshi_health_explain'],
	'L_RABBITOSHI_HEALTH_VALUE' => $lang['Rabbitoshi_health_value'],
	'L_RABBITOSHI_HYGIENE_TIME' => $lang['Rabbitoshi_hygiene_time'],
	'L_RABBITOSHI_HYGIENE_VALUE' => $lang['Rabbitoshi_hygiene_value'],
	'L_EXPLANATIONS' => $lang['Rabbitoshi_settings_explanations'],
	'L_SUBMIT' => $lang['Submit'],
	'L_TRANSLATOR' => $lang['Rabbitoshi_translation'],
	'L_SECONDS' => $lang['Rabbitoshi_seconds'],
	'L_RABBITOSHI_USE_CRON' => $lang['Rabbitoshi_cron_use'],
	'L_RABBITOSHI_USE_CRON_EXPLAIN' => $lang['Rabbitoshi_cron_explain'],
	'L_RABBITOSHI_CRON_TIME' => $lang['Rabbitoshi_cron_time'],
	'S_RABBITOSHI_ACTION' => append_sid(basename(__FILE__)))
);

$template->pparse('body');

include('./page_footer_admin.'.$phpEx);

?>