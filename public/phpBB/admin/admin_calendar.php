<?php
/*********************************************
*	Calendar Lite
*
*	$Author: Martin $
*	$Date: 2005-03-04 10:33:47 +0000 (Fri, 04 Mar 2005) $
*	$Revision: 5 $
*
*********************************************/

/***************************************************************************
 *                            admin_calendar.php
 *                            -------------------
 * Copyright:   (C) 2003 SnailSource.com
 * Mod Title: 	phpBB2 Calendar Lite
 * Mod Version: 1.4.4
 * Author:      WebSnail < http://www.snailsource.com >
 *
 ***************************************************************************/

/***************************************************************************
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

define('IN_PHPBB', 1);

if( !empty($setmodules) )
{
	$filename = basename(__FILE__);
	$module['Calendar']['Calendar Config'] = $filename;

	return;
}

global $db;

//
// Load default header
//
$phpbb_root_path = "../";
require($phpbb_root_path . 'extension.inc');
require('pagestart.' . $phpEx);

require($phpbb_root_path . 'language/lang_' . $board_config['default_lang'] . '/lang_calendar.' . $phpEx);
$page_title =  $lang['Config_Cal'];
$caltable = $table_prefix . "cal_config";

$sql = "SELECT * FROM ". $caltable;
if(!$result = $db->sql_query($sql))
{
	message_die(GENERAL_ERROR, "Couldn't query calendar config table", "", __LINE__, __FILE__, $sql);
}
else
{
	while( $row = $db->sql_fetchrow($result) )
	{
		$config_name = $row['config_name'];
		$config_value = $row['config_value'];
		$default_config[$config_name] = $config_value;
		
		$new[$config_name] = ( isset($HTTP_POST_VARS[$config_name]) ) ? $HTTP_POST_VARS[$config_name] : $default_config[$config_name];
		if( isset($HTTP_POST_VARS['submit']) )
		{
			$sql = "UPDATE " . $caltable . " SET
				config_value = '" . str_replace("\'", "''", $new[$config_name]) . "'
				WHERE config_name = '$config_name'";
			if( !$db->sql_query($sql) )
			{
				message_die(GENERAL_ERROR, "Failed to update calendar configuration for $config_name", "", __LINE__, __FILE__, $sql);
			}
		}
	}

	if( isset($HTTP_POST_VARS['submit']) )
	{
		$message = $lang['Cal_config_updated'] . "<br /><br />" . sprintf($lang['Cal_return_config'], "<a href=\"" . append_sid("admin_calendar.$phpEx") . "\">", "</a>") . "<br /><br />" . sprintf($lang['Click_return_admin_index'], "<a href=\"" . append_sid("index.$phpEx?pane=right") . "\">", "</a>");
		message_die(GENERAL_MESSAGE, $message);
	}
}
// Build Week Start select box
$week_start_select  = "<select name='week_start'>";
$week_start_select .= "<option value='0' ";
$week_start_select .= ( $new['week_start'] == '0' ) ? "selected='selected'" : "";
$week_start_select .= ">".$lang['datetime']['Sunday']."</option>";
$week_start_select .= "<option value='1' ";
$week_start_select .= ( $new['week_start'] == '1' ) ? "selected='selected'" : "";
$week_start_select .= ">".$lang['datetime']['Monday']."</option>";
$week_start_select .= "</select>";

$allow_anon_yes = ( $new['allow_anon'] ) ? "checked=\"checked\"" : "";
$allow_anon_no = ( !$new['allow_anon'] ) ? "checked=\"checked\"" : "";

$allow_old_yes = ( $new['allow_old'] ) ? "checked=\"checked\"" : "";
$allow_old_no = ( !$new['allow_old'] ) ? "checked=\"checked\"" : "";

$show_headers_yes = ( $new['show_headers'] ) ? "checked=\"checked\"" : "";
$show_headers_no = ( !$new['show_headers'] ) ? "checked=\"checked\"" : "";

$cal_levels[0] = $lang['no_public'];
$cal_levels[1] = $lang['view_only'];
$cal_levels[2] = $lang['view_suggest'];
$cal_levels[3] = $lang['view_add'];
$cal_levels[4] = $lang['view_edit_own'];
$s_cal_type = "<select name='allow_user_default'>";
for ($i=0; $i<=4; $i++) {
	$s_cal_type .="<option value='". $i;
	if ($i == $new['allow_user_default']) {
		$s_cal_type .="' selected='selected'>\n";
		}
	else {
		$s_cal_type .="'>\n";
		}
	$s_cal_type .= $cal_levels[$i] ."</option>";
	}
$s_cal_type .="</select>";



$template->set_filenames(array(
	"body" => "admin/calendar_config_body.tpl")
);

$sql = "SELECT user_birthday FROM " . USERS_TABLE . " LIMIT 0,1";
if($result = @$db->sql_query($sql))
{
	$template->assign_block_vars('birthday', array(
		"L_SHOW_BIRTHDAYS" => $lang['show_birthday'],
		"L_SHOW_BIRTHDAYS_EXPLAIN" => $lang['show_birthday_explain'],
		"S_SHOW_BIRTHDAYS_YES" => ( $new['show_birthdays'] ) ? "checked=\"checked\"" : "",
		"S_SHOW_BIRTHDAYS_NO" => ( !$new['show_birthdays'] ) ? "checked=\"checked\"" : "")
	);
}

$template->assign_vars(array(
	"S_CONFIG_ACTION" => append_sid("admin_calendar.$phpEx"),

	"L_YES" => $lang['Yes'],
	"L_NO" => $lang['No'],
	"L_CONFIGURATION_TITLE" => $lang['Config_Calendar'],
	"L_GENERAL_SETTINGS" => $lang['Config_Calendar'],
	"L_CONFIGURATION_EXPLAIN" => $lang['Config_Calendar_explain'],
	"L_WEEK_START" => $lang['week_start'],
	"L_SUBJECT_LENGTH" => $lang['subject_length'], 
	"L_SUBJECT_LENGTH_EXPLAIN" => $lang['subject_length_explain'],
	"L_ALLOW_ANON" => $lang['allow_anon'], 
	"L_ALLOW_USER_POST" => $lang['allow_user_post'],
	"L_ALLOW_USER_DEFAULT" => $lang['allow_user_post_default'],
	"L_ALLOW_OLD" => $lang['allow_old'], 
	"L_ALLOW_OLD_EXPLAIN" => $lang['allow_old_explain'], 
	"L_SHOW_HEADERS" => $lang['show_headers'],
	"L_DATE_FORMAT" => $lang['Date_format'],
	"L_DATE_FORMAT_EXPLAIN" => $lang['cal_date_explain'], 

	"L_SUBMIT" => $lang['Submit'], 
	"L_RESET" => $lang['Reset'], 
	
	"WEEK_START_SELECT" => $week_start_select, 
	"SUBJECT_LENGTH" => $new['subject_length'], 
	"SCIPT_PATH" => $new['cal_script_path'],

	"S_ALLOW_ANON_YES" => $allow_anon_yes,
	"S_ALLOW_ANON_NO" => $allow_anon_no,
	"S_ALLOW_USER_POST_YES" => $allow_user_post_yes,
	"S_ALLOW_USER_POST_NO" => $allow_user_post_no,
	"S_ALLOW_USER_DEFAULT" => $s_cal_type,
	"S_ALLOW_OLD_YES" => $allow_old_yes,
	"S_ALLOW_OLD_NO" => $allow_old_no, 
	"S_SHOW_HEADERS_YES" => $show_headers_yes,
	"S_SHOW_HEADERS_NO" => $show_headers_no,

	"CAL_DATEFORMAT" => $new['cal_dateformat'])
);

$template->pparse("body");

include('./page_footer_admin.'.$phpEx);

?>