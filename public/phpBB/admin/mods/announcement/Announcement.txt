############################################################## 
## MOD Title: Admin Announcement
## MOD Author: cherokee red < mods@cherokeered.co.uk> (Kenny Cameron) http://www.cherokeered.co.uk/f/
## MOD Description: Adds an Announcement Block in your forum header - editable from the Admin Panel
## MOD Version: 1.0.0
## 
## Installation Level: Easy 
## Installation Time: ~5 Minutes 
## Files To Edit: admin/admin_board.php
##                includes/page_header.php, 
##                language/lang_english/lang_admin.php,
##                language/lang_english/lang_main.php,
##                templates/subSilver/admin/board_config_body.tpl
##                templates/subSilver/overall_header.tpl
##      
## Included Files: n/a
## 
## License: http://opensource.org/licenses/gpl-license.php GNU General Public License v2 
############################################################## 
## For security purposes, please check: http://www.phpbb.com/mods/ 
## for the latest version of this MOD. Although MODs are checked 
## before being allowed in the MODs Database there is no guarantee 
## that there are no security problems within the MOD. No support 
## will be given for MODs not found within the MODs Database which 
## can be found at http://www.phpbb.com/mods/ 
############################################################## 
## Author Notes: 
## 
## As requested in this topic - http://www.phpbb.com/phpBB/viewtopic.php?p=2667252#2667252
##
## The announcement is editable in the Adminstration Panel of your forum.
## Like the site description, simple HTML is allowed here.
##
## Please note that this MOD is was designed with EasyMod in mind.
## It will install no problem, but best on a vanilla phpBB.
## It has not been tested pre phpBB 2.0.21 and most likely won't be.
## If you have trouble with EasyMod or installing this MOD (specifically with the templates side)
## then it's best to manually edit the code in. Then you have freedom over where you place the announcement.
## 
##
############################################################## 
## MOD History: 
## 
##   2006-12-14 - Version 1.0.0
##      - First release 
## 
############################################################## 
## Before Adding This MOD To Your Forum, You Should Back Up All Files Related To This MOD 
############################################################## 
# 
#-----[ SQL ]------------------------------------------ 
#
INSERT INTO phpbb_config (config_name, config_value) VALUES ('admin_announcement','Welcome to the Board!<br />Hope you enjoy it');
# 
#-----[ OPEN ]------------------------------------------ 
# 
admin/admin_board.php
# 
#-----[ FIND ]------------------------------------------ 
#
$new['sitename'] = str_replace('"', '&quot;', strip_tags($new['sitename']));
# 
#-----[ AFTER, ADD ]------------------------------------------ 
# 
// Admin Announcement :: cherokee red
$new['admin_announcement'] = str_replace('"', '&quot;', $new['admin_announcement']);
# 
#-----[ FIND ]------------------------------------------ 
# 
	"L_SITE_DESCRIPTION" => $lang['Site_desc'],
# 
#-----[ AFTER, ADD ]------------------------------------------ 
# 
	// Admin Announcement :: cherokee red
	"L_ADMIN_ANNOUNCEMENT" => $lang['Admin_Announcement'],
	"L_ADMIN_ANNOUNCEMENT_EXPLAIN" => $lang['Admin_Announcement_Explain'],
# 
#-----[ FIND ]------------------------------------------ 
# 
	"SITE_DESCRIPTION" => $new['site_desc'], 
# 
#-----[ AFTER, ADD ]------------------------------------------ 
# 
	// Admin Announcement :: cherokee red
	"ADMIN_ANNOUNCEMENT" => $new['admin_announcement'], 
# 
#-----[ OPEN ]------------------------------------------ 
# 
includes/page_header.php

#-----[ FIND ]------------------------------------------ 
# 
	'SITE_DESCRIPTION' => $board_config['site_desc'],
# 
#-----[ AFTER, ADD ]------------------------------------------ 
# 
	// Admin Announcement :: cherokee red
	'ADMIN_ANNOUNCEMENT' => $board_config['admin_announcement'],
	'L_ADMIN_ANNOUNCEMENT' => $lang['Admin_Announcement'],
# 
#-----[ OPEN ]------------------------------------------ 
# 
language/lang_english/lang_admin.php
# 
#-----[ FIND ]------------------------------------------ 
# 
$lang['Site_desc'] = 'Site description';
# 
#-----[ AFTER, ADD ]------------------------------------------ 
# 
// Admin Announcement :: cherokee red
$lang['Admin_Announcement'] = 'Admin Announcement';
$lang['Admin_Announcement_Explain'] = 'This message will show in the header of your forum, to all users';
# 
#-----[ OPEN ]------------------------------------------ 
# 
language/lang_english/lang_main.php
# 
#-----[ FIND ]------------------------------------------ 
# 
?>
# 
#-----[ BEFORE, ADD ]------------------------------------------ 
# 
// Admin Announcement :: cherokee red
$lang['Admin_Announcement'] = 'Admin Announcement';
# 
#-----[ OPEN ]------------------------------------------ 
# 
templates/subSilver/admin/board_config_body.tpl
# 
#-----[ FIND ]------------------------------------------ 
# 
	<tr>
		<td class="row1">{L_SITE_DESCRIPTION}</td>
		<td class="row2"><input class="post" type="text" size="40" maxlength="255" name="site_desc" value="{SITE_DESCRIPTION}" /></td>
	</tr>
# 
#-----[ AFTER, ADD ]------------------------------------------ 
# 
<!-- Admin Announcements :: cherokee red -->
	<tr>
		<td class="row1">{L_ADMIN_ANNOUNCEMENT}<br /><span class="gensmall">{L_ADMIN_ANNOUNCEMENT_EXPLAIN}</span></td>
		<td class="row2"><textarea name="admin_announcement" rows="6" cols="40">{ADMIN_ANNOUNCEMENT}</textarea></td>
	</tr>
<!-- Admin Announcements :: cherokee red -->
# 
#-----[ OPEN ]------------------------------------------ 
# 
templates/subSilver/overall_header.tpl
# 
#-----[ FIND ]------------------------------------------ 
# 
				</table></td>
			</tr>
		</table>

		<br />

# 
#-----[ AFTER, ADD ]------------------------------------------ 
# 
<!-- Admin Announcements :: cherokee red -->
<table width="100%" cellpadding="2" cellspacing="0" border="0" class="forumline"> 
   <thead> 
      <tr> 
         <th height="28 class="thHead">{L_ADMIN_ANNOUNCEMENT}</th> 
      </tr> 
   </thead> 
   <tr> 
      <td class="row1" align="center">&nbsp;</td> 
   </tr>
   <tr> 
      <td class="row1" align="center"><span class="gen">{ADMIN_ANNOUNCEMENT}</span></td> 
   </tr>
   <tr> 
      <td class="row1" align="center">&nbsp;</td>
   </tr>
</table>
<br /> 
<!-- Admin Announcements :: cherokee red -->
<br />

# 
#-----[ SAVE/CLOSE ALL FILES ]------------------------------------------ 
# 
# EoM 
