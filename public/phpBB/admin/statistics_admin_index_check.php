<?php

global $session_id;
include($phpbb_root_path . 'stats_mod/includes/constants.' . $phpEx);
@include_once($phpbb_root_path . 'language/lang_' . $board_config['default_lang'] . '/lang_admin_statistics.' . $phpEx);

$sql = "SELECT * FROM " . STATS_CONFIG_TABLE;
if ( !($result = $db->sql_query($sql)) )
{
	message_die(GENERAL_ERROR, 'Could not query statistics config table', '', __LINE__, __FILE__, $sql);
}
$stats_config = array();
while ($row = $db->sql_fetchrow($result))
{
	$stats_config[$row['config_name']] = trim($row['config_value']);
}

// See wich modules are installed and get current versions and new versions
$sql = "SELECT i.*, m.short_name FROM ".MODULE_INFO_TABLE." i, ".MODULES_TABLE." m 
		WHERE i.module_id = m.module_id
		ORDER BY i.long_name ASC";
if (!($result = $db->sql_query($sql)) )
{
	message_die(GENERAL_ERROR, 'Unable to get Module Informations', '', __LINE__, __FILE__, $sql);
}
if ($db->sql_numrows($result) == 0)
{
	message_die(GENERAL_MESSAGE, 'No modules found on your board.');
}

$url = 'http://www.detecties.com/phpbb2018/stat_updates/statistics_updates.txt';
$allow_url_fopen = ini_get('allow_url_fopen');
if ($allow_url_fopen == 1)
{
	if (!@copy ($url, $phpbb_root_path.'modules/statistics_updates.txt')) 
	{
		message_die(GENERAL_MESSAGE, 'Cannot get remote file needed for updatescheck, it seems to be missing.<br />Please contact the MOD Author about this issue.');
	}
}
else if (CURLOPT_HTTPGET)
{
	if (substr(sprintf('%o', @fileperms($phpbb_root_path.'modules/statistics_updates.txt')), -4) == '0666')
	{
    	$fp = fopen($phpbb_root_path.'modules/statistics_updates.txt', 'wt');
    	$curl = curl_init();
    	curl_setopt ($curl, CURLOPT_URL, $url);
    	curl_setopt($curl, CURLOPT_FILE, $fp);
    	curl_exec ($curl);
    	curl_close ($curl);
		fclose($fp);
	}
	else
	{
		message_die(GENERAL_MESSAGE, 'Cannot open modules/statistics_updates.txt for writing, create an (empty) file modules/statistics_updates.txt and chmod it to 666.');
	}
}
else
{
	message_die(GENERAL_MESSAGE, 'Cannot get remote file, needed for checking for updates.');
}

$infofile = $phpbb_root_path.'modules/statistics_updates.txt';

// Get Statistics MOD version
$handle = fopen($infofile, "rt");
if ($handle) 
{
		$line = fgets($handle);
		$pos1 = strpos($line, ']');
		$pos2 = strpos($line, '@');
		$statistics = substr($line, 1, $pos1-1);
		$newversion = substr($line, $pos1+1, ($pos2-1)-$pos1);
		if ($newversion != $stats_config['version'])
		{
			$updates = 1;
		}
}
fclose($handle);

while ($row = $db->sql_fetchrow($result))
{
	$handle = fopen($infofile, "rt");
	if ($handle) 
	{
		$line = fgets($handle); //rule out first line
		while (!feof($handle)) 
		{
			$line = fgets($handle);
			$pos1 = strpos($line, ']');
			$pos2 = strpos($line, '@');
			$newmodule = substr($line, 1, $pos1-1);
			if ($newmodule == trim($row['long_name']))
			{
				$newversion = substr($line, $pos1+1, ($pos2-1)-$pos1);
				$download = '<a href="'.substr($line, $pos2+1, strlen($line)-($pos2-1)).'">'.$newmodule.'</a>';
				if (trim($newversion) != trim($row['version']))
				{
					$updates = 1;
				}
			}
		}
	}
	fclose($handle);
}
//@unlink($phpbb_root_path.'modules/statistics_updates.txt');

if ($updates == 1)
{
	$template->assign_vars(array(
		'L_STATISTICS_VERSION_INFORMATION' => $lang['statistics_version_info'],
		'STATS_AVAILABLE' => '<p style="color:red">' . $lang['updates_available']. ' <a href="./admin_statistics_updates.'.$phpEx . '?sid=' . $userdata['session_id'] . '">' . $lang['here'] .'</a></p>'
	));
}
?>
