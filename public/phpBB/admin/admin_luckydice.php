<?php
/***************************************************************************
 *                           admin_luckydice.php
 *                            -------------------
 *
 ***************************************************************************/

/***************************************************************************
 *
 *   copyright (C) 2003-2006  RC-Technologies
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License
 *   as published by the Free Software Foundation; either version 2
 *   of the License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   http://www.gnu.org/copyleft/gpl.html
 *
 ***************************************************************************/

define('IN_PHPBB', 1);

if(	!empty($setmodules) )
{
	$file = basename(__FILE__);
	$module['General']['Lucky Dice'] = $file;
	return;
}

//
// Lets set the root dir for phpBB
//
$phpbb_root_path = '../';
require($phpbb_root_path . 'extension.inc');
require('pagestart.' . $phpEx);
include($phpbb_root_path . 'language/lang_' . $board_config['default_lang'] . '/lang_luckydice.' . $phpEx);

//
//check for userlevel
//
if( !$userdata['session_logged_in'] )
{
	header('Location: ' . append_sid("login.$phpEx?redirect=admin/admin_luckydice.$phpEx", true));
}

if( $userdata['user_level'] != ADMIN )
{
	message_die(GENERAL_MESSAGE, ''.$lang['Not_Authorised'].'');
}
//end check

// LuckyDice ADMIN

// get current item-info
$enable = $board_config['luckydice_enable'];
$maxbet = $board_config['luckydice_maxbet'];
$windouble = $board_config['luckydice_windouble'];
$wintriple = $board_config['luckydice_wintriple'];
$winperfect = $board_config['luckydice_winperfect'];
$bonussmall = $board_config['luckydice_bonussmall'];
$bonusbig = $board_config['luckydice_bonusbig'];
$betsaday = $board_config['luckydice_betsaday'];
// end getting item-info

if ($_POST['process'] != $lang['admin_submit']) 
{
	if( $enable == '1' ) 
	{
		$diceon = 'checked';
		$diceoff = '';
	}
	if( $enable == '0' ) 
	{
		$diceon = '';
		$diceoff = 'checked';
	}
	
	$overall = '<form method="POST" action="'.append_sid("admin_luckydice.$phpEx").'">
	<table width="100%" cellpadding="4" cellspacing="1" border="0">
	  <tr>
		<th width="50%"  class="thHead" colspan="2"><center>'.$lang['admin_title'].'</center></th>
	  </tr>
	  <tr>
		<td width="75%" class="row1">'.$lang['admin_enable'].'</td>
		<td width="25%" class="row2"><input type="radio" name="R1" value="1" '.$diceon.'>On | Off<input type="radio" name="R1" value="0" '.$diceoff.'></td>
	  </tr>
		<tr>
		<td width="75%" class="row1">'.$lang['admin_maxbet'].'</td>
		<td width="25%" class="row2"><input type="text" name="maxbet" size="10" value="'.$maxbet.'"></td>
		</tr>
		<tr>
		<td width="75%" class="row1">'.$lang['admin_betsaday'].'</td>
		<td width="25%" class="row2"><input type="text" name="betsaday" size="10" value="'.$betsaday.'"></td>
		</tr>
		<tr>
		<td width="75%" class="row1">'.$lang['admin_windouble'].'</td>
		<td width="25%" class="row2"><input type="text" name="windouble" size="10" value="'.$windouble.'"></td>
		</tr>
		<tr>
		<td width="75%" class="row1">'.$lang['admin_wintriple'].'</td>
		<td width="25%" class="row2"><input type="text" name="wintriple" size="10" value="'.$wintriple.'"></td>
		</tr>
		<tr>
		<td width="75%" class="row1">'.$lang['admin_winperfect'].'</td>
		<td width="25%" class="row2"><input type="text" name="winperfect" size="10" value="'.$winperfect.'"></td>
		</tr>
		<tr>
		<td width="75%" class="row1">'.$lang['admin_bonussmall'].'</td>
		<td width="25%" class="row2"><input type="text" name="bonussmall" size="10" value="'.$bonussmall.'"></td>
		</tr>
		<tr>
		<td width="75%" class="row1">'.$lang['admin_bonusbig'].'</td>
		<td width="25%" class="row2"><input type="text" name="bonusbig" size="10" value="'.$bonusbig.'"></td>
		</tr>
		<tr>
		<td width="50%"  class="catBottom" colspan="2"><center><input type="submit" value="'.$lang['admin_submit'].'" name="process" size="20" class="button"></form></center></td>
	  </tr>
	</table>
	';
}
if( $_POST['process'] == $lang['admin_submit'] ) 
{
	if( $_POST['R1'] == '1' ) 
	{
		$sql = "UPDATE " . CONFIG_TABLE . " SET config_value='1' WHERE config_name='luckydice_enable'";
		if(! ($db->sql_query($sql)) ) { message_die(GENERAL_MESSAGE, 'Fatal Error Updating Lucky Dice config<br>'.mysql_error()); }
	}
	if( $_POST['R1'] == '0' ) 
	{
		$sql = "UPDATE " . CONFIG_TABLE . " SET config_value='0' WHERE config_name='luckydice_enable'";
		if(! ($db->sql_query($sql)) ) { message_die(GENERAL_MESSAGE, 'Fatal Error Updating Lucky Dice config<br>'.mysql_error()); } 
	}
	if( $_POST['maxbet'] != $maxbet )
	{
	 	if(! is_numeric($_POST['maxbet']) )
		{  message_die(GENERAL_MESSAGE, 'Max bet must be an integer !!'); }
		$sql = 'UPDATE ' . CONFIG_TABLE . ' SET config_value='. $_POST['maxbet'] .' WHERE config_name="luckydice_maxbet"';
		if(! ($db->sql_query($sql)) ) { message_die(GENERAL_MESSAGE, 'Fatal Error Updating Lucky Dice config<br>'.mysql_error()); }
	}
	if( $_POST['windouble'] != $windouble )
	{
	 	if(! is_numeric($_POST['windouble']) )
		{ message_die(GENERAL_MESSAGE, 'windouble must be an integer !!'); }
		$sql = 'UPDATE ' . CONFIG_TABLE . ' SET config_value='. $_POST['windouble'] .' WHERE config_name="luckydice_windouble"';
		if(! ($db->sql_query($sql)) ) { message_die(GENERAL_MESSAGE, 'Fatal Error Updating Lucky Dice config<br>'.mysql_error()); }
	}
	if( $_POST['wintriple'] != $wintriple )
	{
	 	if(! is_numeric($_POST['wintriple']) )
		{ message_die(GENERAL_MESSAGE, 'wintriple must be an integer !!'); }
		$sql = 'UPDATE ' . CONFIG_TABLE . ' SET config_value='. $_POST['wintriple'] .' WHERE config_name="luckydice_wintriple"';
		if(! ($db->sql_query($sql)) ) { message_die(GENERAL_MESSAGE, 'Fatal Error Updating Lucky Dice config<br>'.mysql_error()); }
	}
	if( $_POST['winperfect'] != $winperfect )
	{
	 	if(! is_numeric($_POST['winperfect']) )
		{ message_die(GENERAL_MESSAGE, 'winperfect must be an integer !!'); }
		$sql = 'UPDATE ' . CONFIG_TABLE . ' SET config_value='. $_POST['winperfect'] .' WHERE config_name="luckydice_winperfect"';
		if(! ($db->sql_query($sql)) ) { message_die(GENERAL_MESSAGE, 'Fatal Error Updating Lucky Dice config<br>'.mysql_error()); }
	}
	if( $_POST['bonussmall'] != $bonussmall )
	{
	 	if(! is_numeric($_POST['bonussmall']) )
		{ message_die(GENERAL_MESSAGE, 'bonussmall must be an integer !!'); }
		$sql = 'UPDATE ' . CONFIG_TABLE . ' SET config_value='. $_POST['bonussmall'] .' WHERE config_name="luckydice_bonussmall"';
		if(! ($db->sql_query($sql)) ) { message_die(GENERAL_MESSAGE, 'Fatal Error Updating Lucky Dice config<br>'.mysql_error()); }
	}
	if( $_POST['bonusbig'] != $bonusbig )
	{
	 	if(! is_numeric($_POST['bonusbig']) )
		{ message_die(GENERAL_MESSAGE, 'bonusbig must be an integer !!'); }
		$sql = 'UPDATE ' . CONFIG_TABLE . ' SET config_value='. $_POST['bonusbig'] .' WHERE config_name="luckydice_bonusbig"';
		if(! ($db->sql_query($sql)) ) { message_die(GENERAL_MESSAGE, 'Fatal Error Updating Lucky Dice config<br>'.mysql_error()); }
	}
	
	//version 1.4.0
	if( $_POST['betsaday'] != $betsaday )
	{
	 	if(! is_numeric($_POST['betsaday']) )
		{ message_die(GENERAL_MESSAGE, 'betsaday must be an integer !!'); }
		$sql = 'UPDATE ' . CONFIG_TABLE . ' SET config_value='. $_POST['betsaday'] .' WHERE config_name="luckydice_betsaday"';
		if(! ($db->sql_query($sql)) ) { message_die(GENERAL_MESSAGE, 'Fatal Error Updating Lucky Dice config<br>'.mysql_error()); }
	}
	//----
	
	$overall = '
	<table width="100%" cellpadding="4" cellspacing="1" border="0">
	  <tr>
		<th width="50%"  class="thHead" colspan="2"><center>'.$lang['admin_title'].'</center></th>
	  </tr>
	  <tr>
		<td class="row1" width="100%">'.$lang['admin_updated'].' | <b><a href="'.append_sid("admin_luckydice.$phpEx").'">'.$lang['admin_return'].'</a></b></td>
	  </tr>
	</table>';

}




// END SLOT MACHINE ADMIN

$template->assign_vars(array(
	'OVERALL' => $overall,
	'TTL' => $lang['admin_title'],
	'DESC' => $lang['admin_desc'],
));
$template->set_filenames(array(
	'body' => 'admin/luckydice_config.tpl')
);
//
// Generate the page
//
$template->pparse('body');

include('page_footer_admin.' . $phpEx);


?>
