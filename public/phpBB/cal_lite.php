<?php
/*********************************************
*	Calendar Lite
*
*	$Author: martin $
*	$Date: 2005-09-04 20:53:51 +0100 (Sun, 04 Sep 2005) $
*	$Revision: 30 $
*
*********************************************/

/*###############################################################
## Mod Title: phpBB2 Calendar Lite
## Mod Version: 1.4.6
## Author: WebSnail < Martin Smallridge >
## SUPPORT: http://www.snailsource.com/forum/ 
## Description: Add a Calendar to your phpBB2 installation!
##			  All registerd and logged in users can post to the calendar
##			  And Admins can modify, remove, add also.
##
## Installation Level: MEDIUM
## Installation Time: 10 minutes
## Files to Edit: 2 
## Files to Execute: 1 
##
##
## NOTE: Please read readme.txt
#################################################################*/

// Set $phpbb_root_path to location of phpBB2 root directory.
$phpbb_root_path = "./";



/*##########################################
##				 STOP					#
## DO NOT MODIFY ANYTHING BELOW THIS LINE  #
##########################################*/

define('IN_PHPBB', true);

// Clear the critical variables.
unset($caluser);
unset($userdata);

$thisscript = basename(__FILE__);

// connect to phpbb
include_once($phpbb_root_path . 'extension.inc');
include_once($phpbb_root_path . 'common.'.$phpEx);
include_once($phpbb_root_path . 'includes/bbcode.'.$phpEx);
include_once($phpbb_root_path . 'includes/functions.'.$phpEx);

include_once($phpbb_root_path . 'cal_lite_settings.php');

// Added 1.4.5
include_once($phpbb_root_path . 'cal_lite_functions.php');

$params = array(
	'sid' => 'sid',
	'id' => 'id', 
	'cl_d' => 'cl_d', 
	'cl_m' => 'cl_m', 
	'cl_y' => 'cl_y', 
	'mode' => 'mode', 
	'action' => 'action', 
	'cl_h' => 'cl_h', 
	'cl_min' => 'cl_min',
	'cl_time' => 'cl_time',
	'cl_ed' => 'cl_ed',
	'cl_em' => 'cl_em',
	'cl_ey' => 'cl_ey',
	'subject' => 'subject',
	'description' => 'message',
	'modify' => 'modify',
	'validate_id' => 'validate_id'
	);

//echo "<pre>";
while( list($var, $param) = @each($params) )
{
	//echo "P: $param , V: ".$HTTP_POST_VARS[$param]."\n";
	if ( isset($HTTP_POST_VARS[$param]) || isset($HTTP_GET_VARS[$param]) ) {
		$$var = ( isset($HTTP_POST_VARS[$param]) ) ? $HTTP_POST_VARS[$param] : $HTTP_GET_VARS[$param];
		$$var = str_replace("\'", "''", $$var);
		$$var = str_replace("\\\"", "&quot;", $$var);
		$$var = str_replace("\"", "&quot;", $$var);
	} else {
		unset($$var);
	}
}

//################################################
// Exploit code check and fix.

// Check we have the necessary function(s) available.
if(!function_exists('clean_me')) {
	message_die(GENERAL_ERROR, "<b>cal_lite_functions.php</b> needs to be updated. Please update NOW!", "", __LINE__, __FILE__, '');
}
// Remind admins that they need to delete the old cal_functions.php file.
if(file_exists(realpath('cal_functions.php')) && $userdata['user_level'] == ADMIN) {
	message_die(GENERAL_ERROR, "Cal_functions.php has been renamed 'cal_lite_functions.php', please delete the old version");
}

$num_params = array(
	'cl_d',
	'cl_m',
	'cl_y',
	'cl_h',
	'cl_min',
	'cl_time',
	'cl_ed',
	'cl_em',
	'cl_ey',
);

// Arrays to be checked.
$array_check = array(
	 0 => array('param' => 'validate_id', 'key' => 'num', 'val' => 'str', 'var' => 'num')
);

// Check key value for array. (Mainly for Validation routine)
foreach($array_check AS $test_array => $test_bits) {
	if(is_array($$test_bits['param'])) {
		$test_var = $test_bits['param'];
		$test_var_array = $$test_bits['param'];
		$new = array();
		foreach($test_var_array AS $this_key => $this_value) {
			$new[clean_me($this_key, $test_bits['key'])] = clean_me($this_value, $test_bits['val']);
		}
		$$var = $new;
	} else {
		$$var = clean_me($chubb, $test_bits['var']);
	}
}
// End Array Check

// End Exploit code fix
//################################################


// Start session management
$userdata = session_pagestart($user_ip, PAGE_INDEX, $session_length);
init_userprefs($userdata);

// Get Calendar Settings from Cal_config table
$cal_config = array();

$sql = "SELECT * FROM ". CAL_CONFIG;
if(!$result = $db->sql_query($sql))
{
	message_die(GENERAL_ERROR, "Couldn't query calendar config table", "", __LINE__, __FILE__, $sql);
}
else
{
	while( $row = $db->sql_fetchrow($result) )
	{
		$cal_config[$row['config_name']] = $row['config_value'];
	}
}
// End of Calendar settings

// Interim days id to help with transfer to $lang['datetime'] format.
// DO NOT CHANGE THESE!!!!!!!!!!!!!!!!
$langdays[0] = $lang['datetime']['Sunday'];
$langdays[1] = $lang['datetime']['Monday'];
$langdays[2] = $lang['datetime']['Tuesday'];
$langdays[3] = $lang['datetime']['Wednesday'];
$langdays[4] = $lang['datetime']['Thursday'];
$langdays[5] = $lang['datetime']['Friday'];
$langdays[6] = $lang['datetime']['Saturday'];
$langdays[7] = $lang['datetime']['Sunday'];	// Repeated to cover a Monday start

// Set Users permissions.
if ($userdata['user_level'] == 1)	{
	$caluser = 5;
}
elseif (!$userdata['session_logged_in'] && $cal_config['allow_anon']) {
	$caluser = 1;	
}
else {
	$test = $userdata['user_id'];
	$caluser = calendarperm($test);	// Set the user level for the user.
}

if ( $userdata['session_logged_in'] ) { 
	$lvd = sprintf($lang['You_last_visit'], cal_create_date($board_config['default_dateformat'], $userdata['user_lastvisit'], $board_config['board_timezone'])); 
} 
else { 
	$lvd = "Not Logged In"; 
} 


// Force login for logged out users. Still fails access if login results in 0 level access rights
if(!$userdata['session_logged_in'] && ($cal_config['allow_anon'] != '1')) {
	header("Location: " . append_sid("login.$phpEx?redirect=cal_lite.$phpEx", true)); 
}
elseif ($caluser <= 0) {
	$er_msg =  $lang['Cal_not_enough_access']."<br><br>\n";
	$er_msg .= $lang['Cal_must_member'];
	message_die(GENERAL_MESSAGE, $er_msg);
}

$page_title = $lang['Calendar'];
include ($phpbb_root_path . "includes/page_header.php"); 

// Set Calendar Home URL (used in all templates)
$homeurl = append_sid($thisscript); 

if ($cal_config['show_headers'] == 1) {
	$ct = sprintf($lang['Current_time'], cal_create_date($board_config['default_dateformat'], time(), $board_config['board_timezone'])); 
	if ( $userdata['session_logged_in'] ) { 
		$lvd = sprintf($lang['You_last_visit'], cal_create_date($board_config['default_dateformat'], $userdata['user_lastvisit'], $board_config['board_timezone'])); 
	} 
	else { 
		$lvd = "Not Logged In"; 
	} 
	$phpbbheaders  = '<span class=gensmall>'. $lvd ."<br>\n";
	$phpbbheaders .= $ct.'<br></span>';
}
else {
	$phpbbheaders = '';
}

// Default date
if ($userdata && $userdata['user_id'] != '-1') {
	if (!$cl_d) { $cl_d = cal_create_date("j", time(), $userdata['user_timezone']); }
	if (!$cl_m) { $cl_m = cal_create_date("n", time(), $userdata['user_timezone']); }
	if (!$cl_y) { $cl_y = cal_create_date("Y", time(), $userdata['user_timezone']); }
	} 
else {
	if (!$cl_d) { $cl_d = cal_create_date("j", time(), $userdata[board_timezone]); }
	if (!$cl_m) { $cl_m = cal_create_date("n", time(), $userdata[board_timezone]); }
	if (!$cl_y) { $cl_y = cal_create_date("Y", time(), $userdata[board_timezone]); }
	}

$lastday = 1;
while (checkdate($cl_m,$lastday,$cl_y))
	{
	$lastday++;
	}
if ($mode == 'validate' && $caluser >=5) { 
	validate(); 
}
elseif ($mode == 'display') {
	display(); 
}
elseif ($action == 'Delete_marked' && $caluser >= 4 ) {
	delete_marked(); 
}
elseif ($action == 'Modify_marked' && $caluser >= 4 ) { 
	modify_marked(); 
}
elseif ($action == 'Cal_add_event' && $caluser >= 2 ) { 
	cal_add_event(); 
}
elseif ($action == 'Addsucker') {
	addsucker($modify); 
}
else {
	defaultview($todaycolor); 
}

include_once($phpbb_root_path . 'includes/page_tail.php');
exit;



//#################################################
// START OF FUNCTIONS.

function validate()
{
	// Start of MOD function (validate untrusted events)
	global $thisscript, $phpbb_root_path, $phpEx, $db, $action, $template,
		$id, $cl_d, $cl_m, $cl_y, $userdata, $lang, $config_footer, $footer, $caluser,
		$cl_ed, $cl_em, $cl_ey, $homeurl, $board_config, $phpbbheaders,
		$validate_id, $cal_config;

	if ($caluser >= 5) {
		switch ($action) {
			case "validevent":
		  		// Validate the selected events.
		  		if (!is_array($validate_id)) {
					message_die(GENERAL_MESSAGE, $lang['Cal_must_sel_event'], '', __LINE__, __FILE__, '');
				} elseif (!is_array($validate_id) && $userdata['user_level'] == ADMIN) {
					message_die(GENERAL_MESSAGE, 'Validate ID is not an array', '', __LINE__, __FILE__, '');	
				}
				reset($validate_id);
				// $id is an array where checkboxes
				foreach($validate_id AS $this_id => $value) {
					$sql = '';
					if ($value=='yes') {
						$sql = "UPDATE ".CAL_TABLE." SET valid = 'yes' WHERE id = '$this_id'";
					}
					elseif ($value=='del') {
						$sql = "DELETE FROM ".CAL_TABLE." WHERE id = '$this_id'";
					}

					if($sql) {
						if ( !($query = $db->sql_query($sql)) ) {
							message_die(GENERAL_ERROR, 'Could not validate events', '', __LINE__, __FILE__, $sql);
						}
					}
				}
				$message = $lang['Cal_event_validated'];
				$url = append_sid($thisscript."?cl_m=".$cl_m."&cl_y=".$cl_y);
				$message .= "</br></br><a href='".$url."'>".$lang['Cal_back2cal']."</a>";
				message_die(GENERAL_MESSAGE, $message, '', __LINE__, __FILE__, '');

				// end of case
				break;


			case "getlist":
				// Get the list of events waiting to be validated and display them for selection

				$sql = "SELECT * FROM ".CAL_TABLE." WHERE valid='no' ORDER BY stamp";
				if ( !($query = $db->sql_query($sql)) ) {
					message_die(GENERAL_ERROR, 'Could not get events list to validate', '', __LINE__, __FILE__, $sql);
				}
				$url = append_sid($thisscript."?cl_m=".$cl_m."&cl_y=".$cl_y);

				$template->set_filenames(array(
					'body' => 'cal_validate_events_lite.tpl')
				);

				$template->assign_vars(array(
					'VALIDATE' => $lang['Validate'],
					'SELECT' => $lang['Select'],
					'SUBJECT' => $lang['Subject'],
					'DATE' => $lang['Date'],
					'END_DATE' => $lang['End_day'],
					'AUTHOR' => $lang['Username'],
					'BUTTON_HOME' => button_main($url, $lang['Cal_back2cal'], 'center'))
				);

				$template->assign_vars(array(
					'PHPBBHEADER' => $phpbbheaders,
					'S_POST_ACTION' => append_sid($thisscript),
					'CAL_VERSION' => 'Ver: '.$cal_config['version'],
					'CALENDAR' => $lang['Calendar'],
					'L_CAL_NEW' => $lang['Cal_add_event'],
					'U_INDEX' => append_sid("index.$phpEx"),
					'L_INDEX' => sprintf($lang['Forum_Index'], $board_config['sitename']),
					'U_CAL_HOME' => $homeurl)
				);
				$i = 0;
				while ($row = $db->sql_fetchrow($query)) {
					$options = "<select name=validate_id[".$row['id']."]>
						<option value='hold' SELECTED>Hold</option>
						<option value='yes'>Accept</option>
						<option value='del'>Deny</option>
						</select>";

					$zdesc  = stripslashes($row['description']);
					$bbcode_uid = $row['bbcode_uid'];
					if( $board_config['allow_bbcode'] ) {
						$zdesc = ( $board_config['allow_bbcode'] ) ? bbencode_second_pass($zdesc, $bbcode_uid) : preg_replace("/\:[0-9a-z\:]+\]/si", "]", $zdesc);
					}
					if ( $board_config['allow_smilies'] ) {
						$zdesc = smilies_pass($zdesc);
					}
					$zsujet = stripslashes($row['subject']);

					$template->assign_block_vars('event_row', array(
						'SELECT' => $options,
						'SUBJECT' => $zsujet,
						'DATE' => mydateformat($row['stamp'], $userdata['user_dateformat']),
						'END_DATE' => mydateformat($row['eventspan'], $userdata['user_dateformat'], 1),
						'AUTHOR' => stripslashes($row['username']),
						'DESC' => $zdesc)
					);
					$i++;
		  		}
				if($i == 0) {
					$template->assign_block_vars('no_events', array(
						'NO_EVENTS' => $lang['No records'])
					);
					$submit_button = '';
				}
				else {
					$submit_button = "<input type='submit' accesskey='s' tabindex='6' name='post' class='mainoption' value='".$lang['Submit']."' />";
				}
				$template->assign_vars(array(
					'SUBMIT' => $submit_button)
				);
				$template->pparse('body');
				break;
			}
		}
	else
		{
		message_die(GENERAL_MESSAGE, $lang['Cal_delete_event'], '', __LINE__, __FILE__, '');
		}
	return;
}

function delete_marked()
{
	global $thisscript, $phpbb_root_path, $phpEx, $action,
		$id, $cl_d, $cl_m, $cl_y, $userdata, $lang, $config_footer, $footer, $caluser,
		$cl_ed, $cl_em, $cl_ey, $homeurl, $db;
	if ($caluser >= 4) {
		if (!$id) {
			message_die(GENERAL_ERROR, $lang['Cal_must_sel_event'], '', __LINE__, __FILE__, $sql);
		}
		$sql = "SELECT id, user_id FROM ".CAL_TABLE." WHERE id = '$id'";
		if ($caluser < 5) { 
			$sql .= " AND user_id = '".$userdata['user_id']."'"; 
		}
		if ( !($query = $db->sql_query($sql)) ) {
			message_die(GENERAL_ERROR, 'Could not select event to delete from Table', '', __LINE__, __FILE__, $sql);
		}
		$row = $db->sql_fetchrow($query);
		if ($row['id'] != '') {
			$sql = "DELETE FROM ".CAL_TABLE." WHERE id = '$id'";
			if ( !($query = $db->sql_query($sql)) ) {
				message_die(GENERAL_ERROR, 'Could Not delete event from Table', '', __LINE__, __FILE__, $sql);
			}
			else {
				$url = append_sid($thisscript);
				$message = $lang['Cal_event_delete']. "<br><br><a href='".$url."'>".$lang['Cal_back2cal']."</a>";
				message_die(GENERAL_MESSAGE, $message, '', __LINE__, __FILE__, $sql);
			}
		}
		else
		{
	  	// Failed
		message_die(GENERAL_ERROR, $lang['Cal_delete_event'], '', __LINE__, __FILE__, $sql);
		}
	}
	else
	{
		// Failed
		message_die(GENERAL_ERROR, $lang['Cal_delete_event'], '', __LINE__, __FILE__, $sql);
	}
	return;
}


function modify_marked()
{
	global $html_entities_match, $html_entities_replace;
	global $thisscript, $board_config, $phpbb_root_path, $phpEx, $action, $lastday, $phpbbheaders,
		$id, $cl_d, $cl_m, $cl_y, $userdata, $lang, $config_footer, $footer, $caluser,
		$cl_ed, $cl_em, $cl_ey, $bbcode_uid, $homeurl, $db, $template, $cal_config;

	include_once($phpbb_root_path . 'includes/functions_post.'.$phpEx);

	if ($caluser >= 4) {
		if (!$id) {
			message_die(GENERAL_ERROR, $lang['Cal_must_sel_event'], '', __LINE__, __FILE__, '');  
		}
		$sql = "SELECT *, SUBSTRING(stamp FROM 12 FOR 5) AS thetime, 
			SUBSTRING(stamp FROM 9 FOR 2) AS theday,
			SUBSTRING(eventspan FROM 9 FOR 2) AS theendday,
			SUBSTRING(stamp FROM 6 FOR 2) AS themonth,
			SUBSTRING(eventspan FROM 6 FOR 2) AS theendmonth,
			SUBSTRING(stamp FROM 1 FOR 4) AS theyear,
			SUBSTRING(eventspan FROM 1 FOR 4) AS theendyear
			FROM ".CAL_TABLE." WHERE id = '$id'";
		if ( !($query = $db->sql_query($sql)) ) {
			// CHECK echo "<B>".mysql_error()."</B><BR><BR>"; exit;
			message_die(GENERAL_ERROR, 'Could not select event to modify from Table', '', __LINE__, __FILE__, $sql);
		}
		$row = $db->sql_fetchrow($query);
		if ($caluser == 5 || $userdata['user_id'] == $row['user_id'])  {
			$bbcode_uid = $row['bbcode_uid'];
			$zdesc = ereg_replace("<br />", "", $row['description']);
			$zdesc = preg_replace("/\:(([a-z0-9]:)?)" . $bbcode_uid . "/si", "", $zdesc);

			$hidden_form_fields = "<input type=hidden name=id value='".$row['id']."'>
				<input type=hidden name=bbcode_uid value='".$row['bbcode_uid']."'>
				<input type=hidden name=modify value='Modify'>";

			generate_smilies('inline', PAGE_POSTING);
			$template->set_filenames(array(
				'body' => 'cal_posting_body_lite.tpl')
				);

			$template->assign_vars(array(
				'PHPBBHEADER' => $phpbbheaders,
				'CAL_VERSION' => 'Ver: '.$cal_config['version'],
				'CALENDAR' => $lang['Calendar'],
				'L_CAL_NEW' => $lang['Cal_mod_marked'],
				'U_INDEX' => append_sid("index.$phpEx"),
				'L_INDEX' => sprintf($lang['Forum_Index'], $board_config['sitename']),
				'U_CAL_HOME' => $homeurl)
				);

			$template->assign_vars(array(
				'SUBJECT' => $row['subject'],
				'MESSAGE' => stripslashes($zdesc),

				'L_SUBJECT' => $lang['Subject'],
				'L_MESSAGE_BODY' =>  $lang['Cal_description'],
				'L_SUBMIT' => $lang['Cal_mod_only'],
				'L_CANCEL' => $lang['Cancel'],

				'L_EMPTY_SUBJECT' => $lang['Empty subject'],
				'L_EMPTY_DESC' => $lang['Empty description'],
				'L_MAX' => $lang['max'],
				'L_DAY' => $lang['days'],

				'L_BBCODE_B_HELP' => $lang['bbcode_b_help'], 
				'L_BBCODE_I_HELP' => $lang['bbcode_i_help'], 
				'L_BBCODE_U_HELP' => $lang['bbcode_u_help'], 
				'L_BBCODE_Q_HELP' => $lang['bbcode_q_help'], 
				'L_BBCODE_C_HELP' => $lang['bbcode_c_help'], 
				'L_BBCODE_L_HELP' => $lang['bbcode_l_help'], 
				'L_BBCODE_O_HELP' => $lang['bbcode_o_help'], 
				'L_BBCODE_P_HELP' => $lang['bbcode_p_help'], 
				'L_BBCODE_W_HELP' => $lang['bbcode_w_help'], 
				'L_BBCODE_A_HELP' => $lang['bbcode_a_help'], 
				'L_BBCODE_S_HELP' => $lang['bbcode_s_help'], 
				'L_BBCODE_F_HELP' => $lang['bbcode_f_help'], 
				'L_EMPTY_MESSAGE' => $lang['Empty_message'],

				'L_FONT_COLOR' => $lang['Font_color'], 
				'L_COLOR_DEFAULT' => $lang['color_default'], 
				'L_COLOR_DARK_RED' => $lang['color_dark_red'], 
				'L_COLOR_RED' => $lang['color_red'], 
				'L_COLOR_ORANGE' => $lang['color_orange'], 
				'L_COLOR_BROWN' => $lang['color_brown'], 
				'L_COLOR_YELLOW' => $lang['color_yellow'], 
				'L_COLOR_GREEN' => $lang['color_green'], 
				'L_COLOR_OLIVE' => $lang['color_olive'], 
				'L_COLOR_CYAN' => $lang['color_cyan'], 
				'L_COLOR_BLUE' => $lang['color_blue'], 
				'L_COLOR_DARK_BLUE' => $lang['color_dark_blue'], 
				'L_COLOR_INDIGO' => $lang['color_indigo'], 
				'L_COLOR_VIOLET' => $lang['color_violet'], 
				'L_COLOR_WHITE' => $lang['color_white'], 
				'L_COLOR_BLACK' => $lang['color_black'], 

				'L_FONT_SIZE' => $lang['Font_size'], 
				'L_FONT_TINY' => $lang['font_tiny'], 
				'L_FONT_SMALL' => $lang['font_small'], 
				'L_FONT_NORMAL' => $lang['font_normal'], 
				'L_FONT_LARGE' => $lang['font_large'], 
				'L_FONT_HUGE' => $lang['font_huge'], 

				'L_BBCODE_CLOSE_TAGS' => $lang['Close_Tags'], 
				'L_STYLES_TIP' => $lang['Styles_tip'], 

				'S_POST_ACTION' => append_sid($thisscript."?action=Addsucker"),
				'S_HIDDEN_FORM_FIELDS' => $hidden_form_fields)
				);

			// Day field
			$this_day = create_day_drop($row['theday'], 31);

			// Month field
			$this_month = create_month_drop($row['themonth'], $row['theyear']);

			// Year field
			$this_year = create_year_drop($row['theyear']);

			// End Day field
			$end_day = create_day_drop($row['theendday'], 31);

			// End Month field
			$end_month = create_month_drop($row['theendmonth'], $row['theendyear']);

			// End Year field
			$end_year = create_year_drop($row['theendyear']);

			$currentmonth = cal_create_date("m", time(), $userdata['user_timezone']);

			$the_time = ($row['thetime'] != '00:00') ? $row['thetime'] : '';

			// Set the rest of the Calendar fields
			$template->assign_vars(array(
				'L_CAL_DATE' => $lang['Cal_day'],
				'L_CAL_TIME' => $lang['Cal_hour'],
				'L_CAL_END_DATE' => $lang['End_day'],
				'THIS_DAY' => $this_day,
				'THIS_MONTH' => $this_month,
				'THIS_YEAR' => $this_year,
				'TIME' => $the_time,
				'END_DAY' => $end_day,
				'END_MONTH' => $end_month,
				'END_YEAR' => $end_year,
				'L_CAL_HOME' => $lang['Cal_back2cal'])
			);
			$template->pparse('body');
			return;
		}
		else {
			message_die(GENERAL_ERROR, $lang['Cal_edit_own_event'], '', __LINE__, __FILE__, $sql);
		}
	}
	else {
		message_die(GENERAL_ERROR, $lang['Cal_not_enough_access'], '', __LINE__, __FILE__, $sql);
	}
	return;
}

function cal_add_event()
{
	global $thisscript, $phpbb_root_path, $phpEx, $board_config, $action,
		$id, $cl_d, $cl_m, $cl_y, $userdata, $lang, $caluser, $lastday, $phpbbheaders,
		$cl_ed, $cl_em, $cl_ey, $bbcode_uid, $db, $template, $homeurl, $cal_config;

	if ($caluser >= 2) {
		$currentday = cal_create_date("j", time(), $userdata['user_timezone']);
		if ($cl_d) { $currentday = $cl_d; }

		include_once($phpbb_root_path . 'includes/functions_post.'.$phpEx);
		generate_smilies('inline', PAGE_POSTING);
		$template->set_filenames(array(
			'body' => 'cal_posting_body_lite.tpl')
		);

		$template->assign_vars(array(
			'PHPBBHEADER' => $phpbbheaders,
			'CAL_VERSION' => 'Ver: '.$cal_config['version'],
			'CALENDAR' => $lang['Calendar'],
			'L_CAL_NEW' => $lang['Cal_add_event'],
			'U_INDEX' => append_sid("index.$phpEx"),
			'L_INDEX' => sprintf($lang['Forum_Index'], $board_config['sitename']),
			'U_CAL_HOME' => $homeurl)
		);

		$template->assign_vars(array(
			'SUBJECT' => $subject,
			'MESSAGE' => $message,

			'L_SUBJECT' => $lang['Subject'],
			'L_MESSAGE_BODY' =>  $lang['Cal_description'],
			'L_SUBMIT' => $lang['Submit'],
			'L_CANCEL' => $lang['Cancel'],

			'L_EMPTY_SUBJECT' => $lang['Empty subject'],
			'L_EMPTY_DESC' => $lang['Empty description'],
			'L_MAX' => $lang['max'],
			'L_DAY' => $lang['days'],

			'L_BBCODE_B_HELP' => $lang['bbcode_b_help'], 
			'L_BBCODE_I_HELP' => $lang['bbcode_i_help'], 
			'L_BBCODE_U_HELP' => $lang['bbcode_u_help'], 
			'L_BBCODE_Q_HELP' => $lang['bbcode_q_help'], 
			'L_BBCODE_C_HELP' => $lang['bbcode_c_help'], 
			'L_BBCODE_L_HELP' => $lang['bbcode_l_help'], 
			'L_BBCODE_O_HELP' => $lang['bbcode_o_help'], 
			'L_BBCODE_P_HELP' => $lang['bbcode_p_help'], 
			'L_BBCODE_W_HELP' => $lang['bbcode_w_help'], 
			'L_BBCODE_A_HELP' => $lang['bbcode_a_help'], 
			'L_BBCODE_S_HELP' => $lang['bbcode_s_help'], 
			'L_BBCODE_F_HELP' => $lang['bbcode_f_help'], 
			'L_EMPTY_MESSAGE' => $lang['Empty_message'],

			'L_FONT_COLOR' => $lang['Font_color'], 
			'L_COLOR_DEFAULT' => $lang['color_default'], 
			'L_COLOR_DARK_RED' => $lang['color_dark_red'], 
			'L_COLOR_RED' => $lang['color_red'], 
			'L_COLOR_ORANGE' => $lang['color_orange'], 
			'L_COLOR_BROWN' => $lang['color_brown'], 
			'L_COLOR_YELLOW' => $lang['color_yellow'], 
			'L_COLOR_GREEN' => $lang['color_green'], 
			'L_COLOR_OLIVE' => $lang['color_olive'], 
			'L_COLOR_CYAN' => $lang['color_cyan'], 
			'L_COLOR_BLUE' => $lang['color_blue'], 
			'L_COLOR_DARK_BLUE' => $lang['color_dark_blue'], 
			'L_COLOR_INDIGO' => $lang['color_indigo'], 
			'L_COLOR_VIOLET' => $lang['color_violet'], 
			'L_COLOR_WHITE' => $lang['color_white'], 
			'L_COLOR_BLACK' => $lang['color_black'], 

			'L_FONT_SIZE' => $lang['Font_size'], 
			'L_FONT_TINY' => $lang['font_tiny'], 
			'L_FONT_SMALL' => $lang['font_small'], 
			'L_FONT_NORMAL' => $lang['font_normal'], 
			'L_FONT_LARGE' => $lang['font_large'], 
			'L_FONT_HUGE' => $lang['font_huge'], 

			'L_BBCODE_CLOSE_TAGS' => $lang['Close_Tags'], 
			'L_STYLES_TIP' => $lang['Styles_tip'], 

			'S_POST_ACTION' => append_sid($thisscript."?action=Addsucker"),
			'S_HIDDEN_FORM_FIELDS' => '')
		);

		// Day field
		$this_day = create_day_drop($currentday, 31);

		// Month field
		$this_month = create_month_drop($cl_m, $cl_y);

		// Year field
		$this_year = create_year_drop($cl_y);

		// End Day field
		$end_day = create_day_drop($currentday, 31);

		// End Month field
		$end_month = create_month_drop($cl_m, $cl_y);

		// End Year field
		$end_year = create_year_drop($cl_y);

		$currentmonth = cal_create_date("m", time(), $userdata['user_timezone']);

	// Set the rest of the Calendar fields
		$template->assign_vars(array(
			'L_CAL_DATE' => $lang['Cal_day'],
			'L_CAL_TIME' => $lang['Cal_hour'],
			'L_CAL_END_DATE' => $lang['End_day'],
			'THIS_DAY' => $this_day,
			'THIS_MONTH' => $this_month,
			'THIS_YEAR' => $this_year,
			'END_DAY' => $end_day,
			'END_MONTH' => $end_month,
			'END_YEAR' => $end_year,
			'L_CAL_HOME' => $lang['Cal_back2cal'])
		);
		$template->pparse('body');
		return;
	}
	else {
		message_die(GENERAL_ERROR, $lang['Cal_not_enough_access'], '', __LINE__, __FILE__, '');
	}
}


function addsucker( $modify='')
{
	global $html_entities_match, $html_entities_replace; 
	global $thisscript, $phpbb_root_path, $phpEx, $db, $template, $action, $phpbbheaders,
		$board_config, $id, $cl_d, $cl_m, $cl_y, $cl_time, $userdata, $modify, $lang, $description, $subject, $caluser,
		$cl_ed, $cl_em, $cl_ey, $bbcode_uid, $homeurl, $cal_config;

	if (($subject =='') || ($description =='')) {
		// Nothing in the subject line: Reject it.
		message_die(GENERAL_ERROR, $lang['No information'], '', __LINE__, __FILE__, '');
	}
	$currentdate = time();

	// Valid Start Date?
	if (!checkdate($cl_m,$cl_d,$cl_y)) {
		message_die(GENERAL_ERROR, $lang['Invalid date']);
	}
	// Valid End Date?
	if (!checkdate($cl_em,$cl_ed,$cl_ey)) {
		message_die(GENERAL_ERROR, $lang['Invalid date']);
	}

	// Valid Time?
	$cl_h = substr($cl_time, 0,2);
	$symbol = substr($cl_time,2,1);
	$cl_min = substr($cl_time,3,2);
	if (!empty($cl_time)) {
		if(!is_numeric($cl_h) || $cl_h < 0 || $cl_h > 23) {
			$err_time = true;
		}
		elseif(!is_numeric($cl_min) || $cl_min < 0 || $cl_min > 59) {
			$err_time = true;
		}
		else {
			unset($err_time);
		}
		if(isset($err_time)) {
			message_die(GENERAL_ERROR, ("Invalid ". $lang['Cal_hour']));
		}
	}

	// Check that date info has been set.
	if ($cl_m != '' && $cl_d != '' && $cl_y != '' && $cl_em != '' && $cl_ed != '' && $cl_ey != '') {
		$submitdate = mktime (23,59,59,$cl_m,$cl_d,$cl_y);
		$submitenddate = mktime (23,59,59,$cl_em,$cl_ed,$cl_ey);
	}
	else {
		message_die(GENERAL_ERROR, $lang['No date']);
	}

	if (($currentdate > $submitdate) && !$cal_config['allow_old'])
		{
		// The event is before "today" and we're not allowing old events.
		message_die(GENERAL_ERROR, $lang['Date before today'], '', __LINE__, __FILE__, '');
		}
	if ($submitdate > $submitenddate)
		{
		// The event ends before it begins... Oops!
		message_die(GENERAL_ERROR,  $lang['Date before start'], '', __LINE__, __FILE__, '');
		}

	include_once($phpbb_root_path . 'includes/functions_post.'.$phpEx);

	if (!$bbcode_uid) { $bbcode_uid = make_bbcode_uid(); }

	// PREPARE MESSAGE
/* 
// OLD
	$description = prepare_message($description,
		$board_config['allow_html'],
		$board_config['allow_bbcode'],
		$board_config['allow_smilies'],
		$bbcode_uid);

	$description = nl2br($description);
	$description = addslashes($description);

	// Get rid of any commas in your subject field (causes untold problems with the HTML form)
	$subject = ereg_replace("[\"]", "", $subject);
	$subject = cal_strip_bbcode($subject);			// Remove any bbcode
	$subject = strip_tags($subject);				// Remove any HTML tags
	$subject = addslashes($subject);
*/

	$description = ($board_config['allow_html']) ? $description : strip_tags($description);

	$description = prepare_message($description,
		$board_config['allow_html'],
		$board_config['allow_bbcode'],
		$board_config['allow_smilies'],
		$bbcode_uid);

	$description = str_replace("''", "'", $description);	// Deal with any single quote issues from security update
	$description = addslashes( str_replace("\n", '<br />', $description) );
	$description = ($board_config['allow_bbcode']) ? $description : cal_strip_bbcode($description);
	

	// Get rid of any commas in your subject field (causes untold problems with the HTML form)
	$subject = ereg_replace("[\"]", "", $subject);
	$subject = str_replace("''", "'", $subject);	// Deal with any single quote issues from security update
	$subject = cal_strip_bbcode($subject);			// Remove any bbcode
	$subject = strip_tags($subject);				// Remove any HTML tags
	$subject = addslashes($subject);





	$valid = 'no';
	if ($caluser >= 3) {
		$valid = 'yes';
	}
	if ($modify) {
		ereg_replace("<br />", "", $description);
		$sql = "UPDATE ".CAL_TABLE." SET stamp='$cl_y-$cl_m-$cl_d $cl_time', subject='$subject', description='$description', eventspan='$cl_ey-$cl_em-$cl_ed', bbcode_uid='$bbcode_uid' WHERE id = '$id'";
	}
	else {
		$sql = "INSERT INTO ".CAL_TABLE." (username, stamp, subject, description, user_id, valid, eventspan, bbcode_uid) VALUES ('".addslashes($userdata[username])."', '$cl_y-$cl_m-$cl_d $cl_time', '$subject', '$description', '".$userdata['user_id']."', '$valid', '$cl_ey-$cl_em-$cl_ed', '$bbcode_uid')";
	}
	if ( !($query = $db->sql_query($sql)) ) {
		message_die(GENERAL_ERROR, $lang['Cal_event_not_add'], '', __LINE__, __FILE__, $sql);
	}
	else {

		// Success the event is now pending or actually added.

		// Temp measure until the language files all get updated.
		$lang['Cal_add4valid'] = (!empty($lang['Cal_add4valid'])) ? $lang['Cal_add4valid'] : 'Event submitted for validation by an Administrator';

		$l_add = ($valid != 'no') ? $lang['Cal_event_add'] : $lang['Cal_add4valid'];

		$url = append_sid($thisscript);
		$message = $l_add. "</br></br><a href='".$url."'>".$lang['Cal_back2cal']."</a>";
		message_die(GENERAL_MESSAGE, $message, '', __LINE__, __FILE__, $sql);
	}
	return;
}


function display()
{
	global $thisscript, $phpbb_root_path, $phpEx, $action, $homeurl, $images, $phpbbheaders,
		$id, $cl_d, $cl_m, $cl_y, $userdata, $lang, $config_footer, $footer, $caluser,
		$cl_ed, $cl_em, $cl_ey, $board_config, $bbcode_uid, $template, $db, $cal_config;

	$currentmonth = cal_create_date("m", time(), $userdata['user_timezone']);

	$template->set_filenames(array(
		'body' => 'cal_day_events_lite.tpl')
	);

	$lastseconds = mktime(0,0,0,$cl_m,$cl_d,$cl_y)-(24*60*60);
		$lastday = date('j', $lastseconds);
		$lastmonth = date('m', $lastseconds);
		$lastyear = date('Y', $lastseconds);

	$nextseconds = mktime(0,0,0,$cl_m,$cl_d,$cl_y)+(24*60*60);
		$nextday = date('j', $nextseconds);
		$nextmonth = date('m', $nextseconds);
		$nextyear = date('Y', $nextseconds);
	$sql = "SELECT * FROM ".CAL_TABLE." WHERE valid = 'yes' AND ";
	if ($id) {
		$sql .= "id = '$id'";
	}
	else {
		$sql .= "eventspan >= '$cl_y-$cl_m-$cl_d 00:00:00' AND stamp <= '$cl_y-$cl_m-$cl_d 23:59:59' ORDER BY stamp";
	}
	if ( !($result = $db->sql_query($sql)) ) {
		message_die(GENERAL_ERROR, 'Could not select Event data', '', __LINE__, __FILE__, $sql);
	}
	$check=0;
	while ($row = $db->sql_fetchrow($result)) {
		$subject=stripslashes($row['subject']);
		$zdesc=stripslashes($row['description']);
		$bbcode_uid = $row['bbcode_uid'];
		if( $board_config['allow_bbcode'] ) {
			$zdesc = ( $board_config['allow_bbcode'] ) ? bbencode_second_pass($zdesc, $bbcode_uid) : preg_replace("/\:[0-9a-z\:]+\]/si", "]", $zdesc);
		}
		if ( $board_config['allow_smilies'] ) {
			$zdesc = smilies_pass($zdesc);
		}
		if ((($caluser >=4) && ($userdata['user_id']==$row['user_id'])) || ($caluser >=5)) {
			// Edit icon
			if (strtotime($row['stamp']) >= time() || $cal_config['allow_old']) {
				$edit_img = '<a href="'. append_sid($thisscript .'?action=Modify_marked&id='.$row['id']) .'"><img src="' . $images['icon_edit'] . '" alt="' . $lang['Edit_delete_post'] . '" title="' . $lang['Edit_delete_post'] . '" border="0" /></a>';
			}
			// Delete icon
			$delpost_img = '<a href="' . append_sid($thisscript .'?action=Delete_marked&id='.$row['id']).'"><img src="' . $images['icon_delpost'] . '" alt="' . $lang['Delete_post'] . '" title="' . $lang['Delete_post'] . '" border="0" /></a>';
		}
		else {
			$edit_img = '';
			$delpost_img = '';
		}

		$zdesc = make_clickable($zdesc);
		$start_date = mydateformat($row['stamp'], $userdata['user_dateformat']);
		$end_date = mydateformat($row['eventspan'], $userdata['user_dateformat'], 1);

		$template->assign_block_vars('event_row', array(
			'SUBJECT' => $subject,
			'DATE' => $start_date,
			'END_DATE' => $end_date,
			'AUTHOR' => stripslashes($row['username']),
			'DESC' => $zdesc,
			'BUTTON_DEL' => $delpost_img,
			'BUTTON_MOD' => $edit_img)
		);
		$check++;
	//echo $row['stamp']; exit;
	}
	
	if($check == 0) {
		$template->assign_block_vars('no_events', array(
			'NO_EVENTS' => $lang["No events"])
		);
	}

	// Previous Month button	
	$url = append_sid($thisscript."?cl_d=".$lastday."&cl_m=".$lastmonth."&cl_y=".$lastyear."&mode=display");
	$button_prev = button_prev($url);

	// Viewed month link
	$monthname = $lang['datetime'][date("F", mktime(0,0,0,$cl_m,1,$cl_y))];
	$select_month_url = append_sid($thisscript."?cl_m=".$cl_m."&cl_y=".$cl_y);

	// Home Button
	$curmonthname = $lang['datetime'][date("F", mktime(0,0,0,$currentmonth,1,$cl_y))];
	$url = append_sid($thisscript."?cl_m=".$currentmonth."&cl_y=".$cl_y);
	$button_home = button_main($url, $lang['Cal_back2cal'], 'center');

	// Next Month button.
	$url = append_sid($thisscript."?cl_d=".$nextday."&cl_m=".$nextmonth."&cl_y=".$nextyear."&mode=display");
	$button_next = button_next($url);
	
	if ($caluser >= 2) {
		// Add button
		$url = append_sid($thisscript."?cl_d=".$cl_d."&cl_m=".$cl_m."&cl_y=".$cl_y."&action=Cal_add_event");
		$button_add = button_add($url);

		// Validate button
		$url = append_sid($thisscript."?mode=validate&action=getlist");
		$button_val = button_validate($url);	
	}

	$template->assign_vars(array(
		'PHPBBHEADER' => $phpbbheaders,
		'CAL_VERSION' => 'Ver: '.$cal_config['version'],
		'CALENDAR' => $lang['Calendar'],
		'L_CAL_NEW' => $lang['Cal_add_event'],
		'U_INDEX' => append_sid("index.$phpEx"),
		'L_INDEX' => sprintf($lang['Forum_Index'], $board_config['sitename']),
		'U_CAL_HOME' => $homeurl)
		);

	$template->assign_vars(array(
		'CAL_MONTH' => " <a href='$select_month_url' class='topictitle'>&nbsp;$monthname&nbsp;</a>",
		'CAL_YEAR' => $cl_y,
		'CAL_DAY' => $cl_d,
		'SUBJECT' => $lang['Subject'],
		'DATE' => $lang['Date'],
		'END_DATE' => $lang['End_day'],
		'AUTHOR' => $lang['Author'],
		'BUTTON_PREV' => $button_prev,
		'BUTTON_NEXT' => $button_next,
		'BUTTON_HOME' => $button_home,
		'BUTTON_ADD' => $button_add,
		'BUTTON_VAL' => $button_val)
	);
	$template->pparse('body');
	return;
}

// End Display
// End Display

// The default display (ie: The main calendar screen)

function defaultview()
{
	global $thisscript, $phpbb_root_path, $phpEx,  $action, $phpbbheaders,
		$board_config, $cal_config, $id, $cl_d, $cl_m, $cl_y, $userdata, $lang, $description, $subject, $caluser, 
		$cl_ed, $cl_em, $cl_ey, $langdays, $template, $cal_config, $db, $homeurl;
		
	if ($userdata && $userdata['user_id'] != '-1') {
		$currentday = cal_create_date("j", time(), $userdata['user_timezone']);
		$currentmonth = cal_create_date("m", time(), $userdata['user_timezone']);
		$currentyear = cal_create_date("Y", time(), $userdata['user_timezone']);
	} 
	else {
		$currentday = cal_create_date("j", time(), $board_config['board_timezone']);
		$currentmonth = cal_create_date("m", time(), $board_config['board_timezone']);
		$currentyear = cal_create_date("Y", time(), $board_config['board_timezone']);
	}

	if ($cal_config['show_birthdays'] == 1)
	{
		$username = array();
		$user_id = array();
		$birthday = array();
		$birthmonth = array();
		$useryear = array();
		$ii = 0;

		$sql = "SELECT username, user_id, user_birthday
			FROM " . USERS_TABLE . "
			WHERE user_birthday < '999999'
			ORDER BY username";
		if( ($result = $db->sql_query($sql)) )
		{
			while( $row = $db->sql_fetchrow($result))
			{
				$ii++;
				$username[$ii] = $row['username'];
				$user_id[$ii] = $row['user_id'];
				$birthday[$ii] = intval(substr($row['user_birthday'], 6, 2));
				$birthmonth[$ii] = intval(substr($row['user_birthday'], 4, 2));
				$useryear[$ii] = intval(substr($row['user_birthday'], 0, 4));
			}
		}
	}

	$lastday = 1;
	
	$pt = '*';
	
	if (!$cl_m) {
		$cl_m = $currentmonth;
		$cl_y = $currentyear;
	}
	else if($cl_m < 10) {
		$cl_m = date('m',  mktime(0,0,0,$cl_m,1,$cl_y));
	}

	$template->set_filenames(array(
		'body' => 'cal_view_month_lite.tpl')
	);


	$firstday =  date('w', (mktime(0,0,0,$cl_m,1,$cl_y) - $cal_config['week_start'])) % 7;
	$first_day = ($first_day < 0) ? ($first_day + 7) : $first_day;
	$lastday = date('t',  mktime(0,0,0,$cl_m,1,$cl_y));
	$end_day = 7-(($firstday + $lastday) % 7);
	$end_day = ($end_day == 7) ? 0 : $end_day;	// day 7 same as day 0

	$nextmonth = ($cl_m < 12) ? ($cl_m + 1) : 1;
	$nextyear = ($cl_m < 12) ? $cl_y : ($cl_y + 1);

	$lastmonth = ($cl_m > 1) ? ($cl_m - 1) : 12;
	$lastyear = ($cl_m > 1) ? $cl_y: ($cl_y - 1);

	$template->assign_vars(array(
		'PHPBBHEADER' => $phpbbheaders,
		'CAL_MONTH' => $lang['datetime'][date("F", mktime(0,0,0,$cl_m,1,$cl_y))],
		'CAL_YEAR' => $cl_y,
		'DAY_HEAD_1' => $langdays[(0+$cal_config['week_start'])%7],
		'DAY_HEAD_2' => $langdays[(1+$cal_config['week_start'])%7],
		'DAY_HEAD_3' => $langdays[(2+$cal_config['week_start'])%7],
		'DAY_HEAD_4' => $langdays[(3+$cal_config['week_start'])%7],
		'DAY_HEAD_5' => $langdays[(4+$cal_config['week_start'])%7],
		'DAY_HEAD_6' => $langdays[(5+$cal_config['week_start'])%7],
		'DAY_HEAD_7' => $langdays[(6+$cal_config['week_start'])%7],
		'L_GO' => $lang['Go'],
		'L_MONTH_JUMP' => $lang['Month_jump'],
		'S_MONTH' => create_month_drop($cl_m, $cl_y),
		'S_YEAR' => create_year_drop($cl_y))
	);
	$rowrow = 1;


	$lastday = date('t',  mktime(0,0,0,$cl_m,1,$cl_y));
	// New optimised SQL query

	$sql = "SELECT * FROM ". CAL_TABLE ."
		WHERE valid = 'yes' AND eventspan >= '$cl_y-$cl_m-1' AND stamp <= '$cl_y-$cl_m-$lastday 23:59:59'
		ORDER BY stamp";

	if ( !($query = $db->sql_query($sql)) ) {
		echo "<BR>$sql<BR>".mysql_error(); exit;
		message_die(GENERAL_ERROR, 'Could not get months data', '', __LINE__, __FILE__, $sql);
	}

	$dates = array();
	while($get_row = $db->sql_fetchrow($query)) {
		$dates[] = $get_row;
	}

	// Changed the range to do ALL the days not require duplicate code later.

	$max_query = 0; 

	for ($i=1; $i <= ($firstday+$lastday+$end_day); $i++) {
		if($i <= $firstday) {
			$today_year = ($cl_m <= 1) ? $cl_y - 1 : $cl_y;
			$today_month = ($cl_m <= 1) ? 12 : $cl_m - 1;
			$today_day = (gmdate('t', gmmktime(0,0,0,$today_month,1,$today_year)) - $firstday) + $i;
		}
		else if($i > ($firstday+$lastday)) {
			$today_year = ($cl_m >= 12) ? $cl_y + 1 : $cl_y;
			$today_month = ($cl_m >= 12) ? 1 : $cl_m + 1;
			$today_day = $i - ($firstday + $lastday);
		}
		else {
			$today_year = $cl_y;
			$today_month = $cl_m;
			$today_day = $i - $firstday;
		}

		// calc todays date range
		$today_start = date("Y-m-d", mktime(0,0,0,$cl_m,$today_day,$cl_y));
		$today_end = date("Y-m-d H:i:s", mktime(23,59,59,$cl_m,$today_day,$cl_y));

		//CHECK echo "TS: $today_start , TE: $today_end <BR>";

		unset($this_date);
		$this_date = array();
		for($d_cnt=0; $d_cnt < count($dates); $d_cnt++) {
			//CHECK echo "TS: ".$dates[$d_cnt]['stamp']." , TE: ".$dates[$d_cnt]['eventspan']." <BR>";

			if( ($dates[$d_cnt]['eventspan'] >= $today_start) && ($dates[$d_cnt]['stamp'] <= $today_end) ) {
				// Compile an array of all the events for today
				$this_date[] = $dates[$d_cnt];
			}
		}
		$this_date = array_qsort2 ($this_date, 'stamp', SORT_ASC, 0, $last= -2);

		$thisday = $i - $firstday;

		$event_list = '';
		$correction = 0;

	if ($cal_config['show_birthdays'] == 1)
	{
		$ii = 0;

		for ($ii; $ii<=count($useryear); $ii++)
		{
			if ( $birthday[$ii] == $thisday AND $birthmonth[$ii] == $month)
			{
				$userage = $year - $useryear[$ii];
				$correction++;
				$event_list .= '<span class="gensmall">-> <a href="' . append_sid("profile.$phpEx?mode=viewprofile&amp;" . POST_USERS_URL . "=$user_id[$ii]") . '">' . $username[$ii] . ' (' . $userage .')</a></span><br />';
			}
		}
	}

		$query_num = count($this_date);

      if (($query_num + $correction) > $max_query) 
      { 
         $max_query = $query_num + $correction; 
      } 

		for ($j = 0; $j < $query_num; $j++) {
			$results = $this_date[$j];

			$subject = stripslashes($results['subject']);
			$full_subject = stripslashes($results['subject']);
			$subjectnum = '';

			// Specific UKRag.net function.
			if ( strlen($subject) > $cal_config['subject_length']) {
				if ((substr($subject,-3,1) == '(') && (substr($subject,-1,1) == ')')) {
					// store the number of permits and tack them on the end of the shortened subject
					$subjectnum = substr($subject,-2,1);
					$subject = substr($subject, 0, -3);
				}
				$subject = substr($subject, 0, $cal_config['subject_length']);
				$subject .= '..';
			}
			if ($subjectnum) {
				$subject .= ' ('.$subjectnum.')';
			}
			// End UKRag.net function

			$url = append_sid($thisscript.'?id='.$results['id'].'&mode=display&cl_d='.$thisday.'&cl_m='.$today_month.'&cl_y='.$today_year, 1);
			// Need to keep the size down
			$event_list .= "<span class=gensmall><acronym title='".stripslashes($results['username']).": $full_subject'>
				$pt <a href='$url' id='cal_id".$results['id']."' onMouseOver=\"swc('cal_id".$results['id']."',1)\" onMouseOut=\"swc('cal_id".$results['id']."',0)\">
				$subject</a></acronym><br></span>\n";
		}
		if ($i % 7 == 0) {
			// we're at the end of a row
			// got another week to run
			$week_end = "\n</tr>\n<tr>\n";
		}
		else {
			$week_end = '';
		}

		// Choose which format to use (ie: pre, during or after this month)

		if ($i <= $firstday) {
			$thisday = '';
			$event_list = '';
			$cellback = 'class=row1';
			$cellhead = 'class=row1';
			$cellbody = 'class=row1';
		}
		else if ($currentmonth == $today_month && $cl_m == $currentmonth && $currentday == $thisday && $currentyear == $cl_y) {
		 	// TODAY
			$cellback = 'class=row3';
			$cellhead = 'class=rowpic';
			$cellbody = 'class=row2';
		}
		else if ($i > ($firstday + $lastday)) {
			// end of the month
			$thisday = '';
			$event_list = '';
			$cellback = 'class=row1';
			$cellhead = 'class=row1';
			$cellbody = 'class=row1';
		}
/* Uncomment if you want to highlight the day header for any days with events on
		elseif ($query_num) {
			$cellback = 'class=row3';
			$cellhead = 'style="background-color:red"';
			$cellbody = 'class=row1';
		}
*/
		else {
			$cellback = 'class=row3';
			$cellhead = 'class=row3';
			$cellbody = 'class=row1';
		}

		$url_day = !empty($thisday) ? append_sid($thisscript."?cl_d=".$thisday."&cl_m=".$cl_m."&cl_y=".$cl_y."&mode=display") : '';

	  	$template->assign_block_vars('daycell', array(
			'S_CELL' => $cellback,
			'U_DAY' => $url_day,
			'NUM_DAY' => $thisday,
			'DAY_EVENT_LIST' => $event_list,
			'S_HEAD' => $cellhead,
			'S_DETAILS' => $cellbody,
			'WEEK_ROW' => $week_end)
			);
		if ($week_end) {
			++$rowrow;
			if ($rowrow == '3') { 
				$rowrow = '1'; 
			}
		}
	}
   if ($max_query < 5) $max_query = 5; 

   $max_height = $max_query * 12; 

   $template->assign_vars(array( 
      'CAL_VERSION' => 'Ver: '.$cal_version, 
      'CALENDAR' => $lang['Calendar'], 
      'L_CAL_NEW' => $lang['Cal_add_event'], 
      'U_INDEX' => append_sid("index.$phpEx"), 
      'L_INDEX' => sprintf($lang['Forum_Index'], $board_config['sitename']), 
      'U_CAL_HOME' => $homeurl, 
      'MAX_HEIGHT' => 'height=' . $max_height . 'px') 
      );


	if ($caluser >= 2) {
		// Previous Month button
		$url = append_sid($thisscript."?cl_m=".$lastmonth."&cl_y=".$lastyear);
		$button_prev = button_prev($url);

		// Add Event button			
		$url = append_sid($thisscript."?cl_m=".$cl_m."&cl_y=".$cl_y."&action=Cal_add_event");
		$button_add = button_add($url);

		// Admin Validate button			
		$url = append_sid($thisscript."?mode=validate&action=getlist");
		$button_validate = button_validate($url);


		// Next Month button			
		$url = append_sid($thisscript."?cl_m=".$nextmonth."&cl_y=".$nextyear);
		$button_next = button_next($url);
	} 
	else {
		// Previous Month button
		$url = append_sid($thisscript."?cl_m=".$lastmonth."&cl_y=".$lastyear);
		$button_prev = button_prev($url);

		// Next Month button			
		$url = append_sid($thisscript."?cl_m=".$nextmonth."&cl_y=".$nextyear);
		$button_next = button_next($url);
	}

	$template->assign_vars(array(
		'BUTTON_PREV' => $button_prev,
		'BUTTON_ADD' => $button_add,
		'BUTTON_VALIDATE' => $button_validate,
		'BUTTON_NEXT' => $button_next)
	);
	$template->pparse('body');
	return;
}




?>