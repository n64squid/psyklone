<?php
/***************************************************************************
 *                              jobs.php
 *                            -------------------
 *   version		: 1.1.3
 *   support forums	: http://forums.knightsofchaos.com
 *   website		: http://www.zarath.com
 *
 ***************************************************************************/

/***************************************************************************
 *
 *   copyright (C) 2004-2006  Zarath
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License
 *   as published by the Free Software Foundation; either version 2
 *   of the License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   http://www.gnu.org/copyleft/gpl.html
 *
 ***************************************************************************/

if (defined('PAY_ME'))
{
	define('IN_PHPBB', true);
	$phpbb_root_path = './';
	require($phpbb_root_path . 'config.' . $phpEx);

	$theTime = time();

	if ( $board_config['jobs_pay_type'] )
	{
		if ($dbms == 'mysql')
		{
			$sql = "SELECT *
				FROM " . EMPLOYED_TABLE . "
				WHERE (" . $theTime . " - last_paid) > job_length";

			if ( !($result = $db->sql_query($sql)) )
			{
				message_die(GENERAL_ERROR, sprintf($lang['jobs_error_selecting'], 'employed' . mysql_error()), '', __LINE__, __FILE__, $sql);
			}

			$sql_count = $db->sql_numrows($result);
			if ($sql_count > 0)
			{
				for ($i = 0; $i < $sql_count; $i++)
				{
					if (!( $row = $db->sql_fetchrow($result) ))
					{
						message_die(GENERAL_ERROR, sprintf($lang['jobs_error_selecting'], 'jobs/employed'), '', __LINE__, __FILE__, $sql);
					}
					$sql = "UPDATE " . EMPLOYED_TABLE . "
						SET last_paid = '$theTime'
						WHERE id = '{$row['id']}'";

					if ( !($db->sql_query($sql)) )
					{
						message_die(GENERAL_ERROR, sprintf($lang['jobs_error_updating'], 'employed'), '', __LINE__, __FILE__, $sql);
					}

					$sql = "UPDATE " . USERS_TABLE . "
						SET user_cash = (user_cash + {$row['job_pay']})
						WHERE user_id = '{$row['user_id']}'";

					if ( !($db->sql_query($sql)) )
					{
						message_die(GENERAL_ERROR, sprintf($lang['jobs_error_updating'], 'users'), '', __LINE__, __FILE__, $sql);
					}
				}
			}
		}
		else
		{
			$sql = "UPDATE
				" . EMPLOYED_TABLE . " as t1, " . USERS_TABLE . " as t2
				SET t1.last_paid = '$theTime',
					t2.user_cash = t2.user_cash + t1.job_pay
				WHERE t1.job_length < (" . $theTime . " - t1.last_paid)
					AND t2.user_id = t1.user_id";

			if ( !($db->sql_query($sql)) )
			{
				message_die(GENERAL_ERROR, sprintf($lang['jobs_error_updating'], 'users/employed'), '', __LINE__, __FILE__, $sql);
			}
		}
	}
	else
	{
		if ($dbms == 'mysql')
		{
			$sql = "SELECT *
				FROM " . EMPLOYED_TABLE . "
				WHERE (" . $theTime . " - last_paid) > job_length
					AND user_id = '{$userdata['user_id']}'";

			if ( !($result = $db->sql_query($sql)) )
			{
				message_die(GENERAL_ERROR, sprintf($lang['jobs_error_selecting'], 'employed'), '', __LINE__, __FILE__, $sql);
			}

			$sql_count = $db->sql_numrows($result);
			if ($sql_count > 0)
			{
				for ($i = 0; $i < $sql_count; $i++)
				{
					if (!( $row = $db->sql_fetchrow($result) ))
					{
						message_die(GENERAL_ERROR, sprintf($lang['jobs_error_selecting'], 'jobs/employed'), '', __LINE__, __FILE__, $sql);
					}
					$sql = "UPDATE " . EMPLOYED_TABLE . "
						SET last_paid = '$theTime'
						WHERE id = '{$row['id']}'";

					if ( !($db->sql_query($sql)) )
					{
						message_die(GENERAL_ERROR, sprintf($lang['jobs_error_updating'], 'employed'), '', __LINE__, __FILE__, $sql);
					}

					$sql = "UPDATE " . USERS_TABLE . "
						SET user_cash = (user_cash + {$row['job_pay']})
						WHERE user_id = '{$row['user_id']}'";

					if ( !($db->sql_query($sql)) )
					{
						message_die(GENERAL_ERROR, sprintf($lang['jobs_error_updating'], 'users'), '', __LINE__, __FILE__, $sql);
					}
				}
			}
		}
		else
		{
			$sql = "UPDATE
				" . EMPLOYED_TABLE . " as t1, " . USERS_TABLE . " as t2
				SET t1.last_paid = '$theTime',
					t2.user_cash = t2.user_cash + t1.job_pay
				WHERE (" . $theTime . " - t1.last_paid) > t1.job_length
					AND t1.user_id = '{$userdata['user_id']}'
					AND t2.user_id = t1.user_id";
			if ( !($db->sql_query($sql)) )
			{
				message_die(GENERAL_ERROR, sprintf($lang['jobs_error_updating'], 'users/employed'), '', __LINE__, __FILE__, $sql);
			}
		}
	}

	// Update user job checks (allow only once every 2 hours)
	$sql = "UPDATE " . USERS_TABLE . "
		SET user_jobs = '$theTime'
		Where user_id = '{$userdata['user_id']}'";
	if ( !($db->sql_query($sql)) )
	{
		message_die(GENERAL_ERROR, sprintf($lang['jobs_error_updating'], 'users'), '', __LINE__, __FILE__, $sql);
	}

	return;
}

define('IN_PHPBB', true);
$phpbb_root_path = './';
include($phpbb_root_path . 'extension.inc');
include($phpbb_root_path . 'common.' . $phpEx);

//
// Start session management
//
$userdata = session_pagestart($user_ip, PAGE_FAQ);
init_userprefs($userdata);
//
// End session management
//
// Start register variables
//

if ( isset($HTTP_GET_VARS['action']) || isset($HTTP_POST_VARS['action']) ) { $action = ( isset($HTTP_POST_VARS['action']) ) ? $HTTP_POST_VARS['action'] : $HTTP_GET_VARS['action']; }
else { $action = ''; }

$user_id = ( isset($HTTP_GET_VARS['user_id']) ) ? intval($HTTP_GET_VARS['user_id']) : 0;

//
// End register variables
//
// Start page functions
//
if ( !(function_exists(has_item)) )
{
	function has_item($item, $type = 0)
	{
		global $user_item_array, $user_itemid_array, $user_itemid2_array, $db, $userdata;

		if ( empty($user_item_array) )
		{
			$sql = "SELECT *
				FROM " . USER_ITEMS_TABLE . "
				WHERE user_id = '{$userdata['user_id']}'";
		
			if ( !( $result = $db->sql_query($sql) ) ) { message_die(GENERAL_MESSAGE, 'Error fetching user items in function!'); }
			if ( $count = $db->sql_numrows($result) )
			{
				$user_item_array = array();
				$user_itemid_array = array();
				$user_itemid2_array = array();

				for ( $i = 0; $i < $count; $i++)
				{
					$row = $db->sql_fetchrow($result);

					$user_item_array[] = $row['item_name'];
					$user_itemid_array[] = $row['id'];
					$user_itemid2_array[] = $row['item_id'];
				}
			}
		}

		if ( is_numeric($item) && ( ( @in_array($item, $user_itemid_array) && ($type == 1) ) || @in_array($item, $user_itemid2_array) ) ) { return true; }
		elseif ( @in_array($item, $user_item_array) ) { return true; }
		else { return false; }
	}
}

function requirements($requires)
{
	global $lang;
	global $userdata;
	$currency_field = 'user_cash';
	$currency = $board_config['cash_name'];

	if (strlen($requires) > 3)
	{
		$rms = explode(";", $requires);
		for ($i = 0; $i < count($rms); $i++)
		{
			$rms[$i] = trim($rms[$i]);

			if ( (substr($rms[$i], 0, 5) == 'admin') || (substr($rms[$i], 0, 3) == 'mod') )
			{
				if (substr($rms[$i], 0, 5) == 'admin' && $userdata['user_level'] != 1)
				{
					$error = 1;
					$msg .= $lang['jobs_requires_admin'] . '<br /><br />';
				}
				elseif (substr($rms[$i], 0, 3) == 'mod' && $userdata['user_level'] == 0)
				{
					$error = 1;
					$msg .= $lang['jobs_requires_mod'] . '<br /><br />';
				}
			}
			elseif ( substr($rms[$i], 0, 3) == 'sex' )
			{
				if ( substr($rms[$i], 3, 1) == '=' )
				{
					if ( substr($rms[$i], 4, 4) == 'male' && $userdata['user_gender'] != '1' )
					{
						$error = 1;
						$msg .= $lang['jobs_requires_male'] . '<br /><br />';
					}
					elseif ( substr($rms[$i], 4, 6) == 'female' && $userdata['user_gender'] != '2' )
					{
						$error = 1;
						$msg .= $lang['jobs_requires_female'] . '<br /><br />';
					}
				}
				elseif ( substr($rms[$i], 3, 1) == '!' )
				{
					if ( substr($rms[$i], 4, 4) == 'male' && $userdata['user_gender'] == '1' )
					{
						$error = 1;
						$msg .= $lang['jobs_requires_nmale'] . '<br /><br />';
					}
					elseif ( substr($rms[$i], 4, 6) == 'female' && $userdata['user_gender'] == '2' )
					{
						$error = 1;
						$msg .= $lang['jobs_requires_nfemale'] . '<br /><br />';
					}
				}
			}
			elseif ( substr($rms[$i], 0, 3) == 'gil' )
			{
				if ( (substr($rms[$i], 3, 1) == '=') && ($userdata[$currency_field] != substr($rms[$i], 4)) )
				{
					$error = 1;
					$msg .= sprintf($lang['jobs_requires_gil'], substr($rms[$i], 4), $currency) . '<br /><br />';
				}
				elseif ( (substr($rms[$i], 3, 1) == '!') && ($userdata[$currency_field] == substr($rms[$i], 4)) )
				{
					$error = 1;
					$msg .= sprintf($lang['jobs_requires_ngil'], substr($rms[$i], 4), $currency) . '<br /><br />';
				}
				elseif ( (substr($rms[$i], 3, 1) == '>') && ($userdata[$currency_field] <= substr($rms[$i], 4)) )
				{
					$error = 1;
					$msg .= sprintf($lang['jobs_requires_mgil'], substr($rms[$i], 4), $currency) . '<br /><br />';
				}
				elseif ( (substr($rms[$i], 3, 1) == '<') && ($userdata[$currency_field] >= substr($rms[$i], 4)) )
				{
					$error = 1;
					$msg .= sprintf($lang['jobs_requires_lgil'], substr($rms[$i], 4), $currency) . '<br /><br />';
				}
			}
			elseif ( substr($rms[$i], 0, 5) == 'posts' )
			{
				if ( (substr($rms[$i], 5, 1) == '=') && ($userdata['user_posts'] != substr($rms[$i], 6)) )
				{
					$error = 1;
					$msg .= sprintf($lang['jobs_requires_posts'], substr($rms[$i], 6)) . '<br /><br />';
				}
				elseif ( (substr($rms[$i], 5, 1) == '!') && ($userdata['user_posts'] == substr($rms[$i], 6)) )
				{
					$error = 1;
					$msg .= sprintf($lang['jobs_requires_nposts'], substr($rms[$i], 6)) . '<br /><br />';
				}
				elseif ( (substr($rms[$i], 5, 1) == '>') && ($userdata['user_posts'] <= substr($rms[$i], 6)) )
				{
					$error = 1;
					$msg .= sprintf($lang['jobs_requires_mposts'], substr($rms[$i], 6)) . '<br /><br />';
				}
				elseif ( (substr($rms[$i], 5, 1) == '<') && ($userdata['user_posts'] >= substr($rms[$i], 6)) )
				{
					$error = 1;
					$msg .= sprintf($lang['jobs_requires_lposts'], substr($rms[$i], 6)) . '<br /><br />';
				}
			}
			elseif ( substr($rms[$i], 0, 4) == 'item')
			{
				if ( (substr($rms[$i], 4, 1) == '=') &&
					( ( !defined('USER_ITEMS_TABLE') && substr_count($userdata['user_items'],"�" . substr($rms[$i], 5) . "�") < 1 ) 
						|| !(has_item(substr($rms[$i], 5))) )
				)
				{
					$error = 1;
					$msg .= sprintf($lang['jobs_requires_item'], substr($rms[$i], 5)) . '<br /><br />';
				}

				elseif ( (substr($rms[$i], 4, 1) == '!') && 
					( ( !(defined('USER_ITEMS_TABLE')) && (substr_count($userdata['user_items'],"�" . substr($rms[$i], 5) . "�") > 0) ) 
					|| has_item(substr($rms[$i], 5)) )
				)
				{
					$error = 1;
					$msg .= sprintf($lang['jobs_requires_nitem'], substr($rms[$i], 5)) . '<br /><br />';
				}
			}
		}
	}
	if ($error) { return $msg; }
	else { return 0; }
}
//
// End page functions
//


if ( !$userdata['session_logged_in'] )
{
	$redirect = "jobs.$phpEx";
	$redirect .= ( isset($user_id) ) ? '&user_id=' . $user_id : '';
	header('Location: ' . append_sid("login.$phpEx?redirect=$redirect", true));
}


if ( empty($action) )
{
	if ( $board_config['jobs_index_body'] )
	{
		$template->set_filenames(array(
			'body' => 'jobs_extended_body.tpl')
		);
	}
	else
	{
		$template->set_filenames(array(
			'body' => 'jobs_body.tpl')
		);
	}

	$sql = "select * 
		FROM " . EMPLOYED_TABLE . "
		WHERE user_id = '{$userdata['user_id']}'";
	if ( !($result = $db->sql_query($sql)) )
	{
		message_die(GENERAL_ERROR, sprintf($lang['jobs_error_selecting'], 'employed'), '', __LINE__, __FILE__, $sql);
	}
	$sql_count = $db->sql_numrows($result);

	if ($sql_count > 0)
	{
		for ($i = 0; $i < $sql_count; $i++)
		{
			if (!( $row = $db->sql_fetchrow($result) ))
			{
				message_die(GENERAL_ERROR, sprintf($lang['jobs_error_selecting'], 'jobs'), '', __LINE__, __FILE__, $sql);
			}

			$row_num = ( $i % 2 ) ? 'row1' : 'row2';
			$last_paid = ( $row['job_started'] == $row['last_paid'] ) ? 'Never' : create_date($board_config['default_dateformat'], $row['last_paid'], $board_config['board_timezone']);

			$template->assign_block_vars('listrow', array(
				'S_MODE_ACTION' => append_sid('jobs.' . $phpEx . '?action=quit&job=' . $row['job_name']),
				'ROW' => $row_num,
				'JOB_PAY' => $row['job_pay'],
				'JOB_LENGTH' => duration($row['job_length']),
				'JOB_LAST_PAID' => $last_paid,
				'JOB_STARTED' => create_date($board_config['default_dateformat'], $row['job_started'], $board_config['board_timezone']),
				'JOB_NAME' => ucwords($row['job_name']))
			);
			$current_jobs .= (empty($current_jobs)) ? "'" . addslashes($row['job_name']) . "'" : ", '" . addslashes($row['job_name']) . "'";
		}
		$template->assign_block_vars('switch_has_job', array());
	}
	else
	{
		$template->assign_block_vars('switch_has_no_job', array());
	}

	if (strlen($current_jobs) < 3) { $current_jobs = "'null'"; }

	if ($sql_count < $board_config['jobs_limit'])
	{
		$sql = "select * 
			FROM " . JOBS_TABLE . "
			WHERE type = 'public'
			AND name NOT IN ($current_jobs)";
		if ( !($result = $db->sql_query($sql)) )
		{
			message_die(GENERAL_ERROR, sprintf($lang['jobs_error_selecting'], 'jobs'), '', __LINE__, __FILE__, $sql);
		}
		$sql_count = $db->sql_numrows($result);

		for ($i = 0; $i < $sql_count; $i++)
		{
			if (!( $row = $db->sql_fetchrow($result) ))
			{
				message_die(GENERAL_ERROR, sprintf($lang['jobs_error_selecting'], 'jobs'), '', __LINE__, __FILE__, $sql);
			}

			$sql = "select * 
				FROM " . EMPLOYED_TABLE . "
				WHERE job_name='{$row['name']}'";
			if ( !($result2 = $db->sql_query($sql)) )
			{
				message_die(GENERAL_ERROR, sprintf($lang['jobs_error_selecting'], 'jobs'), '', __LINE__, __FILE__, $sql);
			}
			$sql_count2 = $db->sql_numrows($result2);

			$row_num = ( $i % 2 ) ? 'row1' : 'row2';

			if ( $sql_count2 < $row['positions'] )
			{
				$template->assign_block_vars('listrow2', array(
					'S_MODE_ACTION' => append_sid('jobs.' . $phpEx . '?action=start&job=' . $row['id']),
					'ROW' => $row_num,
					'JOB_ID' => $row['id'],
					'JOB_PAY' => $row['pay'],
					'JOB_PAY_TIME' => duration($row['payouttime']),
					'JOB_POSITIONS' => $row['positions'],
					'JOB_LEFT' => ($row['positions'] - $sql_count2),
					'JOB_NAME' => $row['name'])

				);
				$can_get_job = 1;
			}
		}
		if ($can_get_job)
		{
			$template->assign_block_vars('switch_can_get_job', array());
		}
		else
		{
			$template->assign_block_vars('switch_cant_get_job', array());
		}
	}
	else
	{
		$template->assign_block_vars('switch_cant_get_job', array());
	}

	$sql = "select count(DISTINCT t2.user_id) as total_employed, count(t2.id) as total_jobs_taken, sum(t1.positions) as total_positions, count(t1.name) as total_jobs 
		FROM " . JOBS_TABLE . " as t1, " . EMPLOYED_TABLE . " as t2
		GROUP BY t2.id";
	if ( !($result = $db->sql_query($sql)) )
	{
		message_die(GENERAL_ERROR, sprintf($lang['jobs_error_selecting'], 'jobs/employed'), '', __LINE__, __FILE__, $sql);
	}
	$sql_count = $db->sql_numrows($result);
	if ($sql_count > 0)
	{
		if (!( $row = $db->sql_fetchrow($result) ))
		{
			message_die(GENERAL_ERROR, sprintf($lang['jobs_error_selecting'], 'jobs/employed'), '', __LINE__, __FILE__, $sql);
		}
	}
	else
	{
		$sql = "select sum(positions) as total_positions, count(name) as total_jobs 
			FROM " . JOBS_TABLE . "
			GROUP by id";
		if ( !($result = $db->sql_query($sql)) )
		{
			message_die(GENERAL_ERROR, sprintf($lang['jobs_error_selecting'], 'jobs/employed'), '', __LINE__, __FILE__, $sql);
		}
		$sql_count = $db->sql_numrows($result);
		if ($sql_count > 0)
		{
			if (!( $row = $db->sql_fetchrow($result) ))
			{
				message_die(GENERAL_ERROR, sprintf($lang['jobs_error_selecting'], 'jobs/employed'), '', __LINE__, __FILE__, $sql);
			}
		}
		else
		{
			$row['total_positions'] = 0;
			$row['total_jobs'] = 0;
		}
	}

	$total_jobs = $row['total_jobs'];
	$total_employed = ($row['total_employed']) ? $row['total_employed'] : 0;
	$total_positions = $row['total_positions'];

	$sql = "select count(id) as total_jobs_taken
		FROM " . EMPLOYED_TABLE;
	if ( !($result = $db->sql_query($sql)) )
	{
		message_die(GENERAL_ERROR, sprintf($lang['jobs_error_selecting'], 'jobs/employed'), '', __LINE__, __FILE__, $sql);
	}
	$sql_count = $db->sql_numrows($result);
	if ($sql_count > 0)
	{
		if (!( $row = $db->sql_fetchrow($result) ))
		{
			message_die(GENERAL_ERROR, sprintf($lang['jobs_error_selecting'], 'jobs/employed'), '', __LINE__, __FILE__, $sql);
		}
	}
	else
	{
		$row['total_jobs_taken'] = 0;
	}
	$total_jobs_taken = $row['total_jobs_taken'];
	$total_free_jobs = $total_positions - $total_jobs_taken;

	$location = ' -> <a href="'.append_sid("jobs." . $phpEx).'" class="nav">' . $lang['jobs'] . '</a>';
	$page_title = $lang['jobs_information'];

	$template->assign_vars(array(
		'S_MODE_ACTION' => append_sid("jobs." . $phpEx),

		'LOCATION' => $location,
		'TOTAL_JOBS' => $total_jobs,
		'TOTAL_JOB_POSITIONS' => $total_positions,
		'TOTAL_EMPLOYED' => $total_employed,
		'TOTAL_JOBS_TAKEN' => $total_jobs_taken,
		'TOTAL_FREE_JOBS' => $total_free_jobs,

		'L_TITLE' => $lang['jobs_information'],
		'L_CURRENT_JOBS' => $lang['jobs_current'],
		'L_AVAILABLE_JOBS' => $lang['jobs_available'],
		'L_YOURE_UNEMPLOYED' => $lang['jobs_youre_unemployed'],
		'L_CANT_BE_EMPLOYED' => $lang['jobs_cant_be_employed'],

		'L_B_ACCEPT' => $lang['jobs_button_accept'],
		'L_B_QUIT' => $lang['jobs_button_quit'],
		'L_IMP' => 'Jobs Modification: Copyright � 2004, 2006 <a href="http://www.zarath.com" class="navsmall">Zarath Technologies</a>.',
		'L_JOB' => $lang['jobs_job'],
		'L_PAY' => $lang['jobs_pay'],
		'L_PAY_LENGTH' => $lang['jobs_pay_length'],
		'L_STARTED' => $lang['jobs_started'],
		'L_LAST_PAID' => $lang['jobs_last_paid'],
		'L_POSITIONS' => $lang['jobs_positions'],

		'L_REMAINING' => $lang['jobs_remaining_positions'],
		'L_TAKEN' => $lang['jobs_taken_positions'],
		'L_EMPLOYED' => $lang['jobs_total_employed'],
		'L_TOTAL_POSITIONS' => $lang['jobs_total_positions'],
		'L_TOTAL_JOBS' => $lang['jobs_total_jobs']
	));
	$template->assign_block_vars('', array());
}

elseif ($action == 'start')
{
	if ( isset($HTTP_GET_VARS['job']) || isset($HTTP_POST_VARS['job']) ) { $job = ( isset($HTTP_POST_VARS['job']) ) ? $HTTP_POST_VARS['job'] : $HTTP_GET_VARS['job']; }
	else { message_die(GENERAL_MESSAGE, sprintf($lang['jobs_error_variable'], 'job')); }

	$sql = "SELECT * 
		FROM " . JOBS_TABLE . "
		WHERE id='$job'";
	if ( !($result = $db->sql_query($sql)) )
	{
		message_die(GENERAL_ERROR, sprintf($lang['jobs_error_selecting'], 'jobs'), '', __LINE__, __FILE__, $sql);
	}
	$sql_count = $db->sql_numrows($result);

	if (!($sql_count)) { message_die(GENERAL_MESSAGE, $lang['jobs_not_exist']); }

	if (!( $row = $db->sql_fetchrow($result) ))
	{
		message_die(GENERAL_ERROR, sprintf($lang['jobs_error_selecting'], 'jobs'), '', __LINE__, __FILE__, $sql);
	}

	$sql = "SELECT * 
		FROM " . EMPLOYED_TABLE . "
		WHERE user_id='{$userdata['user_id']}'";
	if ( !($result = $db->sql_query($sql)) )
	{
		message_die(GENERAL_ERROR, sprintf($lang['jobs_error_selecting'], 'employed'), '', __LINE__, __FILE__, $sql);
	}
	$sql_count = $db->sql_numrows($result);

	if ( $sql_count >= $board_config['jobs_limit'] ) { message_die(GENERAL_MESSAGE, $lang['jobs_too_many']); }
	if ($row['type'] != 'public') { message_die(GENERAL_MESSAGE, $lang['jobs_not_public']); }

	$sql = "SELECT * 
		FROM " . EMPLOYED_TABLE . "
		WHERE job_name = '{$row['name']}'";
	if ( !($result = $db->sql_query($sql)) )
	{
		message_die(GENERAL_ERROR, sprintf($lang['jobs_error_selecting'], 'employment'), '', __LINE__, __FILE__, $sql);
	}
	$sql_count = $db->sql_numrows($result);

	if ($sql_count >= $row['positions']) { message_die(GENERAL_MESSAGE, $lang['jobs_no_positions']); }

	//requirements check
	if (strlen($row['requires']) > 3)
	{
		$failed = requirements($row['requires']);
		if ( $failed )
		{
			message_die(GENERAL_MESSAGE, $failed); 
		}
	}	

	$sql = "SELECT * 
		FROM " . EMPLOYED_TABLE . " 
		WHERE job_name='{$row['name']}' and user_id='{$userdata['user_id']}'";
	if ( !($result = $db->sql_query($sql)) )
	{
		message_die(GENERAL_ERROR, $lang['jobs_error_temployed'], '', __LINE__, __FILE__, $sql);
	}
	$sql_count = $db->sql_numrows($result);

	if ($sql_count > 0) { message_die(GENERAL_MESSAGE, $lang['jobs_already_employed']); }

	$theTime = time();
	$sql = "INSERT INTO " . EMPLOYED_TABLE . " 
		(user_id, job_name, job_pay, job_length, last_paid, job_started)
		values('{$userdata['user_id']}', '{$row['name']}', '{$row['pay']}', '{$row['payouttime']}', '$theTime', '$theTime')";
	if ( !($db->sql_query($sql)) )
	{
		message_die(GENERAL_ERROR, sprintf($lang['jobs_error_inserting'], 'employed'), '', __LINE__, __FILE__, $sql);
	}

	message_die(GENERAL_MESSAGE, sprintf($lang['jobs_now_employed'], $row['name']));
}

elseif ($action == 'quit')
{
	if ( isset($HTTP_GET_VARS['job']) || isset($HTTP_POST_VARS['job']) ) { $job = ( isset($HTTP_POST_VARS['job']) ) ? addslashes(stripslashes($HTTP_POST_VARS['job'])) : addslashes(stripslashes($HTTP_GET_VARS['job'])); }
	else { message_die(GENERAL_MESSAGE, "Could not job name variable!"); }

	$sql = "SELECT * 
		FROM " . EMPLOYED_TABLE . " 
		WHERE job_name='$job' and user_id='{$userdata['user_id']}'";
	if ( !($result = $db->sql_query($sql)) )
	{
		message_die(GENERAL_ERROR, sprintf($lang['jobs_error_selecting'], 'employed'), '', __LINE__, __FILE__, $sql);
	}
	$sql_count = $db->sql_numrows($result);

	if ($sql_count < 1) { message_die(GENERAL_MESSAGE, $lang['jobs_not_employed']); }

	$sql = "DELETE
		FROM " . EMPLOYED_TABLE . "
		WHERE job_name='$job' and user_id='{$userdata['user_id']}'";
	if ( !($db->sql_query($sql)) )
	{
		message_die(GENERAL_ERROR, sprintf($lang['jobs_error_updating'], 'employed'), '', __LINE__, __FILE__, $sql);
	}

	message_die(GENERAL_MESSAGE, sprintf($lang['jobs_quit'], $job));
}

else { message_die(GENERAL_MESSAGE, "Invalid Action!"); }

//
// Start output of page
//
include($phpbb_root_path . 'includes/page_header.' . $phpEx);

//
// Generate the page
//
$template->pparse('body');

include($phpbb_root_path . 'includes/page_tail.' . $phpEx);

?>