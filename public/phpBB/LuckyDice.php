<?php
/***************************************************************************
 *                                LuckyDice.php
 *                            -------------------
 *   Version              : 1.4.0
 *   began                : September 08, 2003
 *   email                : tom.wuyts@a-d-e.net
 *   forums               : http://www.wuyts.org/tom/
 *
 ***************************************************************************/

/***************************************************************************
 *
 *   copyright (C) 2003-2006  RC-Technologies
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License
 *   as published by the Free Software Foundation; either version 2
 *   of the License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   http://www.gnu.org/copyleft/gpl.html
 *
 ***************************************************************************/

define('IN_PHPBB', true);
$phpbb_root_path = './';
include($phpbb_root_path . 'extension.inc');
include($phpbb_root_path . 'common.' . $phpEx);
include($phpbb_root_path . 'language/lang_' . $board_config['default_lang'] . '/lang_luckydice.' . $phpEx);

//
// Start session management
//
$userdata = session_pagestart($user_ip, PAGE_INDEX);
init_userprefs($userdata);
//
// End session management
//

$user_id = $userdata['user_id'];
$username = $userdata['username'];

if ( !$userdata['session_logged_in'] )
{
	$redirect = "LuckyDice.$phpEx";
	header('Location: ' . append_sid("login.$phpEx?redirect=$redirect", true));
}

$bet = ( isset($_POST['bet']) ) ? $_POST['bet'] : '0';
$played = $board_config['luckydice_played'];
$taken = $board_config['luckydice_taken'];
$given = $board_config['luckydice_given'];

// check if game is disabled, ifnot then continue:
if( $board_config['luckydice_enable'] == '0' )
{
  message_die(GENERAL_MESSAGE, ''.$lang['game_disabled'].'');
}
// START GAME
$page_title = 'LuckyDice';

$mainsub = $lang['game_explanation1'] . $lang['game_explanation2'] . $lang['game_explanation3'] . $lang['game_explanation4'];

$game = '<SCRIPT LANGUAGE="JavaScript">'
	.'function DisabSub(frm)'
	.'{'
	.' var btn = frm.play;'
	.'btn.disabled = true;'
	.'return true;'
	.'}' 
	.'</SCRIPT> ';


//version 1.4.0::
//-- max bets a day --
$stop = false;
//check if this feature is enabled: (the config value has to be bigger than 0 (zero)
if( $board_config['luckydice_betsaday'] > 0 )
{
	//this function is enabled::
	//verify the user his stats (reset counter if needed)
	
	$userBets = $userdata['luckydice_bets'];
	
	//first the timer:
	$timerstarted = $userdata['luckydice_time'];
	$currenttime = time();							//--> these are UNIX timestamps !!
	$diffTime = 86400;	//24 hours in seconds.
	if( ($currenttime - $timerstarted) >= $diffTime )
	{
		//counter reset --> update db and let him play:
		$sql = "UPDATE " . USERS_TABLE . " SET luckydice_time='0' WHERE user_id = ".$user_id;
		if(! ($db->sql_query($sql)) ) 
		{ 
			message_die(GENERAL_ERROR, 'Fatal Error Resetting Counter!', '', __LINE__, __FILE__ ,$sql ); 
		}
		
		$sql = "UPDATE " . USERS_TABLE . " SET luckydice_bets='0' WHERE user_id = ".$user_id;
		if(! ($db->sql_query($sql)) ) 
		{ 
			message_die(GENERAL_ERROR, 'Fatal Error Resetting Amount of Bets for the Day!', '', __LINE__, __FILE__ ,$sql ); 
		}
		$userBets = 0;
	}
	
	if( $userBets >= $board_config['luckydice_betsaday'] )
	{
		//stop user from playing --> show a message.
		$game = sprintf($lang['toomanyplays'], $board_config['luckydice_betsaday']);
		$stop = true;
	}
}


if(! $stop ) //--> user has played too many times already, just show the message!
{
//---------------


if ( $bet == '0') 
{
	$main = $lang['home1'] . $lang['home2'] . $lang['home3'];

	$game .= '<form method="POST" onSubmit="return DisabSub(this);">';
	$game .= '<input type="text" name="bet" class="liteoption">';
	$game .= '<input type="submit" class="liteoption" value="Play" name="play" ></form><span class="gensmall"> '.$lang['maxbetis'].''. $board_config['luckydice_maxbet'] .' '. $board_config['cash_name'] .'</span>';
	$game .= '<br>'.$lang['youhave'] . $userdata['user_cash'] .' '. Shens.'.';
	$game .= '<br>
	<span class="gensmall">'. $lang['played'] . $played . $lang['_times']	.'<br>'	. $lang['taken'] . $taken .' '. $board_config['cash_name'] .' '	. $lang['given'] . $given .' '. $board_config['cash_name'] .'</span>';
} 
else 
{
	//version 1.4.0::
	//-- max bets a day --
	//check if this feature is enabled: (the config value has to be bigger than 0 (zero)
	if( $board_config['luckydice_betsaday'] > 0 )
	{
		//update the user's amount of bets for the day (update the counter if needed) ::
		$newTime = $userdata['luckydice_time'];
		if( $userdata['luckydice_time'] == 0 )
		{
			$newTime = time();
		}
		
		$newBets = $userdata['luckydice_bets'] + 1;
		$sql = "UPDATE " . USERS_TABLE . " SET luckydice_bets='".$newBets."' 
											, luckydice_time='".$newTime."'
						WHERE user_id = ".$user_id;
		if(! ($db->sql_query($sql)) ) 
		{ 
			message_die(GENERAL_ERROR, 'Fatal Error Increasing Counter!', '', __LINE__, __FILE__ ,$sql ); 
		}
	}
	//-----------------------
	
	if(! is_numeric($bet) ) 
	{
		message_die(GENERAL_MESSAGE, ''.$lang['error_values'].'');
	}
	if( $bet < '0')
	{
	  message_die(GENERAL_MESSAGE, ''.$lang['zero_error_values'].'');
	}
	if( $bet > $userdata['user_cash'] ) 
	{
		message_die(GENERAL_MESSAGE, ''.$lang['error_money'].'');
	}
	if( $bet > $board_config['luckydice_maxbet'] )
	{
	 	message_die(GENERAL_MESSAGE, ''.$lang['error_maxbet'].'');
	}
	
	// where are the dices ??
	$dice1_place = rand(1,6);
	$dice2_place = rand(1,6);
	// what's the top-number ??
	$dice1_num = rand(1,6);
	$dice2_num = rand(1,6);
	// what are the bonusses ??
	$bunus_hi_lo = rand(1,2);
	if( $bonus_hi_lo == 1 )
	{
	 	$bonus1 = rand(1,3);
		$bonus2 = rand(4,6);
	}
	else
	{
	 	$bonus1 = rand(4,6);
		$bonus2 = rand(1,3);
	}
	
	switch($bonus1){
		case 1: 
			$bonus1_pic = 'one.gif';
			break;
		case 2: 
			$bonus1_pic = 'two.gif';
			break;
		case 3: 
			$bonus1_pic = 'three.gif';
			break;
		case 4: 
			$bonus1_pic = 'four.gif';
			break;
		case 5: 
			$bonus1_pic = 'five.gif';
			break;
		case 6: 
			$bonus1_pic = 'six.gif';
			break;
	}
	switch($bonus2){
		case 1: 
			$bonus2_pic = 'one.gif';
			break;
		case 2: 
			$bonus2_pic = 'two.gif';
			break;
		case 3: 
			$bonus2_pic = 'three.gif';
			break;
		case 4: 
			$bonus2_pic = 'four.gif';
			break;
		case 5: 
			$bonus2_pic = 'five.gif';
			break;
		case 6: 
			$bonus2_pic = 'six.gif';
			break;
	}

	// all possibilities:
		if( $dice1_place == $dice2_place )
		{
		 	$win_count = ( isset($win_count) ) ? ($win_count + 1) : 1;
		}
		if( $dice1_num == $dice2_num )
		{
		 	$win_count = ( isset($win_count) ) ? ($win_count + 1) : 1;
		}
		if( $dice1_place == $dice1_num )
		{
		 	$win_count = ( isset($win_count) ) ? ($win_count + 1) : 1;
		}
		if( $dice2_place == $dice2_num )
		{
		 	$win_count = ( isset($win_count) ) ? ($win_count + 1) : 1;
		}
		if( $dice1_place == $dice2_num )
		{
		 	$win_count = ( isset($win_count) ) ? ($win_count + 1) : 1;
		}
		if( $dice2_place == $dice1_num )
		{
		 	$win_count = ( isset($win_count) ) ? ($win_count + 1) : 1;
		}
	
	//
	// the possibilities:

	$game .= '<a href="'. append_sid("LuckyDice.".$phpEx) .'" class="nav">'.$lang['again'].'</a><br><br>';
	$game .= '<form method="POST" onSubmit="return DisabSub(this);"><input class="liteoption" type="text" name="bet" value="'.$bet.'">';
	$game .= '<input type="submit" class="liteoption" value="Play" name="play"></form><span class="gensmall"> '.$lang['maxbetis'].''. $board_config['luckydice_maxbet'] .' '. $board_config['cash_name'] .'</span>';
	switch ($win_count)
	{
		case 6:	// the perfect, bet X5
	 	$win_numb = ( $dice1_place );
		if( ($win_numb != $bonus1) || ($win_numb != $bonus2) )
		{
		 	$winnings = floor( $bet * $board_config['luckydice_winperfect'] );
			$l_main .= $lang['winperfect'] . $winnings .' '. $board_config['cash_name'].'.';
			$money_str = ' + '.$winnings;
		} 
		else if( ($win_numb == $bonus1) )
		{
		 	$winnings = floor( $bet * $board_config['luckydice_winperfect'] * $board_config['luckydice_bonussmall'] );
			$l_main .= $lang['winbonussmall'] . $winnings .' '. $board_config['cash_name'].'.';
			$money_str = ' + '.$winnings;
		}
		else if( ($win_numb == $bonus2) )
		{
		 	$winnings = floor( $bet * $board_config['luckydice_winperfect'] * $board_config['luckydice_bonusbig'] );
			$l_main .= $lang['winbonusbig'] . $winnings .' '. $board_config['cash_name'].'s';
			$money_str = ' + '.$winnings;
		}
			break;
	case 3:// a triple, bet X3
	 	$winnings = floor( $bet * $board_config['luckydice_wintriple'] );
		$l_main .= $lang['wintriple'] . $winnings .' '. $board_config['cash_name'].'.';
		$money_str = ' + '.$winnings;
		break;
	// 2 pairs, bet X2
	  case 2: 
		$winnings .= floor( $bet * $board_config['luckydice_windouble'] );
		$l_main = $lang['windouble'] . $winnings .' '. $board_config['cash_name'].'.';
		$money_str = ' + '.$winnings;
		break;
	case 1:	// no winning, no lose (1 pair)
			$l_main .= $lang['winpair'];
			$winnings = $bet;
			break;
	default:// if the user lost...
	 	$l_main = $lang['lose'];
		$money_str = ' - '.$bet;
	break;
	} //switch end
	
	// show the javascript of the rolling dices:
  $main = "<!-- Keith Drakard (kif@irt.org) v0.10 on 9th September 98
	-->
	<SCRIPT LANGUAGE=\"JavaScript\"><!-- //
	
	  // global variables
	  var noof_dice= 4;
	  var rolled= new Array(noof_dice);
		var imgpath = 'images/lucky_dice/';
	
	  function roll(x) {
	    // roll the dice
	
	    if (x> 0) {
	      // carry on rolling:
	
	      x--;
	      for (d=0; d<noof_dice; d++) {
					// on last roll, show the right number:
					if( x == 0 ){
							// show dices there id.
	        		switch( d )
							{
							 	case 0 : rolled[d]= ".$dice1_place."; break;
								case 1 : rolled[d]= ".$dice1_num."; break;
								case 2 : rolled[d]= ".$dice2_place."; break;
								case 3 : rolled[d]= ".$dice2_num."; break;
							}
							// show the 'winnings'-text:
							Win_text.innerHTML = \"".$l_main."\";
					} else{
						rolled[d] = rand(6);
					}
	        document.images[\"die\"+d].src= dice[rolled[d]].src;
	      }
	      setTimeout('roll('+x+')', 200);
	
	    }
	  }
	
	
	  function preload() {
	    if (document.images) {
	      dice= new makeArray(7);
	        dice[0].src= imgpath+\"blank.gif\";
	        dice[1].src= imgpath+\"one.gif\";
	        dice[2].src= imgpath+\"two.gif\";
	        dice[3].src= imgpath+\"three.gif\";
	        dice[4].src= imgpath+\"four.gif\";
	        dice[5].src= imgpath+\"five.gif\";
	        dice[6].src= imgpath+\"six.gif\";
	      for (d=0; d<noof_dice; d++) {
	        document.images[\"die\"+d].src= dice[rand(6)].src;
	      }
	
	    } else {
	      alert(\"Sorry, this example needs to run on a browser which supports the image object.\");
	    }    
	  }
	
	  // The Central Randomizer 1.3 (C) 1997 by Paul Houle (houle@msc.cornell.edu)
	  // See:  http://www.msc.cornell.edu/~houle/javascript/randomizer.html
	  // NOTE:- modified by Kif to produce integers between 1-limit
	  rnd.today=new Date(); rnd.seed=rnd.today.getTime();
	  function rnd() {
	    rnd.seed = (rnd.seed*9301+49297) % 233280;
	    return rnd.seed/(233280.0);
	  }
	  function rand(limit) { return Math.ceil(rnd()*limit); }
	
	  // The following function was written by Martin Webb at http://www.irt.org/
	  function makeArray(n) {
	    this.length= n; for (i=0; i<n; i++) { this[i] = new Image(); }
	    return this;
	  }
	//--></SCRIPT>
	<table align=\"center\">
	<tr>

	<td>Minor Perfect Bonus: <img src=\"images/lucky_dice/".$bonus1_pic."\"></td><td>Major Perfect Bonus: <img src=\"images/lucky_dice/".$bonus2_pic."\"></td>
	</tr>
	</table>
	<table align=\"center\">
	<tr>
	<td><img src=\"images/lucky_dice/blank.gif\" name=\"die0\"></td>
	<td><img src=\"images/lucky_dice/blank.gif\" name=\"die1\"></td>
	<td><img src=\"images/lucky_dice/blank.gif\" name=\"die2\"></td>
	<td><img src=\"images/lucky_dice/blank.gif\" name=\"die3\"></td>
	</tr></table><center>
	<br><br><a id=\"Win_text\"></a></td></center>
	<script language=\"JavaScript\"><!--//
	  preload();
		roll( 20 ); // how many times must the dices roll??
	 //-->"
	 ."</script>";
	// end javascript

	
	// update the winnings/loses:
	if( isset($money_str) )
	{
		$newmoney = $userdata['user_cash'] . $money_str;
		$sql = 'UPDATE '. USERS_TABLE .' SET user_cash = '. $newmoney .' WHERE user_id = '. $userdata['user_id'];
		if(! ($result = $db->sql_query($sql)) ) { message_die(GENERAL_ERROR, 'Fatal Error Getting cash Information!', '', __LINE__, __FILE__ ,$sql); }
	}
	
	// update 'played', 'given', 'taken':
	$newplayed = $played + 1;
	$newgiven = (isset($winnings)) ? ($given + $winnings) : $given;
	$newtaken = $taken + $bet;
	
	$sql1 = 'UPDATE '. CONFIG_TABLE .' SET config_value = '. $newplayed .' WHERE config_name = "luckydice_played"';
	if(! $result = $db->sql_query($sql1) )
	{ message_die(GENERAL_ERROR, 'Fatal Error Updating luckydice_played!', '', __LINE__, __FILE__ ,$sql1); }
	
	$sql2 = 'UPDATE '. CONFIG_TABLE .' SET config_value = '. $newgiven .' WHERE config_name = "luckydice_given"';
	if(! $result = $db->sql_query($sql2) )
	{ message_die(GENERAL_ERROR, 'Fatal Error Updating luckydice_given!', '', __LINE__, __FILE__ ,$sql2); }
	
	$sql3 = 'UPDATE '. CONFIG_TABLE .' SET config_value = '. $newtaken .' WHERE config_name = "luckydice_taken"';
	if(! $result = $db->sql_query($sql3) )
	{ message_die(GENERAL_ERROR, 'Fatal Error Updating luckydice_taken!', '', __LINE__, __FILE__ ,$sql3); }
}

//version 1.4.0
} 		//end of if( $stop )
//-------------

// END GAME

$location = ' -> <a href="'.append_sid("LuckyDice.".$phpEx).'" class="nav">'.$page_title.'</a>';


//
// Start page output
//
include($phpbb_root_path . 'includes/page_header.' . $phpEx);

	$template->assign_vars(array(
		'LDLOCATION' => $location,
		'TITLE' => $page_title,
		'MAIN' => $main, 
		'GAME' => $game, 
		'SUB' => $mainsub, 
	));
	$template->assign_block_vars('', array());
		$template->set_filenames(array(
			'body' => 'luckydice_body.tpl')
	);


// Generate page
$template->pparse('body');

include($phpbb_root_path . 'includes/page_tail.' . $phpEx);

?>