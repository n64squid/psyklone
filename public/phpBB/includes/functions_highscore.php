<?php

/***************************************************************************>
 *                          functions_highscore.php
 *                           --------------------
 *   begin                : Sunday, January 29, 2006
 *   copyright            : (C) 2006 Painkiller
 *   email                : painkiller@runequake.com
 *
 *   $Id: functions_highscore.php,v 1.00 2006/01/29 11:00:04 Painkiller Exp $
 *
 *
 ***************************************************************************/

/***************************************************************************
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

function highscore_jump_box()
{
	global $board_config, $template, $lang, $db, $phpEx, $SID;

	$curr_m = date(m);
	$curr_y = date(Y);

	$highscore_start_mon = $board_config['highscore_start_mon'];
	$highscore_start_year = $board_config['highscore_start_year'];

	if ($highscore_start_mon > date(m))
	{
	    $limit = (12 - $highscore_start_mon) + Date(m);
	    $highscore_start_year++;
	}
	else
	{
    	$limit = date(m) - $highscore_start_mon;
	}
	$limit = $limit + ((date(Y) - $highscore_start_year));

	$input = "";

	$tmp_url = append_sid("arcade_highscore.$phpEx");
	$input .= "<table cellspacing=\"2\" cellpadding=\"2\" border=\"1\" align=\"center\">\n<tr><td class=\"row1\" align=\"center\"><div align=\"center\"><span class=\"nav\">".$lang['highscore_other_score'].":<br /><form action=\"".$tmp_url."\" method=\"post\">\n<select name=\"date\">\n";

	for ($i = "0"; $i <= $limit; $i++)
	{
	if ($i >= $curr_m)
	{
		$date = $i - $curr_m;
		if ($data == "0")
		{
			$highscore_date_y = "1";
		} else
		{
			$highscore_date_y = floor($date/12)+1;
		}
		$highscore_date_m = 12 - ($date - (($highscore_date_y - 1)*12));
		$highscore_date_y = $curr_y - $highscore_date_y;
	}
	else
	{
		$highscore_date_m = $curr_m - $i;
		$highscore_date_y = $curr_y;
	}

	if ($highscore_date_m == "1") {$highscore_date = $lang['highscore_jan']." ".$highscore_date_y;
	}
	elseif ($highscore_date_m == "2") {$highscore_date = $lang['highscore_feb']." ".$highscore_date_y;}
	elseif ($highscore_date_m == "3") {$highscore_date = $lang['highscore_mar']." ".$highscore_date_y;}
	elseif ($highscore_date_m == "4") {$highscore_date = $lang['highscore_apr']." ".$highscore_date_y;}
	elseif ($highscore_date_m == "5") {$highscore_date = $lang['highscore_may']." ".$highscore_date_y;}
	elseif ($highscore_date_m == "6") {$highscore_date = $lang['highscore_jun']." ".$highscore_date_y;}
	elseif ($highscore_date_m == "7") {$highscore_date = $lang['highscore_jul']." ".$highscore_date_y;}
	elseif ($highscore_date_m == "8") {$highscore_date = $lang['highscore_aug']." ".$highscore_date_y;}
	elseif ($highscore_date_m == "9") {$highscore_date = $lang['highscore_sep']." ".$highscore_date_y;}
	elseif ($highscore_date_m == "10") {$highscore_date = $lang['highscore_oct']." ".$highscore_date_y;}
	elseif ($highscore_date_m == "11") {$highscore_date = $lang['highscore_nov']." ".$highscore_date_y;}
	elseif ($highscore_date_m == "12") {$highscore_date = $lang['highscore_dec']." ".$highscore_date_y;
	}

	// count Highscores
	$sql = "SELECT COUNT(highscore_game) AS count FROM ".iNA_HIGHSCORES." WHERE highscore_year = '".$highscore_date_y."' AND highscore_mon = '".$highscore_date_m."'";
	if( !($result2 = $db->sql_query($sql)) )
	{
			message_die(GENERAL_ERROR, $lang['highscore_count_err'], '', __LINE__, __FILE__, $sql);
	}
	$row2 = $db->sql_fetchrow($result2);
	$count_hs = $row2['count'];

	if ((!empty($count_hs)) || ($i == "0")) {
		$input .= "<option value=\"".$i."\">".$lang['highscore_for']." ".$highscore_date."</option>\n";
	}

	}
	$input .= "</select>\n<input type=\"submit\" value=\"".$lang['highscore_submit']."\" name=\"submit\" class=\"post\">\n</form></div></span></td></tr></table>";

	return $input;

}

?>