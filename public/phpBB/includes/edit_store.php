<?
/***************************************************************************
 *                              edit_store.php
 *                            --------------------------
 *		Version			: 2.0.6
 *		Email			: majorflam@majormod.com
 *		Site			: http://www.majormod.com
 *		Copyright		: Majorflam 2004/5 
 *
 ***************************************************************************/
 
 if ( !defined('IN_PHPBB') )
{
	die('Hacking attempt');
}
if ( $store_mode == 'store_post' )
{
		// grab the original post_text
		$sql=" SELECT post_text, bbcode_uid FROM " . POSTS_TEXT_TABLE . "
		WHERE post_id=$post_id
		";
		if ( !$result = $db->sql_query($sql) )
		{
			message_die(GENERAL_ERROR, 'Error in posting', '', __LINE__, __FILE__, $sql);
		}
		$row=$db->sql_fetchrow($result);
		$original_message=$row['post_text'];
		$original_bbcode_uid=$row['bbcode_uid'];
		$this_edit_time=time();
		$sql=" INSERT INTO " . EDIT_STORE_TABLE . "
		(post_id,edited_time,post_text, bbcode_uid, edited_by)
		VALUES
		($post_id,$this_edit_time,'" . addslashes($original_message) . "','$original_bbcode_uid'," . $userdata['user_id'] . ")
		";
		if (!$db->sql_query($sql))
		{
			message_die(GENERAL_ERROR, 'Error in posting', '', __LINE__, __FILE__, $sql);
		}
}

?>