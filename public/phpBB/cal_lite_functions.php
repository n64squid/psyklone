<?php 
/*********************************************
*	Calendar Lite
*
*	$Author: martin $
*	$Date: 2005-08-12 23:15:55 +0100 (Fri, 12 Aug 2005) $
*	$Revision: 24 $
*
*********************************************/

/*###############################################################
## Mod Title: phpBB2 Calendar Lite
## Mod Version: 1.4.6
## Author: WebSnail < Martin Smallridge >
## SUPPORT: http://www.snailsource.com/forum/ 
##
## Functions file
#################################################################*/

//#################################
// Check and set calendar perms

function calendarperm($user_id)
{
	global $db, $cal_config;
	// Get the user permissions first.
	$sql = 'SELECT user_calendar_perm FROM ' . USERS_TABLE . " WHERE user_id = '$user_id'";
	if ( !($result = $db->sql_query($sql)) ) {
		message_die(GENERAL_ERROR, 'Could not select Calendar permission from user table', '', __LINE__, __FILE__, $sql);
	}
	$row = $db->sql_fetchrow($result);

	// Get the group permissions second.
	$sql2 = 'SELECT group_calendar_perm FROM ' . USER_GROUP_TABLE . ' ug, ' . GROUPS_TABLE . " g 
		WHERE ug.user_id = '$user_id' AND g.group_id = ug.group_id";
	if ( !($result2 = $db->sql_query($sql2)) ) {
		message_die(GENERAL_ERROR, 'Could not select Calendar permission from user/usergroup table', '', __LINE__, __FILE__, $sql2);
	}
	$topgroup = 0;
	while($rowg = $db->sql_fetchrow($result2)) {
		if($topgroup < $rowg['group_calendar_perm']) {
			$topgroup = $rowg['group_calendar_perm']; 
		}
	}

	// Use whichever value is highest.
	if ($topgroup > $row['user_calendar_perm']) {
		$cal_perm = $topgroup;
	}
	else {
		$cal_perm = $row['user_calendar_perm'];
	}
	if($cal_config['allow_user_default'] > $cal_perm && $user_id != ANONYMOUS) {
		$cal_perm = $cal_config['allow_user_default'];
	}
	return $cal_perm;
}



//#################################
// Create dateformat for user

function mydateformat($thisdate, $dateformat='d M Y G:i', $span=0)
{
	global $cal_config, $lang, $board_config, $userdata;

	if (!empty($cal_config['cal_dateformat'])) {
		$dateformat = $cal_config['cal_dateformat'];
	} elseif (empty($dateformat)) {
		$dateformat = 'd M Y G:i';	// MOD set to default as it's most likely this.
	}

	if ( !empty($userdata['user_lang'])) {
		$board_config['default_lang'] = $userdata['user_lang'];
	}

	// date comes in as the following: 
	$myr = substr($thisdate, 0, 4);
	$mym = substr($thisdate, 5, 2);
	$myd = substr($thisdate, 8, 2);
	$myh = substr($thisdate, 11, 2);
	$myn = substr($thisdate, 14, 2);
	$mys = substr($thisdate, 17, 2);

	if ($span || ($myh=='00' && $myn=='00' && $mys=='00')) {
		// Need to strip out any TIME references so...
		$timerefs = array ('a','A','B','g','G','h','H','i','I','s');
		while (list(,$val) = each ($timerefs))
			{
			$dateformat = ereg_replace($val, "", $dateformat);
			}
		// strip out any characters used for time
		$dateformat = ereg_replace('[:\.]', " ", $dateformat);
	}

/*
	NOTE: You cannot adjust for timezone without first knowing what timezone the original poster was in!
	Timezone adjust code has been disabled as a result.
*/

/*
	// TIMEZONE code disabled
	// Adjust for the timezone 
	if ( $userdata['session_logged_in'] ) {
		$board_config['board_timezone'] = $userdata['user_timezone'];
	}
*/

	static $translate;

	if ( empty($translate) && $board_config['default_lang'] != 'english' )
	{
		@reset($lang['datetime']);
		while ( list($match, $replace) = @each($lang['datetime']) )
		{
			$translate[$match] = $replace;
		}
	}
	$returnstamp = mktime ($myh,$myn,$mys,$mym,$myd,$myr);
	return ( !empty($translate) ) ? strtr(@date($dateformat, $returnstamp), $translate) : @date($dateformat, $returnstamp);
/*
	// TIMEZONE code disabled
	$gmt_time = gmmktime ($myh,$myn,$mys,$mym,$myd,$myr);
	return ( !empty($translate) ) ? strtr(@gmdate($dateformat, $gmt_time + (3600 * $board_config['board_timezone'])), $translate) : @gmdate($dateformat, $gmt_time + (3600 * $board_config['board_timezone']));
*/
}


//#################################
// Create Date

function cal_create_date($format, $gmepoch, $tz) 
{
	global $board_config, $lang;
	static $translate;

	if ( empty($translate) && $board_config['default_lang'] != 'english' )
	{
		@reset($lang['datetime']);
		while ( list($match, $replace) = @each($lang['datetime']) )
		{
			$translate[$match] = $replace;
		}
	}

	return ( !empty($translate) ) ? strtr(@gmdate($format, $gmepoch + (3600 * $tz)), $translate) : @gmdate($format, $gmepoch + (3600 * $tz));
}


//##################################
// Standard Cal functions.


function create_day_drop($cl_d, $lastday)
{
	for ($i=1; $i<=$lastday; $i++) {
		if ($i == $cl_d) {
		$day_drop .= "<option value=$i selected>$i</option>";
		}
		else {
			$day_drop .=  "<option value=$i>$i</option>";
			}
		}
	return $day_drop;
}

function create_month_drop($cl_m, $cl_y)
{
	global $lang;
	for ($i=1; $i<13; $i++) {
		$nm = $lang['datetime'][date("F", mktime(0,0,0,$i,1,$cl_y))]; 
		if ($i == $cl_m) {
		$mon_drop .= "<option value=$i selected>$nm</option>";
		}
		else {
		$mon_drop .= "<option value=$i>$nm</option>";
		}
	}
	return $mon_drop;
}

function create_year_drop($cl_y)
{
	for ($i=$cl_y-2; $i<$cl_y+5; $i++) {
		if ($i == $cl_y) {
		$yr_drop .= "<option value=$i selected>$i</option>";
		}
		else {
		$yr_drop .= "<option value=$i>$i</option>";
		}
	}
	return $yr_drop;
}


// buttons

function button_main($url, $cl_m, $align='center')
{
	global $lang;
	$button_main = "<form method=post action='$url'><td align='$align'>\n";
	$button_main .= "<input type=submit value='$cl_m' class=mainoption>\n";
	$button_main .= "</td></form>";
	return $button_main;
}

function button_validate($url) 
{
	global $lang, $caluser;
	if ($caluser >= 5) {
		// Validate button
		//$url = append_sid($thisscript."?mode=validate&action=getlist");
		$button_validate = "<form method=post action='$url'><td>";
		$button_validate .= "<input type=submit value='" . $lang['Validate'] . "' class=mainoption>";
		$button_validate .= "</td></form>";
	}
	else {
		$button_validate = "";
	}
	return $button_validate;
}

function button_mod_del($url)
{
	global $lang, $caluser;
	if ($caluser >= 4) {
		// Delete/Modify Button
		//$url = append_sid($thisscript."?cl_d=".$cl_d."&cl_m=".$cl_m."&cl_y=".$cl_y."&mode=modify");
		$button_mod_del = "<form method=post action=$url><td>";
		$button_mod_del .= "<input type=submit value=\"";
		if ($caluser >= 5) { $button_mod_del .= $lang['Cal_Del_mod']; }
		else { $button_mod_del .= $lang['Cal_mod_only']; }
		$button_mod_del .= "\" class=mainoption></td></form>";
	}
	else {
		$button_mod_del = "";
	}
	return $button_mod_del;
}

function button_add($url)
{
	global $lang, $caluser;
	// Next Month			
	$button_add = "<form method=post action='$url'><td>";
	$button_add .= "<input type=submit name=zaction value=\"" . $lang['Cal_add_event'] . "\" class=mainoption>";
	$button_add .= "</td></form>";
	return $button_add;
}

function button_prev($url, $align='left')
{
	// Previous Month			
	$button_prev =  "<form method=post action=$url><td align='$align'>&nbsp;";
	$button_prev .=  "<input type=submit value='<<' class=mainoption>&nbsp;</td></form>";
	return $button_prev;
}

function button_next($url, $align='right')
{
	// Next Month			
	$button_next =  "<form method=post action=$url><td align='$align'>&nbsp;";
	$button_next .= "<input type=submit value='>>' class=mainoption>&nbsp;";
	$button_next .= "</td></form>";
	return $button_next;
}

function array_qsort2 (&$array, $column=0, $order=SORT_ASC, $first=0, $last= -2) { 
	// $array  - the array to be sorted 
	// $column - index (column) on which to sort 
	//		  can be a string if using an associative array 
	// $order  - SORT_ASC (default) for ascending or SORT_DESC for descending 
	// $first  - start index (row) for partial array sort 
	// $last	- stop index (row) for partial array sort 

	if($last == -2) { 
		$last = count($array) - 1; 
	}
	if($last > $first) { 
		$alpha = $first; 
		$omega = $last; 
		$guess = $array[$alpha][$column]; 
		while($omega >= $alpha) { 
			if($order == SORT_ASC) { 
				while($array[$alpha][$column] < $guess) {
					$alpha++;
				}
				while($array[$omega][$column] > $guess) {
					$omega--;
				}
			} else { 
				while($array[$alpha][$column] > $guess) {
					$alpha++; 
				}
				while($array[$omega][$column] < $guess) {
					$omega--;
				}
			} 
			if($alpha > $omega) { 
				break; 
			}
			$temporary = $array[$alpha]; 
			$array[$alpha++] = $array[$omega]; 
			$array[$omega--] = $temporary; 
		} 
		array_qsort2 ($array, $column, $order, $first, $omega); 
		array_qsort2 ($array, $column, $order, $alpha, $last); 
	}
	return $array; 
} 



//#################################
// Variable cleaner 

function clean_me($var, $type) {
	if($type == 'str') {
		return str_replace("\'", "''", $var);
	} elseif($type == 'num') {
		return intval($var);
	} else {
		message_die(GENERAL_ERROR, "No type specified for the var cleaner", "", __LINE__, __FILE__, "Var: $var | Type: $type");
	}
}


//#################################
// Strip BBcode

function cal_strip_bbcode($input) {
	$stripped = preg_replace("/\[(\/)?([a-z\=\"])+(\:[0-9a-z\:]+)?\]/si", "", $input);
	return $stripped;
}


?>
