﻿this._y -= _root.bulletspeed;
for (i=1; i<=_root.totene; i++) {
	if (this.hitTest(_root["enemy"+i])) {
		_root["enemylife"+i] -= 1;
		if (_root["enemylife"+i]<=0) {
			xx = _root["enemy"+i]._x+_root["enemy"+i].ship._x;
			yy = _root["enemy"+i]._y+_root["enemy"+i].ship._y;
			_root["enemy"+i].removeMovieClip();
			attachMovie("explode", ["explode"+i], i);
			_root["explode"+i]._x = xx;
			_root["explode"+i]._y = yy;
			for (t=i; t<=(_root.totene); t++) {
				_root["enemy"+(t)]._name = ["enemy"+(t-1)];
				_root["enemylife"+(t)] = _root["enemylife"+(t-1)];
				
			}
			_root.totene--;
			_root.score+=100;
		}
		if (_root.totene<=0) {
			_root.menu.removeMovieClip();
			_root.gotoAndStop("lvlup");
		}
		this.gotoAndPlay("destroy");
	}
}
