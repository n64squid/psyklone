/*-----------------------------------------------------------------------------
    Effects Store for Shop 3 - A phpBB Add-On
  ----------------------------------------------------------------------------
    colors.js
       Color Management JavaScript
    File Version: 1.1.0
    Begun: July, 2006                 Last Modified: September 20, 2006
  ----------------------------------------------------------------------------
    Copyright 2006 by Jeremy Rogers.  Please read the license.txt included
    with the phpBB Add-On listed above for full license and copyright details.
  ----------------------------------------------------------------------------
    Class and Function Quick Reference
             Names                                 Search Labels
         color_select..............................[colsel]
         colorPalette..............................[colpal]
         update_preview............................[upprev]
         check_color_change........................[chkclrchg]
         check_color_length........................[chkclrlen]
-----------------------------------------------------------------------------*/
/*	color_select                                            [colsel]
       Inserts a selected color code into a form field, and updates 
	   username and rank previews.

    Arguments:
       color	- RGB color code.
       field	- Form field name.

    Return:
       None.
                                                                             */
function color_select(color, field)
{
	var form_name = 'post';
	document.forms[form_name].elements[field].value = color;
	update_preview();
	document.forms[form_name].elements[field].focus();
}

/*	colorPalette                                            [colpal]
       Displays a swatch of colors, each of which can be clicked on to place
	   the color in a form field and update color previews.
	   Ported from phpBB 3 Beta 1, with a few tweaks for usage here.

    Arguments:
       dir		- Direction used to build the swatch. Can have a value of
					"h" (for horizontal) or "v" (for vertical).
       width	- Width of each color's block in the swatch.
       height	- Height of each color's block in the swatch.
       field	- Form field name.

    Return:
       None.
                                                                             */
function colorPalette(dir, width, height, field)
{
	var r = 0, g = 0, b = 0;
	var numberList = new Array(6);

	numberList[0] = '00';
	numberList[1] = '40';
	numberList[2] = '80';
	numberList[3] = 'BF';
	numberList[4] = 'FF';

	document.writeln('<table cellspacing="0" cellpadding="0" border="0">');

	for (r = 0; r < 5; r++)
	{
		if (dir == 'h')
		{
			document.writeln('<tr>');
		}

		for (g = 0; g < 5; g++)
		{
			if (dir == 'v')
			{
				document.writeln('<tr>');
			}
			
			for (b = 0; b < 5; b++)
			{
				color = String(numberList[r]) + String(numberList[g]) + String(numberList[b]);
				document.write('<td bgcolor="#' + color + '">');
				document.write('<a href="javascript:color_select(\'' + color + '\', \'' + field + '\');""><img src="images/spacer.gif" width="' + width + '" height="' + height + '" border="0" alt="#' + color + '" title="#' + color + '" /></a>');
				document.writeln('</td>');
			}

			if (dir == 'v')
			{
				document.writeln('</tr>');
			}
		}

		if (dir == 'h')
		{
			document.writeln('</tr>');
		}
	}
	document.writeln('</table>');
}

/*	update_preview                                          [upprev]
       Gets the current color sections from form fields and uses these to
	   update the rank and username color previews.

    Arguments:
       None.

    Return:
       None.
                                                                             */
function update_preview()
{
	/* Get the current color settings */
	var effects = new Array();
	for (i = 0; i<previews.length-1; i++ )
	{
		effects[previews[i]] = document.getElementById(previews[i]).value
	}

	name_block		= document.getElementById('namepreview');
	title_block		= document.getElementById('rankpreview');

	IE_check = /MSIE/;
	if ( IE_check.test(navigator.appVersion) )
	{
		glow_base = 'glow(color=#COLOR, strength=5)';
		shadow_base = 'shadow(color=#COLOR, strength=5)';
	
		new_name_filter = '';
		new_title_filter = '';
		if ( effects['name_color'] && check_color_length(effects['name_color']) )
		{
			name_block.style.color = '#' + effects['name_color'];
		}
		if ( effects['title_color'] && check_color_length(effects['title_color']) )
		{
			title_block.style.color = '#' + effects['title_color'];
		}
		if ( effects['name_glow'] && check_color_length(effects['name_glow']) )
		{
			new_name_filter = new_name_filter + glow_base.replace(/COLOR/, effects['name_glow']);
		}
		if ( effects['name_shadow'] && check_color_length(effects['name_shadow']) )
		{
			new_name_filter = new_name_filter + shadow_base.replace(/COLOR/, effects['name_shadow']);
		}
		if ( effects['title_glow'] && check_color_length(effects['title_glow']) )
		{
			new_title_filter = new_title_filter + glow_base.replace(/COLOR/, effects['title_glow']);
		}
		if ( effects['title_shadow'] && check_color_length(effects['title_shadow']) )
		{
			new_title_filter = new_title_filter + shadow_base.replace(/COLOR/, effects['title_shadow']);
		}
		name_block.style.filter = new_name_filter;
		title_block.style.filter = new_title_filter;
	}
	else
	{
		if ( effects['name_color'] && check_color_length(effects['name_color']) )
		{
			name_block.style.color = '#' + effects['name_color'];
		}
		if ( effects['title_color'] && check_color_length(effects['title_color']) )
		{
			title_block.style.color = '#' + effects['title_color'];
		}
		new_name_bgcolor = '';
		new_title_bgcolor = '';
		if ( effects['name_glow'] && check_color_length(effects['name_glow']) )
		{
			new_name_bgcolor = '#' + effects['name_glow'];
		}
		if ( effects['name_shadow'] && check_color_length(effects['name_shadow']) )
		{
			new_name_bgcolor = '#' + effects['name_shadow'];
		}
		if ( effects['title_glow'] && check_color_length(effects['title_glow']) )
		{
			new_title_bgcolor = '#' + effects['title_glow'];
		}
		if ( effects['title_shadow'] && check_color_length(effects['title_shadow']) )
		{
			new_title_bgcolor = '#' + effects['title_shadow'];
		}
		name_block.style.backgroundColor = new_name_bgcolor;
		title_block.style.backgroundColor = new_title_bgcolor;
	}
}

/*	check_color_change                                      [chkclrchg]
       Automatically updates previews when a color field is changed.

    Arguments:
       field	- The current color field, which has been changed.

    Return:
       None.
                                                                             */
function check_color_change(field)
{
	newval = document.getElementById(field).value;
	if ( check_color_length(newval) )
	{
		update_preview();
	}
}

/*	check_color_length                                      [chkclrlen]
       Checks the length of color strings entered into fields.

    Arguments:
       color	- Current color obtained from document.getElementById().

    Return:
       boolean  1 for a valid color length, 0 for an invalid length.
                                                                             */
function check_color_length(color)
{
	if ( color.length == 3 || color.length == 6 )
	{
		return 1;
	}
	return 0;
}