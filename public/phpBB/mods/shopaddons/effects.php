<?php
/*-----------------------------------------------------------------------------
    Effects Store for Shop 3 - A phpBB Add-On
  ----------------------------------------------------------------------------
    effects.php
       Classes for effects management and display.
    File Version: 1.2.2
    Begun: July, 2006                 Last Modified: October 10, 2006
  ----------------------------------------------------------------------------
    Copyright 2006 by Jeremy Rogers.  Please read the license.txt included
    with the phpBB Add-On listed above for full license and copyright details.
  ----------------------------------------------------------------------------
    Class and Function Quick Reference
             Names                                 Search Labels
         Effects_Shop..............................[shopclass]
            Effects_Shop...........................[efshop]
            secure_vars............................[secvars]
            secure_sql.............................[secsql]
            secure_colors..........................[secclr]
            censor_block_title.....................[cenblock]
            get_filename...........................[getfname]
            get_currency...........................[getcur]
            get_currencies.........................[getcurs]
            record_transaction.....................[rectrans]
         Effects_Admin.............................[adminclass]
            Effects_Admin..........................[efadmin]
            check_effects_edit.....................[chkedit]
            show_effects_entry.....................[shwenty]
            show_effects_admin.....................[shwadm]
            show_effects_config....................[shwcfg]
            save_effects_config....................[savcfg]
         Effects_Main..............................[mainclass]
            Effects_Main...........................[efmain]
            censor.................................[censor]
            cache_user.............................[cacheuser]
            check_immunity.........................[chkimmun]
            post_check.............................[postchk]
            pm_check...............................[pmchk]
            profile_check..........................[prflchk]
            check_privilege........................[chkpriv]
            build_name.............................[bldname]
            build_title............................[bldtitle]
            add_glow...............................[addglow]
            add_shadow.............................[addshad]
            profile_edit_check.....................[prfedchk]
            check_namechange.......................[chknmchg]

    Special Key Quick Reference
		 Poster Name Conflicts.....................[conflictfix]
		 PM Name Conflicts.........................[conflictfix2]
-----------------------------------------------------------------------------*/

if( !defined('IN_PHPBB') )
{
	die('You are not allowed to do that.');
}

require_once($phpbb_root_path . 'language/lang_' . $board_config['default_lang'] . '/lang_effects.' . $phpEx);

// Get Cash Mod files if not already included.
// We must declare some global variables and check for $_GET['pane']
// to prevent multiple inclusion and non-member call bugs when using
// PHP 5 + Profile Control Panel + Cash mod.
global $table_prefix, $cash;
if( !isset($_GET['pane']) && !defined('IN_CASHMOD') )
{
	if( file_exists($phpbb_root_path . 'includes/functions_cash.'.$phpEx) )
	{
		define('IN_CASHMOD', true);
		include_once($phpbb_root_path . 'includes/functions_cash.'.$phpEx);
	}
	else
	{
		define('NO_CASHMOD', true);
	}
}

if( defined('IN_ADMIN') )
{
	$effects_admin = new Effects_Admin();
}
elseif( defined('IN_EFFECTS_SHOP') )
{
	require_once($phpbb_root_path . 'language/lang_' . $board_config['default_lang'] . '/lang_shop.' . $phpEx);
	$effects = new Effects_Shop();
}
else
{
	$effects = new Effects_Main();
}

/*	Effects_Shop                                            [shopclass]
       A group of basic functions and variables used throughout this script.
       This class will be extended by other classes, but is also used without
	   extensions in the public shop interface.
*/
class Effects_Shop 
{
	var $quotes		= FALSE;
	var $vars		= array();
	var $glow_base;
	var $shadow_base;
	var $glow_bg;
	var $shadow_bg;
	var $text_shadow;
	var $currency;
	var $main_filename		= 'shop_effects';
	var $currency_field		= 'effects_currency';
	var $transaction_type	= 'Effects Store';
	var $lp					= 'EFFECTS_';

/*	Effects_Shop                                            [efshop]
       Constructor function that determines the CSS code to be use for glow
	   and shadow effects.

    Arguments:
       None.

    Return:
       None.
                                                                             */
	function Effects_Shop()
	{
		// Let's set the base glow and shadow strings.
		$this->glow_base	= 'width: %2$s%%; filter:glow(color=#%1$s, strength=5);';
		$this->shadow_base	= 'width: %2$s%%; filter:shadow(color=#%1$s, strength=5);';
		$this->glow_bg		= 'background-color: #%1$s; padding: 0.2em 0.2em;';
		$this->shadow_bg	= 'background-color: #%1$s; padding-left: 0.2em; padding-bottom: 0.2em;';
		$this->text_shadow	= 'text-shadow: #%1$s 2px 2px 3px;';
//		if( strpos($_SERVER['HTTP_USER_AGENT'], 'Firefox/2') !== FALSE )
//		{
//			// Firefox 2 gets a shadow effect through text-shadow.
//			$this->shadow_base	= $this->text_shadow;
//			$this->glow_base	= $this->glow_bg;
//		}
//		else
		if( strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === FALSE )
		{
			// For browsers other than Internet Explorer, fall back to background
			// colors.  Some browsers that support glow and/or shadow may
			// actually get excluded by this.
			$this->shadow_base	= $this->shadow_bg;
			$this->glow_base	= $this->glow_bg;
		}
	}

/*	secure_vars                                             [secvars]
       Secures variables that are passed to the script via query string, user
	   input, etc.  These variables cannot be trusted and must be sanitized
	   before use.  Values are pulled from the $_REQUEST array based on
	   variable names passed as arguments to this function.
	   For integer variables, this function will call intval().
	   For strings, it will run addslashes() if magic_quotes_gpc is off, and
	   also htmlspecialchars() and trim().
	   For boolean variables, the variable will be set to TRUE if a variable
	   of the same name was passed to the script and FALSE if no such variable
	   was passed.
	   There is no effect on array variables.
	   Sanitized values will be stored in the object's $vars array for later
	   reference.

    Arguments:
       Accepts a variable number of arguments.
	   If only one arugment is used, that is taken as a variable name and used
	   to sanitize a string variable of that name.
	   When multiple arguments are passed, the first is used to indicate the
	   type that should be used to sanitize variables, with the rest of the
	   arguments taken as variable names.
	   The type indicator argument may have the following values, with each
	   having the listed meaning:
			string	= String variables
			s		= String variables
			int		= Integer variables
			i		= Integer variables
			bool	= Boolean variables
			b		= Boolean variables

    Return:
       Nothing.
                                                                             */
	function secure_vars()
	{
		$args = func_get_args();
		$type = array_shift($args);
		if( !in_array($type, array('string', 'int', 'bool', 's', 'b', 'i')) )
		{
			array_unshift($args, $type);
			$type = 'string';
		}

		$this->quotes = get_magic_quotes_gpc();
		foreach($args as $v)
		{
			if( !is_array($_REQUEST[$v]) )
			{
				switch($type)
				{
					case 'int':
					case 'i':
						$$v = intval($_REQUEST[$v]);
						break;
					case 'bool':
					case 'b':
						$$v = ( isset($_REQUEST[$v]) ) ? TRUE : FALSE;
						break;
					case 'string':
					case 's':
						$$v = ( !$this->quotes ) ? addslashes($_REQUEST[$v]) : $_REQUEST[$v];
						$$v = htmlspecialchars(trim($$v));
						break;
				}
			}
			$this->vars[$v] = $$v;
		}
	}

/*	secure_sql                                              [secsql]
       Secures SQL query strings by replacing \' with '' (two single quotes).

    Arguments:
       $query_part	- An SQL query or string value to be used within such.
       $addslash	- Run addslashes() on $query_part first.  Optional, mainly 
						for use with data pulled from the database rather than
						a form.

    Return:
       string  The secured string.
                                                                             */
	function secure_sql($query_part, $addslash = FALSE)
	{
		if( $addslash )
		{
			$query_part = addslashes($query_part);
		}
		return str_replace("\'", "''", $query_part);
	}

/*	secure_colors                                           [secclr]
       Check color strings from the public shop interface to ensure that the
	   string will work with the effects.  Colors must be in three or six digit
	   RGB notation, such as 000000 or 000 for black.  Will error if strings of
	   other lengths or non-alphanumeric text are used.
	   A special case of an empty string is allowed, but only when updating an
	   effect.

    Arguments:
       $field		- The name of a variable to be checked.
       $type		- Type of input field. If not "colors", we skip the work
						here.
       $allow_empty	- Allows the field to be blank when someone wants to remove
						an effect.

    Return:
       None.
                                                                             */
	function secure_colors($field, $type, $allow_empty = FALSE)
	{
		if( $type == 'colors' )
		{
			global $lang, $return_links;

			$length = (strlen($this->vars[$field]) != 3 && strlen($this->vars[$field]) != 6);
			if( !$allow_empty || ($allow_empty && !empty($this->vars[$field])) )
			{
				$check = $length || !ctype_alnum($this->vars[$field]);
			}
			elseif( $allow_empty && empty($this->vars[$field]) )
			{
				$check = FALSE;
			}

			if( $check )
			{
				message_die(GENERAL_MESSAGE, $lang[$this->lp . 'BAD_COLOR_INPUT'] . $return_links);
			}
		}
	}

/*	censor_block_title                                      [cenblock]
       Integrates Effects Store with the Censor Block modification. Prevents
	   any Custom Titles containing censored words from being entered into
	   the site's database. If Censor Block is not installed, then this
	   function will do nothing.
	   By default, only the custom title field is checked, but it would be
	   easy enough to extend this to check other fields as well.

    Arguments:
       $field	- Name of the field to be checked.

    Return:
       None.
                                                                             */
	function censor_block_title($field)
	{
		global $phpbb_root_path, $phpEx, $board_config, $lang, $return_links;
		if( !in_array($field, array('title', 'titlechange')) || @!file_exists($phpbb_root_path . 'mods/cenblock/cenblock.' . $phpEx) )
		{
			return;
		}

		// Censor Block is also installed - let's use it!
		include_once($phpbb_root_path . 'mods/cenblock/cenblock.' . $phpEx);
		$cenblock->check_field($this->vars[$field]);
		if( $cenblock->triggered )
		{
			// Oops, they tripped the word censor!
			$error_field = $cenblock->highlight_field(stripslashes($this->vars[$field]));
			$error_msg = $cenblock->build_msg($lang[$this->lp . 'CENBLOCK_TITLE'], $lang[$this->lp . 'TITLE_CENBLOCK'], $board_config['board_email'], $error_field);
			message_die(GENERAL_MESSAGE, $error_msg . $return_links);
		}
	}


/*	get_filename                                            [getfname]
       Returns the filename of this modification's main interface file, using
	   global $phpEx variable for the extension.

    Arguments:
       None.

    Return:
       string  See function description.
                                                                             */
	function get_filename()
	{
		return $this->main_filename . '.' . $GLOBALS['phpEx'];
	}

/*	get_currency                                            [getcur]
       Gets details about the currency to be used for purchasing.
	   When used with the Cash mod, any currency created on the board can be
	   used.  Without the Cash mod, the currency defaults to user_points.

    Arguments:
       None.

    Return:
       None.
                                                                             */
	function get_currency()
	{
		global $board_config, $lang;
		if( defined('NO_CASHMOD') )
		{
			$this->currency = array(
				'field'	=>	'user_points',
				'name'	=>	$board_config['points_name'],
				'id'	=>	0
			);
			return;
		}
		global $cash;
		$cashdata = $cash->currency($board_config[$this->currency_field]);
		if( !$cashdata || !$cash->currencies[$board_config[$this->currency_field]]->mask(CURRENCY_ENABLED) )
		{
			message_die(GENERAL_MESSAGE, $lang['REQUIRES_CASH']);
		}
		$this->currency = array(
			'field'	=>	$cashdata->db(),
			'name'	=>	$cashdata->name(),
			'id'	=>	$cashdata->id()
		);
	}

/*	get_currencies                                         [getcurs]
       Gets a list of the currencies that can be used for purchasing.
	   When used with the Cash mod, all enabled currencies will be listed.
       Without the Cash mod, the currency defaults to user_points.

    Arguments:
       $current_val	- The currency being used now.
       $select		- If not FALSE, the list will be an HTML select box.
						The value of this will be used for the name attribute
						of the select box.

    Return:
       string   The list of currencies, or an message about the default to
					user_points.
                                                                             */
	function get_currencies($current_val = '', $select = '')
	{
		global $board_config, $lang;
		$select = ( empty($select) ) ? $this->currency_field : $select;
		if( defined('NO_CASHMOD') )
		{
			return $lang['NO_CASHMOD'] . '<input type="hidden" name="effects_currency" value="user_points" />';
		}
		global $cash;
		$num_currencies = $cash->currency_count(CURRENCY_ENABLED);
		$currencies = '';
		foreach($cash->id_list as $k=>$v)
		{
			if ( $cash->currency_exists($k) && $cash->currencies[$k]->mask(CURRENCY_ENABLED) )
			{
				$cashdata = $cash->currency($k);
				if( $select )
				{
					$selected = ( $cashdata->id() == $current_val ) ? ' selected="selected"': '';
					$currencies .= '<option value="' . $cashdata->id() . '"' . $selected . '>'. $cashdata->name() . ' (' . $cashdata->db() . ')</option>';
				}
				else
				{
					$currencies .= $cashdata->name() . ' (' . $cashdata->db() . ")\n";
				}
			}
		}
		if( empty($currencies) )
		{
			return $lang['NO_CURRENCIES'];
		}
		if( $select )
		{
			$currencies = '<select name="' .  $select. '">' . $currencies . '</select>';
		}
		return $currencies;
	}


/*	record_transaction                                      [rectrans]
       Records an entry relating to this modification in the Shop mod's
	   transactions table.

    Arguments:
       $action		- The action performed.
       $record		- Description of what was changed (255 characters, max).
       $target_id	- User being altered.
       $target_name	- Name of user being altered (optional).
       $details		- Extra details about the transaction (optional).

    Return:
       None.
                                                                             */
	function record_transaction($action, $record, $target_id = 0, $target_name = '', $details = '')
	{
		if( !defined('SHOP_TABLE') && !defined('TRANS_TABLE') )
		{
			return;
		}
		global $userdata, $db;
		$sql = 'INSERT INTO ' . TRANS_TABLE . " (user_id, type, action, value, misc, timestamp, ip, target_id, target_name)
			VALUES('{$userdata['user_id']}', '{$this->transaction_type}', '" . $this->secure_sql($action, true) . "', '" . $this->secure_sql($record, true) . "', '" . $this->secure_sql($details, true) . "', '" . time() . "', '" . $this->secure_sql($_SERVER['REMOTE_ADDR']) . "', '" . intval($target_id) . "', '" . $this->secure_sql($target_name, true) . "')";
		if ( !$db->sql_query($sql) )
		{
			message_die(GENERAL_ERROR, 'Fatal Error with Transaction Recording!', '', __LINE__, __FILE__, $sql);
		}
	}
}


/*	Effects_Admin                                           [adminclass]
       Class for admin panel management of the Effects Store.  This handles all
	   aspects of the admin panel so that changes to admin_shop.php can be kept
	   to a minimum.
                                                                             */
class Effects_Admin extends Effects_Shop
{
	var $settings	= array();

/*	Effects_Admin                                           [efadmin]
       Constructor function that defines setting names for use throughout the
	   class.

    Arguments:
       None.

    Return:
       None.
                                                                             */
	function Effects_Admin()
	{
		$this->settings = array(
			'effects_currency',
			'effects_namechange',
			'effects_titlechange',
			'effects_pm_titlechange',
			'effects_pm_admin_titlechange',
			'effects_pm_admin_namechange',
			'effects_enable',
			'effects_avatar',
			'effects_signature',
			'effects_title',
			'effects_rank_replace',
			'effects_admin',
			'effects_moderator',
			'effects_name_color',
			'effects_name_glow',
			'effects_name_shadow',
			'effects_title_color',
			'effects_title_glow',
			'effects_title_shadow'
		);
	}

/*	check_effects_edit                                      [chkedit]
       Checks the variables sent to the script for attempts to access the
	   special effects shop.  This is a failsafe to prevent someone from
	   editing the special effects shop through the standard shop editor
	   rather than the Effects Store ACP.  It should also prevent the adding
	   of a new shop with the "special" type, which is reserved for the
	   Effects Store.

    Arguments:
       $action	- Indicates the mode of the submitted script.

    Return:
       None.
                                                                             */
	function check_effects_edit($action)
	{
		global $db, $phpEx;
		if( in_array($action, array('updateshop', 'additem', 'updateitem', 'editshop')) )
		{
			$shopid = intval($_REQUEST['shopid']);
			$sql = 'SELECT url FROM ' . SHOP_TABLE . " WHERE id='$shopid'";
			if( !$result = $db->sql_query($sql) )
			{
				message_die(GENERAL_ERROR, 'Could not check for Effects Store redirect', '', __LINE__, __FILE__, $sql);
			}
			if( $row = $db->sql_fetchrow($result) )
			{
				if( $row['url'] == $this->get_filename() )
				{
					$GLOBALS['action'] = 'effects';
				}
			}
		}
	}

/*	show_effects_entry                                      [shwenty]
       Displays a form button to launch the special effects shop editor in the
	   Shop Settings ACP.

    Arguments:
       None.

    Return:
       None.
                                                                             */
	function show_effects_entry()
	{
		global $template, $lang, $phpEx;
		$template->set_filenames(array('effects_entry' => 'admin/shop_effects_entry.tpl'));
		$template->assign_block_vars('effects_store', array(
			'S_CONFIG_ACTION'	=>	append_sid("admin_shop.$phpEx"),
			'L_EFFECTS'			=>	$lang[$this->lp . 'TITLE'],
			'L_EDIT'			=>	$lang[$this->lp . 'EDIT'],
		));
		$template->assign_var_from_handle('EFFECTS_ENTRY', 'effects_entry');
	}

/*	show_effects_admin                                      [shwadm]
       Determines which stage of the Effects Store ACP should be used.  The
	   editor form may be shown or the submitted editor form may be processed
	   and new values saved in the database.

    Arguments:
       None.

    Return:
       None.
                                                                             */
	function show_effects_admin()
	{
		$this->secure_vars('subaction');
		if( !in_array($this->vars['subaction'], array('edit', 'update')) )
		{
			$this->vars['subaction'] = 'edit';
		}

		if( $this->vars['subaction'] == 'edit' )
		{
			$this->show_effects_config();
		}
		elseif( $this->vars['subaction'] == 'update' )
		{
			$this->save_effects_config();
		}
	}

/*	show_effects_config                                     [shwcfg]
       Displays the special effects shop ACP editing form.

    Arguments:
       None.

    Return:
       None.
                                                                             */
	function show_effects_config()
	{
		global $template, $lang, $phpEx, $db, $board_config;
		$template->set_filenames(array('body' => 'admin/shop_effects_config.tpl'));

		$sql = 'SELECT * FROM ' . SHOP_TABLE . " WHERE url='" . $this->get_filename() . "' LIMIT 1";
		if( !$result = $db->sql_query($sql) )
		{
			message_die(GENERAL_ERROR, 'Could not find a special shop! Make sure you have executed the SQL queries or installer file included with the Effects Store mod!', '', __LINE__, __FILE__, $sql);
		}
		if( $row = $db->sql_fetchrow($result) )
		{
			$shopname		= $row['shopname'];
			$shoptype		= $row['shoptype'];
			$shopurl		= $row['url'];
			$is_district	= $row['d_type'];
			$parent			= $row['district'];
			$owner			= $row['shop_owner'];
			$shopid			= $row['id'];
		}
		else
		{
			message_die(GENERAL_ERROR, 'Could not find a special shop! Make sure you have executed the SQL queries or installer file included with the Effects Store mod!', '', __LINE__, __FILE__, $sql);
		}

		// Get list of all districts
		$sql = 'SELECT shopname, district FROM ' . SHOP_TABLE . "
				WHERE d_type = 1
					AND district <> 0
					AND (url = '' OR url IS NULL)
				ORDER BY shopname";
		if( !$result = $db->sql_query($sql) )
		{
			message_die(GENERAL_ERROR, 'Could not get a list of districts.', '', __LINE__, __FILE__, $sql);
		}
		$districts = $db->sql_fetchrowset($result);
		$db->sql_freeresult($result);
		$s_parents = '';
		if( empty($districts) )
		{
			$s_parents = $lang[$this->lp . 'NO_PARENTS'];
			$s_parents .= '<input type="hidden" name="parent" value="' . $parent . '" />';
		}
		else
		{
			$s_parents = '<select name="parent">';
			array_unshift($districts, array('shopname' => $lang['None'], 'district' => 0));
			foreach($districts as $v)
			{
				$s_parents .= '<option value="' . $v['district'] . '"' . (( $v['district'] == $parent ) ? ' selected="selected"': '') . '>';
				$s_parents .= $v['shopname'] . '</option>';
			}
			$s_parents .= '</select>';
		}

		$pm_titlechange_yes = ( $board_config['effects_pm_titlechange'] ) ? 'checked="checked"': '';
		$pm_titlechange_no	= ( $board_config['effects_pm_titlechange'] ) ? '': 'checked="checked"';
		$pm_admin_titlechange_yes = ( $board_config['effects_pm_admin_titlechange'] ) ? 'checked="checked"': '';
		$pm_admin_titlechange_no	= ( $board_config['effects_pm_admin_titlechange'] ) ? '': 'checked="checked"';
		$pm_admin_namechange_yes = ( $board_config['effects_pm_admin_namechange'] ) ? 'checked="checked"': '';
		$pm_admin_namechange_no	= ( $board_config['effects_pm_admin_namechange'] ) ? '': 'checked="checked"';

		$district_yes	= ( $is_district ) ? 'checked="checked"': '';
		$district_no	= ( $is_district ) ? '': 'checked="checked"';
		$enable_yes = ( $board_config['effects_enable'] ) ? 'checked="checked"': '';
		$enable_no	= ( $board_config['effects_enable'] ) ? '': 'checked="checked"';
		$admin_yes	= ( $board_config['effects_admin'] )  ? 'checked="checked"': '';
		$admin_no	= ( $board_config['effects_admin'] )  ? '': 'checked="checked"';
		$moderator_yes	= ( $board_config['effects_moderator'] ) ? 'checked="checked"': '';
		$moderator_no	= ( $board_config['effects_moderator'] ) ? '': 'checked="checked"';
		$template->assign_vars(array(
			'L_SUBMIT'	=>	$lang['Submit'],
			'L_RESET'	=>	$lang['Reset'],
			'L_YES'		=>	$lang['Yes'],
			'L_NO'		=>	$lang['No'],
			'L_TITLE'	=>	$row['shopname'],
			'L_EXPLAIN'	=>	sprintf($lang[$this->lp . 'EXPLAIN'], $row['shopname'], $row['id']),

			'L_ENABLE'					=>	$lang[$this->lp . 'ENABLE'],
			'L_ADMIN_IMMUNE'			=>	$lang[$this->lp . 'ADMIN_IMMUNE'],
			'L_MOD_IMMUNE'				=>	$lang[$this->lp . 'MOD_IMMUNE'],
			'L_COST_EXPLAIN'			=>	$lang[$this->lp . 'COST_EXPLAIN'],
			'L_AVATAR'					=>	$lang[$this->lp . 'AVATAR'],
			'L_SIGNATURE'				=>	$lang[$this->lp . 'SIGNATURE'],
			'L_EFFECT_TITLE'			=>	$lang[$this->lp . 'EFFECT_TITLE'],
			'L_RANK_REPLACE'			=>	$lang[$this->lp . 'RANK_REPLACE'],
			'L_RANK_EXPLAIN'			=>	$lang[$this->lp . 'RANK_EXPLAIN'],
			'L_AVATAR_EXPLAIN'			=>	$lang[$this->lp . 'AVATAR_EXPLAIN'],
			'L_SIGNATURE_EXPLAIN'		=>	$lang[$this->lp . 'SIGNATURE_EXPLAIN'],
			'L_TITLE_EXPLAIN'			=>	$lang[$this->lp . 'TITLE_EXPLAIN'],
			'L_NAME_COLOR'				=>	$lang[$this->lp . 'NAME_COLOR'],
			'L_NAME_GLOW'				=>	$lang[$this->lp . 'NAME_GLOW'],
			'L_NAME_SHADOW'				=>	$lang[$this->lp . 'NAME_SHADOW'],
			'L_TITLE_COLOR'				=>	$lang[$this->lp . 'TITLE_COLOR'],
			'L_TITLE_GLOW'				=>	$lang[$this->lp . 'TITLE_GLOW'],
			'L_TITLE_SHADOW'			=>	$lang[$this->lp . 'TITLE_SHADOW'],
			'L_COLORS'					=>	$lang[$this->lp . 'COLORS'],
			'L_PRIVS'					=>	$lang[$this->lp . 'PRIVS'],
			'L_STORE_NAME'				=>	$lang[$this->lp . 'STORE_NAME'],
			'L_STORE_OWNER'				=>	$lang[$this->lp . 'OWNER'],
			'L_STORE_TYPE'				=>	$lang[$this->lp . 'SHOPTYPE'],
			'L_SHOW_DISTRICT'			=>	$lang[$this->lp . 'SHOWDISTRICT'],
			'L_PARENT_DISTRICT'			=>	$lang[$this->lp . 'PARENTDISTRICT'],
			'L_STORE_TYPE_EXPLAIN'		=>	$lang[$this->lp . 'SHOPTYPE_EXPLAIN'],
			'L_SHOW_DISTRICT_EXPLAIN'	=>	$lang[$this->lp . 'SHOWDISTRICT_EXPLAIN'],
			'L_PARENT_DISTRICT_EXPLAIN'	=>	$lang[$this->lp . 'PARENTDISTRICT_EXPLAIN'],
			'L_PRIV_EXPLAIN'			=>	$lang[$this->lp . 'PRIV_EXPLAIN'],
			'L_STORE_URL'				=>	$lang[$this->lp . 'URL'],
			'L_STORE_URL_EXPLAIN'		=>	$lang[$this->lp . 'URL_EXPLAIN'],
			'L_CHANGE_USERNAME'			=>	$lang[$this->lp . 'COST_CHANGE_USERNAME'],
			'L_OTHER_TITLE'				=>	$lang[$this->lp . 'COST_OTHER_TITLE'],
			'L_CHANGE_USERNAME_EXPLAIN'	=>	$lang[$this->lp . 'CHANGE_USERNAME_EXPLAIN'],
			'L_OTHER_TITLE_EXPLAIN'		=>	$lang[$this->lp . 'OTHER_TITLE_EXPLAIN'],
			'L_PM_ADMIN_NAMECHANGE'		=>	$lang[$this->lp . 'PM_ADMIN_NAMECHANGE'],
			'L_PM_ADMIN_TITLECHANGE'	=>	$lang[$this->lp . 'PM_ADMIN_TITLECHANGE'],
			'L_PM_TITLECHANGE'			=>	$lang[$this->lp . 'PM_TITLECHANGE'],
			'L_CURRENCY'				=>	$lang[$this->lp . 'CURRENCY'],
			'L_CURRENCY_EXPLAIN'		=>	$lang[$this->lp . 'CURRENCY_EXPLAIN'],

			'S_PARENTS'				=>	$s_parents,
			'S_CURRENCY'			=>	$this->get_currencies($board_config[$this->currency_field]),
			'S_CONFIG_ACTION'		=>	append_sid("admin_shop.$phpEx"),

			'SHOP_ID'					=>	$shopid,
			'STORE_NAME'				=>	stripslashes($shopname),
			'STORE_TYPE'				=>	stripslashes($shoptype),
			'STORE_OWNER'				=>	stripslashes($owner),
			'STORE_URL'					=>	stripslashes($shopurl),
			'SELECTED_PM_TITLECHANGE_YES'			=>	$pm_titlechange_yes,
			'SELECTED_PM_TITLECHANGE_NO'			=>	$pm_titlechange_no,
			'SELECTED_PM_ADMIN_TITLECHANGE_YES'		=>	$pm_admin_titlechange_yes,
			'SELECTED_PM_ADMIN_TITLECHANGE_NO'		=>	$pm_admin_titlechange_no,
			'SELECTED_PM_ADMIN_NAMECHANGE_YES'		=>	$pm_admin_namechange_yes,
			'SELECTED_PM_ADMIN_NAMECHANGE_NO'		=>	$pm_admin_namechange_no,
			'SELECTED_DISTRICT_YES'		=>	$district_yes,
			'SELECTED_DISTRICT_NO'		=>	$district_no,
			'SELECTED_ENABLE_YES'		=>	$enable_yes,
			'SELECTED_ENABLE_NO'		=>	$enable_no,
			'SELECTED_ADMIN_YES'		=>	$admin_yes,
			'SELECTED_ADMIN_NO'			=>	$admin_no,
			'SELECTED_MODERATOR_YES'	=>	$moderator_yes,
			'SELECTED_MODERATOR_NO'		=>	$moderator_no,
			'NAME_CHANGE'				=>	$board_config['effects_namechange'],
			'TITLE_CHANGE'				=>	$board_config['effects_titlechange'],
			'AVATAR'					=>	$board_config['effects_avatar'],
			'SIGNATURE'					=>	$board_config['effects_signature'],
			'EFFECT_TITLE'				=>	$board_config['effects_title'],
			'RANK_REPLACE'				=>	$board_config['effects_rank_replace'],
			'TITLE_SHADOW'				=>	$board_config['effects_title_shadow'],
			'TITLE_GLOW'				=>	$board_config['effects_title_glow'],
			'TITLE_COLOR'				=>	$board_config['effects_title_color'],
			'NAME_SHADOW'				=>	$board_config['effects_name_shadow'],
			'NAME_GLOW'					=>	$board_config['effects_name_glow'],
			'NAME_COLOR'				=>	$board_config['effects_name_color'],
			'POINTS_NAME'				=>	$board_config['points_name']
		));

		// Shop Admin Navigation support
		if( isset($GLOBALS['shop_backlinks']) )
		{
			$GLOBALS['shop_backlinks'][] = array(append_sid("admin_shop.$phpEx?action=effects&subaction=edit"), $row['shopname']);
		}
	}

/*	save_effects_config                                     [savcfg]
       Saves Effects Store configuration settings when the editor form is
	   submitted.

    Arguments:
       None.

    Return:
       None.
                                                                             */
	function save_effects_config()
	{
		global $lang, $db, $phpEx;

		foreach($this->settings as $v)
		{
			$this->secure_vars($v);
			$sql = 'UPDATE ' . CONFIG_TABLE . " SET config_value='" . $this->secure_sql($this->vars[$v]) . "' WHERE config_name='$v'";
			if( !$db->sql_query($sql) )
			{
				message_die(GENERAL_ERROR, 'Failed to update Effects Store configuration!', '', __LINE__, __FILE__, $sql);
			}
		}

		// Legacy code that I may need later... don't muck with it!
		//, url='" . $this->secure_sql($this->vars['url']) . "'

		// Update the shop's settings
		
		$this->secure_vars('s', 'shopname', 'shoptype', 'owner', 'url');
		$this->secure_vars('i', 'shopid', 'd_type', 'parent');
		if( $this->vars['d_type'] )
		{
			// The shop will be shown as a district, so it must have a district
			// number not in use by other shops. 127 is the max for a MySQL
			// tinyint column, which is used for this, so we'll give the shop
			// a district number of 127.
			$this->vars['parent'] = 127;
		}
		
		$sql = 'UPDATE ' . SHOP_TABLE . " SET shopname='" . $this->secure_sql($this->vars['shopname']) . "', shoptype='" . $this->secure_sql($this->vars['shoptype']) . "', shop_owner='" . $this->secure_sql($this->vars['owner']) . "', d_type = {$this->vars['d_type']}, district = {$this->vars['parent']}
		WHERE id = {$this->vars['shopid']}";
		if( !$db->sql_query($sql) )
		{
			message_die(GENERAL_ERROR, 'Failed to update Effects Store configuration!', '', __LINE__, __FILE__, $sql);
		}

		message_die(GENERAL_MESSAGE, $lang[$this->lp . 'CONFIG_UPDATE'] . '<br /><br />' . sprintf($lang[$this->lp . 'BACK_SHOP'], append_sid("admin_shop.$phpEx?action=effects"), append_sid("admin_shop.$phpEx")));
	}
}


/*	Effects_Main                                            [mainclass]
       Class containing variables and functions used to display special effects
	   on public pages, such as profiles and posts.
                                                                             */
class Effects_Main extends Effects_Shop
{
	var $users			= array();
	var $cfg_cache		= array();
	var $var_cache		= array();
	var $orig_word		= array();
	var $replace_word	= array();
	var $enabled		= TRUE;
	var $cfg			= array();

/*	Effects_Main                                            [efmain]
       Constructor function that handles basic set up of the class.

    Arguments:
       None.

    Return:
       None.
                                                                             */
	function Effects_Main()
	{
		// Get word censor data.
		obtain_word_list($this->orig_word, $this->replace_word);

		global $board_config;
		if( empty($board_config['effects_enable']) )
		{
			$this->enabled = false;
		}

		// Get configuration.
		$this->cfg = array(
			'namechange'	=>	$board_config['effects_namechange'],
			'titlechange'	=>	$board_config['effects_titlechange'],
			'avatar'		=>	$board_config['effects_avatar'],
			'signature'		=>	$board_config['effects_signature'],
			'title'			=>	$board_config['effects_title'],
			'rank_replace'	=>	$board_config['effects_rank_replace'],
			'admin'			=>	$board_config['effects_admin'],
			'moderator'		=>	$board_config['effects_moderator'],
			'name_color'	=>	$board_config['effects_name_color'],
			'name_glow'		=>	$board_config['effects_name_glow'],
			'name_shadow'	=>	$board_config['effects_name_shadow'],
			'title_color'	=>	$board_config['effects_title_color'],
			'title_glow'	=>	$board_config['effects_title_glow'],
			'title_shadow'	=>	$board_config['effects_title_shadow']
		);

		$this->Effects_Shop();
	}

/*	censor                                                  [censor]
       Passes a string through the phpBB word censors, to prevent display of
	   unwanted text.

    Arguments:
       $string	- A string to be checked in the censors.

    Return:
       string  $string  The censored argument.
                                                                             */
	function censor($string)
	{
		if( !empty($this->orig_word) )
		{
			$string = preg_replace($this->orig_word, $this->replace_word, $string);
		}
		return $string;
	}

/*	cache_user                                              [cacheuser]
       Pulls a member's information and stores it in an array for later use.
	   This reduces database load by allowing us to query the database for a
	   member only once in each script call, no matter how many times we need
	   their information.

    Arguments:
       $user_id	- A member's user_id number.

    Return:
       mixed  $user  Typically an array of user info, may be FALSE
						if no such user exists.
                                                                             */
	function cache_user($user_id)
	{
		if( !isset($this->users[$user_id]) )
		{
			$user = get_userdata($user_id);
			$this->users[$user_id] = $user;
		}
		else
		{
			$user = $this->users[$user_id];
		}
		return $user;
	}

/*	check_immunity                                          [chkimmun]
       Checks to see if a member has immunity, allowing them to use certain
	   effects without purchasing them.  Intended to allow admins and
	   moderators to use signatures, etc., without spending any points.

    Arguments:
       $user_id	- A member's user_id number.

    Return:
       boolean  $return_val  True or False.
                                                                             */
	function check_immunity($user_id)
	{
		if( isset($this->cfg_cache[$user_id]['immunity']) )
		{
			return $this->cfg_cache[$user_id]['immunity'];
		}
		$this->cache_user($user_id);
		$return_val = FALSE;
		if( $this->cfg['admin'] || $this->cfg['moderator'] )
		{
			$user = $this->cache_user($user_id);
			if( $user['user_level'] == ADMIN && $this->cfg['admin'] )
			{
				$return_val = TRUE;
			}
			if( $user['user_level'] == MOD && $this->cfg['moderator'] )
			{
				$return_val = TRUE;
			}
			// Cache it!
			$this->cfg_cache[$user_id]['immunity'] = $return_val;
			return $return_val;
		}
		else
		{
			return $return_val;
		}
	}

/*	post_check                                              [postchk]
       Builds an array of variables to append to the array used in the main
	   $template->assign_block_vars() "postrow" call in viewtopic.php.  We
	   cannot send these variables directly to the template from here without
	   printing out a blank post row.
	   The appending of our array here should overwrite any template variables
	   with matching names in viewtopic.php. So, for example, we'll be
	   overwriting the poster's name and possible any special effects on it
	   from other mods.

    Arguments:
       $post_info	- An array of post information.
						Referenced as $postrow[$i] in viewtopic.php.

    Return:
       array  An array, either empty or containing variables to be appended
				to the array of template variables in viewtopic.php.
                                                                             */
	function post_check($post_info)
	{
		if( !$this->enabled || $post_info['user_id'] == ANONYMOUS )
		{
			return array();	// Effects are turned off!
		}

		$user_id = $post_info['user_id'];
	
		// Check the cached list of post effects.
		if( isset($this->var_cache[$user_id]))
		{
			return $this->var_cache[$user_id];
		}

		$user = $this->cache_user($user_id);
		$post_info += $user;

		// Not cached, we have to check everything
		$vars = array();
		if( !$this->check_privilege($user_id, 'avatar') )
		{
			$vars['POSTER_AVATAR'] = '';
		}
		if( !$this->check_privilege($user_id, 'signature') )
		{
			$vars['SIGNATURE'] = '';
		}

		$vars += $this->build_title($post_info, '100');

		// [conflictfix]
		// Pull the global $poster to inherit any existing username colors or
		// effects. This might cause conflicts with other mods. If you have
		// problems and want to disable username effects, add two slashes (//)
		// to the beginning of the next line or two.
		$post_info['username'] = $GLOBALS['poster'];
		$vars['POSTER_NAME'] = $this->build_name($post_info, '100');

		// Now add all this to the cache.
		$this->var_cache[$user_id] = $vars;

		return $vars;
	}


/*	pm_check                                                [pmchk]
       Applies effects to private messages, much the same as post_check does for
	   topics.

    Arguments:
       None.

    Return:
       None.
                                                                             */
	function pm_check()
	{
		if( !$this->enabled )
		{
			return;	// Effects are turned off!
		}

		global $username_from, $username_to, $user_id_from, $user_id_to, $template;
		$vars = array();
		if( !$this->check_privilege($user_id_from, 'avatar') )
		{
			$vars['POSTER_AVATAR'] = '';
		}

		// [conflictfix2]
		// Pull the global username values to inherit any existing username
		// colors or effects. This might cause conflicts with other mods. If you
		// have problems and want to disable username effects, add two slashes
		// (//) to the beginning of every line below this, up to the end marker.
		$user_from = $this->cache_user($user_id_from);
		$user_to = $this->cache_user($user_id_to);
		$user_from['username'] = $username_from;
		$user_to['username'] = $username_to;
		$vars['MESSAGE_FROM'] = $this->build_name($user_from, '100');
		$vars['MESSAGE_TO'] = $this->build_name($user_to, '100');
		// End of [conflictfix2]

		$template->assign_vars($vars);
	}

/*	profile_check                                           [prflchk]
       Adds effects in user profiles. Unlike post_check(), this function can
	   send variables to the template directly without negative effects, so
	   this is a little easier to work with.  But again, we'll probably
	   overwrite any username effects from other modifications.
	   Includes support for signatures, although this may or may not work
	   with mods that display signatures in profiles.

    Arguments:
       $profiledata	- Array of member information, from usercp_viewprofile.php.

    Return:
       None.
                                                                             */
	function profile_check($profiledata)
	{
		if( !$this->enabled || $profiledata['user_id'] == ANONYMOUS )
		{
			return;	// Effects are disabled!
		}

		global $template, $lang;
		$new_name = $this->build_name($profiledata, '5');
		$vars = array(
			'USERNAME'	=>	$new_name,
			'L_VIEWING_PROFILE' => sprintf($lang['Viewing_user_profile'], $new_name), 
			'L_ABOUT_USER' => sprintf($lang['About_user'], $new_name), 
		);

		$vars += $this->build_title($profiledata, '100');
		if( !$this->check_privilege($user_id, 'avatar') )
		{
			$vars['AVATAR_IMG'] = '';
		}
		if( !$this->check_privilege($user_id, 'signature') )
		{
			$vars['SIGNATURE'] = '';
		}
		$template->assign_vars($vars);
	}

/*	check_privilege                                         [chkpriv]
       Checks to see if a user is allow to use a privilege, such as
	   avatars and signatures.

    Arguments:
       $user_id	- A member's user_id number.
       $priv	- String name of the privilege to check.

    Return:
       boolean  True or false.
                                                                             */
	function check_privilege($user_id, $priv)
	{
		if( !$this->enabled || $this->check_immunity($user_id) )
		{
			return TRUE;	// Effect shop is off or the user has immunity
		}

		// Check the cached list of users
		if( isset($this->cfg_cache[$user_id]) )
		{
			if( isset($this->cfg_cache[$user_id][$priv]) && $this->cfg_cache[$user_id][$priv] === FALSE )
			{
				return FALSE;
			}
			return TRUE;
		}

		$user = $this->cache_user($user_id);
		if( !empty($this->cfg[$priv]) && empty($user['effects_' . $priv]) )
		{
			$return_val = FALSE;
		}
		else
		{
			$return_val = TRUE;
		}
		$this->cfg_cache[$user_id][$priv] = $return_val;
		return $return_val;
	}

/*	build_name                                              [bldname]
       Adds special effects to a member's username.

    Arguments:
       $info	- An array of user information, such as from get_userdata().
					Can also be a user_id number, in which case an array will
					be fetched.
       $width	- Integer to be used in a CSS width : X; property.

    Return:
       string  $user_name  The username with special effects added.
                                                                             */
	function build_name($info, $width)
	{
		if( !$this->enabled || $info['user_id'] == ANONYMOUS )
		{
			return $info['username'];	// Effect shop is off or the user is a guest
		}

		if( !is_array($info) || !isset($info['effects_name_color']) )
		{
			// Get user info array
			$info = $this->cache_user($info);
		}
		$user_name = $info['username'];
		$span_style = '';
		if( !empty($this->cfg['name_color']) && !empty($info['effects_name_color']) )
		{
			$span_style .= "color: #{$info['effects_name_color']};";
		}

		if( !empty($this->cfg['name_shadow']) && !empty($info['effects_name_shadow']) )
		{
			$span_style .= $this->add_shadow($info['effects_name_shadow'], $width);
		}

		if( !empty($this->cfg['name_glow']) && !empty($info['effects_name_glow']) )
		{
			$span_style .= $this->add_glow($info['effects_name_glow'], $width);
		}

		if( !empty($span_style) )
		{
			$user_name = '<span style="' . $span_style . '">' . $user_name . '</span>';
		}

		return $user_name;
	}

/*	build_title                                             [bldtitle]
       Builds an array containing a custom title entry, and possibly a rank
	   entry, with special effects applied. The array is meant to be sent to
	   the template object later after this function is called.

    Arguments:
       $user	- An array of user information, such as from get_userdata().
					Can also be a user_id number, in which case an array will
					be fetched.
       $width	- Integer to be used in a CSS width : X; property.

    Return:
       array  $vars  The rank and/or title, with effects applied.
                                                                             */
	function build_title($user, $width)
	{
		$vars = array();
		$span_style = '';
		if( !$this->enabled || $user['user_id'] == ANONYMOUS )
		{
			return $vars;	// Effect shop is off or the user is a guest
		}
		if( !is_array($user) || !isset($user['effects_title_color']) )
		{
			// Get user info array
			$user = $this->cache_user($user);
		}
		if( !empty($this->cfg['title_color']) && !empty($user['effects_title_color']) )
		{
			$span_style .= "color: #{$user['effects_title_color']};";
		}

		if( !empty($this->cfg['title_shadow']) && !empty($user['effects_title_shadow']) )
		{
			$span_style .= $this->add_shadow($user['effects_title_shadow'], $width);
		}

		if( !empty($this->cfg['title_glow']) && !empty($user['effects_title_glow']) )
		{
			$span_style .= $this->add_glow($user['effects_title_glow'], $width);
		}

		if( !empty($this->cfg['title']) && !empty($user['effects_title']) )
		{
			// Check for record of title change in transaction table
			$alt = '';
			global $db, $lang;
			$sql = 'SELECT u.username FROM ' . TRANS_TABLE . " t, " . USERS_TABLE . " u
					WHERE t.target_id = {$user['user_id']}
						AND t.value='Custom Title Change'
						AND t.target_name = '" . $this->secure_sql($user['effects_title']) . "'
						AND t.user_id = u.user_id
					ORDER BY t.timestamp DESC
					LIMIT 1";
			if( $result = $db->sql_query($sql) )
			{
				if( $row = $db->sql_fetchrow($result) )
				{
					$alt = sprintf($lang[$this->lp . 'TITLE_CHANGED_BY'], $row['username']);
					$alt = ' alt="' . $alt . '" title="' . $alt . '"';
				}
			}
					
			$title = stripslashes($this->censor($user['effects_title']));
			if( !empty($span_style) )
			{
				$title = '<span style="' . $span_style . '"' . $alt . '>' . $title . '</span>';
			}

			if( !empty($this->cfg['rank_replace']) && $user['effects_rank_replace'] == 1 )
			{
				$vars['POSTER_RANK'] = $title;
			}
			else
			{
				$vars['POSTER_EFFECT_TITLE'] = $title;
			}
		}
		elseif( !empty($span_style) )
		{
			$title = $GLOBALS['poster_rank'];
			$title = '<span style="' . $span_style . '">' . $title . '</span>';
			$vars['POSTER_RANK'] = $title;
		}

		return $vars;
	}

/*	add_glow                                                [addglow]
       Builds CSS for a glow effect, with variable colors and width.

    Arguments:
       $color	- Color to be used in the glow.
       $width	- Width of the glow effect.

    Return:
       string  A string of CSS code to trigger the glow effect.
                                                                             */
	function add_glow($color, $width)
	{
		return sprintf($this->glow_base, $color, $width);
	}

/*	add_shadow                                              [addshad]
       Builds CSS for a shadow effect, with variable colors and width.

    Arguments:
       $color	- Color to be used in the shadow.
       $width	- Width of the shadow effect.

    Return:
       string  A string of CSS code to trigger the shadow effect.
                                                                             */
	function add_shadow($color, $width)
	{
		return sprintf($this->shadow_base, $color, $width);
	}

/*	profile_edit_check                                      [prfedchk]
       Checks permissions for display of options on registration and profile
	   editing forms.  Only compatible with original phpBB style forms.
	   Profile Control Panel, Categories Hierarchy, etc., are not supported.
	   I have no current plans to add support for those packages in this
	   function.

    Arguments:
       $user	- An array of user information, such as $userdata.

    Return:
       array  $check  Boolean flags indicating if permissions are allowed.
                                                                             */
	function profile_edit_check($user)
	{
		global $template, $mode;
		$check = array('avatar' => TRUE, 'signature' => TRUE);
		if( $this->enabled )
		{
			if( $mode == 'register' )
			{
				$check = array('avatar' => FALSE, 'signature' => FALSE);
			}
			else
			{
				if ( $this->cfg['avatar'] && !$user['effects_avatar'] )
				{
					$check['avatar'] = FALSE;
				}

				if ( $this->cfg['signature'] && !$user['effects_signature'] )
				{
					$check['signature'] = FALSE;
				}
			}

			if( $this->check_immunity($user['user_id']) )
			{
				$check = array('avatar' => TRUE, 'signature' => TRUE);
			}
		}

		if( $check['signature'] )
		{
			$template->assign_block_vars('effects_signature', array());
		}
		return $check;
	}


/*	check_namechange                                        [chknmchg]
       Determines if a user has permissions needed to change their username.

    Arguments:
       $user	- An array of user information, such as $userdata.

    Return:
       boolean  $ret_val  TRUE/FALSE if the user has the permission or not.
                                                                             */
	function check_namechange($user)
	{
		$ret_val = TRUE;
		if( !$this->enabled || $this->check_immunity($user['user_id']) )
		{
			return $ret_val;
		}
		if ( $this->cfg['namechange'] && !$user['effects_namechange'] )
		{
			$ret_val = FALSE;
		}
		return $ret_val;
	}
}

?>