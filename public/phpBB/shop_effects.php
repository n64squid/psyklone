<?php
/*-----------------------------------------------------------------------------
    Effects Store for Shop 3 - A phpBB Add-On
  ----------------------------------------------------------------------------
    shop_effects.php
       Store script for Store
    File Version: 1.2.2
    Begun: July, 2006                 Last Modified: October 10, 2006
  ----------------------------------------------------------------------------
    Copyright 2006 by Jeremy Rogers.  Please read the license.txt included
    with the phpBB Add-On listed above for full license and copyright details.
-----------------------------------------------------------------------------*/

define('IN_PHPBB', true);
define('IN_EFFECTS_SHOP', true);
$phpbb_root_path = './';
include($phpbb_root_path . 'extension.inc');
include($phpbb_root_path . 'common.' . $phpEx);

// Session management
$userdata = session_pagestart($user_ip, PAGE_INDEX);
init_userprefs($userdata);

require_once($phpbb_root_path . 'mods/shopaddons/effects.' . $phpEx);

if( !$userdata['session_logged_in'] )
{
	redirect(append_sid("login.$phpEx?redirect=shop_effects.$phpEx", true));
}

if( !$board_config['effects_enable'] )
{
	message_die(GENERAL_MESSAGE, $lang['EFFECTS_DISABLED']);
}

$effects->get_currency();
$cash_field = $effects->currency['field'];

$settings = array(
	'effects_avatar'		=>	'privs',
	'effects_signature'		=>	'privs',
	'effects_title'			=>	'privs',
	'effects_rank_replace'	=>	'privs',
	'effects_namechange'	=>	'privs',
	'effects_titlechange'	=>	'privs',
	'effects_name_color'	=>	'colors',
	'effects_name_glow'		=>	'colors',
	'effects_name_shadow'	=>	'colors',
	'effects_title_color'	=>	'colors',
	'effects_title_glow'	=>	'colors',
	'effects_title_shadow'	=>	'colors'
);

$sql = 'SELECT * FROM ' . SHOP_TABLE . " WHERE url='" . $effects->get_filename() . "' LIMIT 1";
if( !$result = $db->sql_query($sql) )
{
	message_die(GENERAL_ERROR, 'Could not locate an effects shop!', '', __LINE__, __FILE__, $sql);
}
$row = $db->sql_fetchrow($result);
$db->sql_freeresult($result);
$page_title = $shopname	= $row['shopname'];
$parent		= $row['district'];
$shop_location = array();
$shop_location[$lang['shop_list']] = 'shop.' . $phpEx;
// Get district
if( !empty($parent) )
{
	$sql = 'SELECT * FROM ' . SHOP_TABLE . "
			WHERE district='$parent'
				AND d_type = 1
			LIMIT 1";
	if( !$result = $db->sql_query($sql) )
	{
		message_die(GENERAL_ERROR, 'Could not locate a district!', '', __LINE__, __FILE__, $sql);
	}
	$row = $db->sql_fetchrow($result);
	$db->sql_freeresult($result);
	$shop_location[$row['shopname']] = "shop.".$phpEx."?action=district&d=" . $row['district'];
}
$shop_location[$shopname] = 'shop_effects.' . $phpEx;

$return_links = sprintf($lang['EFFECTS_RETURN'], '<a href="' . append_sid('shop_effects.' . $phpEx) . '">', '<a href="' . append_sid('shop.' . $phpEx) . '">', '</a>', $shopname);

if( isset($_POST['submit']) )
{
	// The form has been submitted. Let's see what we've got here...
	$field_sql = $buy = $update = array();
	$total = 0;
	foreach($settings as $v=>$k)
	{
		$field = str_replace('effects_', '', $v);
		if( isset($_POST[$field]) )
		{
			$effects->secure_vars($field);
			// Check for changing other user's custom title
			if( $field == 'titlechange' && isset($_POST['username']) )
			{
				if( empty($board_config[$v]) )
				{
					message_die(GENERAL_MESSAGE, $lang['EFFECTS_NOT_AVAILABLE'] . $return_links);
				}
				if( $other_user = get_userdata($_POST['username'], true) )
				{
					$effects->censor_block_title($field);
					$sql = 'UPDATE ' . USERS_TABLE . " SET effects_title='" . $effects->secure_sql($effects->vars[$field]) . "' WHERE user_id = '{$other_user['user_id']}'";
					if( !$db->sql_query($sql) )
					{
						message_die(GENERAL_ERROR, 'Could not update effect fields!', '', __LINE__, __FILE__, $sql);
					}
					$effects->record_transaction('buy', "Custom Title Change", $other_user['user_id'], $effects->vars[$field], 'Previous Title: ' . $other_user['effects_title']);
					$total += $board_config[$v];
				}
			}
			if( !isset($userdata[$v]) || $effects->vars[$field] == $userdata[$v] )
			{
				continue;
			}
			// Field has been changed.
			$effects->secure_colors($field, $k, true);
			$effects->censor_block_title($field);
			if( !empty($userdata[$v]) )
			{
				$update[$v] = $effects->vars[$field];
			}
			else
			{
				$buy[$v] = $effects->vars[$field];
			}
		}
	}

	// Ok, now we should have the things they want to buy and update.
	// First, let's process all the stuff they want to buy.
	if( !empty($buy) )
	{
		foreach($buy as $k=>$v)
		{
			if( empty($board_config[$k]) )
			{
				message_die(GENERAL_MESSAGE, $lang['EFFECTS_NOT_AVAILABLE'] . $return_links);
			}
			if( empty($v) )
			{
				message_die(GENERAL_MESSAGE, $lang['EFFECTS_BUY_BLANK'] . $return_links);
			}
			$total += $board_config[$k];
		}
		// Can they afford all the things they want to buy?
		if( $userdata[$cash_field] < $total )
		{
			message_die(GENERAL_MESSAGE, $lang['EFFECTS_NOT_AFFORD'] . $return_links);
		}
		// If we get to here, then there should be no problem. Let's give them
		// the effects!
		foreach($buy as $k=>$v)
		{
			$field_sql[] = "$k = '" . $effects->secure_sql($v) . "'";
		}
	}

	if( !empty($update) )
	{
		foreach($update as $k=>$v)
		{
			if( empty($board_config[$k]) )
			{
				message_die(GENERAL_MESSAGE, $lang['EFFECTS_NOT_AVAILABLE'] . $return_links);
			}
			$field_sql[] = "$k = '" . $effects->secure_sql($v) . "'";
		}
	}

	if( !empty($field_sql) || !empty($total) )
	{
		$total_sql  = ( !empty($total) )     ? "$cash_field = $cash_field - $total" : '';
		if( !empty($field_sql) )
		{
			$field_sql  = implode(', ', $field_sql);
			$field_sql .= ( !empty($total_sql) ) ? ', ' : '';
		}
		else
		{
			$field_sql = '';
		}
		$sql = 'UPDATE ' . USERS_TABLE . " SET $field_sql $total_sql
			WHERE user_id={$userdata['user_id']}";
		if( !$db->sql_query($sql) )
		{
			message_die(GENERAL_ERROR, 'Could not update effect fields!', '', __LINE__, __FILE__, $sql);
		}
		if( !empty($field_sql) )
		{
			$effects->record_transaction('buy', "Effects Update", '', '', $field_sql);
		}

		message_die(GENERAL_MESSAGE, $lang['EFFECTS_UPDATED'] . $return_links);
	}
}

// Get the rank title
$rank_title = '';
if( !empty($userdata['effects_title']) && $userdata['effects_rank_replace'] == 1 )
{
	$rank_title = $userdata['effects_title'];
}
else
{
	$sql = 'SELECT *
		FROM ' . RANKS_TABLE . "
		ORDER BY rank_special, rank_min";
	if ( !$result = $db->sql_query($sql) )
	{
		message_die(GENERAL_ERROR, 'Could not obtain ranks information', '', __LINE__, __FILE__, $sql);
	}

	$ranksrow = $db->sql_fetchrowset($result);
	$rank_count = count($ranksrow);
	$db->sql_freeresult($result);
	if ( $userdata['user_rank'] )
	{
		for($i = 0; $i < $rank_count; $i++)
		{
			if ( $userdata['user_rank'] == $ranksrow[$i]['rank_id'] && $ranksrow[$i]['rank_special'] )
			{
				$rank_title = $ranksrow[$i]['rank_title'];
			}
		}
	}
	else
	{
		for($i = 0; $i < $rank_count; $i++)
		{
			if ( $userdata['user_posts'] >= $ranksrow[$i]['rank_min'] && !$ranksrow[$i]['rank_special'] )
			{
				$rank_title = $ranksrow[$i]['rank_title'];
			}
		}
	}
}
if( empty($rank_title) )
{
	$rank_title = $lang['Poster_rank'];
}

$template->set_filenames(array(
	'body'		=> 'shop/effects.tpl',
	'personal'	=> 'shop/personal.tpl'
));

foreach($shop_location as $k=>$v)
{
	$template->assign_block_vars('backlinks', array(
		'URL'	=>	append_sid($v),
		'TEXT'	=>	$k
	));
}
$template->assign_vars(array(
	'L_SHOP_TITLE'		=>	$shopname,
	'POINTS_NAME'		=>	$effects->currency['name'],
	'CURRENT_CASH'		=>	$userdata[$cash_field],
	'L_USERNAME'		=>	$lang['Username'],
	'L_FIND_USERNAME'	=>	$lang['Find_username'],
	'L_RANK'			=>	$lang['Poster_rank'],
	'L_INVENTORY'		=>	$lang['shop_your_inv'],
	'L_PERSONAL_INFO'	=>	$lang['shop_personal_info'],
	'U_INVENTORY'		=>	append_sid("shop.$phpEx?action=inventory&searchid=" . $userdata['user_id']),
	'U_SEARCH_USER'		=>	append_sid("search.$phpEx?mode=searchuser"),

	'S_ACTION'			=>	append_sid('shop_effects.' . $phpEx),
	'MY_USERNAME'		=>	$userdata['username'],
	'MY_RANK'			=>	$rank_title,
	'GLOW_BASE'			=>	$effects->glow_base,
	'SHADOW_BASE'		=>	$effects->shadow_base,

	'L_EXPLAIN'			=>	$lang['EFFECTS_PUBLIC_EXPLAIN'],
	'L_COLORS'			=>	$lang['EFFECTS_COLORS'],
	'L_PRIVS'			=>	$lang['EFFECTS_PRIVS'],
	'L_COST'			=>	$lang['EFFECTS_COST'],
	'L_STATUS'			=>	$lang['EFFECTS_STATUS'],
	'L_PURCHASE'		=>	$lang['EFFECTS_PURCHASE'],
	'L_BUY'				=>	$lang['EFFECTS_BUY'],
	'L_UPDATE'			=>	$lang['EFFECTS_UPDATE'],
	'L_PREVIEW'			=>	$lang['EFFECTS_PREVIEW'],
	'L_OWN_EFFECT'		=>	$lang['EFFECTS_OWN'],
	'L_ON'				=>	$lang['EFFECTS_ON'],
	'L_OFF'				=>	$lang['EFFECTS_OFF']
));

$vars = array();
$privs = $colors = FALSE;

foreach($settings as $v=>$k)
{
	if( !empty($board_config[$v]) )
	{
		if( !$$k )
		{
			$$k = TRUE;
			$template->assign_block_vars($k, array());
		}

		switch( $k )
		{
			case 'privs':
				$block = $k . '.effect_priv';
				break;
			case 'colors':
				$block = $k . '.effect_color';
			$template->assign_block_vars('color_preview', array(
					'VAR_VALUE' => str_replace('effects_', '', $v)
				));
				break;
		}
		$template->assign_block_vars($block, array(
			'VALUE'				=>	$userdata[$v],
			'FIELD'				=>	str_replace('effects_', '', $v),
			'L_EFFECT_NAME'		=>	$lang[strtoupper($v) . '_PUBLIC'],
			'L_EFFECT_EXPLAIN'	=>	(( isset($lang[strtoupper($v) . '_PUBLIC_EXPLAIN']) ) ? $lang[strtoupper($v) . '_PUBLIC_EXPLAIN']: ''),
			'EFFECT_COST'		=>	number_format($board_config[$v])
		));

		if( empty($userdata[$v]) )
		{
			$template->assign_block_vars($block . '.sell_effect', array());
		}
		else
		{
			$template->assign_block_vars($block . '.bought_effect', array());
		}

		switch( $v )
		{
			case 'effects_titlechange':
				$template->assign_block_vars($block . '.title_other', array());
			case 'effects_title':
				$template->assign_block_vars($block . '.title_effect', array());
				break;
			case 'effects_rank_replace':
				$template->assign_block_vars($block . '.rank_effect', array(
					'YES_SELECTED'	=>	($userdata[$v] == 1) ? 'checked="checked"': '',
					'NO_SELECTED'	=>	($userdata[$v] == 2) ? 'checked="checked"': '',
				));
				break;
			default:
				if( empty($userdata[$v]) )
				{
						$template->assign_block_vars($block . '.priv_effect', array());
				}
				else
				{
					$template->assign_block_vars($block . '.priv_effect_bought', array());
				}
				break;
		}
	}
}
$template->assign_vars($vars);

include($phpbb_root_path . 'includes/page_header.' . $phpEx);
$template->assign_var_from_handle('PERSONAL', 'personal');
$template->pparse('body');
include($phpbb_root_path . 'includes/page_tail.' . $phpEx);

?>