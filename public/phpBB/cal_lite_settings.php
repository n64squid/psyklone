<?php
/*********************************************
*	Calendar Lite
*
*	$Author: martin $
*	$Date: 2005-09-04 20:51:20 +0100 (Sun, 04 Sep 2005) $
*	$Revision: 29 $
*
*********************************************/

/*###############################################################
## Mod Title: 	phpBB2 Calendar
## Mod Version: 1.4.6
## Author: 	WebSnail < Martin Smallridge >
## SUPPORT: 	http://www.snailsource.com/forum/ 
## Description: Variable settings for Calendar.php
##
##
## NOTE: Please read readme.txt
###############################################################*/

/*##########################################
##                 STOP                    #
##  DO NOT MODIFY ANYTHING IN THIS FILE    #
##########################################*/

$cal_version = "[1.4.6]";
$cal_lang_file = "lang_calendar.php";

define('CAL_TABLE', ($table_prefix . 'calendar'));
define('CAL_CONFIG', ($table_prefix . 'cal_config'));
?>