<?php
/*********************************************
*	Calendar Lite
*
*	$Author: martin $
*	$Date: 2005-07-27 23:27:24 +0100 (Wed, 27 Jul 2005) $
*	$Revision: 22 $
*
*********************************************/

/*###############################################################
## Mod Title: 	Installation/Update Script for Calendar Lite
## Mod Version: 1.4.6
## Author: 	WebSnail < Martin Smallridge >
## SUPPORT: 	http://www.snailsource.com/forum/ 
## Description: Variable settings for Calendar.php
##
##
## NOTE: Please read readme.txt
###############################################################*/

// Security Check
if ( isset($HTTP_GET_VARS['caluser']) || isset($HTTP_POST_VARS['caluser']) || isset($caluser)) {
	// Failed the test... Someone tried to spoof as a user.
}
else if ( isset($HTTP_GET_VARS['userdata']) || isset($HTTP_POST_VARS['userdata']) || isset($userdata)) {
	// Failed the test... Someone tried to spoof as a user.
}
else {
	// Passed.
	define('IN_PHPBB', true);
}


$currentver = '1.4.6';

// Remember to update with old versions as new ones come out.
$old_versions = array('1.2.2', '1.4.1', '1.4.2', '1.4.3', '1.4.4', '1.4.5');


$filename = basename(__FILE__);
$phpbb_root_path = './';
$language = 'english';


//############################
// Start phpBB Session

// Test we are in the correct directory.
if(!file_exists($phpbb_root_path . 'extension.inc')) {
	echo "You appear to be trying to run this installer from the wrong directory. <br /><br />
		Please refer to the instructions provided at:
		<a href='http://www.snailsource.com/callite/install.html'>Snailsource.com</a>";
	exit;
}
include_once($phpbb_root_path . 'extension.inc');

// Test if files are available for installer.
if(!file_exists($phpbb_root_path . 'cal_lite_settings.'.$phpEx) || !file_exists($phpbb_root_path . 'cal_install/cal_convert_fn.'.$phpEx)) {	
	echo "Can't find the cal_settings.php and/or cal_convert_fn.php files required for installer <br /><br />
		Please refer to the instructions provided at:
		<a href='http://www.snailsource.com/callite/install.html'>Snailsource.com</a>";
	exit;
}

include_once($phpbb_root_path . 'config.'.$phpEx);
include_once($phpbb_root_path . 'common.'.$phpEx);
include_once($phpbb_root_path . 'includes/functions.'.$phpEx);
include_once($phpbb_root_path . 'includes/functions_selects.'.$phpEx);
include_once($phpbb_root_path . 'includes/sql_parse.'.$phpEx);

include_once($phpbb_root_path . 'cal_lite_settings.'.$phpEx);	// Variables that users can change (tablenames, etc..)
include_once($phpbb_root_path . 'cal_install/cal_convert_fn.'.$phpEx);	// Functions & variables needed to complete the installation

$userdata = session_pagestart($user_ip, PAGE_INDEX, $session_length);
init_userprefs($userdata);

include($phpbb_root_path.'language/lang_' . $language . '/lang_main.'.$phpEx);
include($phpbb_root_path.'language/lang_' . $language . '/lang_admin.'.$phpEx);
include($phpbb_root_path.'language/lang_' . $language . '/lang_calendar.'.$phpEx);

//############################
// Setup templates for display

$template->set_filenames(array(
	"body" => "cal_install.tpl")
	);
$template->assign_vars(array(
	"L_INSTALLATION" => 'Calendar Lite Installation',
	"S_FORM_ACTION" => $filename)
	);


/*
// Disabled the admin check as causing too many problems.

//############################
// Ensure Admin only runs this

// If register_globals get user to login.
if(!$userdata['session_logged_in']) {
	if ( isset($HTTP_COOKIE_VARS[$board_config['cookie_name'] . '_sid']) || isset($HTTP_GET_VARS['sid']) ) {
		$session_id = isset($HTTP_COOKIE_VARS[$board_config['cookie_name'] . '_sid']) ? $HTTP_COOKIE_VARS[$board_config['cookie_name'] . '_sid'] : $HTTP_GET_VARS['sid'];
	}
	$vars = '?redirect='.$filename;
	$vars .= ( $session_id ) ? '&sid='.$session_id : '';
	redirect('login.'.$phpEx.$vars);
}
// Logged in and not the Admin... 2 fingers to you buddy!
if($userdata['user_level'] != ADMIN) {
	$error = $lang['Not_Authorised'];
	cal_update_err($error);
}
*/

//###########################
// What are we doing?

if(!isset($HTTP_POST_VARS['install_action'])) {
	thisdefault();
}
else if ($HTTP_POST_VARS['install_action'] == 0) {
	$ver = test();
	if($ver == '') {
		install();
	}
	elseif ($ver == 'empty') {
		$error = "<b>You appear to have an empty calendar installed.</b><br /><br /> 
			Recommend you check (and if empty) wipe your existing \"calendar\" SQL table and install from scratch";
		cal_update_err($error);
	}
	elseif ($ver == $currentver) {
		$error = '<b>You already appear to have the latest calendar installed.</b><br /><br />';
		cal_update_err($error);
	}
	elseif ($ver == '2.0.1' || $ver == '2.0.21') {
		$error = "<b>You appear to have Calendar Pro installed already.</b><br /><br /> 
			Why you want to downgrade to Calendar Lite is a mystery";
		cal_update_err($error);
	}
	else {
		$error = "<b>A version of Calendar is already installed.</b><br /><br /> 
			Recommend you choose 'upgrade' instead ";
		cal_update_err($error);
	}
}
else if ($HTTP_POST_VARS['install_action'] == 1){
	$ver = test();

	// while(list(,$this) = each($old_versions)) {	//PHP5 break.
	foreach($old_versions AS $test_ver) {
		if($test_ver == $ver) {
			upgrade($ver);
			exit;
		}
	}
	if ($ver == $currentver) {
		$error = '<b>You already appear to have the latest Calendar Lite version installed.</b><br /><br />';
		cal_update_err($error);
	}
	elseif ($ver == 'empty') {
		$error = "<b>You appear to have an empty Calendar installed.</b><br /><br /> 
			Recommend you wipe your existing SQL tables and install from scratch";
		cal_update_err($error);
	}
	elseif ($ver == '2.0.1' || $ver == '2.0.21') {
		$error = "<b>You appear to have Calendar Pro installed already.</b><br /><br /> 
			Why you'd want to downgrade to Calendar Lite 1.4.1 is a mystery";
		cal_update_err($error);
	}
	else {
		$error = "<b>You don't appear to have a valid calendar version installed.</b><br /><br /> 
			Recommend you wipe your existing \"calendar\" SQL table and install from scratch";
		cal_update_err($error);
	}
}
else {
	thisdefault();
}
exit;

//#####################
// Functions Start

function thisdefault() {
	global $template, $lang, $HTTP_GET_VARS;
	// Display the form for any user selections :)
	$lang_options = language_select($language, 'language');


	$upgrade_option = '<select name="install_action">';
	$upgrade_option .= '<option value="0">' . $lang['Install'] . '</option>';
	$upgrade_option .= '<option value="1">' . $lang['Upgrade'] . '</option></select>';
	
	$s_hidden_fields = "<input type='hidden' name='install_step' value='1' />\n<input type='hidden' name='sid' value='".$HTTP_GET_VARS['sid']."'>";

	$instruction_text = 'Please choose whether you wish to install or upgrade Calendar. <br /><br />
		<span class="genmed">NB: This script will check for pre-existing versions of Calendar and advise you (if necessary) 
		whether you can continue with an installation/upgrade.<br />
		If you experience any problems then please visit the Calendar Lite forums at 
		<a href="http://www.snailsource.com/forum/" class="nav">Snailsource.com</a></span>';

	$template->assign_block_vars('switch_stage_one_install', array());
	$template->assign_block_vars('switch_common_install', array());

	$template->assign_vars(array(
		"L_INSTRUCTION_TEXT" => $instruction_text,
		"L_INITIAL_CONFIGURATION" => $lang['Initial_config'], 
		"L_LANGUAGE" => $lang['Default_lang'], 
		"L_UPGRADE" => $lang['Install_Method'],
		"L_SUBMIT" => $lang['Start_Install'], 
		
		"S_LANG_SELECT" => $lang_options, 
		"S_HIDDEN_FIELDS" => $s_hidden_fields,
		"S_UPGRADE_SELECT" => $upgrade_option)
	);
	$template->pparse('body');
	exit;
}



function test() {
	global $db, $lang, $table_prefix, $tablename;
	$ver = '';

	// Test for DB access.
	$sql_test = 'SELECT * FROM '.FORUMS_TABLE;
	if(!$test = @$db->sql_query($sql_test)) {
		$error = "Can't connect to database at all";
		cal_update_err($error);
	}

	// Test for Calendar Table and version information

	$sql_test = 'SELECT config_name, config_value FROM '.CAL_CONFIG.' WHERE config_name = "version"';
	if($query = $db->sql_query($sql_test)) {
		$test = $db->sql_fetchrow($query);
		if(isset($test['config_value'])) {
			// ver 1.4.2+ stores the version number in the cal_config table.
			$ver = $test['config_value'];
		} else {
			// We've found Cal_config so it must be version 1.4.1
			$ver = '1.4.1';
		}
	}
	else {
		// Test for Calendar Table
		$sql_test = 'SELECT * FROM '.CAL_TABLE;
		if($test = $db->sql_query($sql_test)) {
			if($test = $db->sql_fetchrow($test)) {
				if (isset($test['stamp'])) {
					$ver = '1.2.2';
				}
				if (isset($test['event_date'])) {
					$ver = '2.0.1';
				}
				if (isset($test['event_start'])) {
					$ver = '2.0.21';
				}
			}
			else {
				$ver = 'empty';
			}
		}
	}
	return $ver;
}



function install() {
	global $db, $template, $lang, $dbms, $available_dbms, $table_prefix, $tablename, $currentver;
	$dbms_schema = './cal_install/schema/' . $available_dbms[$dbms]['SCHEMA'] . '_141_schema.sql';
	$dbms_basic = './cal_install/schema/' . $available_dbms[$dbms]['SCHEMA'] . '_141_inserts.sql';


//	if( $available_dbms[$dbms]['SCHEMA'] != 'mysql' && $available_dbms[$dbms]['SCHEMA'] != 'mssql' ) {

	if( !preg_match("/^(mysql|mssql|postgres)$/", $available_dbms[$dbms]['SCHEMA']) ) {
		$error = "<b>Sorry, only MySQL is currently supported at present.</b><br /><br /> 
			However additional DBMS support will be available shortly as part of this update";
		cal_update_err($error);
	}

	if( $dbms != 'msaccess' ) {

		// Ok we have the db info go ahead and read in the relevant schema
		//

		$report .= 'Add and populate new tables:<br /><br />';
		$report .= install_sql($dbms_schema, $report);
		$report .= 'New tables installed... (<font color=green><b>Done</b></font>)<br />';
		$report .= install_sql($dbms_basic, $report);

		// Add permission fields to user and usergroup tables
	        switch( $dbms )
        	{
			case 'mysql':
			case 'mysql4':
				$inserts[] = 'ALTER TABLE '.GROUPS_TABLE.' ADD group_calendar_perm TINYINT(1) UNSIGNED DEFAULT "0" NOT NULL';
				$inserts[] = 'ALTER TABLE '.USERS_TABLE.' ADD user_calendar_perm TINYINT(1) UNSIGNED DEFAULT "0" NOT NULL';
				break;

			case 'mssql-odbc':
			case 'mssql':

				$inserts[] = 'ALTER TABLE '.GROUPS_TABLE.' ADD group_calendar_perm tinyint NOT NULL CONSTRAINT [DF_" . $table_prefix . "groups_group_calendar_perm] DEFAULT (0) ';

				$inserts[] = 'ALTER TABLE '.USERS_TABLE.' ADD user_calendar_perm tinyint NOT NULL CONSTRAINT [DF_" . $table_prefix . "users_user_calendar_perm] DEFAULT (0) ';
				break;

			case 'postgres';
				$inserts[] = 'ALTER TABLE '.GROUPS_TABLE.' ADD group_calendar_perm smallint';
				$inserts[] = 'ALTER TABLE '.GROUPS_TABLE.' ALTER group_calendar_perm SET DEFAULT 0';

				$inserts[] = 'ALTER TABLE '.USERS_TABLE.' ADD user_calendar_perm smallint';
				$inserts[] = 'ALTER TABLE '.USERS_TABLE.' ALTER user_calendar_perm SET DEFAULT 0';

				$inserts[] = 'UPDATE '.GROUPS_TABLE.' SET group_calendar_perm = 0';
				$inserts[] = 'UPDATE '.USERS_TABLE.' SET user_calendar_perm = 0';
				break; 
		}

		// Add the version number to the cal_config table
		$inserts[] = "INSERT INTO ".CAL_CONFIG." VALUES ('version', '$currentver')";

		$report .= 'Start processing INSERT and ALTER sql updates:<br /><br />';
		$report .= go_sql($inserts, $report);
		$report .= '<br /><br />';

		$sql = 'INSERT INTO '.  CAL_TABLE ." (username, stamp, subject, description, user_id, valid, eventspan) 
			VALUES ('test', '". date("Y-m-d",time()) ."', 'Calendar Lite Installed', 
			'This is just a test event to prove it works. <br /><br />Delete as required :)',	-1 , 'yes', '". date("Y-m-d",time()) ."')";

		$result = $db->sql_query($sql);
		if( !$result ) {
			$db_err = $db->sql_error($result);
			$error .= 'Could not insert test event :: ' . $sql . ' :: ' . __LINE__ . ' :: ' . __FILE__ . '<br />DB_ERROR'.$db_err["message"].'<br />';
			cal_update_err($error);
		}

	}
	// Success..
	$template->assign_block_vars('switch_success_install', array());

	$report .= 'Congratulations, you have successfully installed <b>Calendar Lite</b>.<br /><br />
		If you have any questions, need documentation, or just want to catch up on new releases for CalLite then please
		visit Snailsource at <br /><br /><a href="http://www.snailsource.com/" class="nav">http://www.snailsource.com/</a>
		<br /><br /><center><img src="http://www.snailsource.com/files/'.$currentver.'__16191.jpeg"></center>';

	$template->assign_vars(array(
		"L_INSTRUCTION_TEXT" => '',
		"L_SUCCESS_TITLE" => '** Success **',
		"L_MESSAGE" => $report)
	);

	$template->pparse('body');
	exit;
}


function upgrade($ver) {
	global $db, $template, $lang, $dbms, $available_dbms, $table_prefix, $tablename, $rec_table, $conf_table, $cat_table, $currentver;
	// Upgrade older versions of Calendar to the latest.

	$report = "Commencing Upgrade for Calendar Version: $ver upgrading to Version: $currentver <br/><br />";
	if($ver == '1.2.2') {
		// Update 1.2.2 to 1.4.x

		$dbms_tables = './cal_install/schema/' . $available_dbms[$dbms]['SCHEMA'] . '_122_to_141_schema.sql';
		$dbms_basic = './cal_install/schema/' . $available_dbms[$dbms]['SCHEMA'] . '_141_inserts.sql';

		$inserts = array();
		$drops = array();

		// Install new tables.
		//
		$report .= 'Add and populate new tables:<br /><br />';
		$report .= install_sql($dbms_tables, $report);
		$report .= 'New tables installed... (<font color=green><b>Done</b></font>)<br />';
		$report .= install_sql($dbms_basic, $report);
		$report .= 'New tables populated with required data... (<font color=green><b>Done</b></font>)<br />';
		$report .= '<br/><br />';

		// Convert Calendar Lite to the new version.
		// phpbb_calendar table

		// Add permission fields to user and usergroup tables
		$inserts[] = 'ALTER TABLE '.$table_prefix.'groups ADD group_calendar_perm TINYINT(1) UNSIGNED DEFAULT "0" NOT NULL';
		$inserts[] = 'ALTER TABLE '.$table_prefix.'users ADD user_calendar_perm TINYINT(1) UNSIGNED DEFAULT "0" NOT NULL';

		// change type	description -> text
		$inserts[] = 'ALTER TABLE '. CAL_TABLE.' CHANGE `description` `description` TEXT DEFAULT NULL';

		$report .= 'Start processing INSERT and ALTER sql updates:<br /><br />';
		$report .= go_sql($inserts, $report);
		$report .= '<br /><br />';
	} 
	if($ver == '1.4.1' || $ver == '1.4.2' || $ver == '1.4.3' || $ver == '1.4.4' || $ver == '1.4.5') {
		// nada :)
	}

	// Update/Insert version information
	$sql = 'SELECT * FROM '.CAL_CONFIG.' WHERE config_name = "version"';
	$query = $db->sql_query($sql);
	$row = $db->sql_fetchrow($query);
	if(isset($row['config_value'])) {
		$sql = "UPDATE ".CAL_CONFIG." SET config_value = '$currentver' WHERE config_name = 'version'";
	} else {
		$sql = "INSERT INTO ".CAL_CONFIG." (config_name, config_value) VALUES ('version', '$currentver')";
	}
	$query = $db->sql_query($sql);
	$report .= 'New CalLite Version information inserted/updated<br /><br />';
	$report .= '<br /><br />';

	// Success..
	$template->assign_block_vars('switch_success_install', array());

	$success_message = 'Congratulations, you have successfully installed <b>Calendar Lite</b>.<br /><br />
		If you have any questions, need documentation, or just want to catch up on new releases for Calendar Lite then please
		visit Snailsource at <br /><br /><a href="http://www.snailsource.com/" class="nav">http://www.snailsource.com/</a>
		<br /><br /><center><img src="http://www.snailsource.com/files/'.$currentver.'__16191.jpeg"></center>';

	$template->assign_vars(array(
		"L_INSTRUCTION_TEXT" => '',
		"L_SUCCESS_TITLE" => '** Success **',
		"L_MESSAGE" => ($report.$success_message))
	);

	$template->pparse('body');
	exit;
}



?>