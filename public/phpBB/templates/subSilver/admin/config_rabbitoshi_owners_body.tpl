
<h1>{L_RABBITOSHI_TITLE}</h1>

<p>{L_RABBITOSHI_TEXT}</p>

<form action="{S_RABBITOSHI_ACTION}" method="post">

<table class="forumline" cellpadding="4" cellspacing="1" border="0" align="center" width="80%">
	<tr>
		<td class="row2" align="center">{L_SELECT_OWNER}&nbsp;&nbsp;{S_SELECT_OWNER}&nbsp;&nbsp;<input type="submit" class="mainoption" value="{L_SELECT}" /></td>
	</tr>
</table>

<br clear="all" />

<table class="forumline" cellpadding="4" cellspacing="1" border="0" align="center" width="90%">
	<tr>
		<td class="row1" width="38%"><span class="gen">{L_OWNER}</span></td>
		<td class="row2"><span class="gen">{OWNER}</span></td>
	</tr>
	<tr>
		<td class="row1" width="38%"><span class="gen">{L_OWNER_PET}</span></td>
		<td class="row2"><input class="post" type="text" name="pet_name" size="30" maxlength="255" value="{OWNER_PET}" /></td>
	</tr>
	<tr>
		<td class="row1" width="38%"><span class="gen">{L_PET_TYPE}</span></td>
		<td class="row2"><span class="gen">{PET_TYPE}</span></td>
	</tr>
  	<tr> 
		<td colspan="5" height="1" class="row3"><img src="../templates/subSilver/images/spacer.gif" width="1" height="1" alt="."></td>
  	</tr>
	<tr>
		<td class="row1" width="38%"><span class="gen">{L_OWNER_PET_HEALTH}</span></td>
		<td class="row2"><input class="post" type="text" name="health" size="8" maxlength="8" value="{OWNER_PET_HEALTH}" /> / {PET_HEALTH}</td>
	</tr>
	<tr>
		<td class="row1" width="38%"><span class="gen">{L_OWNER_PET_HUNGER}</span></td>
		<td class="row2"><input class="post" type="text" name="hunger" size="8" maxlength="8" value="{OWNER_PET_HUNGER}" /> / {PET_HUNGER}</td>
	</tr>
	<tr>
		<td class="row1" width="38%"><span class="gen">{L_OWNER_PET_THIRST}</span></td>
		<td class="row2"><input class="post" type="text" name="thirst" size="8" maxlength="8" value="{OWNER_PET_THIRST}" /> / {PET_THIRST}</td>
	</tr>
	<tr>
		<td class="row1" width="38%"><span class="gen">{L_OWNER_PET_HYGIENE}</span></td>
		<td class="row2"><input class="post" type="text" name="hygiene" size="8" maxlength="8" value="{OWNER_PET_HYGIENE}" /> / {PET_HYGIENE}</td>
	</tr>
  	<tr> 
		<td colspan="5" height="1" class="row3"><img src="../templates/subSilver/images/spacer.gif" width="1" height="1" alt="."></td>
  	</tr>
	<tr>
		<td class="row1" width="60%"><span class="gen">{L_RABBITOSHI_PREFERENCES_NOTIFY}</span></td>
		<td class="row2" align="center" valign="top"><input type="checkbox" name="notify" value="1" {RABBITOSHI_PREFERENCES_NOTIFY_CHECKED} /></td>
	</tr>
	<tr>
		<td class="row1" width="60%"><span class="gen">{L_RABBITOSHI_PREFERENCES_HIDE}</span></td>
		<td class="row2" align="center" valign="top"><input type="checkbox" name="hide" value="1" {RABBITOSHI_PREFERENCES_HIDE_CHECKED} /></td>
	</tr>
	<tr>
		<td class="row1" width="60%"><span class="gen">{L_RABBITOSHI_PREFERENCES_FEED_FULL}</span><br /><span class="gensmall">{L_RABBITOSHI_PREFERENCES_FEED_FULL_EXPLAIN}</span></td>
		<td class="row2" align="center" valign="top"><input type="checkbox" name="feed_full" value="1" {RABBITOSHI_PREFERENCES_FEED_FULL_CHECKED} /></td>
	</tr>
	<tr>
		<td class="row1" width="60%"><span class="gen">{L_RABBITOSHI_PREFERENCES_DRINK_FULL}</span><br /><span class="gensmall">{L_RABBITOSHI_PREFERENCES_DRINK_FULL_EXPLAIN}</span></td>
		<td class="row2" align="center" valign="top"><input type="checkbox" name="drink_full" value="1" {RABBITOSHI_PREFERENCES_DRINK_FULL_CHECKED} /></td>
	</tr>
	<tr>
		<td class="row1" width="60%"><span class="gen">{L_RABBITOSHI_PREFERENCES_CLEAN_FULL}</span><br /><span class="gensmall">{L_RABBITOSHI_PREFERENCES_CLEAN_FULL_EXPLAIN}</span></td>
		<td class="row2" align="center" valign="top"><input type="checkbox" name="clean_full" value="1" {RABBITOSHI_PREFERENCES_CLEAN_FULL_CHECKED} /></td>
	</tr>
	<tr>
		<td class="catBottom" colspan="2" align="center"><input type="submit" name="submit" value="{L_SUBMIT}" class="mainoption" /></td>
	</tr>

</table>

<br clear="all" />

<table class="forumline" cellpadding="4" cellspacing="1" border="0" align="center" width="95%">
	<tr>
		<td class="row2" align="center" colspan="2" ><span class="gen">{L_MANUAL_UPDATE_EXPLAIN}</span></td>
	</tr>
	<tr>
		<td class="catBottom" colspan="4" align="center"><input type="submit" name="update" value="{L_MANUAL_UPDATE}" class="mainoption" /></td>
	</tr>
</table>

</form>
