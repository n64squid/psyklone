<!-- BEGIN quick_reply -->
<script language='JavaScript'>
	function quoteSelection() {

		theSelection = false;
		theSelection = document.selection.createRange().text; // Get text selection

		if (theSelection) {
			// Add tags around selection
			emoticon( '[quote]\n' + theSelection + '\n[/quote]\n');
			document.post.input.focus();
			theSelection = '';
			return;
		}else{
			alert('{L_NO_TEXT_SELECTED}');
		}
	}

	function storeCaret(textEl) {
		if (textEl.createTextRange) textEl.caretPos = document.selection.createRange().duplicate();
	}

	function emoticon(text) {
		if (document.post.input.createTextRange && document.post.input.caretPos) {
			var caretPos = document.post.input.caretPos;
			caretPos.text = caretPos.text.charAt(caretPos.text.length - 1) == ' ' ? text + ' ' : text;
			document.post.input.focus();
		} else {
			document.post.input.value  += text;
			document.post.input.focus();
		}
	}

	function checkForm() {
		formErrors = false;
		if (document.post.input.value.length < 2) {
			formErrors = '{L_EMPTY_MESSAGE}';
		}
		if (formErrors) {
			alert(formErrors);
			return false;
		} else {
			if (document.post.quick_quote.checked) {
				document.post.message.value = document.post.last_msg.value;
			}
			document.post.message.value += document.post.input.value;
			return true;
		}
	}
</script>
<form action='{quick_reply.POST_ACTION}' method='post' name='post' onsubmit='return checkForm(this)'>
<table border="0" cellpadding="0" cellspacing="0" width="70%" height="14" align="center">
  <tr> 
    <td rowspan="1" colspan="1" width="1%" height="14" background="templates/FSDark/images/front/Image2_1x2.gif" align="left"> 
      <img name="Image20" src="templates/FSDark/images/front/Image2_1x1.gif" width="5" height="14" border="0"></td>
    <td rowspan="1" colspan="1" width="98%" height="14" background="templates/FSDark/images/front/Image2_1x2.gif" align="center" valign="top" nowrap>     
</td>
    <td rowspan="1" colspan="1" width="1%" height="14" align="right" background="templates/FSDark/images/front/Image2_1x2.gif"> 
      <img name="Image22" src="templates/FSDark/images/front/Image2_1x3.gif" width="14" height="14" border="0"></td>
  </tr>
</table>	
<table border='0' cellpadding='0' cellspacing='0' width='70%' class='forumline' align='center'>
		<tr>
			<th class='thHead' colspan='2' height='25' align="center"><b>{L_QUICK_REPLY}</b></th>
		</tr>
		<!-- BEGIN user_logged_out -->
		<tr>
			<td class='row2' align='center'><span class='gen'><b>{L_USERNAME}:</b></span></td>
			<td class='row2' width='100%' align="center"><span class='genmed'><input type='text' class='post' tabindex='1' name='username' size='25' maxlength='25' value='' /></span></td>
		</tr>
		<!-- END user_logged_out -->
		<tr>
			<td class='row1'>
			</td>
			<td class='row1' valign='top' align="center">
				<textarea name='input' rows='10' cols='85' wrap='virtual' tabindex='3' class='post' onselect='storeCaret(this);' onclick='storeCaret(this);' onkeyup='storeCaret(this);'></textarea><br>
				<!-- BEGIN smilies -->
				<a href='javascript:emoticon("{quick_reply.smilies.CODE}")'><img src="{quick_reply.smilies.URL}" border=0 alt="{quick_reply.smilies.DESC}" alt="{quick_reply.smilies.DESC}"></a> 
				<!-- END smilies -->
				<br />
				<input type='button' name='quoteselected' class='liteoption' value='{L_QUOTE_SELECTED}' onclick='javascript:quoteSelection()'></td>
		</tr>
		<tr>
			<td class='row2'>
			</td>
			<td class='row2' valign='top' align="center"><span class='gen'>
				<b>{L_OPTIONS}</b><br />
				<input type='checkbox' name='quick_quote'>{L_QUOTE_LAST_MESSAGE}<br>
				<!-- BEGIN user_logged_in -->
				<input type='checkbox' name='attach_sig' {quick_reply.user_logged_in.ATTACH_SIGNATURE}>{L_ATTACH_SIGNATURE}<br>
				<input type='checkbox' name='notify' {quick_reply.user_logged_in.NOTIFY_ON_REPLY}>{L_NOTIFY_ON_REPLY}</td>
				<!-- END user_logged_in -->
		</tr>
		<tr>
			<td class='catBottom' align='center' height='28' colspan='2'>
				<input type='hidden' name='mode' value='reply'>
				<input type='hidden' name='t' value='{quick_reply.TOPIC_ID}'>
				<input type='hidden' name='last_msg' value='{quick_reply.LAST_MESSAGE}'>
				<input type='hidden' name='message' value=''>
				<input type='submit' name='preview' class='liteoption' value='{L_PREVIEW}'>&nbsp;
				<input type='submit' name='post' class='mainoption' value='{L_SUBMIT}'>
			</td>
		</tr>
	</table>
<table border="0" cellpadding="0" cellspacing="0" width="70%" height="14" align="center">
  <tr> 
    <td rowspan="1" colspan="1" width="1%" height="14" background="templates/FSDark/images/front/Image2_1x2down.gif" align="left"> 
      <img name="Image20" src="templates/FSDark/images/front/Image2_1x1down.gif" width="5" height="14" border="0"></td>
    <td rowspan="1" colspan="1" width="98%" height="14" background="templates/FSDark/images/front/Image2_1x2down.gif"> 
    </td>
    <td rowspan="1" colspan="1" width="1%" height="14" align="right" background="templates/FSDark/images/front/Image2_1x2down.gif"> 
      <img name="Image22" src="templates/FSDark/images/front/Image2_1x3down.gif" width="14" height="14" border="0"></td>
  </tr>
</table>
{S_HIDDEN_FIELDS}</form>
<!-- END quick_reply -->