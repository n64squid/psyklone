<h1>{TITLE}</h1>
<p>{EXPLAIN}</p>
  <table width="99%" cellpadding="4" cellspacing="1" border="0" align="center" class="forumline">
	<tr> 
	  <th class="thHead" colspan="2">User Shop Information</th>
	</tr>
	<tr>
	  <td class="row1"><span class="gensmall">Total Shops</span></td>
	  <td class="row1"><span class="gensmall">{TOTAL_SHOPS}</span></td>
	</tr>
	<tr>
	  <td class="row2"><span class="gensmall">Total Shop Items</span></td>
	  <td class="row2"><span class="gensmall">{TOTAL_ITEMS}</span></td>
	</tr>
	<tr>
	  <td class="row1"><span class="gensmall">Total Points Held</span></td>
	  <td class="row1"><span class="gensmall">{TOTAL_HOLDING}</span></td>
	</tr>
	<tr>
	  <td class="row1"><span class="gensmall">Total Points Earnt</span></td>
	  <td class="row1"><span class="gensmall">{TOTAL_EARNT}</span></td>
	</tr>
  </table>

<br /><br />

<form action="{S_CONFIG_ACTION}" method="post">
<input type="hidden" name="action" value="update_vars" />
  <table width="99%" cellpadding="4" cellspacing="1" border="0" align="center" class="forumline">
	<tr> 
	  <th class="thHead" colspan="2">User Shop Configuration</th>
	</tr>
	<tr>
	  <td class="row1"><span class="gensmall">User Shops</span></td>
	  <td class="row1">
		<select name="status">
		  <option value="1" {SHOP_OPEN}>Open</option>
		  <option value="0" {SHOP_CLOSED}>Closed</option>
		</select>
	  </td>
	</tr>
	<tr>
	  <td class="row1"><span class="gensmall">Percent Taken on Sales</span></td>
	  <td class="row1"><input type="text" name="tax_percent" size="5" value="{TAX_PERCENT}" maxlength="3" class="post" /></td>
	</tr>
	<tr>
	  <td class="row2"><span class="gensmall">Max Items Per Shop</span></td>
	  <td class="row2"><input type="text" name="max_items" size="10" value="{MAX_ITEMS}" maxlength="10" class="post" /></td>
	</tr>
	<tr>
	  <td class="row1"><span class="gensmall">Cost to Open Shop</span></td>
	  <td class="row1"><input type="text" name="open_cost" size="10" value="{OPEN_COST}" maxlength="15" class="post" /></td>
	</tr>
	<tr>
	  <td class="row1" colspan="2" align="center"><input type="submit" value="Update Configuration" name="Update" class="liteoption"></td>
	</tr>
  </table>
</form>

<br /><br />

<!-- BEGIN switch_are_shops -->
<form action="{S_CONFIG_ACTION}" method="post">
<input type="hidden" name="action" value="close_shop" />
  <table width="450" cellpadding="4" cellspacing="1" border="0" align="center" class="forumline">
	<tr> 
	  <th class="thHead" colspan="2">Close User Shop</th>
	</tr>
	<tr>
	  <td class="row1"><span class="gensmall">Shop Name</span></td>
	  <td class="row1">
		<select name="id">
<!-- END switch_are_shops -->
<!-- BEGIN list_shops -->
			<option value="{list_shops.SHOP_ID}">{list_shops.STRING}</option>
<!-- END list_shops -->
<!-- BEGIN switch_are_shops -->
		</select>
	  </td>
	</tr>
	<tr>
	  <td class="row2"><span class="gensmall">Return Items:</span></td>
	  <td class="row2">
		<select name="items">
		  <option value="1">On</option>
		  <option value="0">Off</option>
		</select>
	  </td>
	</tr>
	<tr>
	  <td class="row2" colspan="2" align="center"><input type="submit" value="Close Shop" name="update" class="liteoption"></td>
	</tr>
  </table>
</form>

<br />

<form action="{S_CONFIG_ACTION}" method="post">
<input type="hidden" name="action" value="close_shop" />
  <table width="450" cellpadding="4" cellspacing="1" border="0" align="center" class="forumline">
	<tr> 
	  <th class="thHead" colspan="2">Close User's Shop</th>
	</tr>
	<tr>
	  <td class="row1"><span class="gensmall">Shop Owner</span></td>
	  <td class="row1">
		<select name="id">
<!-- END switch_are_shops -->
<!-- BEGIN list_users -->
			<option value="{list_users.SHOP_ID}">{list_users.STRING}</option>
<!-- END list_users -->
<!-- BEGIN switch_are_shops -->
		</select>
	  </td>
	</tr>
	<tr>
	  <td class="row2"><span class="gensmall">Return Items:</span></td>
	  <td class="row2">
		<select name="items">
		  <option value="1">On</option>
		  <option value="0">Off</option>
		</select>
	  </td>
	</tr>
	<tr>
	  <td class="row2" colspan="2" align="center"><input type="submit" value="Close Shop" name="update" class="liteoption"></td>
	</tr>
  </table>
</form>

<br /><br />
<!-- END switch_are_shops -->


  <table width="99%" cellpadding="4" cellspacing="1" border="0" align="center" class="forumline">
	<tr>
		<td width="100%" align="center" class="row2"><br><span class="gensmall">User-Shops Modification: Copyright � 2004, <a href="http://www.zarath.com/mods/" class="navsmall">Zarath Technologies</a>.</span><br><br></td>
	</tr>
  </table>
<br	clear="all" />
