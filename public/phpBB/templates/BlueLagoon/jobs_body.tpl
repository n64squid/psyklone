  <table width="100%" cellspacing="2" cellpadding="2" border="0" align="center">
	<tr> 
	  <td align="left"><span class="nav"><a href="{U_INDEX}" class="nav">{L_INDEX}</a>{LOCATION}</span></td>
	</tr>
  </table>
  <table width="100%" cellpadding="4" cellspacing="1" border="0" align="center" class="forumline">
	<tr> 
	  <th class="thHead" colspan="2">{L_TITLE}</th>
	</tr>
	<tr>
	  <td class="row1" width="70%"><span class="gen">{L_TOTAL_JOBS}</span></td>
	  <td class="row1" width="30%"><span class="gen">{TOTAL_JOBS}</span></td>
	</tr>
	<tr>
	  <td class="row2" width="70%"><span class="gen">{L_TOTAL_POSITIONS}</span></td>
	  <td class="row2" width="30%"><span class="gen">{TOTAL_JOB_POSITIONS}</span></td>
	</tr>
	<tr>
	  <td class="row1" width="70%"><span class="gen">{L_EMPLOYED}</span></td>
	  <td class="row1" width="30%"><span class="gen">{TOTAL_EMPLOYED}</span></td>
	</tr>
	<tr>
	  <td class="row2" width="70%"><span class="gen">{L_TAKEN}</span></td>
	  <td class="row2" width="30%"><span class="gen">{TOTAL_JOBS_TAKEN}</span></td>
	</tr>
	<tr>
	  <td class="row1" width="70%"><span class="gen">{L_REMAINING}</span></td>
	  <td class="row1" width="30%"><span class="gen">{TOTAL_FREE_JOBS}</span></td>
	</tr>
	<tr>
	  <td colspan="2" class="row2"><br /></td>
	</tr>
<!-- BEGIN switch_has_job -->
	<tr>
	  <td class="row2" colspan="2"><span class="gen"><b>{L_CURRENT_JOBS}</b></span></td>
	</tr>
<form method="post" action="{S_MODE_ACTION}">
<input type="hidden" name="action" value="quit">
	<tr>
	  <td class="row2">
		<select name="job">
<!-- END switch_has_job -->
<!-- BEGIN listrow -->
		  <option value="{listrow.JOB_NAME}">{listrow.JOB_NAME}</option>
<!-- END listrow -->
<!-- BEGIN switch_has_job -->
		</select>
	  </td>
	  <td class="row2"><input type="submit" value="{L_B_QUIT}" class="liteoption"></td>
	</tr>
</form>
<!-- END switch_has_job -->
<!-- BEGIN switch_has_no_job -->
	<tr>
	  <td class="row2" colspan="2"><span class="gen"><b>{L_YOURE_UNEMPLOYED}</b></span></td>
	</tr>
<!-- END switch_has_no_job -->
	<tr>
	  <td colspan="2" class="row2"><br /></td>
	</tr>
<!-- BEGIN switch_can_get_job -->

	<tr>
	  <td class="row2" colspan="2"><span class="gen"><b>{L_AVAILABLE_JOBS}</b></span></td>
	</tr>
<form method="post" action="{S_MODE_ACTION}">
<input type="hidden" name="action" value="start">
	<tr>
	  <td class="row2">
		<select name="job">
<!-- END switch_can_get_job -->
<!-- BEGIN listrow2 -->
		  <option value="{listrow2.JOB_ID}">{listrow2.JOB_NAME}</option>
<!-- END listrow2 -->
<!-- BEGIN switch_can_get_job -->
		</select>
	  </td>
	  <td class="row2"><input type="submit" value="{L_B_ACCEPT}" class="liteoption"></td>
	</tr>
</form>
<!-- END switch_can_get_job -->
<!-- BEGIN switch_cant_get_job -->
	<tr>
	  <td class="row2" colspan="2"><span class="gen"><b>{L_CANT_BE_EMPLOYED}</b></span></td>
	</tr>
<!-- END switch_cant_get_job -->
</table>
<br />
<table width="100%" cellpadding="4" cellspacing="1" border="0" align="center" class="forumline">
	<tr>
	  <td class="row2" align="center"><span class="gensmall"><br />{L_IMP}<br /><br /></span></td>
	</tr>
</table>
<br	clear="all" />