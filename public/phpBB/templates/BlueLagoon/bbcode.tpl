<!-- BEGIN ulist_open --><ul><!-- END ulist_open -->
<!-- BEGIN ulist_close --></ul><!-- END ulist_close -->

<!-- BEGIN olist_open --><ol type="{LIST_TYPE}"><!-- END olist_open -->
<!-- BEGIN olist_close --></ol><!-- END olist_close -->

<!-- BEGIN listitem --><li><!-- END listitem -->

<!-- BEGIN quote_username_open --></span>
<table width="90%" cellspacing="1" cellpadding="3" border="0" align="center">
<tr> 
	  <td><span class="genmed"><b>{USERNAME} {L_WROTE}:</b></span></td>
	</tr>
	<tr>
	  <td class="quote"><!-- END quote_username_open -->
<!-- BEGIN quote_open --></span>
<table width="90%" cellspacing="1" cellpadding="3" border="0" align="center">
<tr> 
	  <td><span class="genmed"><b>{L_QUOTE}:</b></span></td>
	</tr>
	<tr>
	  <td class="quote"><!-- END quote_open -->
<!-- BEGIN quote_close --></td>
	</tr>
</table>
<span class="postbody"><!-- END quote_close -->

<!-- BEGIN code_open --></span>
<table width="90%" cellspacing="1" cellpadding="3" border="0" align="center">
<tr> 
	  <td><span class="genmed"><b>{L_CODE}:</b></span></td>
	</tr>
	<tr>
	  <td class="code"><!-- END code_open -->
<!-- BEGIN code_close --></td>
	</tr>
</table>
<span class="postbody"><!-- END code_close -->

<!-- BEGIN spoiler_open -->
<table width="90%" cellspacing="1" cellpadding="3" border="0" align="center">
<tr>
	<td><span style="font-size: 11px;"><b>Spoiler:</b></span></td>
</tr>
<tr>
	<td class="spoil"><!-- END spoiler_open -->
<!-- BEGIN spoiler_close -->
</td>
</tr>
</table>
<!-- END spoiler_close -->

<!-- BEGIN spoil_open --><span style="color: 000000; background-color: 000000"><!-- END spoil_open --> 
<!-- BEGIN spoil_close --></span><!-- END spoil_close --> 

<!-- BEGIN b_open --><span style="font-weight: bold"><!-- END b_open -->
<!-- BEGIN b_close --></span><!-- END b_close -->

<!-- BEGIN blur_open --><span style="height: 20; filter:blur(add=1,direction=270,strength=10)"><!-- END blur_open --> 
<!-- BEGIN blur_close --></span><!-- END blur_close -->  

<!-- BEGIN u_open --><span style="text-decoration: underline"><!-- END u_open -->
<!-- BEGIN u_close --></span><!-- END u_close -->

<!-- BEGIN i_open --><span style="font-style: italic"><!-- END i_open -->
<!-- BEGIN i_close --></span><!-- END i_close -->

<!-- BEGIN color_open --><span style="color: {COLOR}"><!-- END color_open -->
<!-- BEGIN color_close --></span><!-- END color_close -->

<!-- BEGIN size_open --><span style="font-size: {SIZE}px; line-height: normal"><!-- END size_open -->
<!-- BEGIN size_close --></span><!-- END size_close -->

<!-- BEGIN img --><img src="{URL}" border="0" /><!-- END img -->

<!-- BEGIN url --><a href="{URL}" target="_blank" class="postlink">{DESCRIPTION}</a><!-- END url -->

<!-- BEGIN email --><a href="mailto:{EMAIL}">{EMAIL}</A><!-- END email -->

<!-- BEGIN glow_open --><span style="filter: glow(color={GLOWCOLOR}); height:20"><!-- END glow_open -->
<!-- BEGIN glow_close --></span><!-- END glow_close -->

<!-- BEGIN shadow_open --><span style="filter: shadow(color={SHADOWCOLOR}); height:20"><!-- END shadow_open -->
<!-- BEGIN shadow_close --></span><!-- END shadow_close -->
