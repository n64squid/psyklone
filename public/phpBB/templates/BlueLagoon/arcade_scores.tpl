<table width="100%" cellspacing="2" cellpadding="2" border="0" align="center">
	<tr> 
		<td align="left"><span class="nav"><a href="{U_INDEX}" class="nav">{L_INDEX}</a>{U_CAT}</span></td>
	</tr>
</table>

<table class="forumline" width="100%" cellspacing="1" cellpadding="2" border="0" align="center">
	<tr>
		<th class="thCornerL" width="40">#</th>
		<th class="thTop" align="center">{L_HIGHSCORE}</th>
		<th class="thTop" align="center" width="10%">{L_SCORE}</th>
		<th class="thTop" align="center" width="25%">{L_PLAYED}</th>
		<th class="thTop" align="center" width="15%">{L_TIME_TAKEN}</th>
		<th class="thCornerR" align="center" width="15%">{L_TIME_HELD}</th>
	</tr>

	<!-- BEGIN scores -->
	<tr>
		<td class="{scores.ROW_CLASS}" align="center"><span class="gen">{scores.POS}</span></td>
		<td class="{scores.ROW_CLASS}" align="center">&nbsp;&nbsp;<span class="gen">{scores.NAME}</span></td>
		<td class="{scores.ROW_CLASS}" align="center"><span class="gen">{scores.SCORE}</span></td>
		<td class="{scores.ROW_CLASS}" align="center"><span class="gen">{scores.DATE}</span></td>
		<td class="{scores.ROW_CLASS}" align="center"><span class="gen">{scores.TIME_TAKEN}</span></td>
		<td class="{scores.ROW_CLASS}" align="center"><span class="gen">{scores.TIME_HELD}</span></td>
	</tr>
	<!-- END scores -->

	<tr>
		<td class="cat" colspan="6" height="28"></td>
	</tr>
</table>

<table width="100%" cellspacing="2" cellpadding="2" border="0" align="center">
	<tr> 
		<td align="left"><span class="nav"><a href="{U_INDEX}" class="nav">{L_INDEX}</a>{U_CAT}</span></td>
	</tr>
</table>
<br />
<div align="center"><span class="gensmall">{ARCADE_MOD}</span></div>
