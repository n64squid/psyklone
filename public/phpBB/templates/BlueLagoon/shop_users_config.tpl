  <table width="100%" cellspacing="2" cellpadding="2" border="0" align="center">
	<tr> 
	  <td align="left"><span class="nav"><a href="{U_INDEX}" class="nav">{L_INDEX}</a>{SHOPLOCATION}</span></td>
	</tr>
  </table>
<form method="post" action="shop_users_edit.php">
<input type="hidden" name="action" value="update_config" />
  <table width="100%" cellpadding="4" cellspacing="1" border="0" align="center" class="forumline">
	<tr> 
	  <th class="thHead" colspan="2">{L_SHOP_TITLE}</th>
	</tr>
<!-- BEGIN switch_are_shops -->
	<tr>
	  <td class="row3" colspan="2" align="center"><span class="gensmall"><b>Shop Settings</b></span></td>
	</tr>
	<tr>
	  <td class="row1"><span class="gensmall"><b>Shop Name</b></span></td><td class="row1"><input type="text" name="shop_name" class="post" size="25" maxlength="32" value="{switch_are_shops.SHOP_NAME}" /></td>
	</tr>
	<tr>
	  <td class="row2"><span class="gensmall"><b>Shop Type</b></span></td><td class="row2"><input type="text" name="shop_type" class="post" size="25" maxlength="32" value="{switch_are_shops.SHOP_TYPE}" /></td>
	</tr>
	<tr>
	  <td class="row1"><span class="gensmall"><b>Shop Status</b></span></td><td class="row1"><select name="shop_status"><option value="0" {switch_are_shops.STATUS_SELECT_1}>Opened</option><option value="1" {switch_are_shops.STATUS_SELECT_2}>Closed</option><option value="2" {switch_are_shops.STATUS_SELECT_3}>Restocking</option></td>
	</tr>
	<tr>
	  <td class="row2"><span class="gensmall"><b>Shop Opened</b></span></td><td class="row2"><span class="gensmall">{switch_are_shops.SHOP_OPENED}</span></td>
	</tr>
	<tr>
	  <td class="row1"><span class="gensmall"><b>Currency Earned</b></span></td><td class="row1"><span class="gensmall">{switch_are_shops.SHOP_EARNT}</span></td>
	</tr>
	<tr>
	  <td class="row2"><span class="gensmall"><b>Currency Holding</b></span></td><td class="row2"><span class="gensmall">{switch_are_shops.SHOP_HOLDING}
<!-- END switch_are_shops -->
<!-- BEGIN switch_withdraw_holdings -->
&nbsp;&nbsp;&nbsp;[<a href="{switch_withdraw_holdings.WITHDRAW_URL}" class="navsmall">Withdraw Holdings</a>]
<!-- END switch_withdraw_holdings -->
<!-- BEGIN switch_are_shops -->
</span></td>
	</tr>
	<tr>
	  <td class="row1"><span class="gensmall"><b>Items Left</b></span></td><td class="row1"><span class="gensmall">{switch_are_shops.SHOP_ITEMS_LEFT}</span></td>
	</tr>
	<tr>
	  <td class="row2"><span class="gensmall"><b>Items Sold</b></span></td><td class="row2"><span class="gensmall">{switch_are_shops.SHOP_ITEMS_SOLD}</span></td>
	</tr>
	<tr>
	  <td class="row1" colspan="2" align="center"><input type="submit" value="Update Settings" class="liteoption" /></td>
	</tr>
<!-- END switch_are_shops -->
  </table>
</form>
<!-- BEGIN switch_are_shops -->
  <table width="100%" cellpadding="4" cellspacing="1" border="0" align="center" class="forumline">
	<tr> 
	  <th class="thHead" colspan="5">Edit Shop Items</th>
	</tr>
<!-- END switch_are_shops -->
<!-- BEGIN switch_are_items -->
	<tr> 
	  <td class="row3"><span class="gensmall"><b>Item Name</b></span></td><td class="row3"><span class="gensmall"><b>Seller Notes</b></span></td><td class="row3"><span class="gensmall"><b>Cost</b></span></td><td class="row3" colspan="2" align="center"><span class="gensmall"><b>Actions</b></span></td>
	</tr>
<!-- END switch_are_items -->
<!-- BEGIN switch_edit_item -->
<form action="{switch_edit_item.UPDATE_URL}" method="post">
	<tr> 
	  <td class="row3" valign="top"><span class="gensmall"><b>{switch_edit_item.ITEM_NAME}</b></span></td><td class="row3" valign="top"><textarea name="item_notes" cols="35" rows="2">{switch_edit_item.ITEM_NOTES}</textarea></td><td class="row3" valign="top"><input type="text" size="5" maxlength="10" name="item_cost" class="post" value="{switch_edit_item.ITEM_COST}" /></td><td class="row3" colspan="2" align="center" valign="top"><input type="submit" class="liteoption" value="Update Item" /></td>
	</tr>
</form>
<!-- END switch_edit_item -->
<!-- BEGIN list_items -->
	<tr>
	  <td class="{list_items.ROW_CLASS}"><span class="gensmall">{list_items.ITEM_NAME}</span></td><td class="{list_items.ROW_CLASS}"><span class="gensmall">{list_items.ITEM_NOTES}</span></td><td class="{list_items.ROW_CLASS}"><span class="gensmall">{list_items.ITEM_COST}</span></td><td class="{list_items.ROW_CLASS}"><a href="{list_items.EDIT_URL}" class="nav">Edit</a></td><td class="{list_items.ROW_CLASS}"><a href="{list_items.DELETE_URL}" class="nav">Remove</a></td>
	</tr>
<!-- END list_items -->
<!-- BEGIN switch_no_items -->
	<tr> 
	  <td class="row3" colspan="5"><span class="gensmall">There are currently no items in your store!</span></td>
	</tr>
<!-- END switch_no_items -->
<!-- BEGIN switch_are_shops -->
  </table>
<!-- END switch_are_shops -->
<!-- BEGIN switch_are_a_items -->
<br /><br />
<form method="post" action="shop_users_edit.php">
  <input type="hidden" name="action" value="change_items" />
  <input type="hidden" name="sub_action" value="add_item" />
  <table width="45%" cellpadding="4" cellspacing="1" border="0" align="center" class="forumline">
	<tr> 
	  <th class="thHead" colspan="2">Add Items</th>
	</tr>
	<tr>
	  <td class="row1" width="40%"><span class="gensmall"><b>Item</b></span></td><td class="row1">
		<select name="item_id">
		  <option value="-1">None</option>
<!-- END switch_are_a_items -->
<!-- BEGIN list_add_items -->
		  <option value="{list_add_items.ITEM_ID}">{list_add_items.ITEM_NAME}</option>
<!-- END list_add_items -->
<!-- BEGIN switch_are_a_items -->
		</select>
	  </td>
	</tr>
	<tr>
	  <td class="row2" width="40%"><span class="gensmall"><b>Cost</b></span></td><td class="row2"><input type="text" name="item_cost" class="post" size="5" maxlength="10" /></td>
	</tr>
	<tr>
	  <td class="row1" width="40%"><span class="gensmall"><b>Notes</b></span></td><td class="row1"><textarea name="item_notes" cols="35" rows="5"></textarea></td>
	</tr>
	<tr>
	  <td class="row2" colspan="2" align="center"><input type="submit" value="Add Item" class="liteoption" /></td>
	</tr>
  </table>
<!-- END switch_are_a_items -->
<br />
  <table width="100%" cellpadding="4" cellspacing="1" border="0" align="center" class="forumline">
	<tr> 
	  <th class="thHead" colspan="2">Personal Information</th>
	</tr>
	<tr>
	  <td class="row1" width="50%"><span class="gensmall"><a href="{U_INVENTORY}" class="navsmall">{L_INVENTORY}</a></span></td><td class="row1" align="right" width="50%"><span class="gensmall">{USER_POINTS}</span></td>
	</tr> 
<!-- BEGIN switch_special_msgs -->
	<tr>
	  <td class="row2" colspan="2"><span class="gensmall"><font color="red">{switch_special_msgs.SPECIAL_MSGS}</font></span></td>
	</tr>
	<tr>
	  <td class="row2" colspan="2"><span class="gensmall"><a href="{switch_special_msgs.SPECIAL_MSGS_URL}" class="gen">{switch_special_msgs.L_CLEAR}</a></span></td>
	</tr>
<!-- END switch_special_msgs -->
  </table>
  <table width="100%" cellpadding="4" cellspacing="1" border="0" align="center" class="forumline">
	<tr>
		<td width="100%" align="center" class="row3"><br /><span class="gensmall">User Shop Addon: Copyright � 2004, <a href="http://www.zarath.com/mods/" class="navsmall">Zarath Technologies</a>.</span><br /><br /></td>
	</tr>
  </table>
<br	clear="all" />
