<table width="100%" cellspacing="2" cellpadding="2" border="0" align="center">
	<tr> 
		<td align="left"><span class="nav"><a href="{U_INDEX}" class="nav">{L_INDEX}</a>{U_CAT}</span></td>
	</tr>
</table>

<table class="forumline" width="100%" cellspacing="1" cellpadding="2" border="0" align="center">
	<tr>
		<th class="thTop" colspan="2" align="center">{L_GAME_STATS}</th>
	</tr>

	<tr>
		<td class="row2" width="20%" align="center"><img src ="{IMAGE}" border="0" align="middle"></td>
		<td class="row1"><span class="gen">{DESC}<br />
<!-- BEGIN switch_user_logged_in -->
{U_ADD_FAV}
<!-- END switch_user_logged_in --> 
</span></td>
	</tr>
	<tr>
		<td class="row2" align="right"><span class="gen">{L_CONTROL}&nbsp;&nbsp;</span></td>
		<td class="row1"><span class="gen">{CONTROL}</span></td>
	</tr>
	<tr>
		<td class="row2" align="right"><span class="gen">{L_CATEGORY}&nbsp;&nbsp;</span></td>
		<td class="row1"><span class="gen">{CATEGORY}</span></td>
	</tr>
	<tr>
		<td class="row2" align="right"><span class="gen">{L_PLAYED}&nbsp;&nbsp;</span></td>
		<td class="row1"><span class="gen"><b>{PLAYED}</b> {TIMES}</span></td>
	</tr>
	<tr>
		<td class="row2" align="right"><span class="gen">{L_PRICE}&nbsp;&nbsp;</span></td>
		<td class="row1"><span class="gen">{COST}</span></td>
	</tr>
	<tr>
		<td class="row2" align="right"><span class="gen">{L_HIGHSCORE}&nbsp;&nbsp;</span></td>
		<td class="row1"><span class="gen">{BONUS}</span></td>
	</tr>
	<tr>
		<td class="row2" align="right"><span class="gen">{L_AT_HIGHSCORE}&nbsp;&nbsp;</span></td>
		<td class="row1"><span class="gen">{AT_BONUS}</span></td>
	</tr>
	<tr>
		<td class="row2" align="right"><span class="gen">{L_SCORE_REWARD}&nbsp;&nbsp;</span></td>
		<td class="row1"><span class="gen">{REWARD}</span></td>
	</tr>
	<tr>
		<td class="row2" align="right"><span class="gen">{L_BEST_PLAYER}&nbsp;&nbsp;</span></td>
		<td class="row1"><span class="gen">{BEST_AT_PLAYER}</span></td>
	</tr>
	<tr>
		<td class="row2" align="right"><span class="gen">{L_ALL_TIME_SCORE}&nbsp;&nbsp;</span></td>
		<td class="row1"><span class="gen">{BEST_AT_SCORE}</span></td>
	</tr>
	<tr>
		<td class="row2" align="right"><span class="gen">{L_CURRENT_BEST}&nbsp;&nbsp;</span></td>
		<td class="row1"><span class="gen">{BEST_PLAYER}</span></td>
	</tr>
	<tr>
		<td class="row2" align="right"><span class="gen">{L_HIGHEST_SCORE}&nbsp;&nbsp;</span></td>
		<td class="row1"><span class="gen">{BEST_SCORE}</span></td>
	</tr>

	<tr>
		<th class="thTop" colspan="2" align="center">{L_INSTRUCTIONS}</th>
	</tr>
	<tr>
		<td class="row2" colspan="2"><span class="gen">{INSTRUCTIONS}</span></td>
	</tr>

	<tr>
		<td class="cat" colspan="2" align="center">&nbsp;</th>
	</tr>
<!-- BEGIN tournament_menu -->
	<tr>
		<th class="thTop" colspan="2" align="center">{L_TOURNAMENT}</th>
	</tr>
	<tr>
		<td class="row4" align="center"><span class="gen"><img src="templates/subSilver/images/trophy.jpg"</span></td>
	</tr>
<!-- END tournament_menu -->
</table>

<table width="100%" cellspacing="2" cellpadding="2" border="0" align="center">
	<tr> 
		<td align="left"><span class="nav"><a href="{U_INDEX}" class="nav">{L_INDEX}</a>{U_CAT}</span></td>
	</tr>
</table>
<br /><div align="center"><span class="gensmall">{ARCADE_MOD}</span></div>