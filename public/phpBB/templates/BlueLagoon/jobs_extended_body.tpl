<table width="100%" cellspacing="2" cellpadding="2" border="0" align="center">
	<tr> 
	  <td align="left"><span class="nav"><a href="{U_INDEX}" class="nav">{L_INDEX}</a>{LOCATION}</span></td>
	</tr>
</table>
<table width="100%" cellpadding="4" cellspacing="1" border="0" align="center" class="forumline">
	<tr> 
	  <th class="thHead" colspan="2">{L_TITLE}</th>
	</tr>
	<tr>
	  <td class="row1" width="70%"><span class="gen">{L_TOTAL_JOBS}</span></td>
	  <td class="row1" width="30%"><span class="gen">{TOTAL_JOBS}</span></td>
	</tr>
	<tr>
	  <td class="row2" width="70%"><span class="gen">{L_TOTAL_POSITIONS}</span></td>
	  <td class="row2" width="30%"><span class="gen">{TOTAL_JOB_POSITIONS}</span></td>
	</tr>
	<tr>
	  <td class="row1" width="70%"><span class="gen">{L_EMPLOYED}</span></td>
	  <td class="row1" width="30%"><span class="gen">{TOTAL_EMPLOYED}</span></td>
	</tr>
	<tr>
	  <td class="row2" width="70%"><span class="gen">{L_TAKEN}</span></td>
	  <td class="row2" width="30%"><span class="gen">{TOTAL_JOBS_TAKEN}</span></td>
	</tr>
	<tr>
	  <td class="row1" width="70%"><span class="gen">{L_REMAINING}</span></td>
	  <td class="row1" width="30%"><span class="gen">{TOTAL_FREE_JOBS}</span></td>
	</tr>
</table>
<br />
<table width="100%" cellpadding="4" cellspacing="1" border="0" align="center" class="forumline">
<!-- BEGIN switch_has_job -->
	<tr>
	  <th class="thHead" colspan="5">{L_CURRENT_JOBS}</th>
	</tr>
	<tr>
	  <td class="row1"><span class="gen"><b>{L_JOB}</b></span></td>
	  <td class="row1"><span class="gen"><b>{L_PAY}</b></span></td>
	  <td class="row1"><span class="gen"><b>{L_PAY_LENGTH}</b></span></td>
	  <td class="row1"><span class="gen"><b>{L_STARTED}</b></span></td>
	  <td class="row1"><span class="gen"><b>{L_LAST_PAID}</b></span></td>
	</tr>
<!-- END switch_has_job -->
<!-- BEGIN listrow -->
	<tr>
	  <td class="{listrow.ROW}"><span class="gen"><a href="{listrow.S_MODE_ACTION}" class="gensmall">{listrow.JOB_NAME}</a></span></td>
	  <td class="{listrow.ROW}"><span class="gen">{listrow.JOB_PAY}</span></td>
	  <td class="{listrow.ROW}"><span class="gen">{listrow.JOB_LENGTH}</span></td>
	  <td class="{listrow.ROW}"><span class="gen">{listrow.JOB_STARTED}</span></td>
	  <td class="{listrow.ROW}"><span class="gen">{listrow.JOB_LAST_PAID}</span></td>
	</tr>
<!-- END listrow -->
<!-- BEGIN switch_has_no_job -->
	<tr>
	  <td class="row2" width="100%"><span class="gen"><b>{L_YOURE_UNEMPLOYED}</b></span></td>
	</tr>
<!-- END switch_has_no_job -->
</table>
<br />
<table width="100%" cellpadding="4" cellspacing="1" border="0" align="center" class="forumline">
<!-- BEGIN switch_can_get_job -->
	<tr>
	  <th class="thHead" colspan="4">{L_AVAILABLE_JOBS}</th>
	</tr>
	  <td class="row1"><span class="gen"><b>{L_JOB}</b></span></td>
	  <td class="row1"><span class="gen"><b>{L_PAY}</b></span></td>
	  <td class="row1"><span class="gen"><b>{L_PAY_LENGTH}</b></span></td>
	  <td class="row1"><span class="gen"><b>{L_POSITIONS}</b></span></td>
	</tr>
<!-- END switch_can_get_job -->
<!-- BEGIN listrow2 -->
	<tr>
	  <td class="{listrow2.ROW}"><span class="gen"><a href="{listrow2.S_MODE_ACTION}" class="gensmall">{listrow2.JOB_NAME}</a></span></td>
	  <td class="{listrow2.ROW}"><span class="gen">{listrow2.JOB_PAY}</span></td>
	  <td class="{listrow2.ROW}"><span class="gen">{listrow2.JOB_PAY_TIME}</span></td>
	  <td class="{listrow2.ROW}"><span class="gen">{listrow2.JOB_LEFT} / {listrow2.JOB_POSITIONS}</span></td>
	</tr>
<!-- END listrow2 -->
<!-- BEGIN switch_cant_get_job -->
	<tr>
	  <td class="row2" align="ecnter"><span class="gen"><b>{L_CANT_BE_EMPLOYED}</b></span></td>
	</tr>
<!-- END switch_cant_get_job -->
</table>
<br />
<table width="100%" cellpadding="4" cellspacing="1" border="0" align="center" class="forumline">
	<tr>
	  <td class="row2" align="center"><span class="gensmall"><br />{L_IMP}<br /><br /></span></td>
	</tr>
</table>
<br	clear="all" />