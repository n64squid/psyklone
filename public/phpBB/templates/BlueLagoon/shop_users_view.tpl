  <table width="100%" cellspacing="2" cellpadding="2" border="0" align="center">
	<tr> 
	  <td align="left"><span class="nav"><a href="{U_INDEX}" class="nav">{L_INDEX}</a>{SHOPLOCATION}</span></td>
	</tr>
  </table>
  <table width="100%" cellpadding="4" cellspacing="1" border="0" align="center" class="forumline">
<!-- BEGIN switch_main_list -->
	<tr> 
	  <th class="thHead" colspan="3">{L_SHOP_TITLE}</th>
	</tr>
	<tr>
	  <td class="row3" align="center"><span class="gensmall"><b>Item Name</b></span></td>
	  <td class="row3" align="center"><span class="gensmall"><b>Short Description</b></span></td>
	  <td class="row3" align="center"><span class="gensmall"><b>Cost</b></span></td>
	</tr>
<!-- END switch_main_list -->
<!-- BEGIN list_items -->
	<tr>
	  <td class="{list_items.ROW_CLASS}" valign="top"><a href="{list_items.ITEM_URL}" title="Item Information on {list_items.ITEM_NAME}" class="nav"><b>{list_items.ITEM_NAME}</b></a></td>
	  <td class="{list_items.ROW_CLASS}"><span class="gensmall">{list_items.ITEM_S_DESC} <i>[Seller Notes: {list_items.ITEM_NOTES}]</i></span></td>
	  <td class="{list_items.ROW_CLASS}" valign="top"><span class="gensmall">{list_items.ITEM_COST}</span></td>
	</tr>
<!-- END list_items -->
<!-- BEGIN switch_item_info -->
	<tr> 
	  <th class="thHead" colspan="5">{L_TABLE_TITLE}</th>
	</tr>
	<tr>
	  <td width="2%" class="row3"><span class="gensmall"><b>Icon</b></span></td>
	  <td class="row3"><span class="gensmall"><b>Item&nbsp;Name</b></span></td>
	  <td class="row3"><span class="gensmall"><b>Description</b></span></td>
	  <td class="row3"><span class="gensmall"><b>Cost</b></span></td>
	  <td class="row3"><span class="gensmall"><b>Owned</b></span></td>
	</tr>
	<tr>
	  <td class="row1"><img src="shop/images/{switch_item_info.ITEM_IMAGE}" title="{switch_item_info.ITEM_NAME}" alt="{switch_item_info.ITEM_NAME}"></td>
	  <td class="row1" valign="top"><span class="gensmall"><b>{switch_item_info.ITEM_NAME}</b></span></td>
	  <td class="row1" valign="top"><span class="gensmall">{switch_item_info.ITEM_L_DESC}<br /><i>[Seller Notes: {switch_item_info.ITEM_NOTES}]</i></span></td>
	  <td class="row1" valign="top"><span class="gensmall">{switch_item_info.ITEM_COST}</span></td>
	  <td class="row1" valign="top"><span class="gensmall">{switch_item_info.ITEM_OWNED}</span></td>
	</tr>
	<tr>
	  <td colspan="6" class="row2"><b><a href="{switch_item_info.ITEM_URL}" title="Buy {switch_item_info.ITEM_NAME}" class="nav">Buy {switch_item_info.ITEM_NAME}</a></b></span></td>
	</tr>
<!-- END switch_item_info -->
  </table>
<br />
  <table width="100%" cellpadding="4" cellspacing="1" border="0" align="center" class="forumline">
	<tr> 
	  <th class="thHead" colspan="2">Personal Information</th>
	</tr>
	<tr>
	  <td class="row1" width="50%"><span class="gensmall"><a href="{U_INVENTORY}" class="navsmall">{L_INVENTORY}</a></span></td><td class="row1" align="right" width="50%"><span class="gensmall">{USER_POINTS}</span></td>
	</tr> 
<!-- BEGIN switch_special_msgs -->
	<tr>
	  <td class="row2" colspan="2"><span class="gensmall"><font color="red">{switch_special_msgs.SPECIAL_MSGS}</font></span></td>
	</tr>
	<tr>
	  <td class="row2" colspan="2"><span class="gensmall"><a href="{switch_special_msgs.SPECIAL_MSGS_URL}" class="gen">{switch_special_msgs.L_CLEAR}</a></span></td>
	</tr>
<!-- END switch_special_msgs -->
  </table>
  <table width="100%" cellpadding="4" cellspacing="1" border="0" align="center" class="forumline">
	<tr>
		<td width="100%" align="center" class="row3"><br /><span class="gensmall">User Shop Addon: Copyright � 2004, <a href="http://www.zarath.com/mods/" class="navsmall">Zarath Technologies</a>.</span><br /><br /></td>
	</tr>
  </table>
<br	clear="all" />
