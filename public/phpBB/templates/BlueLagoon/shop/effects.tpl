<!-- This file is part of Effects Store for Shop 3 -->
<script language="JavaScript" type="text/javascript" src="mods/shopaddons/colors.js"></script>
<script language="JavaScript">
<!--
	var previews = new Array(
	<!-- BEGIN color_preview -->
	'{color_preview.VAR_VALUE}',
	<!-- END color_preview -->
	''
	);
//-->
</script>
<table width="100%" cellspacing="2" cellpadding="2" border="0" align="center">
	<tr> 
		<td align="left">
			<span class="nav"><a href="{U_INDEX}" class="nav">{L_INDEX}</a>
			<!-- BEGIN backlinks -->
			 -> <a href="{backlinks.URL}" class="nav">{backlinks.TEXT}</a>
			<!-- END backlinks -->
			</span>
		</td>
	</tr>
</table>

<form action="{S_ACTION}" method="post" name="post">
	<table width="99%" cellpadding="4" cellspacing="1" border="0" align="center" class="forumline">
	<tr> 
		<th class="thHead" colspan="3">{L_SHOP_TITLE}</th>
	</tr>
	<tr>
		<td class="row2 genmed" colspan="3">{L_EXPLAIN}</td>
	</tr>
<!-- BEGIN colors -->
	<tr> 
		<th class="thHead" colspan="3">{L_COLORS}</th>
	</tr>
	<tr align="center"> 
		<td class="catLeft gen" width="40%">{L_COLORS}</th>
		<td class="cat gen" width="20%">{L_COST}</th>
		<td class="catRight gen" width="40%">{L_STATUS}</th>
	</tr>
	<!-- BEGIN effect_color -->
	<tr>
		<td class="row1 genmed">{colors.effect_color.L_EFFECT_NAME}</td>
		<!-- BEGIN sell_effect -->
		<td class="row2 genmed" align="right">{colors.effect_color.EFFECT_COST} {POINTS_NAME}</td>
		<!-- END sell_effect -->
		<!-- BEGIN bought_effect -->
		<td class="row2 genmed" align="right">--</td>
		<!-- END bought_effect -->
		<td class="row2 genmed" align="center">
				<input type="text" name="{colors.effect_color.FIELD}" maxlength="6" size="6" value="{colors.effect_color.VALUE}" id="{colors.effect_color.FIELD}" onchange="javascript:check_color_change('{colors.effect_color.FIELD}')" class="post" />
				<script type="text/javascript">
				<!--
					colorPalette('h', 10, 10, '{colors.effect_color.FIELD}')
				//-->
				</script>
		</td>
	</tr>
	<!-- END effect_color -->
<!-- END colors -->
	<tr>
		<td class="row1 genmed" colspan="2">
			<div id="rankpreview" style="float: right; text-align: right; padding-right: 2em; width: 50%;">
				{MY_RANK}
			</div>
			<div id="namepreview" style="padding-left: 2em; width: 50%;">
				{MY_USERNAME}
			</div>
		</td>
		<td class="row2 genmed" align="center">
			<input type="button" name="preview" value="{L_PREVIEW}" onclick="javascript:update_preview()" class="liteoption" />
				<script type="text/javascript">
				<!--
					// Do it on a page load, too.
					update_preview();
				//-->
				</script>
		</td>
	</tr>
<!-- BEGIN privs -->
	<tr> 
		<th class="thHead" colspan="3">{L_PRIVS}</th>
	</tr>
	<tr align="center"> 
		<td class="catLeft gen" width="40%">{L_PRIVS}</th>
		<td class="cat gen" width="20%">{L_COST}</th>
		<td class="catRight gen" width="40%">{L_STATUS}</th>
	</tr>
	<!-- BEGIN effect_priv -->
	<tr>
		<td class="row1 genmed">{privs.effect_priv.L_EFFECT_NAME}<span class="gensmall"><br />{privs.effect_priv.L_EFFECT_EXPLAIN}</span></td>
		<!-- BEGIN sell_effect -->
		<td class="row2 genmed" align="right">{privs.effect_priv.EFFECT_COST} {POINTS_NAME}</td>
		<!-- END sell_effect -->
		<!-- BEGIN bought_effect -->
		<td class="row2 genmed" align="right">--</td>
		<!-- END bought_effect -->
		<td class="row2 genmed">
			<!-- BEGIN priv_effect -->
				<label for="{privs.effect_priv.FIELD}"><input type="checkbox" name="{privs.effect_priv.FIELD}" id="{privs.effect_priv.FIELD}" value="1" /> {L_BUY}</label>
			<!-- END priv_effect -->
			<!-- BEGIN priv_effect_bought -->
				{L_OWN_EFFECT}
			<!-- END priv_effect_bought -->
			<!-- BEGIN title_effect -->
				<input type="text" name="{privs.effect_priv.FIELD}" maxlength="255" size="25" value="{privs.effect_priv.VALUE}" class="post" />
			<!-- END title_effect -->
			<!-- BEGIN title_other -->
				<br />
				<input type="text" name="username" maxlength="25" size="25" value="" class="post" />&nbsp;<input type="submit" name="usersubmit" value="{L_FIND_USERNAME}" class="liteoption" onClick="window.open('{U_SEARCH_USER}', '_phpbbsearch', 'HEIGHT=250,resizable=yes,WIDTH=400');return false;" />
			<!-- END title_other -->
			<!-- BEGIN rank_effect -->
				<label for="{privs.effect_priv.FIELD}_yes"><input type="radio" name="{privs.effect_priv.FIELD}" id="{privs.effect_priv.FIELD}_yes" value="1" {privs.effect_priv.rank_effect.YES_SELECTED} /> {L_ON}</label>
				<label for="{privs.effect_priv.FIELD}_no"><input type="radio" name="{privs.effect_priv.FIELD}" id="{privs.effect_priv.FIELD}_no" value="2" {privs.effect_priv.rank_effect.NO_SELECTED} /> {L_OFF}</label>
			<!-- END rank_effect -->
		</td>
	</tr>
	<!-- END effect_priv -->
<!-- END privs -->
	<tr>
		<td colspan="3" align="center" class="catbottom genmed">
			<input type="submit" name="submit" value="{L_PURCHASE}" class="liteoption" />
		</td>	
	</tr>
	</table>
</form>

<br />
{PERSONAL}

<p align="center" class="copyright">
	Effects Store Copyright &copy; 2006 <a href="http://www.phpbbsmith.com" class="navsmall">Thoul</a>, based on Shop 3 Copyright &copy; 2003, 2005 <a href="http://www.zarath.com/" class="navsmall">Zarath Technologies</a>.
</p>