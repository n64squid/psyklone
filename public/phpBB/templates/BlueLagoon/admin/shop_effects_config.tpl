<!-- This file is part of Effects Store for Shop 3 -->
<h1>{L_TITLE}</h1>
<p>{L_EXPLAIN}</p>
{SHOP_BACKLINKS}

<form action="{S_CONFIG_ACTION}" method="post">
	<input type="hidden" name="action" value="effects" />
	<input type="hidden" name="subaction" value="update" />
	<input type="hidden" name="shopid" value="{SHOP_ID}" />
	<table width="99%" cellpadding="4" cellspacing="1" border="0" align="center" class="forumline">
	<tr> 
		<th class="thHead" colspan="2">{L_TITLE}</th>
	</tr>
	<tr>
		<td class="row1 genmed" width="70%">{L_ENABLE}</td>
		<td class="row2 genmed" width="30%">
			<input type="radio" name="effects_enable" value="1" id="enable_yes" {SELECTED_ENABLE_YES} /><label for="enable_yes"> {L_YES}</label>
			<input type="radio" name="effects_enable" value="0" id="enable_no" {SELECTED_ENABLE_NO} /><label for="enable_no"> {L_NO}</label>
		</td>
	</tr>
	<tr>
		<td class="row1 genmed">{L_STORE_NAME}</td>
		<td class="row2 genmed"><input type="text" name="shopname" maxlength="32" size="32" value="{STORE_NAME}" /></td>
	</tr>
	<tr>
		<td class="row1 genmed">{L_STORE_OWNER}</td>
		<td class="row2 genmed"><input type="text" name="owner" maxlength="32" size="32" value="{STORE_OWNER}" /></td>
	</tr>
<!-- 	<tr>
		<td class="row1 genmed">{L_STORE_URL}<br />
			<span class="gensmall">{L_STORE_URL_EXPLAIN}</span>
		</td>
		<td class="row2 genmed"><input type="text" name="url" maxlength="255" size="32" value="{STORE_URL}" /></td>
	</tr> -->
	<tr>
		<td class="row1 genmed">{L_STORE_TYPE}<br />
			<span class="gensmall">{L_STORE_TYPE_EXPLAIN}</span>
		</td>
		<td class="row2 genmed"><input type="text" name="shoptype" maxlength="32" size="32" value="{STORE_TYPE}" /></td>
	</tr>
	<tr>
		<td class="row1 genmed">{L_SHOW_DISTRICT}<br />
			<span class="gensmall">{L_SHOW_DISTRICT_EXPLAIN}</span>
		</td>
		<td class="row2 genmed" width="30%">
			<input type="radio" name="d_type" value="1" id="d_type_yes" {SELECTED_DISTRICT_YES} /><label for="d_type_yes"> {L_YES}</label>
			<input type="radio" name="d_type" value="0" id="d_type_no" {SELECTED_DISTRICT_NO} /><label for="d_type_no"> {L_NO}</label>
		</td>
	</tr>
	<tr>
		<td class="row1 genmed">{L_PARENT_DISTRICT}<br />
			<span class="gensmall">{L_PARENT_DISTRICT_EXPLAIN}</span>
		</td>
		<td class="row2 genmed">{S_PARENTS}</td>
	</tr>
	<tr>
		<td class="row1 genmed">{L_CURRENCY}<br />
			<span class="gensmall">{L_CURRENCY_EXPLAIN}</span>
		</td>
		<td class="row2 genmed">{S_CURRENCY}</td>
	</tr>
	<tr> 
		<th class="thHead" colspan="2">{L_COLORS}</th>
	</tr>
	<tr>
		<td class="row2 genmed" colspan="2">{L_COST_EXPLAIN}</td>
	</tr>
	<tr>
		<td class="row1 genmed">{L_NAME_COLOR}</td>
		<td class="row2 genmed"><input type="text" name="effects_name_color" maxlength="11" size="10" value="{NAME_COLOR}" /></td>
	</tr>
	<tr>
		<td class="row1 genmed">{L_NAME_GLOW}</td>
		<td class="row2 genmed"><input type="text" name="effects_name_glow" maxlength="11" size="10" value="{NAME_GLOW}" /></td>
	</tr>
	<tr>
		<td class="row1 genmed">{L_NAME_SHADOW}</td>
		<td class="row2 genmed"><input type="text" name="effects_name_shadow" maxlength="11" size="10" value="{NAME_SHADOW}" /></td>
	</tr>
	<tr>
		<td class="row1 genmed">{L_TITLE_COLOR}</td>
		<td class="row2 genmed"><input type="text" name="effects_title_color" maxlength="11" size="10" value="{TITLE_COLOR}" /></td>
	</tr>
	<tr>
		<td class="row1 genmed">{L_TITLE_GLOW}</td>
		<td class="row2 genmed"><input type="text" name="effects_title_glow" maxlength="11" size="10" value="{TITLE_GLOW}" /></td>
	</tr>
	<tr>
		<td class="row1 genmed">{L_TITLE_SHADOW}</td>
		<td class="row2 genmed"><input type="text" name="effects_title_shadow" maxlength="11" size="10" value="{TITLE_SHADOW}" /></td>
	</tr>
	<tr> 
		<th class="thHead" colspan="2">{L_PRIVS}</th>
	</tr>
	<tr>
		<td class="row2 genmed" colspan="2">{L_PRIV_EXPLAIN}</td>
	</tr>
	<tr>
		<td class="row1 genmed">{L_ADMIN_IMMUNE}</td>
		<td class="row2 genmed">
			<input type="radio" name="effects_admin" value="1" id="admin_yes" {SELECTED_ADMIN_YES} /><label for="admin_yes"> {L_YES}</label>
			<input type="radio" name="effects_admin" value="0" id="admin_no" {SELECTED_ADMIN_NO} /><label for="admin_no"> {L_NO}</label>
		</td>
	</tr>
	<tr>
		<td class="row1 genmed">{L_MOD_IMMUNE}</td>
		<td class="row2 genmed">
			<input type="radio" name="effects_moderator" value="1" id="moderator_yes" {SELECTED_MODERATOR_YES} /><label for="moderator_yes"> {L_YES}</label>
			<input type="radio" name="effects_moderator" value="0" id="moderator_no" {SELECTED_MODERATOR_NO} /><label for="moderator_no"> {L_NO}</label>
		</td>
	</tr>
<!-- 	<tr>
		<td class="row1 genmed">{L_PM_TITLECHANGE}</td>
		<td class="row2 genmed">
			<input type="radio" name="effects_pm_titlechange" value="1" id="effects_pm_titlechange_yes" {SELECTED_PM_TITLECHANGE_YES} /><label for="effects_pm_titlechange_yes"> {L_YES}</label>
			<input type="radio" name="effects_pm_titlechange" value="0" id="effects_pm_titlechange_no" {SELECTED_PM_TITLECHANGE_NO} /><label for="effects_pm_titlechange_no"> {L_NO}</label>
		</td>
	</tr>
	<tr>
		<td class="row1 genmed">{L_PM_ADMIN_NAMECHANGE}</td>
		<td class="row2 genmed">
			<input type="radio" name="effects_pm_admin_titlechange" value="1" id="effects_pm_admin_titlechange_yes" {SELECTED_PM_ADMIN_TITLECHANGE_YES} /><label for="effects_pm_admin_titlechange_yes"> {L_YES}</label>
			<input type="radio" name="effects_pm_admin_titlechange" value="0" id="effects_pm_admin_titlechange_no" {SELECTED_PM_ADMIN_TITLECHANGE_NO} /><label for="effects_pm_admin_titlechange_no"> {L_NO}</label>
		</td>
	</tr>
	<tr>
		<td class="row1 genmed">{L_PM_ADMIN_TITLECHANGE}</td>
		<td class="row2 genmed">
			<input type="radio" name="effects_pm_admin_namechange" value="1" id="effects_pm_admin_namechange_yes" {SELECTED_PM_ADMIN_NAMECHANGE_YES} /><label for="effects_pm_admin_namechange_yes"> {L_YES}</label>
			<input type="radio" name="effects_pm_admin_namechange" value="0" id="effects_pm_admin_namechange_no" {SELECTED_PM_ADMIN_NAMECHANGE_NO} /><label for="effects_pm_admin_namechange_no"> {L_NO}</label>
		</td>
	</tr> -->
	<tr>
		<td class="row1 genmed">
			{L_AVATAR}<br />
			<span class="gensmall">{L_AVATAR_EXPLAIN}</span>
		</td>
		<td class="row2 genmed"><input type="text" name="effects_avatar" maxlength="11" size="10" value="{AVATAR}" /></td>
	</tr>
	<tr>
		<td class="row1 genmed">
			{L_SIGNATURE}<br />
			<span class="gensmall">{L_SIGNATURE_EXPLAIN}</span>
		</td>
		<td class="row2 genmed"><input type="text" name="effects_signature" maxlength="11" size="10" value="{SIGNATURE}" /></td>
	</tr>
	<tr>
		<td class="row1 genmed">
			{L_EFFECT_TITLE}<br />
			<span class="gensmall">{L_TITLE_EXPLAIN}</span>
		</td>
		<td class="row2 genmed"><input type="text" name="effects_title" maxlength="11" size="10" value="{EFFECT_TITLE}" /></td>
	</tr>
	<tr>
		<td class="row1 genmed">
			{L_RANK_REPLACE}<br />
			<span class="gensmall">{L_RANK_EXPLAIN}</span>
		</td>
		<td class="row2 genmed"><input type="text" name="effects_rank_replace" maxlength="11" size="10" value="{RANK_REPLACE}" /></td>
	</tr>
	<tr>
		<td class="row1 genmed">
			{L_CHANGE_USERNAME}<br />
			<span class="gensmall">{L_CHANGE_USERNAME_EXPLAIN}</span>
		</td>
		<td class="row2 genmed"><input type="text" name="effects_namechange" maxlength="11" size="10" value="{NAME_CHANGE}" /></td>
	</tr>
	<tr>
		<td class="row1 genmed">
			{L_OTHER_TITLE}<br />
			<span class="gensmall">{L_OTHER_TITLE_EXPLAIN}</span>
		</td>
		<td class="row2 genmed"><input type="text" name="effects_titlechange" maxlength="11" size="10" value="{TITLE_CHANGE}" /></td>
	</tr>
	<tr>
		<td colspan="2" align="center" class="catbottom genmed">
			<input type="submit" value="{L_SUBMIT}" class="mainoption" />
			<input type="reset" value="{L_RESET}" class="liteoption" />
		</td>	
	</tr>
	</table>
</form>
<p>{SHOP_BACKLINKS}</p>

<table width="99%" cellpadding="4" cellspacing="1" border="0" align="center" class="forumline">
	<tr>
		<td width="100%" class="row3" align="center"><br /><span class="gensmall">Shop Modification: Copyright &copy; 2003, 2005 <a href="http://www.zarath.com/mods/" class="navsmall">Zarath Technologies</a>.
		
		<br /><br />
		Effects Store Add-on: Copyright &copy; 2006 <a href="http://www.phpbbsmith.com/" class="navsmall">Thoul</a>, based on Shop Modification by <a href="http://www.zarath.com/" class="navsmall">Zarath Technologies</a>.
		</span><br /><br /></td>
	</tr>
</table>
<br	clear="all" />
{SHOP_BACKLINKS}
