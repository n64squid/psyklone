<h1>{TOPICTITLE}</h1>
<p>{TOPICEXPLAIN}</p>
<form action="{S_CONFIG_ACTION}" method="post">
  <input type="hidden" name="action" value="updatejob">
  <input type="hidden" name="jobid" value="{JOB_ID}">
<table width="30%" cellpadding="4" cellspacing="1" border="0" align="center" class="forumline">
	<tr> 
	  <th class="thHead" colspan="2">{TABLE_TITLE}</th>
	</tr>
	<tr>
	  <td class="row2"><span class="gensmall">{L_JOB_NAME}</span></td>
	  <td class="row2"><input type="text" name="name" size="30" maxlength="32" value="{JOB_NAME}" class="post" /></td>
	</tr>
	<tr>
	  <td class="row2"><span class="gensmall">{L_MAX_POSITIONS}</span></td>
	  <td class="row2"><input type="text" name="positions" size="5" maxlength="5" value="{JOB_POSITIONS}" class="post" /></td>
	</tr>
	<tr>
	  <td class="row2"><span class="gensmall">{L_PAY_AMOUNT}</span></td>
	  <td class="row2"><input type="text" name="pay" size="5" maxlength="10" value="{JOB_PAY}" class="post" /> {cash_NAME}</td>
	</tr>
	<tr>
	  <td class="row2"><span class="gensmall">{L_PAY_TIME}</span></td>
	  <td class="row2"><input type="text" name="paytime" size="15" maxlength="30" value="{JOB_PAYTIME}" class="post" /> {L_SECONDS}</td>
	</tr>
	<tr>
	  <td class="row2"><span class="gensmall">{L_REQUIREMENTS}</span></td>
	  <td class="row2"><input type="text" name="requirements" size="30" value="{JOB_REQUIREMENTS}" class="post" /></td>
	</tr>
	<tr>
	  <td class="row2"><span class="gensmall">{L_TYPE}</span></td>
	  <td class="row2">
		<select name="type">
		  <option value="public" {JOB_PUBLIC_SELECTED}>{L_PUBLIC}</option>
		  <option value="private" {JOB_PRIVATE_SELECTED}>{L_PRIVATE}</option>
		</select>
	  </td>
	</tr>
	<tr>
	  <td width="100%" class="row1" colspan="2">
		<table width="100%" cellpadding="2" cellspacing="1" border="0" align="center" class="forumline">
			<tr>
			  <td class="row1" align="center" width="50%"><input type="submit" name="update" value="{L_UPDATE_JOB}" class="liteoption" /></td>
			  <td class="row1" align="center" width="50%"><input type="submit" name="delete" value="{L_DELETE_JOB}" class="liteoption" /></td>
			</tr>
		</table>
	  </td>
	</tr>
</table>
</form>
<br />
<table width="100%" cellpadding="4" cellspacing="1" border="0" align="center" class="forumline">
	<tr>
	  <td class="row4" align="center"><br /><span class="gensmall">{L_IMP}</span><br /><br /></td>
	</tr>
</table>
<br	clear="all" />