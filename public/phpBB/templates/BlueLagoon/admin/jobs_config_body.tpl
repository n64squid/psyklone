<h1>{TOPICTITLE}</h1>
<p>{TOPICEXPLAIN}</p>
<form action="{S_CONFIG_ACTION}" method="post">
  <input type="hidden" name="action" value="update_globals" />
<table width="100%" cellpadding="4" cellspacing="1" border="0" align="center" class="forumline">
	<tr> 
	  <th class="thHead" colspan="2">{TABLE_TITLE}</th>
	</tr>
	<tr>
	  <td class="row2"><span class="gensmall">{L_JOB_STATUS}</span></td>
	  <td class="row2"><select name="status"><option value="on" {ON_JOB_STATUS}>{L_ON}<option value="off" {OFF_JOB_STATUS}>{L_OFF}</option></select></td>
	</tr>
	<tr>
	  <td class="row2"><span class="gensmall">{L_JOB_INDEX}</span></td>
	  <td class="row2"><select name="index_type"><option value="0" {INDEX_1}>{L_COMPACT}<option value="1" {INDEX_2}>{L_EXTENDED}</option></select></td>
	</tr>
	<tr>
	  <td class="row2"><span class="gensmall">{L_PAY_TYPE}</span></td>
	  <td class="row2"><select name="pay_type"><option value="0" {PAY_TYPE_1}>{L_PAY_PP}<option value="1" {PAY_TYPE_2}>{L_PAY_ALL}</option></select></td>
	</tr>
	<tr>
	  <td class="row2"><span class="gensmall">{L_MAX_JOBS}</span></td>
	  <td class="row2"><input type="text" name="limit" size="5" value="{MAX_JOBS}" maxlength="5" class="post" /></td>
	</tr>
	<tr>
	  <td class="row2" colspan="2" align="center"><input type="submit" value="{L_UPDATE}" name="Update" class="liteoption" /></td>
	</tr>
</form>
</table>

<br />

<form method="post" action="{S_CONFIG_ACTION}" name="post">
  <input type="hidden" name="action" value="edit_user" />
<table width="400" cellpadding="4" cellspacing="1" border="0" align="center" class="forumline">
	<tr> 
	  <th class="thHead">{USER_TABLE_TITLE}</th>
	</tr>
	<tr>
	  <td class="row2" align="center"><input type="text" class="post" name="username" maxlength="25" size="25" /> <input type="submit" value="{L_EDIT_JOBS}" class="liteoption" /> <input type="submit" name="usersubmit" value="{L_FIND_USERNAME}" class="liteoption" onClick="window.open('./../search.php?mode=searchuser', '_phpbbsearch', 'HEIGHT=250,resizable=yes,WIDTH=400');return false;" /></td>
	</tr>
</form>
</table>

<br /><br />

<table width="100%" cellpadding="4" cellspacing="1" border="0" align="center" class="forumline">
<form method="post" action="{S_CONFIG_ACTION}">
  <input type="hidden" name="action" value="editjob" />
	<tr> 
	  <th class="thHead" colspan="2">{JOB_TABLE_TITLE}</th>
	</tr>
	<tr>
	  <td class="row2">
		<select name="job">
		<!-- BEGIN listrow -->
		  <option value="{listrow.JOB_ID}">{listrow.JOB_NAME}</option>
		<!-- END listrow -->
		</select>
	  </td>
	  <td class="row2"><input type="submit" value="{L_EDIT_JOB}" /></td>
	</tr>
</form>
</table>

<br />

<table width="100%" cellpadding="4" cellspacing="1" border="0" align="center" class="forumline">
<form method="post" action="{S_CONFIG_ACTION}">
  <input type="hidden" name="action" value="createjob" />
	<tr>
	  <th colspan="2" class="thHead" align="center">{ADD_TABLE_TITLE}</th>
	</tr>
	<tr>
	  <td class="row2"><span class="gensmall">{L_JOB_NAME}</span></td>
	  <td class="row2"><input type="text" name="name" size="30" maxlength="32" class="post" /></td>
	</tr>
	<tr>
	  <td class="row2"><span class="gensmall">{L_MAX_POSITIONS}</span></td>
	  <td class="row2"><input type="text" name="positions" size="5" maxlength="5" class="post" /></td>
	</tr>
	<tr>
	  <td class="row2"><span class="gensmall">{L_PAY_AMOUNT}</span></td>
	  <td class="row2"><input type="text" name="pay" size="5" maxlength="10" class="post" /> {cash_NAME}</td>
	</tr>
	<tr>
	  <td class="row2"><span class="gensmall">{L_PAY_TIME}</span></td>
	  <td class="row2"><input type="text" name="paytime" size="15" maxlength="30" value="500000" class="post" /> {L_SECONDS}</td>
	</tr>
	<tr>
	  <td class="row2"><span class="gensmall">{L_REQUIREMENTS}</span></td>
	  <td class="row2"><input type="text" name="requirements" size="30" class="post" /></td>
	</tr>
	<tr>
	  <td class="row2"><span class="gensmall">{L_TYPE}</span></td>
	  <td class="row2"><select name="type"><option value="public">{L_PUBLIC}</option><option value="private">{L_PRIVATE}</option></select></td>
	</tr>
	<tr>
	  <td class="row2" colspan="2" align="center"><input type="submit" value="{L_CREATE_JOB}" class="liteoption" /></td>
	</tr>
</table>
</form>
<br />
<table width="100%" cellpadding="4" cellspacing="1" border="0" align="center" class="forumline">
	<tr>
	  <td class="row4" align="center"><br /><span class="gensmall">{L_IMP}</span><br /><br /></td>
	</tr>
</table>
<br	clear="all" />