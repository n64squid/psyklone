
<h1>{L_STAT_UPDATES_TITLE}</h1>

<p>{L_STAT_UPDATES_EXPLAIN}</p>

<h1>List of all Active modules and available updates</h1>
<table width="100%" cellpadding="2" cellspacing="1" border="0" class="forumline">
	<tr>
		<th colspan="2" width="25%" class="thHead">{L_CURRENT_ACTIVE}</th>
		<th width="25%" class="thHead">{L_CURRENT_VERSION}</th>
		<th width="25%" class="thHead">{L_MODULE_UPDATE}</th>
		<th width="25%" class="thHead">{L_NEWEST_VERSION}</th>
	</tr>

<!-- BEGIN statsversion -->
	<tr onMouseOver="this.bgColor = '{statsversion.TR_COLOR}'" onMouseOut ="this.bgColor = '#EAEDF4'" bgcolor="#EAEDF4">
		<td class="row2" nowrap="nowrap" colspan="2"><span class="gensmall">{statsversion.MODULE_NAME}</span>
		</td>
		<td nowrap="nowrap"><span class="gensmall">{statsversion.MODULE_VERSION}</span>
		</td>
		<td class="row2" nowrap="nowrap"><span class="gensmall">{statsversion.NEW_MODULE_NAME}</span>
		</td>
		<td nowrap="nowrap"><span class="gensmall">{statsversion.NEW_MODULE_VERSION}</span>
		</td>
	</tr>
<!-- END statsversion -->
	<tr>
		<td colspan="5" class="catLeft"><span class="gensmall">{L_ACTIVATED_ON_BOARD}</span>
		</td>
	</tr>
<!-- BEGIN latestmodulerow -->
	<tr onMouseOver="this.bgColor = '{latestmodulerow.TR_COLOR}'" onMouseOut ="this.bgColor = '#EAEDF4'" bgcolor="#EAEDF4">
		<td nowrap="nowrap"><span class="gensmall">{latestmodulerow.MODULE_NUM}</span>
		</td>
		<td class="row2" nowrap="nowrap"><span class="gensmall">{latestmodulerow.MODULE_NAME}</span>
		</td>
		<td nowrap="nowrap"><span class="gensmall">{latestmodulerow.MODULE_VERSION}</span>
		</td>
		<td class="row2" nowrap="nowrap"><span class="gensmall">{latestmodulerow.NEW_MODULE_NAME}</span>
		</td>
		<td nowrap="nowrap"><span class="gensmall">{latestmodulerow.NEW_MODULE_VERSION}</span>
		</td>
	</tr>
<!-- END latestmodulerow -->
	<tr>
		<td colspan="5" height="1" class="spaceRow"><img src="../templates/subSilver/images/spacer.gif" alt="" width="1" height="1" /></td>
	</tr>
</table>
<h1>Complete list of all available modules</h1>
<table bgcolor="#efefef" width="100%" cellpadding="2" cellspacing="1" border="0" class="forumline">
	<tr>
		<th colspan="2" width="25%" class="thHead">{L_MODULE}</th>
		<th width="25%" class="thHead">{L_MODULEVERSION}</th>
		<th width="25%" class="thHead">{L_LATEST_INSTALLED}</th>
		<th width="25%" class="thHead">{L_ACTIVE}</th>
	</tr>
<!-- BEGIN completelistnew -->
	<tr onMouseOver="this.bgColor = '{completelistnew.TR_COLOR}'" onMouseOut ="this.bgColor = '#EAEDF4'" bgcolor="#EAEDF4">
		<td nowrap="nowrap"><span class="gensmall">{completelistnew.MODULE_NUM}</span>
		</td>
		<td class="row2" nowrap="nowrap"><span class="gensmall">{completelistnew.NEW_MODULE_NAME}</span>
		</td>
		<td nowrap="nowrap"><span class="gensmall">{completelistnew.NEW_MODULE_VERSION}</span>
		</td>
		<td class="row2" nowrap="nowrap"><span class="gensmall">{completelistnew.L_INSTALLED}</span>
		</td>
		<td nowrap="nowrap"><span class="gensmall">{completelistnew.L_ACTIVE}</span>
		</td>
	</tr>
<!-- END completelistnew -->
	<tr>
		<td colspan="5" height="1" class="spaceRow"><img src="../templates/subSilver/images/spacer.gif" alt="" width="1" height="1" /></td>
	</tr>
</table>


