<?php
/***************************************************************************
 *                               rabbitoshi_db_update.php
 *                            -------------------
 *
 *   copyright            : �2003 Freakin' Booty ;-P & Antony Bailey
 *
 ***************************************************************************/

/***************************************************************************
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

define('IN_PHPBB', true);
$phpbb_root_path = './';
include($phpbb_root_path . 'extension.inc');
include($phpbb_root_path . 'common.'.$phpEx);

//
// Start session management
//
$userdata = session_pagestart($user_ip, PAGE_INDEX);
init_userprefs($userdata);
//
// End session management
//


if( !$userdata['session_logged_in'] )
{
	$header_location = ( @preg_match('/Microsoft|WebSTAR|Xitami/', getenv('SERVER_SOFTWARE')) ) ? 'Refresh: 0; URL=' : 'Location: ';
	header($header_location . append_sid("login.$phpEx?redirect=db_update.$phpEx", true));
	exit;
}

if( $userdata['user_level'] != ADMIN )
{
	message_die(GENERAL_MESSAGE, 'You are not authorised to access this page');
}


$page_title = 'Updating the database';
include($phpbb_root_path . 'includes/page_header.'.$phpEx);

echo '<table width="100%" cellspacing="1" cellpadding="2" border="0" class="forumline">';
echo '<tr><th>Updating the database</th></tr><tr><td><span class="genmed"><ul type="circle">';


$sql = array();
$sql[] = "INSERT INTO " . $table_prefix . "config (config_name, config_value) VALUES ('rabbitoshi_enable', '1')";
$sql[] = "INSERT INTO " . $table_prefix . "config (config_name, config_value) VALUES ('rabbitoshi_name', 'Rabbistoshi')";
$sql[] = "INSERT INTO " . $table_prefix . "config (config_name, config_value) VALUES ('rabbitoshi_enable_cron', '1')";
$sql[] = "INSERT INTO " . $table_prefix . "config (config_name, config_value) VALUES ('rabbitoshi_cron_time', '86400')";
$sql[] = "INSERT INTO " . $table_prefix . "config (config_name, config_value) VALUES ('rabbitoshi_cron_last_time', '0')";
$sql[] = "CREATE TABLE " . $table_prefix . "rabbitoshi_config (
  creature_id smallint(2) NOT NULL default '0',
  creature_name varchar(255) NOT NULL default '',
  creature_prize int(8) NOT NULL default '0',
  creature_max_hunger int(8) NOT NULL default '0',
  creature_max_thirst int(8) NOT NULL default '0',
  creature_max_health int(8) NOT NULL default '0',
  creature_max_hygiene int(8) NOT NULL default '0',
  creature_food_id smallint(2) NOT NULL default '0',
  creature_buyable tinyint(1) NOT NULL default '1',
  creature_evolution_of int(8) NOT NULL default '0',
  creature_img varchar(255) NOT NULL default '',
  PRIMARY KEY  (creature_id)
) TYPE=MyISAM";
$sql[] = "INSERT INTO " . $table_prefix . "rabbitoshi_config (creature_id, creature_name, creature_prize, creature_max_hunger, creature_max_thirst, creature_max_health, creature_max_hygiene, creature_food_id, creature_buyable, creature_evolution_of, creature_img) VALUES (10, 'Eagle', 500, 150, 60, 150, 60, 3, 1, 0, 'Eagle.gif')";
$sql[] = "INSERT INTO " . $table_prefix . "rabbitoshi_config (creature_id, creature_name, creature_prize, creature_max_hunger, creature_max_thirst, creature_max_health, creature_max_hygiene, creature_food_id, creature_buyable, creature_evolution_of, creature_img) VALUES (9, 'Tiger', 5000, 60, 400, 60, 400, 5, 1, 0, 'Tiger.gif')";
$sql[] = "INSERT INTO " . $table_prefix . "rabbitoshi_config (creature_id, creature_name, creature_prize, creature_max_hunger, creature_max_thirst, creature_max_health, creature_max_hygiene, creature_food_id, creature_buyable, creature_evolution_of, creature_img) VALUES (8, 'Beaver', 30, 20, 20, 20, 50, 1, 1, 0, 'Beaver.gif')";
$sql[] = "INSERT INTO " . $table_prefix . "rabbitoshi_config (creature_id, creature_name, creature_prize, creature_max_hunger, creature_max_thirst, creature_max_health, creature_max_hygiene, creature_food_id, creature_buyable, creature_evolution_of, creature_img) VALUES (7, 'Hiloterium', 50, 200, 200, 200, 1500, 5, 1, 0, 'Hiloterium.gif')";
$sql[] = "INSERT INTO " . $table_prefix . "rabbitoshi_config (creature_id, creature_name, creature_prize, creature_max_hunger, creature_max_thirst, creature_max_health, creature_max_hygiene, creature_food_id, creature_buyable, creature_evolution_of, creature_img) VALUES (6, 'Dragon', 20000, 300, 1000, 5000, 250, 6, 1, 0, 'Dragon.gif')";
$sql[] = "INSERT INTO " . $table_prefix . "rabbitoshi_config (creature_id, creature_name, creature_prize, creature_max_hunger, creature_max_thirst, creature_max_health, creature_max_hygiene, creature_food_id, creature_buyable, creature_evolution_of, creature_img) VALUES (11, 'Polar Bear', 700, 500, 500, 500, 500, 4, 1, 0, 'Polar_Bear.gif')";
$sql[] = "INSERT INTO " . $table_prefix . "rabbitoshi_config (creature_id, creature_name, creature_prize, creature_max_hunger, creature_max_thirst, creature_max_health, creature_max_hygiene, creature_food_id, creature_buyable, creature_evolution_of, creature_img) VALUES (12, 'Rabbit', 20, 20, 20, 20, 50, 2, 1, 0, 'Rabbit.gif')";
$sql[] = "INSERT INTO " . $table_prefix . "rabbitoshi_config (creature_id, creature_name, creature_prize, creature_max_hunger, creature_max_thirst, creature_max_health, creature_max_hygiene, creature_food_id, creature_buyable, creature_evolution_of, creature_img) VALUES (13, 'Chaos Rabbit', 8000, 1500, 1500, 1500, 100, 2, 1, 12, 'Chaos_Rabbit.gif')";
$sql[] = "INSERT INTO " . $table_prefix . "rabbitoshi_config (creature_id, creature_name, creature_prize, creature_max_hunger, creature_max_thirst, creature_max_health, creature_max_hygiene, creature_food_id, creature_buyable, creature_evolution_of, creature_img) VALUES (4, 'Lion', 240, 80, 70, 400, 120, 5, 1, 0, 'Lion.gif')";
$sql[] = "INSERT INTO " . $table_prefix . "rabbitoshi_config (creature_id, creature_name, creature_prize, creature_max_hunger, creature_max_thirst, creature_max_health, creature_max_hygiene, creature_food_id, creature_buyable, creature_evolution_of, creature_img) VALUES (3, 'Cat', 40, 20, 20, 90, 20, 4, 1, 0, 'Cat.gif')";
$sql[] = "INSERT INTO " . $table_prefix . "rabbitoshi_config (creature_id, creature_name, creature_prize, creature_max_hunger, creature_max_thirst, creature_max_health, creature_max_hygiene, creature_food_id, creature_buyable, creature_evolution_of, creature_img) VALUES (2, 'Dog', 40, 50, 50, 60, 100, 3, 1, 0, 'Dog.gif')";
$sql[] = "INSERT INTO " . $table_prefix . "rabbitoshi_config (creature_id, creature_name, creature_prize, creature_max_hunger, creature_max_thirst, creature_max_health, creature_max_hygiene, creature_food_id, creature_buyable, creature_evolution_of, creature_img) VALUES (1, 'Chinchilla', 120, 100, 100, 15, 100, 1, 1, 0, 'Chinchilla.gif')";
$sql[] = "INSERT INTO " . $table_prefix . "rabbitoshi_config (creature_id, creature_name, creature_prize, creature_max_hunger, creature_max_thirst, creature_max_health, creature_max_hygiene, creature_food_id, creature_buyable, creature_evolution_of, creature_img) VALUES (5, 'Penguin', 500, 120, 60, 75, 50, 4, 1, 0, 'Penguin.gif')";
$sql[] = "CREATE TABLE " . $table_prefix . "rabbitoshi_general (
  config_name varchar(255) NOT NULL default '0',
  config_value int(15) NOT NULL default '0',
  PRIMARY KEY  (config_name)
) TYPE=MyISAM";
$sql[] = "INSERT INTO " . $table_prefix . "rabbitoshi_general (config_name, config_value) VALUES ('thirst_time', 600)";
$sql[] = "INSERT INTO " . $table_prefix . "rabbitoshi_general (config_name, config_value) VALUES ('thirst_value', 1)";
$sql[] = "INSERT INTO " . $table_prefix . "rabbitoshi_general (config_name, config_value) VALUES ('hunger_time', 600)";
$sql[] = "INSERT INTO " . $table_prefix . "rabbitoshi_general (config_name, config_value) VALUES ('hunger_value', 1)";
$sql[] = "INSERT INTO " . $table_prefix . "rabbitoshi_general (config_name, config_value) VALUES ('hygiene_time', 3600)";
$sql[] = "INSERT INTO " . $table_prefix . "rabbitoshi_general (config_name, config_value) VALUES ('hygiene_value', 1)";
$sql[] = "INSERT INTO " . $table_prefix . "rabbitoshi_general (config_name, config_value) VALUES ('health_time', 86400)";
$sql[] = "INSERT INTO " . $table_prefix . "rabbitoshi_general (config_name, config_value) VALUES ('health_value', 5)";
$sql[] = "INSERT INTO " . $table_prefix . "rabbitoshi_general (config_name, config_value) VALUES ('rebirth_enable', 1)";
$sql[] = "INSERT INTO " . $table_prefix . "rabbitoshi_general (config_name, config_value) VALUES ('rebirth_price', 50000)";
$sql[] = "INSERT INTO " . $table_prefix . "rabbitoshi_general (config_name, config_value) VALUES ('vet_enable', 1)";
$sql[] = "INSERT INTO " . $table_prefix . "rabbitoshi_general (config_name, config_value) VALUES ('vet_price', 300)";
$sql[] = "INSERT INTO " . $table_prefix . "rabbitoshi_general (config_name, config_value) VALUES ('hotel_enable', 1)";
$sql[] = "INSERT INTO " . $table_prefix . "rabbitoshi_general (config_name, config_value) VALUES ('hotel_cost', 500)";
$sql[] = "INSERT INTO " . $table_prefix . "rabbitoshi_general (config_name, config_value) VALUES ('evolution_enable', 1)";
$sql[] = "INSERT INTO " . $table_prefix . "rabbitoshi_general (config_name, config_value) VALUES ('evolution_cost', 100)";
$sql[] = "INSERT INTO " . $table_prefix . "rabbitoshi_general (config_name, config_value) VALUES ('evolution_time', 25)";
$sql[] = "CREATE TABLE " . $table_prefix . "rabbitoshi_shop (
  item_id smallint(1) NOT NULL default '0',
  item_name varchar(255) NOT NULL default '',
  item_prize int(8) NOT NULL default '0',
  item_desc varchar(255) NOT NULL default '',
  item_type smallint(1) NOT NULL default '0',
  item_power int(8) NOT NULL default '0',
  item_img varchar(255) NOT NULL default '',
  PRIMARY KEY  (item_id)
) TYPE=MyISAM";
$sql[] = "INSERT INTO " . $table_prefix . "rabbitoshi_shop (item_id, item_name, item_prize, item_desc, item_type, item_power, item_img) VALUES (1, 'Rabbitoshi_items_seeds', 10, 'Rabbitoshi_items_seeds_desc', 1, 1, 'Seeds.gif')";
$sql[] = "INSERT INTO " . $table_prefix . "rabbitoshi_shop (item_id, item_name, item_prize, item_desc, item_type, item_power, item_img) VALUES (2, 'Rabbitoshi_items_carrots', 20, 'Rabbitoshi_items_carrots_desc', 1, 20, 'Carotts.gif')";
$sql[] = "INSERT INTO " . $table_prefix . "rabbitoshi_shop (item_id, item_name, item_prize, item_desc, item_type, item_power, item_img) VALUES (3, 'Rabbitoshi_items_meat', 30, 'Rabbitoshi_items_meat_desc', 1, 3, 'Fresh_meat.gif')";
$sql[] = "INSERT INTO " . $table_prefix . "rabbitoshi_shop (item_id, item_name, item_prize, item_desc, item_type, item_power, item_img) VALUES (4, 'Rabbitoshi_items_fish', 30, 'Rabbitoshi_items_fish_desc', 1, 3, 'Fish.gif')";
$sql[] = "INSERT INTO " . $table_prefix . "rabbitoshi_shop (item_id, item_name, item_prize, item_desc, item_type, item_power, item_img) VALUES (5, 'Rabbitoshi_items_gazelle', 100, 'Rabbitoshi_items_gazelle_desc', 1, 8, 'Gazelle.gif')";
$sql[] = "INSERT INTO " . $table_prefix . "rabbitoshi_shop (item_id, item_name, item_prize, item_desc, item_type, item_power, item_img) VALUES (6, 'Rabbitoshi_items_diamonds', 500, 'Rabbitoshi_items_diamonds_desc', 1, 10, 'Diamonds.gif')";
$sql[] = "INSERT INTO " . $table_prefix . "rabbitoshi_shop (item_id, item_name, item_prize, item_desc, item_type, item_power, item_img) VALUES (7, 'Rabbitoshi_items_limpid_water', 2, 'Rabbitoshi_items_limpid_water_desc', 2, 4, 'Limpid_water.gif')";
$sql[] = "INSERT INTO " . $table_prefix . "rabbitoshi_shop (item_id, item_name, item_prize, item_desc, item_type, item_power, item_img) VALUES (8, 'Rabbitoshi_items_cleaner', 500, 'Rabbitoshi_items_cleaner_desc', 3, 10, 'Cleaner.gif')";
$sql[] = "CREATE TABLE " . $table_prefix . "rabbitoshi_shop_users (
  item_id mediumint(8) NOT NULL default '0',
  user_id mediumint(8) NOT NULL default '0',
  item_amount mediumint(8) NOT NULL default '0',
  KEY item_id (item_id),
  KEY user_id (user_id)
) TYPE=MyISAM";
$sql[] = "CREATE TABLE " . $table_prefix . "rabbitoshi_users (
  owner_id int(8) NOT NULL default '0',
  owner_last_visit int(11) NOT NULL default '0',
  owner_creature_id smallint(2) NOT NULL default '0',
  owner_creature_name varchar(255) NOT NULL default '',
  creature_hunger int(8) NOT NULL default '0',
  creature_thirst int(8) NOT NULL default '0',
  creature_health int(8) NOT NULL default '0',
  creature_hygiene int(8) NOT NULL default '0',
  creature_age int(11) NOT NULL default '0',
  creature_hotel int(11) NOT NULL default '0',
  owner_notification tinyint(1) NOT NULL default '0',
  owner_hide tinyint(1) NOT NULL default '0',
  owner_feed_full tinyint(1) NOT NULL default '1',
  owner_drink_full tinyint(1) NOT NULL default '1',
  owner_clean_full tinyint(1) NOT NULL default '1',
  PRIMARY KEY  (owner_id)
) TYPE=MyISAM";

for( $i = 0; $i < count($sql); $i++ )
{
	if( !$result = $db->sql_query ($sql[$i]) )
	{
		$error = $db->sql_error();

		echo '<li>' . $sql[$i] . '<br /> +++ <font color="#FF0000"><b>Error:</b></font> ' . $error['message'] . '</li><br />';
	}
	else
	{
		echo '<li>' . $sql[$i] . '<br /> +++ <font color="#00AA00"><b>Successfull</b></font></li><br />';
	}
}


echo '</ul></span></td></tr><tr><td class="catBottom" height="28">&nbsp;</td></tr>';

echo '<tr><th>End</th></tr><tr><td><span class="genmed">Installation is now finished. Please be sure to delete this file now.</span></td></tr>';
echo '<tr><td class="catBottom" height="28" align="center"><span class="genmed"><a href="' . append_sid("index.$phpEx") . '">Have a nice day</a></span></td></table>';

include($phpbb_root_path . 'includes/page_tail.'.$phpEx);

?>