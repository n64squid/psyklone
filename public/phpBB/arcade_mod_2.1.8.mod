##############################################################
## MOD Title: Arcade / Activities MOD
## MOD Author: Napoleon < napoleon@inetangel.com > (N/A) N/A
## MOD Author: Chris Moore < N/A > (N/A) www.iNetAngel.com
## MOD Author: defender-uk < defenders_realm@yahoo.com > (N/A) N/A
## MOD Author: Andy Smith < N/A > (N/A) http://www.phpbb-arcade.com
## MOD Description: Online Arcade/Activities Manager / Plug in games and other activities for your users.
## MOD Version: 2.1.8
## 
## Installation Level: Moderate
## Installation Time: 20 minutes
## Files To Edit: 
##                viewonline.php
##                admin/index.php
##                admin/admin_users.php
##                language/lang_english/lang_main.php
##                includes/constants.php
##                includes/page_header.php
##                includes/usercp_avatar.php
##                includes/usercp_register.php
##                templates/subSilver/admin/user_edit_body.tpl
##                templates/subSilver/profile_add_body.tpl
##                templates/subSilver/overall_header.tpl
## Included Files: 	
##			216_fix_1.php
##			216_fix2.php
##			218_fix_1.php
##			activity.php
##			arcade.php
##			arcade_comment.php
##			arcade_highscore.php
##			arcade_modcp.php
##			arcade_rate.php
##			IBProArcade.php
##			loader.php
##			newscore.php
##			pnFlashGames.php
##			/admin/admin_arcade.php
##			/admin/admin_arcade_cache.php
##			/admin/admin_arcade_games.php
##			/admin/admin_arcade_reset.php
##			/admin/admin_arcade_scores.php
##			/admin/admin_arcade_set.php
##			/admin/admin_arcade_tournaments.php
##			/admin/admin_arcade_update.php
##			/games/donkeykong/donkeykong.swf
##			/games/donkeykong/donkeyknog.gif
##			/images/arcade.gif
##			/images/arrow_down.gif
##			/images/arrow_end.gif
##			/images/arrow_top.gif
##			/images/arrow_up.gif
##			/images/close.gif
##			/images/comment.gif
##			/images/comments.gif
##			/images/crown.gif
##			/images/favorite.gif
##			/images/games.gif
##			/images/icon_delete.gif
##			/images/icon_half_star.gif
##			/images/icon_star.gif
##			/images/keyboard.gif
##			/images/moderate.gif
##			/images/mouse.gif
##			/images/new.gif
##			/images/play_again.gif
##			/images/rate.gif
##			/images/remove_favorite.gif
##			/images/trophy2.gif
##			/images/trophy3.gif
##			/images/trophy.gif
##			/includes/classes_arcade.php
##			/includes/constants_arcade.php
##			/includes/functions_arcade.php
##			/includes/functions_highscore.php
##			/language/lang_english/lang_admin_arcade.php
##			/language/lang_english/lang_extend_arcade.php
##			/templates/subSilver/admin/arcade_games_edit_body.tpl
##			/templates/subSilver/admin/arcade_cache.tpl
##			/templates/subSilver/admin/arcade_cats_body.tpl
##			/templates/subSilver/admin/arcade_cats_edit_body.tpl
##			/templates/subSilver/admin/arcade_config_body.tpl
##			/templates/subSilver/admin/arcade_games_body.tpl
##			/templates/subSilver/admin/arcade_games_edit_body.tpl
##			/templates/subSilver/admin/arcade_import_body.tpl
##			/templates/subSilver/admin/arcade_messages_body.tpl
##			/templates/subSilver/admin/arcade_moderators_body.tpl
##			/templates/subSilver/admin/arcade_scores.tpl
##			/templates/subSilver/admin/arcade_set.tpl
##			/templates/subSilver/admin/arcade_switches_body.tpl
##			/templates/subSilver/admin/arcade_tour_add_games.tpl
##			/templates/subSilver/admin/arcade_tournament_add.tpl
##			/templates/subSilver/admin/arcade_tournament_body.tpl
##			/templates/subSilver/images/game_info.gif
##			/templates/subSilver/images/games.gif
##			/templates/subSilver/images/icon_mini_activity.gif
##			/templates/subSilver/arcade_body.tpl
##			/templates/subSilver/arcade_cats.tpl
##			/templates/subSilver/arcade_comment_body.tpl
##			/templates/subSilver/arcade_game_body.tpl
##			/templates/subSilver/arcade_game_body_self.tpl
##			/templates/subSilver/highscore_body.tpl
##			/templates/subSilver/ibp_body.tpl
##			/templates/subSilver/mod_body.tpl
##			/templates/subSilver/arcade_rate_body.tpl
##			/templates/subSilver/arcade_saved_body.tpl
##			/templates/subSilver/arcade_scores.tpl
##			/templates/subSilver/arcade_stats.tpl
##			/templates/subSilver/arcade_stats_body.tpl
##			/templates/subSilver/arcade_tour_body.tpl
##
## Required for Rewards:
##			      /includes/rewards_api.php
##
## License: http://opensource.org/licenses/gpl-license.php GNU General Public License v2
## Generator: MOD Studio [ ModTemplateTools 1.0.2288.38406 ]
## 2nd Generator: WWX's Hands & Crimson Editor 
##############################################################
## For security purposes, please check: http://www.phpbb.com/mods/
## for the latest version of this MOD. Although MODs are checked
## before being allowed in the MODs Database there is no guarantee
## that there are no security problems within the MOD. No support
## will be given for MODs not found within the MODs Database which
## can be found at http://www.phpbb.com/mods/
##############################################################
## Author Notes: 
##              Based on the Original by Napoleon Activity Mod by (Chris Moore) - Consent Given for Re-Distribution.
##              This mod was made for version 2.0.6+ of phpBB.
##	            If you are upgrading from v2.0.0 then follow the steps found in "Upgrading".
##
## Installation
##
## 1) Upload all files to their respective locations in you main phpbb directory.
## 2) Edit all the files below or run the arcade_mod_2.1.2b.mod file in EasyMod.
## 3) Goto your ACP (Admin Control Panel) and click <UPDATE DB> 
## OR 
## Alternate Install Method: (If <UPDATE DB> doesn't show up in your ACP)
## 1) Goto your ACP (Admin Control Panel) and copy your sid from the browser's address bar.
## 2) Now paste that infront of "http://www.yoursite.com/phpbb/admin/admin_arcade_update.php?" in a new browser window and run it.
## 2a)It should look something like this "http://www.yoursite.com/phpbb/admin/admin_arcade_update.php?sid=lotsofnumbersandletters"
## 3a)After installation has been succesful you should refresh your browser.
##
##
## Credits
##	- Napoleon		:: Original Version.
##	- Endoxus		:: subSilver Icon / Game button template.
##	- Q-zar 		:: French Translation.
##	- mondi 		:: General support.
##	- Scott Porter 	:: gamelib Author.
##	- Whoo			:: Games and Cheat Fix.
##  - WWX           :: Easy Mod & phpBB 2.0.20 Update.  
##############################################################
## MOD History:
## 
## 2003-06-25 - Version 1.0.0
## -First release.
## 
## 2003-12-01 - Version 2.0.0
## -Cheat Fix.
## -Division by Zero bug fixed.
## -Added an installer / uninstaller / upgrader.
## -Removed Allowance System support.
## -Merged ina_score.php with newscore.php [ina_score.php now useless]
## -Optimized admin_activity.php [activity_config_body2.tpl now useless]
## -Optimized Game handling [added game.php, remove /games/gamename/gamename.php]
## -No longer have to edit game.php when adding a new game.
## -Inserted new Move routines to move games up/down on game list.
## -Added Repair button to fix game index.
## -Added Pagination support.
## -Added List limit to Highscores.
## -Added Highscore Reverse List option.
## -Added return link in activity_scores.tpl
## -Added Info button & routines.
## -New ACP.
## -Optimized Points System support.
## -Added Rewards API for Rewards routines. [Cash / Allowance]
## 
## 2004-10-06 - Version 2.0.1
## -Added Allow Guest Access.## 

## -Added Take Game Offline.
## -Added Default Image if No Game Image is Available.
## -Added All Time HighScores
## 
## 2004-07-17 - Version 2.0.4
## -Made Fully EasyMod complient
## -Merged add and edit game [add_game.tpl now useless]
## -Added Activities Offline Function
## -Added Cheat Mode off function.
## -Fixed all template errors.
## -Added Guest Text.
## -Added Default game number (so that data can be pulled from another game)
## -Added Java and Director game support.
## -Changed scores tables to use user_id
## -Added Better cheat mode.
## -Fixed Zeros being entered in to the game window sizes.
## -Added Extra fields to ACP Games menu so you can see more at a glance.
## -Added Reset All Time High Scores data.
## 
## 2004-08-19 - Version 2.0.6
## -Added option to turn off AT Highscores
## -Added option for default icon size
## -Added user Stats
## -Added pagination to ACP edit option.
## -Added Catagories
## -Added Search Function in Catagories Window
## -Added advanced navigation bar.
## -Added Support for RealMedia, Quicktime Media, Windows Media and Images.
## -Added Extra Sort Mod options
## -Added Extra cheat Mode support
## -Added Automatic Score Save type.
## -Added Automatic FLASH and IMAGE size on ADD
## -Added Option to Use Auto Size on Loading pages.
## -Added Icon Support from included functions.
## -Updated all the templates to default names.
## -Fixed Pagination
## -Fixed Points Bonus
## -Fixed Arcade Hash (bandwidth issue)
## -Changes score tables to support decimal places.
## 
## 2006-01-01 - Version 2.1.0
## -Added Rating System
## -Added pnFlashGames Support
## -Added IBProArcade Support
## -Added Comment System
## -Added Monthly Highscore System (Thanks to PainKiller and CHris)
## -Added Class Support (for reduced SQL Queries)
## -Added FileSize on Main Page
## -Added Category Info on 'All Games' Page
## -Added More Stats displayed on Categories Page
## -Added Top x Games & New Games/Worst Games on Categories AND Main Page's
## -Added SEARCH Feature
## -Added Last Played for Categories
## -Added Number of Plays to Main Page
## -Added Favourites System
## -Added Private Message System
## -Added More ACP Control over System
## -Added Automatic Flash Size Reconision
## -Added BAN System
## -Added More Sort Options, plus Sort options now available in ACP
## -Added Edit Games by Category and Move Games in Category.
## -Added TOP Player Stat's
## -Fixed NEWSCORE issues.
## -Fixed Scores not displaying correctly
## -Fixed Automated Import - Now FULLY Automated
## -Fixed Pagination issues when sorting by non-default mode
## -Moved Multiply called routines into either Functions or Classes
## -Moved Constants into own arcade_constants.php file (users kept missing the changes)
## 
## 2006-04-23 - Version 2.1.2
## -Rewritten in Mod Studio (Thanks to WWX @ http://www.gamegaia.com)
## -Easy Mod Complient (Thanks to WWX)
## -Number of files to edit updated (Thanks to WWX)
## -Code changes for phpBB 2.0.20 (Thanks to WWX)
## 
##############################################################
## Before Adding This MOD To Your Forum, You Should Back Up All Files Related To This MOD 
##############################################################

#
#-----[ OPEN ]------------------------------------------
#
viewonline.php
#
#-----[ FIND ]------------------------------------------
#
				case PAGE_FAQ:
					$location = $lang['Viewing_FAQ'];
					$location_url = "faq.$phpEx";
					break;
#
#-----[ AFTER, ADD ]------------------------------------------
#
                case PAGE_ACTIVITY: 
                    $location = $lang['Activity']; 
                    $location_url = "activity.$phpEx"; 
                    break; 
#
#-----[ OPEN ]------------------------------------------
#
includes/constants.php
#
#-----[ FIND ]------------------------------------------
#
?>
#
#-----[ BEFORE, ADD ]------------------------------------------
#
// Arcade Mod
include_once($phpbb_root_path . 'includes/constants_arcade.' . $phpEx);
// End Arcade Mod
#
#-----[ OPEN ]------------------------------------------
#
includes/page_header.php
#
#-----[ FIND ]------------------------------------------
#
	'U_GROUP_CP' => append_sid('groupcp.'.$phpEx),
#
#-----[ AFTER, ADD ]------------------------------------------
#
    // Arcade MOD 
    'U_ACTIVITY' => append_sid('activity.'.$phpEx), 
    'L_ACTIVITY' => $lang['Activity'], 
#
#-----[ OPEN ]------------------------------------------
#
admin/index.php
#
#-----[ FIND ]------------------------------------------
#
						case PAGE_FAQ:
							$location = $lang['Viewing_FAQ']; 
							$location_url = "index.$phpEx?pane=right"; 
							break;
#
#-----[ AFTER, ADD ]------------------------------------------
#
                        case PAGE_ACTIVITY: 
                            $location = $lang['Activity']; 
                            $location_url = "activity.$phpEx"; 
                            break; 
#
#-----[ OPEN ]------------------------------------------
#
templates/subSilver/overall_header.tpl
#
#-----[ FIND ]------------------------------------------
#
</head>
#
#-----[ BEFORE, ADD ]------------------------------------------
#
<SCRIPT LANGUAGE="Javascript"> 
var win = null; 
function Gk_PopTart(mypage,myname,w,h,scroll) 
{ 
  LeftPosition = (screen.width) ? (screen.width-w)/2 : 0; 
  TopPosition = (screen.height) ? (screen.height-h)/2 : 0; 
  settings = 'height='+h+',width='+w+',top='+TopPosition+',left='+LeftPosition+',scrollbars='+scroll+',resizable=yes'; 
  win = window.open(mypage,myname,settings); 
} 
</SCRIPT> 
#
#-----[ FIND ]------------------------------------------
#
 					<tr> 
						<td height="25" align="center" valign="top" nowrap="nowrap"><span class="mainmenu">&nbsp;<a href="{U_PROFILE}" class="mainmenu"><img src="templates/subSilver/images/icon_mini_profile.gif" width="12" height="13" border="0" alt="{L_PROFILE}" hspace="3" />{L_PROFILE}</a>&nbsp; &nbsp;<a href="{U_PRIVATEMSGS}" class="mainmenu"><img src="templates/subSilver/images/icon_mini_message.gif" width="12" height="13" border="0" alt="{PRIVATE_MESSAGE_INFO}" hspace="3" />{PRIVATE_MESSAGE_INFO}</a>&nbsp; &nbsp;<a href="{U_LOGIN_LOGOUT}" class="mainmenu"><img src="templates/subSilver/images/icon_mini_login.gif" width="12" height="13" border="0" alt="{L_LOGIN_LOGOUT}" hspace="3" />{L_LOGIN_LOGOUT}</a>&nbsp;</span></td>
					</tr> 
#
#-----[ AFTER, ADD ]------------------------------------------
#
                    <tr> 
                        <td align="center" valign="top" nowrap="nowrap"><span class="mainmenu">&nbsp; 
                         <a href="{U_ACTIVITY}" class="mainmenu"><img src="templates/subSilver/images/icon_mini_activity.gif" width="12" height="13" border="0" alt="{L_ACTIVITY}" hspace="3" />{L_ACTIVITY}</a>&nbsp; &nbsp;</span> 
                        </td> 
                    </tr>
#
#-----[ OPEN ]------------------------------------------
#
language/lang_english/lang_main.php
#
#-----[ FIND ]------------------------------------------
#
$lang['Notify_on_privmsg'] = 'Notify on new Private Message';
#
#-----[ AFTER, ADD ]------------------------------------------
#
$lang['Block_Arcade_pm'] = 'Block PM\'s from the Arcade';
#
#-----[ FIND ]------------------------------------------
#
?>
#
#-----[ BEFORE, ADD ]------------------------------------------
#
// Activity MOD 
$lang['Activity'] = "Games";
#
#-----[ OPEN ]------------------------------------------
#
admin/admin_users.php
#
#-----[ FIND ]------------------------------------------
#
		$notifypm = ( isset( $HTTP_POST_VARS['notifypm']) ) ? ( ( $HTTP_POST_VARS['notifypm'] ) ? TRUE : 0 ) : TRUE;
#
#-----[ AFTER, ADD ]------------------------------------------
#
        $games_block_pm = ( isset($HTTP_POST_VARS['games_block_pm']) ) ? ( ($HTTP_POST_VARS['games_block_pm']) ? TRUE : 0 ) : TRUE;
#
#-----[ FIND ]------------------------------------------
#
				SET " . $username_sql . $passwd_sql . "user_email = '" . str_replace("\'", "''", $email) . "', user_icq = '" . str_replace("\'", "''", $icq) . "', user_website = '" . str_replace("\'", "''", $website) . "', user_occ = '" . str_replace("\'", "''", $occupation) . "', user_from = '" . str_replace("\'", "''", $location) . "', user_interests = '" . str_replace("\'", "''", $interests) . "', user_sig = '" . str_replace("\'", "''", $signature) . "', user_viewemail = $viewemail, user_aim = '" . str_replace("\'", "''", $aim) . "', user_yim = '" . str_replace("\'", "''", $yim) . "', user_msnm = '" . str_replace("\'", "''", $msn) . "', user_attachsig = $attachsig, user_sig_bbcode_uid = '$signature_bbcode_uid', user_allowsmile = $allowsmilies, user_allowhtml = $allowhtml, user_allowavatar = $user_allowavatar, user_allowbbcode = $allowbbcode, user_allow_viewonline = $allowviewonline, user_notify = $notifyreply, user_allow_pm = $user_allowpm, user_notify_pm = $notifypm,
#
#-----[ IN-LINE FIND ]------------------------------------------
#
, user_notify_pm = $notifypm,
#
#-----[ IN-LINE AFTER, ADD ]------------------------------------------
#
 games_block_pm = $games_block_pm,
#
#-----[ FIND ]------------------------------------------
#
		$notifypm = $this_userdata['user_notify_pm'];
#
#-----[ AFTER, ADD ]------------------------------------------
#
        $games_block_pm = $this_userdata['games_block_pm'];
#
#-----[ FIND ]------------------------------------------
#
			$s_hidden_fields .= '<input type="hidden" name="notifypm" value="' . $notifypm . '" />';
#
#-----[ AFTER, ADD ]------------------------------------------
#
	        $s_hidden_fields .= '<input type="hidden" name="games_block_pm" value="' . $games_block_pm . '" />';
#
#-----[ FIND ]------------------------------------------
#
			'NOTIFY_PM_NO' => (!$notifypm) ? 'checked="checked"' : '',
#
#-----[ AFTER, ADD ]------------------------------------------
#
            'BLOCK_PM_YES' => ( $games_block_pm ) ? 'checked="checked"' : '',
            'BLOCK_PM_NO' => ( !$games_block_pm ) ? 'checked="checked"' : '',
#
#-----[ FIND ]------------------------------------------
#
			'L_NOTIFY_ON_PRIVMSG' => $lang['Notify_on_privmsg'],
#
#-----[ AFTER, ADD ]------------------------------------------
#
            'L_BLOCK_ARCADE_PM' => $lang['Block_Arcade_pm'],
#
#-----[ OPEN ]------------------------------------------
#
includes/usercp_avatar.php
#
#-----[ FIND ]------------------------------------------
#
function display_avatar_gallery($mode, &$category, &$user_id, &$email, &$current_email, &$coppa, &$username, &$email, &$new_password, &$cur_password, &$password_confirm, &$icq, &$aim, &$msn, &$yim, &$website, &$location, &$occupation, &$interests, &$signature, &$viewemail, &$notifypm,
#
#-----[ IN-LINE FIND ]------------------------------------------
#
, &$viewemail, &$notifypm,
#
#-----[ IN-LINE AFTER, ADD ]------------------------------------------
#
 &$games_block_pm,
#
#-----[ OPEN ]------------------------------------------
#
includes/usercp_register.php
#
#-----[ FIND ]------------------------------------------
#
	$notifypm = ( isset($HTTP_POST_VARS['notifypm']) ) ? ( ($HTTP_POST_VARS['notifypm']) ? TRUE : 0 ) : TRUE;
#
#-----[ AFTER, ADD ]------------------------------------------
#
    $games_block_pm = ( isset($HTTP_POST_VARS['games_block_pm']) ) ? ( ($HTTP_POST_VARS['games_block_pm']) ? TRUE : 0 ) : TRUE;
#
#-----[ FIND ]------------------------------------------
#
				SET " . $username_sql . $passwd_sql . "user_email = '" . str_replace("\'", "''", $email) ."', user_icq = '" . str_replace("\'", "''", $icq) . "', user_website = '" . str_replace("\'", "''", $website) . "', user_occ = '" . str_replace("\'", "''", $occupation) . "', user_from = '" . str_replace("\'", "''", $location) . "', user_interests = '" . str_replace("\'", "''", $interests) . "', user_sig = '" . str_replace("\'", "''", $signature) . "', user_sig_bbcode_uid = '$signature_bbcode_uid', user_viewemail = $viewemail, user_aim = '" . str_replace("\'", "''", str_replace(' ', '+', $aim)) . "', user_yim = '" . str_replace("\'", "''", $yim) . "', user_msnm = '" . str_replace("\'", "''", $msn) . "', user_attachsig = $attachsig, user_allowsmile = $allowsmilies, user_allowhtml = $allowhtml, user_allowbbcode = $allowbbcode, user_allow_viewonline = $allowviewonline, user_notify = $notifyreply, user_notify_pm = $notifypm,
#
#-----[ IN-LINE FIND ]------------------------------------------
#
, user_notify_pm = $notifypm,
#
#-----[ IN-LINE AFTER, ADD ]------------------------------------------
#
 games_block_pm = $games_block_pm,
#
#-----[ FIND ]------------------------------------------
#
			$sql = "INSERT INTO " . USERS_TABLE . "	(user_id, username, user_regdate, user_password, user_email, user_icq, user_website, user_occ, user_from, user_interests, user_sig, user_sig_bbcode_uid, user_avatar, user_avatar_type, user_viewemail, user_aim, user_yim, user_msnm, user_attachsig, user_allowsmile, user_allowhtml, user_allowbbcode, user_allow_viewonline, user_notify, user_notify_pm,
#
#-----[ IN-LINE FIND ]------------------------------------------
#
user_notify, user_notify_pm,
#
#-----[ IN-LINE AFTER, ADD ]------------------------------------------
#
 games_block_pm,
#
#-----[ FIND ]------------------------------------------
#
				VALUES ($user_id, '" . str_replace("\'", "''", $username) . "', " . time() . ", '" . str_replace("\'", "''", $new_password) . "', '" . str_replace("\'", "''", $email) . "', '" . str_replace("\'", "''", $icq) . "', '" . str_replace("\'", "''", $website) . "', '" . str_replace("\'", "''", $occupation) . "', '" . str_replace("\'", "''", $location) . "', '" . str_replace("\'", "''", $interests) . "', '" . str_replace("\'", "''", $signature) . "', '$signature_bbcode_uid', $avatar_sql, $viewemail, '" . str_replace("\'", "''", str_replace(' ', '+', $aim)) . "', '" . str_replace("\'", "''", $yim) . "', '" . str_replace("\'", "''", $msn) . "', $attachsig, $allowsmilies, $allowhtml, $allowbbcode, $allowviewonline, $notifyreply, $notifypm,
#
#-----[ IN-LINE FIND ]------------------------------------------
#
$notifyreply, $notifypm,
#
#-----[ IN-LINE AFTER, ADD ]------------------------------------------
#
 $games_block_pm,
#
#-----[ FIND ]------------------------------------------
#
	$notifypm = $userdata['user_notify_pm'];
#
#-----[ AFTER, ADD ]------------------------------------------
#
    $games_block_pm = $userdata['games_block_pm'];
#
#-----[ FIND ]------------------------------------------
#
	display_avatar_gallery($mode, $avatar_category, $user_id, $email, $current_email, $coppa, $username, $email, $new_password, $cur_password, $password_confirm, $icq, $aim, $msn, $yim, $website, $location, $occupation, $interests, $signature, $viewemail, $notifypm,
#
#-----[ IN-LINE FIND ]------------------------------------------
#
$viewemail, $notifypm,
#
#-----[ IN-LINE AFTER, ADD ]------------------------------------------
#
 $games_block_pm,
#
#-----[ FIND ]------------------------------------------
#
		'NOTIFY_PM_NO' => ( !$notifypm ) ? 'checked="checked"' : '',
#
#-----[ AFTER, ADD ]------------------------------------------
#
        'BLOCK_PM_YES' => ( $games_block_pm ) ? 'checked="checked"' : '',
        'BLOCK_PM_NO' => ( !$games_block_pm ) ? 'checked="checked"' : '',
#
#-----[ FIND ]------------------------------------------
#
		'L_NOTIFY_ON_PRIVMSG' => $lang['Notify_on_privmsg'],
#
#-----[ AFTER, ADD ]------------------------------------------
#
        'L_BLOCK_ARCADE_PM' => $lang['Block_Arcade_pm'],
#
#-----[ OPEN ]------------------------------------------
#
templates/subSilver/admin/user_edit_body.tpl
#
#-----[ FIND ]------------------------------------------
#
	<tr>
	  <td class="row1"><span class="gen">{L_NOTIFY_ON_PRIVMSG}</span></td>
	  <td class="row2">
		<input type="radio" name="notifypm" value="1" {NOTIFY_PM_YES} />
		<span class="gen">{L_YES}</span>&nbsp;&nbsp;
		<input type="radio" name="notifypm" value="0" {NOTIFY_PM_NO} />
		<span class="gen">{L_NO}</span></td>
	</tr>
#
#-----[ AFTER, ADD ]------------------------------------------
#
    <tr>
      <td class="row1"><span class="gen">{L_BLOCK_ARCADE_PM}</span></td>
      <td class="row2">
        <input type="radio" name="games_block_pm" value="1" {BLOCK_PM_YES} />
        <span class="gen">{L_YES}</span>&nbsp;&nbsp;
        <input type="radio" name="games_block_pm" value="0" {BLOCK_PM_NO} />
        <span class="gen">{L_NO}</span></td>
    </tr>
#
#-----[ OPEN ]------------------------------------------
#
templates/subSilver/profile_add_body.tpl
#
#-----[ FIND ]------------------------------------------
#
	<tr>
	  <td class="row1"><span class="gen">{L_NOTIFY_ON_PRIVMSG}:</span></td>
	  <td class="row2">
		<input type="radio" name="notifypm" value="1" {NOTIFY_PM_YES} />
		<span class="gen">{L_YES}</span>&nbsp;&nbsp;
		<input type="radio" name="notifypm" value="0" {NOTIFY_PM_NO} />
		<span class="gen">{L_NO}</span></td>
	</tr>
#
#-----[ AFTER, ADD ]------------------------------------------
#
    <tr>
      <td class="row1"><span class="gen">{L_BLOCK_ARCADE_PM}:</span></td>
      <td class="row2">
        <input type="radio" name="games_block_pm" value="1" {BLOCK_PM_YES} />
        <span class="gen">{L_YES}</span>&nbsp;&nbsp;
        <input type="radio" name="games_block_pm" value="0" {BLOCK_PM_NO} />
        <span class="gen">{L_NO}</span></td>
    </tr>
#
#-----[ SAVE/CLOSE ALL FILES ]------------------------------------------
#
# EoM
