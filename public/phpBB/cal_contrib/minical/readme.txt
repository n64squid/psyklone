/*********************************************
*	Calendar Lite
*
*	$Author: martin $
*	$Date: 2005-05-26 12:23:50 +0100 (Thu, 26 May 2005) $
*	$Revision: 20 $
*
*********************************************/

############################################################## 
## Mod Title: 	Mini-Cal (mod to provide Cal Lite/Pro compat)
## Mod Version: 2.0.2 (beta)
## Author:	Netclectic < Adrian Cockburn >
## Contributor:	WebSnail < Martin Smallridge >
## SUPPORT: 	http://www.snailsource.com/forum/ 
## Description: Compatability mod to enable Calendar for phpBB2 
##		to work with Mini-Cal (both Lite and Pro)
## 
## Installation Level: moderate) 
## Installation Time: 2 Minutes 
## Files To Edit: 
##		mods/netclectic/mini_cal/mini_cal.php
##		mods/netclectic/mini_cal/mini_cal_config.php
##
##############################################################
## This MOD is released under the GPL License. 
## Intellectual Property is retained by the MOD Author(s) listed above 
############################################################## 
## Please note that this MOD is NOT listed on phpBB.com's main
## site and should under no circumstances be trusted as secure
## and genuine unless downloaded from the Snailsource.com web
## site.
##############################################################


This MOD is provided as a compatability update to the existing Mini-Calendar
Mod provided by NetClectic < http://www.netclectic.com > 

The MOD is for use with the BETA version of Mini_cal and will undoubtedly
break as updated and improvements are added to Mini-Calendar or new releases
of Calendar are produced and the 2 mods fall out of synch. Speaking for myself
I'm sure this will be minimal but please CHECK compatability before applying
upgrades if this is an "essential" feature.

Support for this mod will be provided ONLY for Calendar Lite/Pro versions as
Netclectic knows the rest of the mod and provides support for that on his 
own forums.



Release Notes/ New Features:
============================
Still in development... 




Bug-Fixes & Updates
===================

[2.0.2 BETA]

[** NOTE: Version system now uses SubVersion numbering. **]

01/03/2005	- FIX mini_cal_SNAILPRO.php
		Fix excess '?' in URL for events in minical view.


02/02/2005	- FIX: mini_cal_SNAILLITE.php, mini_cal_SNAILPRO.php
		Fix incorrect full_path pre-pending to URL for both files
		Cal_lite.php is now properly set.


[2.0.1 BETA]

14/12/2004	- FIX: mini_cal_SNAILLITE.php
		Fix poor SQL routine... Now displays events list.

08/12/2004	- FIX: mini_cal_SNAILPRO.php
		MINI_CAL_DAYS_AHEAD has been given a hard limit of one year if set as 0 (unlimited).

07/12/2004	- FIX: mini_cal_SNAILPRO.php
		> Months Jan - Sept will now display properly in the upcoming events list
		> commas now display properly without slashes
		> Error message when SQL fails, now re-located to show only when debug output required.

06/12/2004	- Fix: mini_cal_SNAILPRO.php
		Debug output moved from !row to !query else conditional. Fixes problem of 
		error when no events available within mini-cal defined look ahead period

06/12/2004	- Fix: mini_cal_SNAILPRO.php
		Finally figured out how to get the "Upcoming events" list to work properly.

[2.0.0 BETA]

05/12/2004	- Fix: mini_cal_SNAILPRO.php
		Updated the code to display events in mini-cal view properly
		
		[Known Issue] The mod still doesn't display events in list form (being worked on)


18/10/2004	- INITIAL RELEASE
	


