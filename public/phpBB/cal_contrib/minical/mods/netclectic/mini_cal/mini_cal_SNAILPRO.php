<?php
/***************************************************************************
 *                            mini_cal_SNAILPRO.php
 *                            ----------------------
 *   Author  		: 	netclectic - Adrian Cockburn - phpbb@netclectic.com
 *	 Contributor	:	Websnail - Martin Smallridge - www.snailsource.com
 *   Created 		: 	Monday, Oct 1, 2004
 *	 Last Updated	:	$Date: 2005-05-30 15:13:34 +0100 (Mon, 30 May 2005) $	
 *
 *	 Version		: 	MINI_CAL - $Rev: 21 $
 *
 ***************************************************************************/
    if ( !defined('IN_MINI_CAL') )
    {
    	die("Hacking attempt");
    }

	include_once($phpbb_root_path . 'cal_settings.php');
    include_once($phpbb_root_path . 'cal_functions.php');

	$tz_diff = mytime();

    /***************************************************************************
        getMiniCalForumsAuth
        
        version:        1.0.0
        parameters:     $userdata - an initialised $userdata array.
        returns:        a two part array
                            $mini_cal_auth['view'] - a comma seperated list of forums which the user has VIEW permissions for
                            $mini_cal_auth['post'] - a comma seperated list of forums which the user has POST permissions for
     ***************************************************************************/
    function getMiniCalForumsAuth($userdata)
    {
        global $db;

        // initialise our forums auth list
    	$mini_cal_auth_ary = array();

		// MOD CalPro doesn't use normal phpbb2 forum perms so forget this it isn't important
    	//$mini_cal_auth_ary = auth(AUTH_ALL, AUTH_LIST_ALL, $userdata);
    
        $mini_cal_auth = array();
        $mini_cal_auth['view'] = '';
        $mini_cal_auth['post'] = '';
        
		$sql = "SELECT * FROM " . CAL_CONFIG;
		
       	if( $result = $db->sql_query($sql) )
        {
			while( $row = $db->sql_fetchrow($result) )
			{
				$cal_config[$row['config_name']] = $row['config_value'];
			}
		}

		$caluser = calendarperm($userdata['user_id']);
		if($cal_config['allow_user_default'] > $caluser && $userdata['user_id'] != ANONYMOUS) 
		{
			$caluser = $cal_config['allow_user_default'];
		}
		$mini_cal_auth['view'] = (($userdata['user_level'] == ADMIN) || $cal_config['allow_anon'] || $caluser >= 1);
		$mini_cal_auth['post'] = ($caluser >= 2);

		return $mini_cal_auth;
    }
    

    /***************************************************************************
        getMiniCalEventDays
        
        version:        1.0.0
        parameters:     $auth_view_forums - a comma seperated list of forums which the user has VIEW permissions for
        returns:        an array containing a list of day containing event the user has permission to view
     ***************************************************************************/
    function getMiniCalEventDays($auth_view_forums)
    {
        global $db, $mini_cal_this_year, $mini_cal_this_month, $tz_diff;
    
        $mini_cal_event_days = array();
    
		$month = $mini_cal_this_month;
		$year = $mini_cal_this_year;

		$start_point_gmt = gmmktime(0,0,0,$month,1,$year);			// Get start time for year in GMT
		$lastday = gmdate('t',  gmmktime(0,0,0,$month,1,$year));
		$end_point_gmt = gmmktime(23,59,59,$month,$lastday,$year);		// Get end time for year in GMT

		// To get the local time we have to subtract the tz_diff from the GMT timestamp
		$pro['start_point_local'] = $start_point_gmt - $tz_diff;
		$pro['end_point_local'] = $end_point_gmt - $tz_diff;

        if ($auth_view_forums)
        {
            $sql = "SELECT c.id, c.event_start, c.event_end, c.event_time_set "
                ." FROM " . CAL_TABLE . " AS c "
				." LEFT JOIN ".CAL_GROUP_EVENT." AS ge ON c.id = ge.event_id "
				." WHERE c.valid = 'yes' AND c.r_type != 'D' AND ";

			$sql .= pro_gen_sql($pro);	// Add the remainder of the select criteria
			//DEBUG echo $sql; exit;

			if( $result = $db->sql_query($sql) )
            {
				$events = array();
                while( $row = $db->sql_fetchrow($result) )
                {
					if($prev_event_id == $row['id']) {
						continue;
					}
					$prev_event_id = $row['id'];

					if(!$row['event_time_set'] && $board_config['board_timezone'] >= 12) {
						$row['event_start'] -= $time_adjust;
						$row['event_end'] -= $time_adjust;
					}
					$starting_gmt = $row['event_start'];
					$ending_gmt = $row['event_end'];

					$ref = ($starting_gmt - $pro['start_point_local']) / (24*60*60);
					$start_ref = ($ref < 0) ? 0 : intval($ref);

					if($starting_gmt < $ending_gmt) {
						// If more than one day.
						$ref = ($ending_gmt - $pro['start_point_local']) / (24*60*60);
						$end_ref = ($ref < 0) ? 0 : intval($ref);
					} else {
						$end_ref = $start_ref;
					}

					for($this_ref=$start_ref; $this_ref <= $end_ref; $this_ref++) {
						$events[$this_ref] = $this_ref + 1;
					}
				}
				// DEBUG
				/*
				print_r($events);
				exit;
				*/
				
				// Translate into Mini-cals own format.
				foreach($events AS $tmp) {
					$mini_cal_event_days[] = $tmp;
				}
            }
        }
                
        return $mini_cal_event_days;
    }


    /***************************************************************************
        getMiniCalEvents
        
        version:        1.0.0
        parameters:     $mini_cal_auth - a two part array 
                            $mini_cal_auth['view'] - a comma seperated list of forums which the user has VIEW permissions for
                            $mini_cal_auth['post'] - a comma seperated list of forums which the user has POST permissions for
        
        returns:        nothing - it assigns variable to the template
     ***************************************************************************/
    function getMiniCalEvents($mini_cal_auth)
    {
        global $template, $db, $phpEx, $lang, $tz_diff;
    

	
        // initialise some sql bits
        if ($mini_cal_auth['view'])
		{
			$pro['start_point_local'] = time();

			// Set appropriate max of one year if MINI_CAL_DAYS_AHEAD = 0 (ie: unlimited)
			$days_ahead = (empty($days_ahead)) ? 360 : MINI_CAL_DAYS_AHEAD;


			$pro['end_point_local'] = $pro['start_point_local'] + ($days_ahead * 86400);

	        // get the events 
			$cvt_date_start = "FROM_UNIXTIME((c.event_start - $tz_diff))";
			$cvt_date_end = "FROM_UNIXTIME((c.event_end - $tz_diff))";

	    	$sql = "SELECT 
	                  DISTINCT c.id, c.event_start, c.event_end, c.subject, r.r_subject
					  FROM " . CAL_TABLE . " as c 
						LEFT JOIN ".CAL_RECUR." AS r ON c.r_group_id = r.r_group_id
						LEFT JOIN ".CAL_GROUP_EVENT." AS ge ON c.id = ge.event_id

	    	        WHERE c.valid = 'yes' AND c.r_type != 'D' AND ";

			$sql .= pro_gen_sql($pro);	// Add the remainder of the select criteria

			$sql .=	" LIMIT 0," . MINI_CAL_LIMIT;
	    
			// DEBUG echo "SQL: $sql <br />";
			
	        // did we get a result? 
	        // If not there's a problem, so just die quietly unless DEBUG is turned on
	    	if( !$result = $db->sql_query($sql) ) {
				if(DEBUG) {
					message_die(GENERAL_ERROR, 'Could not get data for events', '', __LINE__, __FILE__, $sql);
				}
			}
			else {
	           // ok we've got MyCalendar
	           $template->assign_block_vars('switch_mini_cal_events', array());
	           if ( $db->sql_numrows($result) > 0 )
	           {
	               // we've even got some events
	               // initialise out date formatting patterns
	               $cal_date_pattern = unserialize(MINI_CAL_DATE_PATTERNS);
	    
	               // output our events in the given date format for the current language
	       	       while ($row = $db->sql_fetchrow($result))
	               {
					/* 
					because of the way MySQL screws with timezones we'll get the information we need from event_start and event_end
					*/
						// Timezone fix?
						$row['event_start'] = $row['event_start'] - $tz_diff;
						$row['event_end'] = $row['event_end'] - $tz_diff;
						// End Fix
						
	                    $cal_date = getFormattedDate(
	                                    gmdate("w", $row['event_start']), 
	                                    gmdate("n", $row['event_start']), 
	                                    gmdate("j", $row['event_start']),
	                                    gmdate("Y", $row['event_start']),
	                                    gmdate("H", $row['event_start']), 
	                                    gmdate("i", $row['event_start']),
	                                    gmdate("s", $row['event_start']),
										$lang['Mini_Cal_date_format']
	                                );
									
						if ($row['event_end'] > $row['event_start'] && gmdate("d", $row['event_start']) != gmdate("d", $row['event_end']))
						{
		                    $cal_date .= ' - ' . 
									getFormattedDate(
	                                    gmdate("w", $row['event_end']), 
	                                    gmdate("n", $row['event_end']), 
	                                    gmdate("j", $row['event_end']),
	                                    gmdate("Y", $row['event_end']),
										'', 
										'', 
										'', 
										$lang['Mini_Cal_date_format']
									);
						}
	
						$subject = $row['r_subject']. (!empty($row['r_subject']) && !empty($row['subject']) ? ': ' : '') . $row['subject'];
	
	        			$template->assign_block_vars('mini_cal_events', array(
	        					'MINI_CAL_EVENT_DATE' => $cal_date,
	                            'S_MINI_CAL_EVENT' => stripslashes($subject), 
	        					'U_MINI_CAL_EVENT' => append_sid( 'cal_display.' . $phpEx . '?id=' . $row['id'] )
	                            )
	        			); 
	               }
	           }
	           else
	           {
	                // no events :(
	                $template->assign_block_vars('mini_cal_no_events', array());
	           }
	           $db->sql_freeresult($result);
			}
		}
    }    

	
    /***************************************************************************
        getMiniCalSearchSql
        
        version:        1.0.0
        parameters:     $search_id   - the type of search we're looking for
						$search_date - the date passed to the search
        
        returns:        an sql string
     ***************************************************************************/
	function getMiniCalSearchSql($search_id, $search_date)
	{
		// not used
    }

	
    /***************************************************************************
        getMiniCalSearchURL
        
        version:        1.0.0
        parameters:     $search_date - the date passed to the search
        
        returns:        an url string
     ***************************************************************************/
	function getMiniCalSearchURL($search_date)
	{
		$s_yy = substr($search_date, 0, 4);
		$s_mm = substr($search_date, 4, 2);
		$s_dd = substr($search_date, 6, 2);
	
		global $phpEx;
		$url = append_sid("cal_display.$phpEx?day=$s_dd&month=$s_mm&year=$s_yy");
		
		return $url;
    }


	
    /***************************************************************************
        getMiniCalPostForumsList
        
        version:        1.0.0
        parameters:     $mini_cal_post_auth  - a comma seperated list of forms with post rights
                        
        returns:        adds a forums select list to the template output
    ***************************************************************************/
	function getMiniCalPostForumsList($mini_cal_post_auth)
	{
		if ($mini_cal_post_auth >= 2)
		{
			global $template;
			
			$template->assign_block_vars('switch_mini_cal_add_events', array());
		}
	
	}


	/*
		CalPro specific.
		Generate the complex SQL for selecting events the user has permission to access.
	*/

	function pro_gen_sql($pro) {
		global $cal_config, $userdata, $board_config;

		$groups = get_groups($userdata['user_id']);

		if($groups) {
			// If the user is member of 1+ usegroups then check for these results too.
			while(list(,$group) = each($groups)) {
				if(count($groups > 1) && isset($sql_groups)) {
					 $sql_groups .= " OR ";
				}
				$sql_groups .= "ge.group_id = ".$group['group_id'];
			}
			$sql_access = '(c.event_access = 2 AND ('.$sql_groups.' OR ge.group_id IS NULL OR ge.group_id = 0))';
		}
		$sql_access .= ($groups && $cal_config['allow_private']) ? ' OR ' : '';

		if ($cal_config['admin_private_view'] && $caluser == 5 && $category == ADMIN_PRIVATE_EVENT) {
			// If we're screening users private events and have the requisite permissions/option on.
			$sql_access .= ' c.event_access = 1 ';
		}
		elseif($cal_config['allow_private']) {
			$sql_access .= ' (c.event_access = 1 AND c.user_id = '.$userdata['user_id'].') ';
		}

		$sql .= ($sql_access) ? '('.$sql_access.' OR c.event_access = 0)  AND ' : 'c.event_access = 0 AND ';
		// MOD end

		// MOD Null time/stable date fix
		if($board_config['board_timezone'] < 12) {
			$sql .= "c.event_end >= '".$pro['start_point_local']."' AND c.event_start <= '".$pro['end_point_local']."' ";
		}
		else {
			// If time_set is 0 then the time is going to be GMT 12 noon to keep it in the right day. 
			// Timezones >= 12 screw this up so we allow for it with this SQL :)
			$time_adjust = 12 * 60 * 60;
			$sql .= "((c.event_end >= '".$pro['start_point_local']."' AND c.event_start <= '".$pro['end_point_local']."' AND event_time_set >= 1)
				OR (c.event_end >= '". ($pro['start_point_local'] + $time_adjust) ."' AND c.event_start <= '". ($pro['end_point_local'] + $time_adjust) ."' AND event_time_set = 0)) ";
		}
		$sql .= 'ORDER BY c.event_start ASC, c.event_time_set ASC';

		return $sql;
	}
    
	$template->assign_vars(array(
		'U_MINI_CAL_CALENDAR' => append_sid('cal_view_month.' . $phpEx),
		'U_MINI_CAL_ADD_EVENT' => append_sid('cal_add.' . $phpEx)
        )
    );    
    
?>