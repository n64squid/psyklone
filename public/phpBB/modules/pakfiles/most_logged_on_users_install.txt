You will have to have Last visit Mod 1.2.10em Installed in order to let this module work.
You can download it at: http://www.phpbbhacks.com/download/237
This Module displays the Most Logged On Users on your board.
Anonymous posters are not counted.

If you install this module after install of Statistics Mod 3.0.1BETAt or higher:
In order for correct working of this module please copy  
most_logged_on_users_module-last_visit_update.php to the root of your phpbb and execute it 
from your browser.
>Be aware that that will set all last visit online time to zero.<
If its not included you can download this module from http://www.detecties.com/phpbb2018 again.
Or you may execute folowing via phpmyadmin or somesort of equal program:
UPDATE `phpbb_users` SET user_totaltime = 0

If you installed Statistics Mod 3.0.1BETAt or higher this module will allready be installed.
If you had Last Visit Mod Installed at time of install of Statistics Mod 3.0.1BETAt or higher 
the database change has been executed allready for you.
If you Install Last Visit Mod After installation of Statistics Mod 3.0.1BETAt 
or higher you will have to do the database change yourself 
and activate this module via the Statistics ACP.
