#############################################################################
## MOD Title: 		Adds Top xx Activity Mod Users to the Statistics Mod 3.0.1x and 4.0.x
## MOD Version: 	1.0.2
## MOD Author: Wicher < N/A > (N/A) http://www.detecties.com/phpbb2018
##
## MOD Description: 	This MOD will allow users to see a list of the Top Ten Users by
##                      Game Play - For use with dEfender's Activity Mod v2.1.2
## 
## Installation Level: 	Easy
## Installation Time: 	1 Minutes 
##
## Files To Edit: 	includes/functions_arcade.php
##                  activity.php
##
## Included Files: 	Top_xx_Users_Game_Plays_db_update.php 
##                      
## License: http://opensource.org/licenses/gpl-license.php GNU General Public License v2 
############################################################## 
## For security purposes, please check: http://www.phpbb.com/mods/ 
## for the latest version of this MOD. Although MODs are checked 
## before being allowed in the MODs Database there is no guarantee 
## that there are no security problems within the MOD. No support 
## will be given for MODs not found within the MODs Database which 
## can be found at http://www.phpbb.com/mods/ 
############################################################## 
## Author Notes:
## Make sure you run the db_update.php to add the required field before 
## installing any other parts.
## Original MOD Author: Painkiller < painkiller@sympatico.ca > http://deadzone.runecentral.com/forums
#############################################################################
## MOD History: 
## 
##   2006-11-28 - Version 1.0.2
##      - Third Release
##   2005-09-23 - Version 1.0.1
##      - Second Release
## 
#############################################################################
## Before Adding This MOD To Your Forum, You Should Back Up All Files Related To This MOD 
############################################################################# 

# 
#-----[ DIY ]------------------------------------------ 
#
Copy Top_xx_Users_Game_Plays_db_update.php to the root of your phpbb and execute it from your browser.
After Installation: DELETE Top_xx_Users_Game_Plays_db_update.php from your webspace.
# 
#-----[ OPEN ]------------------------------------------ 
#

includes/functions_arcade.php

# 
#-----[ FIND ]------------------------------------------ 
# 

?>

#
#-----[BEFORE, ADD]-------------------------------------
#

function UpdateUsersGames($user) 
{ 
global $db, $userdata; 

$q = "UPDATE ". USERS_TABLE ." 
SET ina_games_played = ina_games_played + '1' 
WHERE user_id = '". $user ."'"; 
$r = $db->sql_query($q); 

return; 
}

# 
#-----[ OPEN ]------------------------------------------ 
# 
activity.php 

# 
#-----[ FIND ]------------------------------------------ 
#
				else
				{
					message_die(GENERAL_MESSAGE, $lang['not_enough_reward'], '', __LINE__, __FILE__, $sql);
				}
			}
		}
#
#-----[ AFTER, ADD ]------------------------------------
#
		// Added by Wicher
		UpdateUsersGames($userdata['user_id']);
# 
#-----[ SAVE/CLOSE ALL FILES ]--------------------------
# 
# EoM