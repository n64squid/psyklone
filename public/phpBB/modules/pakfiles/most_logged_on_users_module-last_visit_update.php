<?php
#########################################################
## SQL commands to phpBB2
## Author: Paul Norman
## Nickname: acoolwelshbloke
## Email: acoolwelshbloke@tec2spec.co.uk
##
## Ver 1.0.9
##
## phpBB2 database update script for mods.
## This file is intended for use with phpBB2, when updating the Last Visit mod for use with Statistics Mod 3.0.1 or higher.
## After you run this file you may delete it, but remember only admin can use so it really doesen't matter.
## The script will look for whatever prefix you are using, and it will use the existing DB defined by congig.php
## The execution of these/this script(s) including the SQL commands are harmless, so you can run it as meny times you like.
##
#########################################################

define('IN_PHPBB', true);
$phpbb_root_path = './';
include($phpbb_root_path . 'extension.inc');
include($phpbb_root_path . 'common.'.$phpEx);
include($phpbb_root_path . 'includes/functions_selects.'.$phpEx);

###################################################################################################
##
## put the SQL commands below here, the SQL commands listed below are only exampels, substitude them with the one you need ##
##
###################################################################################################
$sql=array(
'UPDATE '. USERS_TABLE .' SET user_totaltime=0 WHERE user_totaltime>"0" AND user_lastlogon>=0'
);


$mods = array ( 
'Last Visit Mod aka Statistics Mod Part 1,Last Visit Mod aka Statistics Mod Part 2'
);

############################################### Do not change anything below this line #######################################

//
// Start session management
//
$userdata = session_pagestart($user_ip, PAGE_INDEX);
init_userprefs($userdata);
//
// End session management
//

if (!$userdata['session_logged_in'])
{
	header('Location: ' . append_sid("login.$phpEx?redirect=last_visit_db_update.$phpEx", true));
}

if ($userdata['user_level'] != ADMIN)
{
	message_die(GENERAL_MESSAGE, $lang['Not_Authorised']);
} 

$n=0;
$message="<b>This list is a result of the SQL queries needed for MOD</b><br/><br/>";
while($sql[$n])
{
	$message .= ($mods[$n-1] != $mods[$n]) ? '<p><b><font size=3>'.$mods[$n].'</font></b><br/>' : '';
	if(!$result = $db->sql_query($sql[$n])) 
	$message .= '<b><font color=#FF0000>[Already added]</font></b> line: '.($n+1).' , '.$sql[$n].'<br />';
	else $message .='<b><font color=#0000fF>[Added/Updated]</font></b> line: '.($n+1).' , '.$sql[$n].'<br />';
	$n++;
}
 message_die(GENERAL_MESSAGE, $message); 
?>