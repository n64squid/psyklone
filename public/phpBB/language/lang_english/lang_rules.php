<?php
/***************************************************************************
 *                          lang_rules.php [english]
 *                            -------------------
 *
 *   copyright            : (C) 2002 Dimitri Seitz
 *   email                : dwing@weingarten-net.de
 *
 *
 *
 ***************************************************************************/

/***************************************************************************
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
 
// 
// To add an entry to your Rules simply add a line to this file in this format:
// $faq[] = array("question", "answer");
// If you want to separate a section enter $faq[] = array("--","Block heading goes here if wanted");
// Links will be created automatically
//
// DO NOT forget the ; at the end of the line.
// Do NOT put double quotes (") in your FAQ entries, if you absolutely must then escape them ie. \"something\"
//
// The FAQ items will appear on the FAQ page in the same order they are listed in this file
//
 
  
$faq[] = array("--","Main Rules");
$faq[] = array("Don't Spam!", "Spamming in these forums will be tolerated... To a certain extent. If the level of spam posted exceeds the limit allowed, then it WILL be terminated without mercy by an administrator or a moderator. Double posting and one-word posts are considered spamming. (One-word posts include those which do not have any meaning to them.)");
$faq[] = array("Links to other websites", "You are allowed to have a link to your website, and even make a thread dedicated to it. However, don't push it too far, or your posts will get deleted.");
$faq[] = array("Coarse Language", "It is allowed to use rude words... just as long a you don't use them to offend someone. This IS a forum, and therefore, we understand that some debates may not lead to what you want, but please don't insult people. It gives you a bad reputation, and may get you temporarily banned. Any sort of racist, sexist or any other type of comment which might result offensive towards a certain social group will not be tolerated..");
$faq[] = array("Images and Signatures", "Unless you are posting in the Art forum, do not post images which are too big for the screen. If you want to post an image on your computer, then just go to http://www.imageshack.us and upload your image. You will then get a URL for forums. Copy and paste this in your post. Keep signature images less than 250x600 pixels of area. It makes reading much more confortable.");
$faq[] = array("Making topics", "Think well before making topics. If you make a topic which has a simple -yes or no- answer, then it will be deleted. Try to make the topic in a way that a discussion can sprout. If you have any questions concerning the forum, consult the FAQ or contact an administrator.");
$faq[] = array("Adult Content", "You are allowed to post adult content, but make sure that in the thread title, there is a warning for those people which are not of age. If there is no warning, and it is considered adult content, a mod or admin will edit the title. If it is really sick, than it will simply be deleted.");
$faq[] = array("Spamming for shens", "If you spam just for shens, which is simply making sort one-word posts one after the other, you will have all of your items removed, and your shens will be changed to  more or less -1000. This will also be counted as a sort of black mark, and will therfore be more disliked.");
$faq[] = array("--","Rules For Each Specific Forum");
$faq[] = array("News - General Forum", "Any topic which has the name - News ... - Will not have any level of spam, and will be terminated. This forum will contain topics related to the website, the forums, or its members.");
$faq[] = array("Intros forum", "You can make a topic in this forum describing yourself, things that you like / hate and such. But please, as much as we may like it, dont post something like - I'm BLAH, and I think this website is cool! - Please spend more time on this...");
$faq[] = array("Suggestions - Bugs forum", "When posting here, remember one thing --> we are always open to criticisms, but we shall obliterate insults.");
$faq[] = array("RPs - Games forum", "Respect the RP/game creator, and this will make the experience much more just for all. If you are the maker of the thread, do not abuse your powers, if you do, the thread will be locked.");
$faq[] = array("Games", "Discuss anything related to the games that will be helpful for their improvement. If you reveal any of the game's secrets, your post will be deleted.");
$faq[] = array("Culture", "Anything can be discussed here! If the topic doesn't fit into any of the following categories: Videogames, Music, Literature, Cinema, Art or Thoughts; just chuck it into the general forum.");



//
// This ends the Rules entries
//

?>