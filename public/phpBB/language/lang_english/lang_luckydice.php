<?php
/***************************************************************************
 *                           lang_luckydice.php
 *                            -------------------
 *
 ***************************************************************************/

/***************************************************************************
 *
 *   copyright (C) 2003-2006  RC-Technologies
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License
 *   as published by the Free Software Foundation; either version 2
 *   of the License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   http://www.gnu.org/copyleft/gpl.html
 *
 ***************************************************************************/

//
// Admin 
$lang['admin_submit'] = 'Submit';
$lang['admin_title'] = 'Lucky Dice administration.';
$lang['admin_enable'] = 'Turn the game On or Off ??';
$lang['admin_maxbet'] = 'What\'s the maxbet ??';
$lang['admin_return'] = 'Return to Lucky Dice configuration??';
$lang['admin_updated'] = 'Configuration succesfully updaded.';
$lang['admin_windouble'] = 'Windouble, the user\'s bet times this number (if they have 2 same dice-numbers).';
$lang['admin_wintriple'] = 'Wintriple, the user\'s bet times this number (if they have 3 same dice-numbers).';
$lang['admin_winperfect'] = 'Winperfect, the user\'s bet times this number (if they have 4 same dice-numbers).';
$lang['admin_bonussmall'] = 'Bonussmall, the user\'s bet times the winperfect times this (when they have a perfect + it are bonus-numbers)';
$lang['admin_bonusbig'] = 'Bonusbig, the user\'s bet times the winperfect times this (when they have a perfect + it are bonus-numbers)';
$lang['admin_desc'] = 'Here, you can easily define all neccesary data for the Lucky Dice game.';
$lang['admin_betsaday'] = 'The amount of times a user can play each 24 hours (enter 0 (zero) to allow infinite games)';

//
// User
$lang['game_disabled'] = 'The game is currently disabled, please try again later!';
$lang['game_explanation1'] = 'If two numbers match on either the dice or the table, it\'s called a pair,';
$lang['game_explanation2'] = 'and you don\'t win or lose anything. Two pair doubles your bet. ';
$lang['game_explanation3'] = 'Triple matches triple your bet. Four of a kind is perfect, and you win five times your bet.';
$lang['game_explanation4'] = 'Plus, if your dice come up perfect on the Perfect Bonus spot on the table, you win an even bigger bonus!';
$lang['title'] = 'Lucky Dice';
$lang['home1'] = 'LuckyDice. Throw the dice on the table and try to get the same numbers. ';
$lang['home2'] = 'Prizes are based on the numbers rolled and where each die lands. ';
$lang['home3'] = 'The more numbers that match, the more you win.';
$lang['maxbetis'] = 'Max bet is: ';
$lang['youhave'] = 'You currently have ';
$lang['error_values'] = 'You can only bet values';
$lang['error_money'] = 'OOPS, looks like you do not have that many '.$board_config['points_name'].'!';
$lang['error_maxbet'] = 'You\'ve exeeded the maximum bet of '.$board_config['luckydice_maxbet'].'!';
$lang['again'] = 'Roll\'em again?';
$lang['winperfect'] = 'Congratulations, you\'ve got four of a kind !!Here, have ';
$lang['winbonussmall'] = 'WOW, you\'ve got the Minor Perfect Bonus !!! Here are your ';
$lang['winbonusbig'] = 'You must be extremely lucky, you\'ve got the Major Perfect Bonus !!!!!!! Here are your ';
$lang['wintriple'] = 'Congratulations, you\'ve got a triple ! Here, have ';
$lang['windouble'] = 'Congratulations, you\'ve got 2 pairs ! Here, have ';
$lang['winpair'] = 'You\'ve got 1 pair, you didn\'t won, nor lose !!';
$lang['lose'] = 'Ohh, that\'s worth nothing, too bad. Better luck next time !!';
$lang['played'] = 'This game is played ';
$lang['_times'] = ' times.';
$lang['taken'] = 'This game has taken ';
$lang['given'] = 'Shen, and has given ';
$lang['zero_error_values'] = 'You can only bet positive amounts';
$lang['toomanyplays'] = 'You can only play %s games each day, please come back later.';

?>
