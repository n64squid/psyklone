<?php
/*-----------------------------------------------------------------------------
    Effects Store for Shop 3 - A phpBB Add-On
  ----------------------------------------------------------------------------
    lang_effects.php
       English language file for Effects Store
    File Version: 1.2.0
    Begun: July, 2006                 Last Modified: October 10, 2006
  ----------------------------------------------------------------------------
    Copyright 2006 by Jeremy Rogers.  Please read the license.txt included
    with the phpBB Add-On listed above for full license and copyright details.
  ----------------------------------------------------------------------------
    Translated by:
        <ATTENTION ALL POTENTIAL TRANSLATORS!
            You are free to translate this file into other languages for your
            own use and to distribute translated versions according to the
            terms of the license under which it is released. Add your name and
            contact details in this area.>
-----------------------------------------------------------------------------*/
/* 
	If you would like to add a message indicating you are the translator,
	you may add it below. This will appear with the phpBB copyright and is
	completely optional.
*/
// $lang['TRANSLATION'] .= 'Your Name Here';

/* This file uses a format of:
		'STRING_NAME'	=>	'text'
	Never edit the STRING_NAME part. That is required to be unchanged.
*/

$lang += array(
	'EFFECTS_TITLE'			=>	'Special Effects Store',
	'EFFECTS_EDIT'			=>	'Edit Effects Configuration',
	'EFFECTS_ENABLE'		=>	'Enable the Effects Store?',
	'EFFECTS_STORE_NAME'	=>	'Name of this shop:',
	'EFFECTS_AVATAR'		=>	'Cost of Avatar ability:',
	'EFFECTS_SIGNATURE'		=>	'Cost of Signature ability:',
	'EFFECTS_EFFECT_TITLE'	=>	'Cost of Custom Title ability:',
	'EFFECTS_NAME_COLOR'	=>	'Cost of Username Color ability:',
	'EFFECTS_NAME_GLOW'		=>	'Cost of Username Glow ability:',
	'EFFECTS_NAME_SHADOW'	=>	'Cost of Username Shadow ability:',
	'EFFECTS_TITLE_COLOR'	=>	'Cost of Rank Color ability:',
	'EFFECTS_TITLE_GLOW'	=>	'Cost of Rank Glow ability:',
	'EFFECTS_TITLE_SHADOW'	=>	'Cost of Rank Shadow ability:',
	'EFFECTS_CONFIG_UPDATE'	=>	'Effects Configuration Updated Successfully!',
	'EFFECTS_RANK_REPLACE'	=>	'Cost of replacing rank with custom title?',
	'EFFECTS_COLORS'		=>	'Name and Rank Colors',
	'EFFECTS_PRIVS'			=>	'Privileges',
	'EFFECTS_EXPLAIN'		=>	'This area allows you to modify the configuration of the %1$s shop.',
	'EFFECTS_COST_EXPLAIN'	=>	'To disable the ability to buy an effect, enter a cost of 0 (zero) or leave the field blank.',
	'EFFECTS_RANK_EXPLAIN'	=>	'This allows members to replace their normal rank title with the custom title. If purchased in combination with rank color, glow, or shadow, the other rank effects would be applied to the custom title.',
	'EFFECTS_AVATAR_EXPLAIN'	=>	"Allows members to purchase the ability to use avatars. When enabled, a member's avatar will not appear and may not be entered in Edit Profile until the ability is purchased. If disabled, avatars are available as normal.",
	'EFFECTS_SIGNATURE_EXPLAIN'	=>	"Allows members to purchase the ability to use signatures. When enabled, a member's signature will not appear and may not be entered in Edit Profile until the ability is purchased. If disabled, signatures are available as normal.",
	'EFFECTS_TITLE_EXPLAIN'	=>	'Allows members to purchase a new title for display in posts and profiles. This is not related to any other custom title features that may be installed. Word censors will be applied to custom titles.',
	'EFFECTS_ADMIN_IMMUNE'	=>	'Allow admins to use signatures, avatars, and username change without purchase?',
	'EFFECTS_MOD_IMMUNE'	=>	'Allow moderators to use signatures, avatars, and username change without purchase?',
	'EFFECTS_BACK_SHOP'	=>	'<a href="%1$s">Return to Effect Store Configuration</a><br /><br /><a href="%2$s">Return to Main Shop Configuration</a>',
	'EFFECTS_DISABLED'	=>	'The effects shop is currently disabled.',
	'EFFECTS_PUBLIC_EXPLAIN'	=>	'In this shop, you may purchase special effects and forum privileges. If you would like to clear a color or title you have purchased, you can delete the value from the matching field and update the effect to clear it. In this case, you will have to buy the effect again if you want to use it later. There is no cost to update an effect you have bought previously (unless you clear an effect and seek to purchase it again later).<br /><br />Please note that glow and shadow effects are not supported by all web browsers. In other browsers, these effects will display as a background color.',
	'EFFECTS_AVATAR_PUBLIC'			=>	'Privilege to use avatars:',
	'EFFECTS_SIGNATURE_PUBLIC'		=>	'Privilege to use signatures:',
	'EFFECTS_TITLE_PUBLIC'			=>	'Custom Title:',
	'EFFECTS_TITLE_CENBLOCK'		=>	'Custom Title',
	'EFFECTS_RANK_REPLACE_PUBLIC'	=>	'Replace rank with Custom Title:',
	'EFFECTS_NAME_COLOR_PUBLIC'		=>	'Name Color:',
	'EFFECTS_NAME_GLOW_PUBLIC'		=>	'Name Glow:',
	'EFFECTS_NAME_SHADOW_PUBLIC'	=>	'Name Shadow:',
	'EFFECTS_TITLE_COLOR_PUBLIC'	=>	'Rank or Title Color:',
	'EFFECTS_TITLE_GLOW_PUBLIC'		=>	'Rank or Title Glow:',
	'EFFECTS_TITLE_SHADOW_PUBLIC'	=>	'Rank or Title Shadow:',
	'EFFECTS_COST'					=>	'Cost to Buy',
	'EFFECTS_STATUS'				=>	'Status',
	'EFFECTS_PURCHASE'				=>	'Purchase or Update Effects',
	'EFFECTS_BUY'					=>	'Buy this Effect',
	'EFFECTS_UPDATE'				=>	'Update this Effect',
	'EFFECTS_PREVIEW'				=>	'Preview Color Effects',
	'EFFECTS_OWN'					=>	'You have purchased this effect.',
	'EFFECTS_ON'					=>	'On',
	'EFFECTS_OFF'					=>	'Off',
	'EFFECTS_BAD_COLOR_INPUT'		=>	'Color values must be in three letter or six letter RGB notation, such as 000000 or 000 for black and should not include a beginning # symbol.<br />Please go back and update your selections to use correct color values.',
	'EFFECTS_NOT_AVAILABLE'	=>	'One of the effects you selected to purchase or update has been disabled.<br />Please go back, reload the page, and update your selections.',
	'EFFECTS_NOT_AFFORD'	=>	'You cannot afford the price of all the effects you have chosen!<br />Please go back and choose fewer effects or return when you have more money.',
	'EFFECTS_BUY_BLANK'	=>	'You selected an effect to buy, but did not enter a value for the effect.<br />Please go back an enter a value for the effects you selected, if possible.',
	'EFFECTS_UPDATED'	=>	'Your effects selections have been updated!',
	'EFFECTS_RETURN'	=>	'<br /><br />%1$sReturn to %4$s%3$s<br /><br />%2$sReturn to Shops%3$s',

	'EFFECTS_SHOPTYPE'				=>	'Shop Type',
	'EFFECTS_OWNER'					=>	'Shop Owner',
	'EFFECTS_URL'					=>	'Shop URL',
	'EFFECTS_SHOWDISTRICT'			=>	'Show this shop as a district?',
	'EFFECTS_PARENTDISTRICT'		=>	'Parent District',
	'EFFECTS_SHOPTYPE_EXPLAIN'		=>	'This can be a short description of the shop. If set to "admin_only," this will hide the shop in listings.',
	'EFFECTS_SHOWDISTRICT_EXPLAIN'	=>	'Displays the shop under the District List. If this is turned on and districts are turned off, the shop will not appear.',
	'EFFECTS_PARENTDISTRICT_EXPLAIN'	=>	'If you have created any districts, you may choose to place the shop in one of them. This has no effect when showing the shop as a district.',
	'EFFECTS_PRIV_EXPLAIN'	=>	'To disable the ability to buy a privilege, enter a cost of 0 (zero) or leave the field blank. In the case of signatures, avatars, and username changes, this will allow the privilege to function as if Effects Store is not installed.',
	'EFFECTS_NO_PARENTS'	=>	'You have not created any districts.',
	'EFFECTS_URL_EXPLAIN'	=>	'Do not change this option unless you have changed the file extension of shop_effects.php for some reason. If you have, update this to reflect the new name of that file.',
	'EFFECTS_COST_CHANGE_USERNAME'	=>	'Cost of Change Username ability:',
	'EFFECTS_COST_OTHER_TITLE'		=>	"Cost of Change Other Member's Title ability:",
	'EFFECTS_CHANGE_USERNAME_EXPLAIN'	=>	'Allows members to change their username in their profile. The forum configuration option "' . $lang['Allow_name_change'] . '" must be enabled for username changes to be allowed.',
	'EFFECTS_OTHER_TITLE_EXPLAIN'		=>	'Allows members to change the custom title of other members. Word censors will be applied to custom titles.',
	'EFFECTS_PM_ADMIN_NAMECHANGE'	=>	'Send a private message to administrators when a member changes their username?',
	'EFFECTS_PM_ADMIN_TITLECHANGE'	=>	'Send a private message to administrators when a member changes the title of another member?',
	'EFFECTS_PM_TITLECHANGE'	=>	'Send a private message to a member when their title is changed by another member?',
	'EFFECTS_NAMECHANGE_PUBLIC'		=>	'Privilege to change your username:',
	'EFFECTS_TITLECHANGE_PUBLIC'	=>	'Change the custom title of another member:',
	'EFFECTS_NAMECHANGE_PUBLIC_EXPLAIN'	=>	'You can change your username in your profile after buying this privilege.',

	'EFFECTS_TITLE_CHANGED_BY'	=>	'Title Bestowed By: %s',
	'EFFECTS_CURRENCY'			=>	'Currency for Purchases:',
	'EFFECTS_CURRENCY_EXPLAIN'	=>	'All costs below will use this currency.',
	'REQUIRES_CASH'	=>	'The currency configured for use could not be located or has been disabled. Please check the configuration.',
	'NO_CURRENCIES'	=>	'No Cash mod Currencies were found. Please create or enable a currency.',
	'NO_CASHMOD'	=>	'The Cash mod does not appear to be installed. The database field "user_points" will be used.',
);


$lang['EFFECTS_CENBLOCK_TITLE'] = <<<EOH
<p>
	Your %s has triggered our word censor feature. The portions of this field that triggered the censor are highlighted in the preview below. Please adjust it and attempt to update your effects again. If you require assistance or feel that the censor may be acting in error, please <a href="mailto:%s">contact us</a>. Thank you.
</p>

<p align="left">
	%s
</p>
EOH;

?>