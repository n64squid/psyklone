<?php 
/*************************************************************************** 
* 						lang_rabbitoshi.php [English] 
* 							------------------- 
*
* 						Translation : seteo bloke & Narc0sis
* 					Forums : http://www.thegamingforum.com/ & http://forums.deviantcore.com/
* 
****************************************************************************/ 

// This is optional, if you would like a _SHORT_ message output 
// indicating you are the translator please add it here. 
// Uncomment the following lines to make appear your translation copyright . 
// Please do not remove the images copyright !
$lang['Rabbitoshi_translation'] = 'Images are property of <a href="http://www.barrysclipart.com/index.php" target="_blank">barrysclipart</a><br />Pet system English translation made by : <a href="http://www.thegamingforum.com/" target="_blank">seteo bloke</a> & <a href="http://forums.deviantcore.com/" target="_blank">Narc0sis</a>'; 

// Admin 
$lang['Rabbitoshi']='Rabbitoshi'; 
$lang['Rabbitoshi_nopet_title']='Buying a pet'; 
$lang['Rabbitoshi_pet_health']='Health cash'; 
$lang['Rabbitoshi_pet_of']='The pet owned by '; 
$lang['Rabbitoshi_pet_prize']='Price'; 
$lang['Rabbitoshi_pet_buy']='Buy this creature '; 
$lang['Rabbitoshi_pet_choose']='Choose a creature '; 
$lang['Rabbitoshi_pet_hunger']='Resistance in the famine'; 
$lang['Rabbitoshi_pet_thirst']='Resistance in the thirst'; 
$lang['Rabbitoshi_pet_hygiene']='Resistance in the dirt'; 
$lang['Rabbitoshi_title']='Rabbitoshi'; 
$lang['Rabbitoshi_name_select']='Please choose a name for your pet'; 
$lang['Rabbitoshi_buypet_success'] ='Congratulations!<br /><br />Click <a href="'.append_sid("rabbitoshi.$phpEx").'">here</a> to go to the pet shop, where you will be able to buy food, drink and other fun things for him.<br /><br />'; 
$lang['Rabbitoshi_pet_call_vet']='Call the vet'; 
$lang['Rabbitoshi_pet_feed']='Give food'; 
$lang['Rabbitoshi_pet_drink']='Give water'; 
$lang['Rabbitoshi_pet_clean']='Clean the place'; 
$lang['Rabbitoshi_pet_caracs']='Characteristics'; 
$lang['Rabbitoshi_pet_health']='Health : '; 
$lang['Rabbitoshi_pet_hunger']='Hunger : '; 
$lang['Rabbitoshi_pet_thirst']='Thirst : '; 
$lang['Rabbitoshi_pet_hygiene']='Hygiene : '; 
$lang['Day']='Day'; 
$lang['Days']='Days'; 
$lang['Hour']='Hour'; 
$lang['Hours']='Hours'; 
$lang['Minute']='Minute'; 
$lang['Minutes']='Minutes'; 
$lang['Rabbitoshi_pet_age']='Age : '; 
$lang['Rabbitoshi_topic']='See the poster\'s pet'; 
$lang['Rabbitoshi_shop_name']='Name'; 
$lang['Rabbitoshi_shop_img']='Image'; 
$lang['Rabbitoshi_item_desc']='Description'; 
$lang['Rabbitoshi_shop_prize']='Price'; 
$lang['Rabbitoshi_item_type']='Type'; 
$lang['Rabbitoshi_item_sum']='Owned'; 
$lang['Rabbitoshi_shop_buy']='Buy'; 
$lang['Rabbitoshi_shop_sell']='Sell'; 
$lang['Rabbitoshi_lack_items']='You can\'t sell an item you don\'t own '; 
$lang['Rabbitoshi_lack']='You do not have enough money to purchase this item'; 
$lang['Rabbitoshi_disable']='The pet system is currently disabled. Please try again later'; 
$lang['Rabbitoshi_owner_last_visit']='OWNER\'S LAST VISIT';
$lang['Rabbitoshi_pet_favorite_food']='PET\'S FAVORITE FOOD'; 
$lang['Rabbitoshi_pet_vet']='Your pet is now fully healed!'; 
$lang['Rabbitoshi_pet_vet_lack']='You don\'t have enough money to pay for the services of the vet'; 
$lang['Rabbitoshi_vet_holidays']='The vet is not available at the moment, please come back later'; 
$lang['Rabbitoshi_food_no_need']='Your pet doesn\'t need to be fed at the moment'; 
$lang['Rabbitoshi_water_no_need']='Your pet doesn\'t need to drink at the moment'; 
$lang['Rabbitoshi_clean_no_need']='You do not need to clean your pets area at the moment'; 

// Admin 
$lang['Rabbitoshi_img_explain']='The name of the picture put into the <i>images/Rabbitoshi/</i> directory must be the same name of this pet'; 
$lang['rRabbitoshi_title']='Edition and deletion of the pets'; 
$lang['rRabbitoshi_config']='Addition of a new pet'; 
$lang['rRabbitoshi_config_edit']='Edition of a pet'; 
$lang['rRabbitoshi_desc']='Here you can edit or add a pet'; 
$lang['Rabbitoshi_del_success']='The pet has been deleted successfully'; 
$lang['Rabbitoshi_add_success']='The pet has been added successfully'; 
$lang['Rabbitoshi_edit_success']='The pet has been edited successfully'; 
$lang['Rabbitoshi_food_type']='Food Type'; 
$lang['Rabbitoshi_shop_title']='Pet Shop Management'; 
$lang['Rabbitoshi_shop_desc']='Here you can manage the items for your forum\'s pets'; 
$lang['Rabbitoshi_item_type_food']='Food'; 
$lang['Rabbitoshi_item_type_water']='Water'; 
$lang['Rabbitoshi_item_type_misc']='Miscellanous'; 
$lang['Rabbitoshi_shop_name']='Name'; 
$lang['Rabbitoshi_shop_prize']='Price'; 
$lang['Rabbitoshi_shop_type']='Type'; 
$lang['Rabbitoshi_item_desc']='Description'; 
$lang['Rabbitoshi_shop_img']='Image'; 
$lang['Rabbitoshi_shop_edit_success']='The item has been updated successfully'; 
$lang['Rabbitoshi_Pets_Management']='Pets Management'; 
$lang['Rabbitoshi_Shop']='Pets Shop'; 
$lang['Rabbitoshi_title']='Management of the pets'; 
$lang['Rabbitoshi_desc']='Here you can manage the pets, add values into, edit, or delete them.'; 
$lang['Rabbitoshi_add']='Add a pet'; 
$lang['Rabbitoshi_name']='Name'; 
$lang['Rabbitoshi_img']='Image'; 
$lang['Rabbitoshi_img2']='Reminder'; 
$lang['Rabbitoshi_img_explain']='The name of the picture put into the <i>images/Rabbitoshi/</i> directory must be the same name of this pet'; 
$lang['rRabbitoshi_title']='Edition and deletion of the pets'; 
$lang['Rabbitoshi_food_type']='Food Type'; 
$lang['Rabbitoshi_shop_title']='Pet Shop Management'; 
$lang['Rabbitoshi_shop_desc']='Here you can manage the items for your forum\'s pets'; 
$lang['Rabbitoshi_item_type_food']='Food'; 
$lang['Rabbitoshi_item_type_water']='Water'; 
$lang['Rabbitoshi_item_type_misc']='Misc'; 
$lang['Rabbitoshi_shop_edit_success']='The item has been updated successfully'; 
$lang['Rabbitoshi_settings']='General settings for the pet system'; 
$lang['Rabbitoshi_settings_explain']='Here you can activate/deactivate the pet system, change the name of it, define the period for feeding pets, and more.'; 
$lang['Rabbitoshi_use']='Use the pet system'; 
$lang['Rabbitoshi_settings_name']='Name of the pet system'; 
$lang['Rabbitoshi_settings_explanations']='For the values listed below , time is the period after the value you set in the second field will be substracted'; 
$lang['Rabbitoshi_hunger_time']='Time before the <b>hunger</b> status decreases (in seconds)'; 
$lang['Rabbitoshi_hunger_value']='Value of the decrease'; 
$lang['Rabbitoshi_thirst_time']='Time before the <b>thirst</b> status decreases (in seconds)'; 
$lang['Rabbitoshi_thirst_value']='Value of the decrease'; 
$lang['Rabbitoshi_health_time']='Time before the <b>health</b> status decreases (in seconds)'; 
$lang['Rabbitoshi_health_value']='Value of the decrease'; 
$lang['Rabbitoshi_hygiene_time']='Time before the <b>hygiene</b> status decreases (in seconds)'; 
$lang['Rabbitoshi_hygiene_value']='Value of the decrease'; 
$lang['Rabbitoshi_updated_return_settings']='General settings of the pet system successfully updated ! <br /><br /> Click %sHere%s to return to the pet system settings'; 
$lang['Rabbitoshi_update_error']='Error during the update of the pet system'; 
$lang['Rabbitoshi_rebirth_enable']='Enable rebirth'; 
$lang['Rabbitoshi_rebirth_enable_explain']='If you check yes, users can pay to revive their pet when dead. If not, they will have to buy another one.'; 
$lang['Rabbitoshi_rebirth_price']='Cost to raise a dead pet'; 
$lang['Rabbitoshi_health_explain']='Please note that the pet is also losing health cash if he is hungry, thirsty, or their hygiene status is too low'; 
$lang['Rabbitoshi_vet_enable']='Enable the vet'; 
$lang['Rabbitoshi_vet_price']='Cost to visit the vet'; 
$lang['Rabbitoshi_rebirth_vet_explain']='Using the vet, pet owners can fully heal their pet'; 
$lang['Rabbitoshi_shop_power']='Power'; 
$lang['Rabbitoshi_shop_power_explain']='Status cash regained while using the item'; 
$lang['Click_return_rabbitoshiadmin']='Click %shere%s to return to the pets administration'; 
$lang['Click_return_rabbitoshi_shopadmin']='Click %shere%s to return to the pet shop management'; 
$lang['Rabbitoshi_pet_dead_rebirth']='Your pet is dead ... <br /><br /> It was very expensive but your pet has been resuscitate! <br />'; 
$lang['Rabbitoshi_pet_dead_lack']='Your pet is dead ... <br /><br /> And you didn\'t have enough money to resuscitate him ... <br />You will have to buy another pet'; 
$lang['Rabbitoshi_pet_dead']='Your pet is dead ... <br /><br /> You should have been more cautious with him ... <br />You have to buy another pet'; 

// Pet Messages 
$lang['Rabbitoshi_general_message']='PET\'S THOUGHTS'; 
$lang['Rabbitoshi_message']='PET\'S ALERTS'; 
$lang['Rabbitoshi_message_hungry']='Please feed me!'; 
$lang['Rabbitoshi_message_very_hungry']='I am going to become a skeleton!'; 
$lang['Rabbitoshi_message_thirst']='Give me a drink please '; 
$lang['Rabbitoshi_message_very_thirst']='Water ... I really need water now ...'; 
$lang['Rabbitoshi_message_health']='Please heal me !'; 
$lang['Rabbitoshi_message_very_health']='Argh ... I\'m dying ... alone ... '; 
$lang['Rabbitoshi_message_hygiene']='I can\'t live in this crap heap !'; 
$lang['Rabbitoshi_message_very_hygiene']='I think I am sick. This place is so dirty.'; 
$lang['Rabbitoshi_general_message_very_bad']='Why?...Why me?... I am dying ... No chance left for me ... Please someone, end my pain ...'; 
$lang['Rabbitoshi_general_message_bad']='Will my master come ? He doesn\'t care about me . This kind of human souldn\'t be allowed to have pets!'; 
$lang['Rabbitoshi_general_message_neutral']='How boring is my life?!'; 
$lang['Rabbitoshi_general_message_good']='*singing*'; 
$lang['Rabbitoshi_general_message_very_good']='I really couldn\'t have dreamt of a better master ! He is still here for me, I am a lucky pet!'; 

// Lang keys added for 0.1.1 
$lang['Rabbitoshi_owner_cash']='Your money'; 
$lang['Rabbitoshi_owner_sell']='Sell your pet'; 
$lang['Rabbitoshi_owner_pet_value']='Value of your pet'; 
$lang['Rabbitoshi_pet_sold']='You have sold your pet for '.$pet_value.' cash <br /><br /> Click <a href="'.append_sid("rabbitoshi.$phpEx").'">here</a> to buy another one<br /><br /> Click <a href="'.append_sid("index.$phpEx").'">here</a> to retun to the forums'; 


// Lang keys added or modified for 0.1.2 
$lang['Rabbitoshi_pet_shop']='Buy and sell items for your pet'; 
$lang['Rabbitoshi_general_return'] ='<br /><br /> Click <a href="'.append_sid("rabbitoshi.$phpEx").'">here</a> to see your pet<br /><br /> Click <a href="'.append_sid("rabbitoshi_shop.$phpEx").'">here</a> to buy items for your pet<br /><br /> Click <a href="'.append_sid("index.$phpEx").'">here</a> to retun to the forums'; 
$lang['Rabbitoshi_shop_action_plus']='Thanks for your purchases . The items you bought have cost you '; 
$lang['Rabbitoshi_shop_action_less']='Thanks for your purchases . With the items you sold , you have earned '; 
$lang['Rabbitoshi_shop_lack_items']='You can\'t sell items you don\'t own'; 
$lang['Rabbitoshi_shop_action']='Perform these purchases'; 
$lang['Rabbitoshi_lack_food']='You don\'t have the favorite food of your pet'; 
$lang['Rabbitoshi_lack_water']='You don\'t have water to give to your pet'; 
$lang['Rabbitoshi_lack_cleaner']='You don\'t have a cleaner to clean your pet place'; 

// Lang keys added or modified for 0.1.4 
$lang['Rabbitoshi_owner_admin_title']='Pet owners management'; 
$lang['Rabbitoshi_owner_admin_title_explain']='Here you can edit the characteristics of the pets for your users'; 
$lang['Rabbitoshi_owner_admin_submit']='Validate the modifications'; 
$lang['Rabbitoshi_owner_admin_select']='Select an owner to edit :'; 
$lang['Rabbitoshi_owner_admin_select_submit']='See this owner'; 
$lang['Rabbitoshi_owner']='Owner name'; 
$lang['Rabbitoshi_owner_pet']='Name of his pet'; 
$lang['Rabbitoshi_owner_pet_health']='Health'; 
$lang['Rabbitoshi_owner_pet_hunger']='Hunger'; 
$lang['Rabbitoshi_owner_pet_thirst']='Thirst'; 
$lang['Rabbitoshi_owner_pet_hygiene']='Hygiene'; 
$lang['Rabbitoshi_owner_pet_type']='Kind of pet'; 
$lang['Rabbitoshi_owner_admin_ok']='Owner caracteristics successfully edited'; 
$lang['Rabbitoshi_admin_general_return']='<br /><br /> Click <a href="'.append_sid("admin_rabbitoshi_owners.$phpEx").'">here</a> to return to the owners management<br /><br />'; 

// Lang keys added or modified for 0.1.7 
$lang['Rabbitoshi_owner_pet_lack']='This user doesn\'t have a pet';

// Lang keys added or modified for 0.2.0
$lang['Rabbitoshi_pet_sell_confirm']='Are you sure you want to sell your pet ?';
$lang['Rabbitoshi_pet_sold']='You have sold your pet for ';
$lang['Rabbitoshi_return']='<br /><br /> Click <a href="'.append_sid("rabbitoshi.$phpEx").'">here</a> to buy another one<br /><br /> Click <a href="'.append_sid("index.$phpEx").'">here</a> to retun to the forums';
$lang['Rabbitoshi_shop_return']='Return to the creature panel';
$lang['Rabbitoshi_services']='Services';
$lang['Rabbitoshi_pet_call_vet_explain']='The vet can fully heal your pet . Cost : ';
$lang['Rabbitoshi_owner_list']='See the pet owners list';
$lang['Rabbitoshi_hotel']='Hotel';
$lang['Rabbitoshi_hotel_explain']='Confide your pet during your holidays';
$lang['Rabbitoshi_evolution']='Evolution';
$lang['Rabbitoshi_evolution_explain']='Maybe your pet can evolve...';
$lang['Rabbitoshi_pet_name']='Pet Name';
$lang['Rabbitoshi_pet_time']='Pet Age';
$lang['Rabbitoshi_pet_type']='Pet Type';
$lang['Rabbitoshi_default_cash_name']='cash';
$lang['Rabbitoshi_pet_into_hotel']='This pet is currently into his hotel . <br /> He will come back the ';
$lang['Rabbitoshi_hotel_use']='Enable the hotel';
$lang['Rabbitoshi_hotel_use_explain']='When a pet is into the hotel , his status bars don\'t decrease';
$lang['Rabbitoshi_hotel_price']='Hotel price';
$lang['Rabbitoshi_hotel_price_explain'] ='Cost for each day into the hotel';
$lang['Rabbitoshi_evolution_use']='Enable evolution';
$lang['Rabbitoshi_evolution_use_explain']='If you check this , some creatures will be able to evolve ( have a look to the pets management too )'; 
$lang['Rabbitoshi_evolution_price']='Evolution price';
$lang['Rabbitoshi_evolution_price_explain']='Percent of the evolved pet cost ( let this value egal to 0 if you want this feature to be free )';
$lang['Rabbitoshi_evolution_time']='Evolution required time';
$lang['Rabbitoshi_evolution_time_explain']='Minimal days the owner has his pet before he can look for an evolution'; 
$lang['Rabbitoshi_is_evolution_of']='Evolution of';
$lang['Rabbitoshi_buyable']='Buyable';
$lang['Rabbitoshi_buyable_explain']='If you check this option , users will be able to buy it';
$lang['Rabbitoshi_is_evolution_of_explain']='You can choose a pet from this one can evolve';
$lang['Rabbitoshi_is_evolution_of_none']='Isn\'t an evolution';
$lang['Rabbitoshi_is_in_hotel']='Your pet is into the hotel until the';
$lang['Rabbitoshi_hotel_welcome']='Welcome to the pet hotel';
$lang['Rabbitoshi_out_of_hotel']='Click here to get back your pet';
$lang['Rabbitoshi_hotel_out_success']='You have gotten back your pet successfully';
$lang['Rabbitoshi_hotel_welcome_services']='We can keep your pet if you wish';
$lang['Rabbitoshi_hotel_welcome_services_select']='Select the number of days';
$lang['Rabbitoshi_hotel_get_in']='Let your pet into the hotel';
$lang['Rabbitoshi_hotel_in_success']='Your pet is now into the hotel';
$lang['Rabbitoshi_hotel_lack_money']='You don\'t have enough money to let your pet a so long time into the hotel';
$lang['Rabbitoshi_no_evolution']='Your pet can\'t evolve';
$lang['Rabbitoshi_no_evolution_time']='Your pet is too young to evolve . You must own him since at least ';
$lang['Rabbitoshi_evolution']='Evolution';
$lang['Rabbitoshi_evolution_welcome']='Please select an evolution for your creature';
$lang['Rabbitoshi_evolution_exec']='Evolution !';
$lang['Rabbitoshi_evolution_lack']='You can\'t afford this evolution';
$lang['Rabbitoshi_evolution_success']='Your pet has successfully evolved !!';

// Lang keys added or modified for 0.2.2
$lang['Rabbitoshi_hotel_no_actions']='You can\'t perform this action while your pet is in the hotel';
$lang['Rabbitoshi_confirm']='Confirmation';
$lang['Rabbitoshi_pet_is_dead']='Your pet is dead';
$lang['Rabbitoshi_pet_is_dead_cost']='To see her again , it will cost to you';
$lang['Rabbitoshi_pet_is_dead_cost_explain']='Do you want to pay to ressucitate your pet ?';
$lang['Rabbitoshi_pet_dead_rebirth_no']='You have a let dying your creature .<br /><br /> Click <a href="'.append_sid("rabbitoshi.$phpEx").'">here</a> to buy another one<br /><br /> Click <a href="'.append_sid("index.$phpEx").'">here</a> to return to the forums index';
$lang['Rabbitoshi_pet_dead_rebirth_ok']='Your pet is alive again !<br /><br /> Click <a href="'.append_sid("rabbitoshi.$phpEx").'">here</a> to take care of her<br /><br /> Click <a href="'.append_sid("index.$phpEx").'">here</a> to return to the forums index';
$lang['Rabbitoshi_pet_sell']='Sale of your creature';
$lang['Rabbitoshi_pet_sell_for']='Are you sure that you want to sell your pet for';
$lang['Rabbitoshi_owner_list_explain']='Pet owners list';
$lang['Rabbitoshi_preferences_explain']='Management of your preferences';
$lang['Rabbitoshi_preferences_notify']='Notify me by PM of the needs of my pet';
$lang['Rabbitoshi_preferences_hide']='Hide my pet to the others users';
$lang['Rabbitoshi_preferences_updated']='Your preferences have been edited successfully';
$lang['Rabbitoshi_hidden']='Sorry , this user doesn\'t want others may see his pet\'s caracteristics<br /><br /> Click <a href="'.append_sid("index.$phpEx").'">here</a> to return to the forums index';
$lang['Rabbitoshi_seconds']='Seconds';
$lang['Rabbitoshi_cron_use']='Use the automatic update of the creatures statistics';
$lang['Rabbitoshi_cron_explain']='This system allows to automatically update the pet statistics . Because it uses much of the CPU ressources , this can\'t be done all the time . If you own many users , you shouldn\'t use this feature more than once a day';
$lang['Rabbitoshi_cron_time']='Time beetween two automatic updates';
$lang['Rabbitoshi_pm_news']='Some news of your pet';
$lang['Rabbitoshi_pm_news_hotel']='Your creature is installed comfortably in the hotel';

// Lang keys added or modified for 0.2.3
$lang['Rabbitoshi_preferences']='Preferences';
$lang['Rabbitoshi_cron_admin_update']='Manual update';
$lang['Rabbitoshi_cron_admin_update_explain']='Because the pet statistics are only updated while the owner is looking at him or during the automatic updates , the informations about the owners you can see on this page could be wrong . Click on the Manual update button if you want to update all the pets statistics';
$lang['Rabbitoshi_owner_admin_cron_ok']='Manual update successfully done';

// Lang keys added or modified for 0.3.0
$lang['Rabbitoshi_language_key']='You can enter text or a language key ( please refer to language/lang_<i>your_language</i>/lang_rabbitoshi.php)';
$lang['Rabbitoshi_img_explain']='Name of the picture file into the directory images/Rabbitoshi/ corresponding to this creature';
$lang['Rabbit']='Rabbit';
$lang['Chaos Rabbit']='Chaos Rabbit';
$lang['Beaver']='Beaver';
$lang['Dog']='Dog';
$lang['Chinchilla']='Chinchilla';
$lang['Cat']='Caat';
$lang['Lion']='Lion';
$lang['Penguin']='Penguin';
$lang['Dragon']='Dragon';
$lang['Hiloterium']='Hiloterium';
$lang['Tiger']='Tiger';
$lang['Eagle']='Eagle';
$lang['Polar Bear']='Polar Bear';
$lang['Rabbitoshi_APM_pm']='The statistics of your creature have been updated . You should go and see her.';
$lang['Rabbitoshi_img_item_explain']='Name of the picture file into the directory images/Rabbitoshi/ corresponding to this item';
$lang['Rabbitoshi_items_seeds']='Seeds';
$lang['Rabbitoshi_items_seeds_desc']='Seeds for your pet';
$lang['Rabbitoshi_items_carrots']='Carrots';
$lang['Rabbitoshi_items_carrots_desc']='The favorite food of the big ears';
$lang['Rabbitoshi_items_meat']='Fresh meat';
$lang['Rabbitoshi_items_meat_desc']='For the carnivorous animals';
$lang['Rabbitoshi_items_fish']='Fish';
$lang['Rabbitoshi_items_fish_desc']='Some creatures only like fish';
$lang['Rabbitoshi_items_gazelle']='Gazelle';
$lang['Rabbitoshi_items_gazelle_desc']='Fresh meat and pleasure to hunt...';
$lang['Rabbitoshi_items_diamonds']='Diamonds';
$lang['Rabbitoshi_items_diamonds_desc']='Dragons and others mythical creatures love eating jewels';
$lang['Rabbitoshi_items_limpid_water']='Limpid water';
$lang['Rabbitoshi_items_limpid_water_desc']='The most thirst-quenching drink';
$lang['Rabbitoshi_items_cleaner']='Cleaner';
$lang['Rabbitoshi_items_cleaner_desc']='To clean the place where lives your pet';
$lang['Rabbitoshi_shop_add']='Add an item';
$lang['Rabbitoshi_shop_power_explain']='This is the number of cash earned when an user uses this item';
$lang['Rabbitoshi_item_type_misc']='Cleaning'; 
$lang['Rabbitoshi_shop_title_add']='Add an item';
$lang['Rabbitoshi_shop_config_add']='This form allows you to add an item into the pets shop';
$lang['Rabbitoshi_shop_added_success']='The new item has been added successfully';
$lang['Rabbitoshi_shop_del_success']='This item has been deleted successfully';
$lang['Rabbitoshi_preferences_feed_full']='Always give to my creature all the food which she/he needs';
$lang['Rabbitoshi_preferences_feed_full_explain']='By marking this option, you will use all the available food so that your creature is no more hungry. Should the opposite occur, you will give her/him only a little food each time';
$lang['Rabbitoshi_preferences_drink_full']='Always give to my creature all the water which she/he needs';
$lang['Rabbitoshi_preferences_drink_full_explain']='By marking this option, you will use all the available water so that your creature is no more thirsty. Should the opposite occur, you will give her/him only a little water each time';
$lang['Rabbitoshi_preferences_clean_full']='Always clean the environment of my creature until it is completely clean';
$lang['Rabbitoshi_preferences_clean_full_explain']='By marking this option, you will use all the item of cleaning until the environment of your creature is completely clean. Should the opposite occur, you will clean only little every time';
$lang['Rabbitoshi_item_type_food_none']='None'; 
?>