<?php
/***************************************************************************
 *                         lang_arcade.php [english]
 *                         -------------------------
 *   begin                : Tuesday 17 Janurary, 2006
 *   copyright            : (c) 2003-2006 dEfEndEr - www.phpbb-amod.co.uk       
 *   email                : support@phpbb-amod.co.uk
 *
 *   $Id: lang_arcade.php, v2.1.2 2006/01/17 23:59:59 dEfEndEr Exp $
 *
 *	Based on the Original Activity Mod by Napoleon.
 *
 ***************************************************************************
 *
 *   This language file is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************
 * 	CREDITS: 
 *  Whoo - Games and part code
 *  Napoleon - Original Activity Mod v2.0.0
 *	Buddystuart
 *  Minesh - Add-On's
 *  Painkiller
 *  Mark
 *  ~Maverick~ - Add-On's
 *  Zorial - Add-On's
 *  qx17417 - beta testing
 *  Madman - Chief Tester :)
 *	Others - Everybody else that helps with the project.
 ***************************************************************************/

// Admin
$lang['admin_main_header'] = 'This control panel will help you maintain and control the online activities.<br />If you are currently having a problem with our MOD, then please contact us at <a href="http://www.phpbb-amod.co.uk"  target=_blank class="copyright">www.phpbb-amod.co.uk</a> so we may help fix the problem.';
$lang['admin_config_menu'] = 'Online Arcade Configuration Menu';
$lang['admin_game_menu'] = 'Online Arcade/Activity Menu';
$lang['admin_game_editor'] = 'Online Arcade/Activity Editor Menu';
$lang['admin_game_import'] = 'Online Arcade/Activity Import Menu';
$lang['admin_editor_info'] = '-:- This control panel allows you to Add/Edit your Arcade/Activity data. Any game that has been released by <a href="http://www.phpbb-amod.co.uk" target="new_window">dEfEndEr</a>, Buddystuart, Whoo, Mullac or Alegis can easily be plugged into your forums and Highscores will be saved. Also you can install any of the 1000\'s of free to download games, music, movies and images to add some great content to your site. If you would like to convert a game to use this control panel, then ask for help at our <a href="http://www.phpbb-amod.co.uk/" target="new_window">forums</a>. If you are having trouble with this menu or the Arcade MOD then please contact us at <a href="http://www.phpbb-amod.co.uk" target=_blank>www.phpbb-amod.co.uk</a>.';
$lang['admin_import_info'] = '-:- This control panel allows you to import data directly in to your database (to save you time). If you are having trouble with this menu or the Arcade MOD then please contact us at <a href="http://www.phpbb-amod.co.uk" target=_blank >www.phpbb-amod.co.uk</a>.';
$lang['admin_game_deleted'] = 'Game Deleted<br /><br />';
$lang['admin_game_not_deleted'] = 'Item NOT Deleted<br /><br />';
$lang['admin_game_repaired'] = 'Database Repaired<br /><br />';
$lang['admin_game_saved'] = 'Item Saved Successfully<br /><br />';
$lang['admin_score_reset'] = 'All scores have been reset<br /><br />';
$lang['admin_return_arcade'] = 'Click %sHere%s to return to the Arcade Menu';
$lang['admin_return_import'] = 'Click %sHere%s to return to the Import Menu';
$lang['admin_config_updated'] = 'Online Activities Configuration Updated<br /><br />';
$lang['admin_toggles'] = 'Support Toggles';
$lang['admin_rewards'] = 'Reward system Toggles';
$lang['admin_arcade_config'] = 'Arcade Configuration';
$lang['admin_use_adar_shop'] = '<b>Use the <a href="http://www.phpbb.com" target="newindow">Adar Item Shop</a></b><br />';
$lang['admin_use_adar_info'] = '-:- If you want to reward Items for Highscore bonuses and you have the<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Adar ItemShop, then toggle this to <b>Yes</b>.<br /><br />-:- <b>NOTE : </b>This will not work with other shops!';
$lang['admin_use_gamelib'] = '<b>Use the Gamelib Javascript Library</a></b><br />';
$lang['admin_use_gl_info'] = '-:- If you are using a <i>JAVA</i> game/application that uses Scott Porter\'s gamelib,<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;then toggle this on.';
$lang['admin_use_points'] = '<b>Use the <a href="http://www.phpbb-amod.co.uk" target="newindow">Points System</a></b><br />';
$lang['admin_use_pts_info'] = '-:- If you are using the Points System then toggle this on so that these MODs<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;can use this reward system for your forums.';
$lang['admin_use_cash'] = '<b>Use the <a href="http://www.phpbb.com/community/" target="newindow">Cash System</a></b><br />';
$lang['admin_use_cash_info'] = '-:- If you are using the Cash System then toggle this on so that these MODs<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;can use this reward system for your forums.';
$lang['admin_use_allowance'] = '<b>Use the <a href="http://www.phpbb-amod.co.uk" target="newindow">Allowance System</a></b><br />';
$lang['admin_use_allowance_info'] = '-:- If you are using the Allowance System then toggle this on so that these MODs<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;can use this reward system for your forums.';
$lang['admin_games_path'] = '<b>Default Games Path</b><br />';
$lang['admin_games_path_info'] = '-:- A directory located in your forums root directory where you wish to hold all of your games.';
$lang['admin_gl_game_path'] = '<b>Gamelib Games Path</b><br />';
$lang['admin_gl_path_info'] = '-:- This is the directory located in your forums root directory where you wish<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;to hold all of your games that use the gamelib.';
$lang['admin_gl_lib_path'] = '<b>Gamelib Javascript Library Path</b><br />';
$lang['admin_gl_lib_info'] = '-:- This Directory is located in your <b>Gamelib Games Path</b> and holds all<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;of the gamelib*.js files.<br /><br />-:- <b>NOTE</b> : If your games are locking up or you don\'t hear any<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sound then check this directory to make sure there are gamelib files<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;in here.';
$lang['admin_games_per_page'] = '<b>Games Per Page</b><br />';
$lang['admin_games_per_info'] = '-:- This is how many games you want listed before a new page is needed. (0 = all)';
$lang['admin_page'] = 'Pages';
$lang['admin_game_id'] = 'Game ID#';
$lang['admin_path'] = 'Path';
$lang['admin_adar_config'] = 'Adarian Shop Options';
$lang['admin_adar_shop'] = '<b>Adar Shop</b><br />';
$lang['admin_no_adar_info'] = '-:- The Adarian Shop options are not installed. Please install Adar before<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;setting these options.';
$lang['admin_games'] = 'Games';
$lang['admin_charge'] = 'Charge';
$lang['admin_button'] = 'Button';
$lang['admin_description'] = 'Description';
$lang['admin_reward'] = 'Reward';
$lang['admin_bonus'] = 'Bonus';
$lang['admin_at_bonus'] = 'AT Bonus';
$lang['admin_flash'] = 'Flash';
$lang['admin_score'] = 'Score';
$lang['admin_gamelib'] = 'Gamelib';
$lang['admin_action'] = 'Action';
$lang['admin_move'] = 'Move';
$lang['admin_repair'] = 'Repair Game Index';
$lang['admin_reset'] = 'Reset High Scores';
$lang['admin_at_reset'] = 'Reset ALL TIME Scores';
$lang['admin_up'] = 'Up';
$lang['admin_down'] = 'Dn';
$lang['admin_down_full'] = 'Down';
$lang['admin_delete'] = 'X';
$lang['admin_delete_full'] = 'Delete';
$lang['admin_limit'] = 'Limit';
$lang['admin_at_limit'] = 'AT Limit';
$lang['admin_width'] = 'Width';
$lang['admin_height'] = 'Height';
$lang['admin_cash'] = 'Cash';
$lang['admin_name'] = '<b>Filename / phpBB Amod Game Name</b><br />';
$lang['admin_name_info'] = '-:- This is the FILENAME of the game you are installing (case sensitive). <br /><br />-:- <b>NOTE</b> : Keep the name of this game exactly as you find it...!<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;phpBB Amod/Activity Mod/Arcade Mod games have the name HARD CODED in to them<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;so do not use the extension .swf (turn ON FLASH support below. Anything else will cause errors.';
$lang['admin_game_path'] = '<b>Path to the file above</b><br />';
$lang['admin_game_path_info'] = '-:- This is the PATH to the location of the file within your phpBB root directory.';
$lang['admin_game_desc'] = '<b>Description</b><br />';
$lang['admin_game_desc_info'] = '-:- This is the description of the activity that is displayed to your users.';
$lang['admin_game_charge'] = '<b>Game Charges</b><br />';
$lang['admin_game_charge_info'] = '-:- This is how much to charge users to play this game.';
$lang['admin_game_per'] = '<b>Score per point reward</b><br />';
$lang['admin_game_per_info'] = '-:- This is the score the user has to get to receive 1 Reward (point etc)  <br /><br />-:- <b>Example</b> : If you put 100 as the value for this option,<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;and a player scores 100 in this game. then the player will get 1 Reward/Point.';
$lang['admin_game_bonus'] = '<b>Highscore Bonus</b><br />';
$lang['admin_game_bonus_info'] = '-:- This is the amount of reward points to give a user if they obtain the highscore.';
$lang['admin_game_gamelib'] = '<b>Use the Gamelib</b><br />';
//
//  Words changed in 2.0.8
//
$lang['admin_game_gamelib_info'] = '-:- Set this to yes if this JAVA game uses the <b>GameLib</b>.';
$lang['admin_game_flash'] = '<b>Macromedia Flash Game Type</b><br />';
$lang['admin_game_flash_info'] = '-:- for Activity Mod/pnFlashGames/Arcade Mod the filename (top) does not require the .swf ext.';
//
$lang['admin_game_show_score'] = '<b>Show the scores</b><br />';
$lang['admin_game_show_info'] = '-:- Set this to yes if you wish to use scores for this arcade.';
$lang['admin_game_reverse'] = '<b>Reverse Highscore Listing</b><br />';
$lang['admin_game_reverse_info'] = '-:- Set this to yes if you wish to list the lowest scores first.';
$lang['admin_game_highscore'] = '<b>Highscore Limit</b><br />';
$lang['admin_game_highscore_info'] = '-:- This is how many scores you want listed for this game. Leave blank for all.';
$lang['admin_game_size'] = '<b>Window Size</b><br />';
$lang['admin_game_size_info'] = '-:- This is how big in pixels, the window will be when a game is started.';
$lang['instructions_info'] = 'Enter this games "How to play" instructions below. HTML Tags will also work in here.<br />';
$lang['admin_game_reset_hs'] = '<b>Reset Highscores</b><br />';
$lang['admin_game_reset_hs_info'] = '-:- By setting this to <b>"Yes"</b> the Highscores will reset.';
$lang['admin_game_reset_at_hs'] = '<b>Reset ALL-TIME Scores</b><br />';
$lang['admin_game_reset_at_hs_info'] = '-:- By setting this to <b>"Yes"</b> the ALL TIME Highscores will reset.';
$lang['admin_use_rewards'] = '<b>Use Rewards MOD</b><br />';
$lang['admin_use_rewards_info'] = '-:- If you have a rewards MOD installed [Points/Cash/Allowance]<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;then turn this on to configure your rewards MOD.';
$lang['admin_cheat'] = '<b>Use Cheat Mode</b><br />';
$lang['admin_cheat_info'] = '-:- If turned on, Proxy <b>guests</b> & users logged out during a session, will NOT be able to save high scores.';
$lang['admin_warn_cheater'] = '<b>Display warning to possible cheater</b><br />';
$lang['admin_warn_cheater_info'] = '-:- If turned on, then this will display a message to anyone who might get caught cheating.';
$lang['admin_cheater_warning'] = '<br />You have been reported to the site <b>Admin</b> as a possible cheater.<br /><br /> If you feel that you have not cheated in an online game, then please contact the site Admin.<br />';
$lang['admin_warn_admin'] = '<b>Report cheaters</b><br />';
$lang['admin_warn_admin_info'] = '-:- If turned on the site Admin will get an E-mail notification if someone is caught cheating.';
$lang['admin_cash_default_info'] = '-:- The Arcade MOD uses only 1 reward field. Please enter in a default user<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;reward field for your users. [<b>Cash MOD Only</b>]';
$lang['admin_games_offline'] = '<b>Take Mod Offline</b><br />';
$lang['admin_games_offline_info'] = '-:- This will show any uses who try and access the games an offline message';
$lang['admin_default_game_id'] = '<b>Default Game ID Number</b><br />';
$lang['admin_default_game_id_info'] = '-:- When Adding a game, use this game as a default. "0" = All fields empty.';
$lang['admin_cat'] = '<b>Game Category</b><br />';
$lang['admin_cat_info'] = '-:- The category that the game will be viewed under.';
$lang['admin_default_img'] = '<b>Default Image</b><br />';
$lang['admin_default_img_info'] = '-:- Default games image, this image will be displayed if you do not have a image available.';
//
// 2.0.4
//
$lang['admin_tournament_header'] = 'Online Arcade Tournaments Menu';
$lang['admin_tournament_info'] = 'This control panel will help you maintain the tournamants for your online activities.<br />If you are currently having a problem with our MOD, then please contact us at <a href="http://www.phpbb-amod.co.uk" target=_blank class="copyright">www.phpbb-amod.co.uk</a> so we may help fix the problem.';
$lang['admin_moderators_header'] = 'Online Arcade Moderators Options Menu';
$lang['admin_moderators_info'] = 'This control panel will allow you to set-up the moderators options for the categories sections.<br />If you are currently having a problem with our MOD, then please contact us at <a href="http://www.phpbb-amod.co.uk" target=_blank class="copyright">www.phpbb-amod.co.uk</a> so we may help fix the problem.';
$lang['admin_default_txt'] = '<b>Default Guest Text</b><br />';
$lang['admin_default_txt_info'] = '-:- Default text that is displayed to Guests to try to get them to register.';
$lang['admin_tournament_txt'] = '<b>Use Tournament Mode</b><br />';
$lang['admin_tournament_txt_info'] = '-:- Turn on Tournament Option.';
$lang['admin_played'] = 'Hits';
$lang['admin_available'] = 'Offline';
$lang['admin_guest'] = 'Guests';
$lang['admin_image_path'] = '<b>Path to Game Image</b><br />';
$lang['admin_image_path_info'] = '-:- This path can be anywhere.<br />     (if left blank default will be be used {filename.gif}, or enter just the ext {.gif})';
//
// Changed in 2.0.8 from admin_game_level to admin_game_guest and Updated
//
$lang['admin_game_guest'] = '<b>Guest Access</b><br />';
$lang['admin_game_guest_info'] = '-:- Allow Guests to play (overrides the next 3 options)';
$lang['admin_game_offline'] = '<b>Game is Available</b><br />';
$lang['admin_game_offline_info'] = '-:- Is the game available to the users? - allows you to fix the game';
$lang['admin_game_import_ok'] = 'Imported "%s" Games Successfully<br /><br />Skipped "%s" Game Records.<br /><br />';
$lang['admin_game_moderator_info'] = 'The name of the user that is to moderate this area.';
//
// 2.0.6
//
$lang['admin_game_exists'] = 'Name Already Exists - Not Saved<br /><br />';
$lang['admin_cat_menu'] = 'Online Arcade Categories Menu';
$lang['admin_cat_header'] = 'This control panel will help you maintain the categories for your online activities.<br />If you are currently having a problem with our MOD, then please contact us at <a href="http://www.phpbb-amod.co.uk" target=_blank class="copyright">www.phpbb-amod.co.uk</a> so we may help fix the problem.';
$lang['admin_cat_saved'] = 'Category Saved Successfully<br /><br />';
$lang['admin_cat_deleted'] = 'Category Deleted<br /><br />';
$lang['admin_cat_not_deleted'] = 'Category NOT Deleted<br /><br />';
$lang['admin_cat_icon'] = '<b>ICON</b><br />';
$lang['admin_cat_icon_info'] = 'Path to the image file for this category. If blank No image will be displayed.';
$lang['admin_cat_name'] = '<b>Category Name</b><br />';
$lang['admin_cat_name_info'] = 'The Name of the Category. This is displayed after the image as a category header.';
$lang['admin_game_special'] = '<b>Special Play</b><br />';
$lang['admin_game_special_info'] = 'The Number of games required before special play is enabled. 0=OFF.';
$lang['admin_games_per_admin_info'] = '-:- This is how many games you want listed before a new page is needed in the ACP. (0 = all)';
$lang['admin_games_image_txt'] = '<b>Games Icon Image Size</b><br />';
$lang['admin_games_image_txt_info'] = '-:- Set this to the size you want your games images to be.';
$lang['admin_auto_size_txt'] = '<b>Use Auto Game Size</b><br />';
$lang['admin_auto_size_txt_info'] = '-:- This will overide the config for Flash and Image files, so they load to the correct size.<br />-:- This does NOT effect the AUTO configuration of the FLASH and IMAGE files when adding. ';
$lang['admin_guest_high_txt'] = '<b>Allow Guests to Post a High Score</b><br />';
$lang['admin_guest_high_txt_info'] = '-:- Guests can NEVER post to the AT highscore. This will turn off normal High Scores too.';
$lang['admin_at_highscore_txt'] = '<b>Use the AT HighScore System</b><br />';
$lang['admin_at_highscore_txt_info'] = '-:- Use the All Time HighScore system to hold scores.';
$lang['admin_show_stats_txt'] = '<b>Show the Stats</b><br />';
$lang['admin_show_stats_txt_info'] = '-:- Turn this off to remove the Stats from the pages';
$lang['admin_return_games'] = 'Click %sHere%s to return to the Games Menu';
//
// 2.0.7
//
$lang['admin_messages_header'] = 'Online Arcade Private Message Options Menu';
$lang['admin_messages_info'] = 'This control panel will help you maintain what private messages are sent via the Arcade mod.<br />If you are currently having a problem with our MOD, then please contact us at <a href="http://www.phpbb-amod.co.uk" target=_blank class="copyright">www.phpbb-amod.co.uk</a> so we may help fix the problem.';
$lang['admin_moderators_txt'] = '<b>Use Moderators Menu</b><br />';
$lang['admin_moderators_txt_info'] = '-:- Turn on the MCP (Moderators Control Panel).';
$lang['admin_min_posts_txt'] = '<b>Minimum Number of Posts Required for Access to the Mod</b><br />';
$lang['admin_min_posts_txt_info'] = '-:- Users below this number of posts will only be shown the guests games available,<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;and will be told to post for better access.';
$lang['admin_use_pms_txt'] = '<b>Use the Private Message System</b>';
$lang['admin_use_pms_txt_info'] = '<br />-:- If this is set the MOD will send PM\'s to the members when required.';
$lang['admin_min_rank_txt'] = '<b>Minimum Rank for Access</b>';
$lang['admin_min_rank_txt_info'] = '<br />-:- If this is set the MOD will only allow guest access to those below this rank.';
$lang['admin_game_wrong_name'] = 'You can not have spaces in game names or path names<br /><br />';
$lang['admin_game_score_type'] = '<b>Score Save Type</b><br />';
$lang['admin_game_score_type_info'] = '-:- What method of saving does this game have? (autoset by admin/mod playing)';
$lang['Auto'] = 'Automatic';
$lang['Extras'] = 'Extras';
$lang['admin_get_method'] = 'Napoleon\'s Method';
$lang['admin_post_method'] = 'Whoo\'s Method';
$lang['admin_new_method'] = 'dEfEndEr\'s Method';
$lang['admin_game_autosize'] = '<b>Turn OFF the AutoSize Setting</b><br />';
$lang['admin_game_autosize_info'] = '-:- This option will override the ACP setting if the Autosize option is ON.';
$lang['admin_cat_image_txt'] = '<b>Categories Icon Image Size</b><br />';
$lang['admin_cat_image_txt_info'] = '-:- Set this to the size you want your Categories images to be.';
$lang['admin_rank_required'] = '<b>Rank Required</b><br />-:- Set this if you want to limit this game to one rank of users.';
$lang['admin_level_required'] = '<b>Access Level Required</b><br />-:- Set this if you want to limit this game to one level of users.';
$lang['admin_import_path'] = '<br />Enter the <b>path and name</b> of the file that holds the game data:<br /><br />';
$lang['admin_import_dir'] = '<br />Enter the <b>directory</b> that holds the games.<br /><br />';
$lang['admin_import_amod'] = 'Import which type of file ?';
$lang['admin_import_online'] = 'Import All the Games as:';
$lang['admin_auto_size'] = 'Auto Size will only work on FLASH files and IMAGES';
$lang['admin_show_played_txt'] = '<b>Show the Games Played</b><br />';
$lang['admin_show_played_txt_info'] = '-:- Turn this off to remove the \'Games Played\' from the Games Menu';
$lang['admin_show_all_txt'] = '<b>Show All The Games</b><br />';
$lang['admin_show_all_txt_info'] = '-:- Turn this off to remove the \'Show All The Games\' option on the Categories Menu';
$lang['admin_show_new_txt'] = '<b>Show New Games</b><br />';
$lang['admin_show_new_txt_info'] = '-:- Turn this off to remove the \'New Games\' list and replace it with \'Special Play\' (Least Played Games)';
$lang['admin_games_zero_txt'] = '<b>Category Zero Text</b><br />';
$lang['admin_games_zero_txt_info'] = '-:- The test that is shown in the categories options for \'All Games\' (if used)';
$lang['admin_num_top_games_txt'] = '<b>Number of Top Games</b><br />';
$lang['admin_num_top_games_txt_info'] = '-:- How many games will the Top X Games list\'s show?';
//
// 2.0.8
//
$lang['admin_return_cats'] = 'Click %sHere%s to return to the Categories Menu';
$lang['admin_no_guests'] = '<b>Block All Guest Access</b><br />';
$lang['admin_no_guests_info'] = '-:- All players must me logged in members for access';
$lang['admin_ibPro_method'] = 'ibPro Method';
$lang['admin_pnflashgames_method'] = 'pnFlashGames Method';
$lang['admin_mixed_method'] = 'Mixed Method';
$lang['admin_return_games_new'] = 'Click %sHere%s to Edit Games in the Category';
$lang['admin_game_level'] = '<b>Level Required</b><br />';
$lang['admin_game_level_info'] = '-:- Only allow Admin / Mods to play';
$lang['admin_game_rank'] = '<b>Rank Required</b><br />';
$lang['admin_game_rank_info'] = '-:- Limit the game to user higher than rank.';
$lang['admin_game_group'] = '<b>Group Required</b><br />';
$lang['admin_game_group_info'] = '-:- Limit the game to this usergroup.';
$lang['admin_ban_users_txt'] = '<b>Use the Auto BAN System</b><br />';
$lang['admin_ban_users_txt_info'] = '-:- Turn this on to allow mods/admin to BAN users from the Arcade.';
$lang['admin_use_rate_txt'] = '<b>Rating System</b><br />';
$lang['admin_use_rate_txt_info'] = '-:- Turn this off to remove the option to RATE Games.';
$lang['admin_use_comment_txt'] = '<b>Comment System</b><br />';
$lang['admin_use_comment_txt_info'] = '-:- Turn this off to remove the option to Post/view Comments.';
$lang['admin_category'] = 'Category';
$lang['admin_file_not_found'] = '<br />file: <b><i>%s</b></i> NOT FOUND<br />';
$lang['admin_min_group_txt'] = '<b>Required Group for Access</b>';
$lang['admin_min_group_txt_info'] = '<br />-:- If this is set the MOD will only allow access to those in this group.';
//
//  2.1.2
//
$lang['admin_show_fav_txt'] = '<b>Show the Favourites</b><br />';
$lang['admin_show_fav_txt_info'] = '-:- Turn this off to remove the \'Favourites\' from the Games Menu';
$lang['admin_vbulletin_method'] = 'vBulletin Method';

//
// General
//
$lang['Edit_Games'] = 'Edit Games';
$lang['None'] = 'None';
$lang['All'] = 'All';
$lang['all_games'] = 'All Activities';
//
// 2.0.6
//
$lang['date_added'] = 'Date Added';
$lang['alphabetically'] = 'Alphabetically';
$lang['admin_edit_games'] = 'Edit Games';
$lang['games_not_enough_posts'] = 'Sorry but your access level will only allow guest access<br /><br />Please post in the forums for better access<br /><br />[%s]';
$lang['Played_Times'] = 'Played: %d times.';
//
// 2.0.7
//
$lang['cat_info_stats'] = 'Info / Stats';
$lang['cat_total_games'] = 'Total Games';
$lang['cat_total_played'] = 'Games Played';
$lang['cat_last_played'] = 'Last Played';
$lang['games_test_noscore'] = 'This user can not submit scores to the database<br />';
$lang['games_highscore'] = '<br /><b>Highscore ';
$lang['games_athighscore'] = '<br /><b>All Time ';
$lang['highscore_you_have'] = ' you have:</b><br /><br />';
$lang['place'] = ' place';
$lang['places'] = ' places.';
$games_position_text = array('> 20th place','st place','nd place','rd place','th place');
//
// 2.1.2
//

//
// Arcade 
//
$lang['not_enough_points'] = 'You do NOT have enough ' . $board_config['points_name'] . ' to play this game. <br /><br /><a href="javascript:parent.window.close();">[Close Window]</a>';
$lang['not_enough_reward'] = 'You do NOT have enough to play this game. <br /><br /><a href="javascript:parent.window.close();">[Close Window]</a>';
$lang['game_instructions'] = 'Instructions';
$lang['game_no_instructions'] = 'No Instructions.';
$lang['game_free'] = 'Free';
$lang['game_cost'] = 'Cost';
$lang['game_dash'] = ':';
$lang['game_number'] = '#';
$lang['game_points'] = 'Points';
$lang['game_list'] = $board_config['sitename'] . ' Activities';
$lang['game_score'] = 'Score';
$lang['game_info'] = 'Info';
$lang['game_bonuses'] = 'Bonuses';
$lang['game_best_player'] = 'Best Player';
$lang['game_highscores'] = 'Highscores';
$lang['game_highscore'] = 'Highscore';
$lang['game_at_highscores'] = 'All Time';
$lang['game_new_high_score'] = '<b>*** Congratulations - New High Score ***</b><br /><br />';
$lang['game_new_at_high_score'] = '<b>* Congratulations - New All Time High Score *</b><br /><br />';
$lang['game_score_saved'] = 'High Score was Saved';
$lang['game_score_updated'] = 'Score was Updated';
$lang['game_score_text'] = '<b>%s</b> your %s<br /><br />';
$lang['game_highscore_off'] = 'Highscores for this game are OFF';
$lang['game_no_score_saved'] = 'You don\'t have a score so your score was not saved<br />';
$lang['game_no_high_score'] = 'score did not beat your current best';
$lang['game_score_close'] = 'Close';
$lang['game_cheater'] = 'Busted!';
$lang['game_statistics'] = 'Game Statistics';
$lang['game_played'] = 'Played';
$lang['game_stat_price'] = 'Price to play';
$lang['game_stat_highscore'] = 'Highscore Bonus';
$lang['game_stat_at_highscore'] = 'AT Highscore Bonus';
$lang['game_score_reward'] = 'Score Reward';
$lang['game_all_time_score'] = 'All Time Score';
$lang['game_current_best'] = 'Current Best Player';
$lang['game_highest_score'] = 'Highest Score';
//
// 2.0.4
//
$lang['game_welcome'] = 'Welcome to the %s ';
$lang['game_guest_welcome'] = 'Welcome to %s, Please ';
$lang['game_stats'] = 'Game Stats and Information';
$lang['game_tournament'] = 'Tournament';
$lang['Game_Select'] = 'Select Game';
$lang['Active_Tournaments'] = 'There are <b>%s</b> Active Tournaments:';
$lang['at_score_no_guest'] = 'Sorry, Guests can not post to the all time high score<br /><br />Registration is free, and you could hold the game trophy..<br />';
$lang['total_games'] = 'We have <b>%s</b> activities to do in this section.';
$lang['total_games_played'] = 'In total there has been <b>%s</b> games played in this section.';
$lang['games_are_offline'] = 'Sorry, but the Activities are currently unavailable. Please try again later.';
$lang['games_register'] = ' - for access to even more games - ITS FREE.<br />';
$lang['games_top_header'] = 'Top %d Activities';
$lang['games_bum_header'] = 'Special Play';
$lang['games_catagories'] = 'Games Categories';
$lang['games_section'] = 'Section';
$lang['games_total_points'] = 'You have <b>%s</b> %s.<br />';
$lang['games_top_players'] = 'The best players are:<br />';
$lang['game_your_score'] = 'Your <i><b>%s</b></i> Score of \'<b>%s</b>\' has been submitted.<br /><br />';
$lang['game_hidden'] = 'Hidden';
//
// 2.0.6
//
$lang['remove_fav_data'] = 'Could not remove data from favorites table';
$lang['insert_fav_data'] = 'Could not insert data into favorites table';
$lang['no_fav_topic'] = 'No topic to set as favorite was set';
$lang['play_favorites'] = 'Play your Favorites List';
$lang['add_fav'] = 'Add To Favorites';
$lang['del_fav'] = 'Deleted from Favorites List<br /><br />';
$lang['already_fav'] = 'Already in Favorites List<br /><br />';
//
// 2.0.7
//
$lang['games_important_info'] = "Important Information from the Arcade";
$lang['games_pm_info'] = "Your Highscore list is being viewed by %s but he/she does not know this message was sent. \n\nYou could be about to loose some highscore trophies to him/her..!\n\n\nYours {The phpBB Arcade Mod}\n\n(You can disable messages from the arcade in your user profile)";
$lang['games_new_header'] = '%d New Activities';
$lang['games_play_again'] = '<img src="images/play_again.gif" alt="[Play Again]" border="0" />';
$lang['games_add_fav'] = 'Add to Favourites';
$lang['games_best_player'] = '<b><u>Best Player</b></u><br /><img src="images/crown.gif" alt="[Best]" border="0" /> <b><i><a href="profile.' . $phpEx . '?mode=viewprofile&u=%d">%s</a></b></i> with <b>%d</b> Wins.<br />';
$lang['games_best_at_player'] = '<b><u>Best All Time Player</b></u><br /><img src="images/crown.gif" alt="[Best]" border="0" /> <b><i><a href="profile.' . $phpEx . '?mode=viewprofile&u=%d">%s</a></b></i> with <b>%d</b> Wins.<br />';
$lang['games_last_viewed'] = 'The last item viewed was <b>%s</b>';
$lang['games_last_u_viewed'] = 'The last item you viewed was <b>%s</b>';
$lang['games_time_taken'] = 'Time';
$lang['games_unrecorded'] = 'Un-Recorded';
$lang['games_seconds'] = '%d Seconds';
$lang['Game'] = 'Game';
//
//  Updated for 2.0.8.2
//
$lang['games_pm_info_lost'] = "Your Highscore for '<b>[url=http://%sactivity.".$phpEx."?mode=game&amp;id=%d&win=self]%s[/url]</b>' has been lost to the player that this message is from.\n\n\nYours {The phpBB Arcade Mod}\n\n(You can disable messages from the arcade in your user profile)";
$lang['games_pm_info_lost_at'] = "Your All Time Highscore for '<b>[url=http://%sactivity.".$phpEx."?mode=game&amp;id=%d&win=self]%s[/url]</b>' has been lost to the player that this message is from.\n\n\nYours {The phpBB Arcade Mod}\n\n(You can disable messages from the arcade in your user profile)";
//
// 2.0.8
//
$lang['games_minutes'] = '%d min %d sec';
$lang['games_hours'] = '%d hr %d min %d sec';
$lang['games_days'] = '%d %d:%d:%d days';
$lang['games_reward_givem'] = 'Reward given for every %s points scored';
$lang['newscore_return'] = '<br />[<a href="activity.'.$phpEx.'">Activities</a>]';
$lang['return_to_arcade'] = 'Click %sHere%s to return to the Arcade';
$lang['game_size'] = '<br /><i>Filesize: %ld kb.</i>';
$lang['ON'] = 'ON';
$lang['OFF'] = 'OFF';
$lang['No_Instructions'] = 'No Instructions';
$lang['games_image_default'] = '<span class="gensmall"><i>{default}</i></span>';
$lang['games_not_enough_posts'] = 'You need to post more in the forums for access';
//
// 2.1.2
//
$lang['games_last_score_gained'] = '<br /><b>%s</b> has just scored <b>%s</b> playing <b>%s</b>';
$lang['games_new_game_added_info'] = 'New Activity Added to the Arcade';
$lang['games_new_game_added'] = "A NEW Activity has been added called '%s' be the first to get a Highscore for it.\n\n\nYours {The phpBB Arcade Mod}\n\n(You can disable messages from the arcade in your user profile)";

//
//  Comment System (2.0.8)
//
$lang['arcade_comment_edit'] = 'Arcade Comment Editor';
$lang['arcade_comment_delete'] = 'Arcade Comment Deletion';
$lang['arcade_comment_sure'] = 'Are you sure you want to delete the comment?';
$lang['no_comment_text'] = 'Sorry, you did not enter enough information';
$lang['to_much_comment_text'] = 'Sorry, This site only excepts comments up to %d charactures';
$lang['comment_poster'] = 'Poster:';
$lang['games_comment_added_info'] = "A new comment has been added to your comment left for %s..!\n\n\nYours {The phpBB Arcade Mod}\n\n(You can disable messages from the arcade in your user profile)";
$lang['games_comment_info'] = "Re: Comment from the Arcade";
//
//  2.1.2
//
$lang['arcade_added'] = 'Added';
$lang['arcade_comments'] = 'Comments';


//
//  Rating System
//
$lang['rating'] = 'Rating';
$lang['times'] = ' times.';
$lang['already_rated'] = 'Already Rated';
//
//  2.1.2
//
$lang['arcade_not_rated'] = 'Not Rated';
$lang['arcade_rated'] = 'Rated';
$lang['arcade_title'] = 'Title';
$lang['arcade_played'] = 'Played';
$lang['arcade_current_rating'] = 'Current Rating';
$lang['arcade_rate'] = 'Rate';
$lang['arcade_rate_return_cat'] = 'Click to return to the %sCategory%s';
$lang['arcade_rate_return_forum'] = 'Click to return to the %sForum%s';

//
// PM Message System
//
$lang['amod_mess_new'] = 'Send PM to all users when a new game is added.';
$lang['amod_mess_highscore'] = 'Send PM when Highscore is lost.';
$lang['amod_mess_at_highscore'] = 'Send PM when All Time Highscore is lost.';
$lang['amod_mess_comment'] = 'Send PM when new comment is added to comment left.';

//
// Moderators
//
$lang['amod_mod_config_updated'] = 'Arcade / Activites Moderators Configuration Updated<br /><br />';
$lang['moderators_options'] = 'Moderators Options';
$lang['amod_admin_offline'] = '<b>Take Mod Offline</b><br />-:- Will your moderators be able to take the mod offline.';
$lang['amod_admin_scores'] = '<b>Moderators Scores Options</b><br />-:- What control over the scores will your Moderators have?';
$lang['amod_admin_games'] = '<b>Moderators Game Options</b><br />-:- Can your Moderators take games online/offline?';
$lang['amod_admin_ban'] = '<b>Moderators Ban Users</b><br />-:- Can your moderators ban users from posting scores?';
//
// 2.0.8
//
$lang['arcade_mod_menu'] = 'phpBB Arcade Mod - Moderators Menu';
//
// 2.1.0
//
$lang['games_rate'] = "[Rate]";
$lang['games_add_comments'] = "[Comment]";
$lang['arcade_score_sure'] = "Remove Score %d for user %s<br /><br />Are you Sure";

//
// Tournament
//
$lang['amod_tour_config_updated'] = 'Arcade / Activities Tournaments Updated<br /><br />';
$lang['Total'] = 'Total';
$lang['tournament_options'] = 'Tournament Options';
$lang['tournament_max_number'] = '<b>Maximum number of Active Tournaments.</b>';
$lang['tournament_max_games'] = '<b>Maximum number of Games per Tournament.</b>';
$lang['tournament_max_players'] = '<b>Maximum number of Players per Tournament.</b>';
//
// 2.0.8
//


// If anything is changed below this line, then don't be surprised if you don't get very
// good support from the MOD Author. The next few lines deal with error handling & GPL
// licenses. By changing them you could break the law as well as cause errors.
//========================================================================================
// DO NOT TAKE THIS LINK OUT! Scott Porters Gamelib requires that this link is included
// by anyone using his library. If you don't have any games using gamelib, then turn it
// off and this link will not get displayed!
$lang['game_lib_link'] = '<br />Some of the <b>JAVA</b> games here have been created with &copy; <A HREF="http://www.javascript-games.org/gamelib/" TARGET="New_Window">GameLib</A> v2.08<br />Check out <A HREF="http://www.javascript-games.org" TARGET="New_Window">JavaScript Games</A> for more info.';
$lang['activitiy_mod_info'] = 'phpBB Activity / Arcade Mod %s &copy 2000 - 2006 - Napoleon / dEfEndEr';

//
// Errors
//
$lang['no_main_data'] = 'Couldn\'t obtain main data';
$lang['no_game_data'] = 'Couldn\'t obtain game data';
$lang['no_cat_update'] = 'Couldn\'t update category data';
$lang['no_cat_data'] = 'Couldn\'t obtain category data';
$lang['no_cat_data_enter'] = 'No category Name or Description - Saved Failed';
$lang['no_game_update'] = 'Couldn\'t update game data';
$lang['no_game_total'] = 'Error getting total games';
$lang['no_game_user'] = 'Error obtaining user game data';
$lang['no_game_delete'] = 'Couldn\'t delete game';
$lang['no_game_repair'] = 'Couldn\'t repair game tables';
$lang['no_game_save'] = 'Couldn\'t save game data';
$lang['no_user_data'] = 'Couldn\'t obtain user data';
$lang['no_user_update'] = 'Couldn\'t update user data';
$lang['no_score_data'] = 'Couldn\'t obtain scores data';
$lang['no_score_reset'] = 'Couldn\'t reset scores data';
$lang['no_score_insert'] = 'Couldn\'t insert score';
$lang['no_score_reset'] = 'Couldn\'t reset scores';
$lang['no_config_data'] = 'Could not access Online Activities configuration';
$lang['no_config_update'] = 'Failed to update Online Activities configuration for ';
$lang['no_game_info_data'] = 'ERROR, No Game Data Received<br />';
$lang['no_game_import'] = 'No file to Import / Export<br /><br />';
$lang['no_game_import_found'] = 'Import file not found<br /><br />';
$lang['no_read_game_data'] = 'Error reading from file';
$lang['no_write_game_data'] = 'Error writing to file';
//
// 2.0.4 - 2.0.6
//
$lang['no_game_data_inform'] = 'ERROR, Reading Game Data from System<br /><br />This Error is caused by your Proxy settings.<br /><br />Registering will allow you to save scores.';
$lang['session_error'] = 'Error creating new session';
$lang['game_invalid_game'] = 'ERROR - Invalid Game Options Received<br /><br />This game was not crated for this Arcade Mod<br />';
$lang['no_special_play_games'] = 'None Available';
$lang['games_no_guests'] = 'Sorry, You Have NO Access to that Game';
$lang['game_not_compatable'] = 'This game is NOT compatable with this version of Arcade mod<br />';
//
// 2.0.7
//
$lang['game_move_error'] = 'Hit an Error, Deleted a game from slot Zero<br /><br />Added it as a NEW game, Move not processed - Please Try Again';
$lang['game_at_bottom'] = 'You are already at the bottom';
$lang['game_at_top'] = 'You are already at the top';
$lang['newscore_close'] = '<br /><a href="javascript:self.close();"><img src="images/close.gif" alt="[Close]" border="0" /></a>';
/*****************************************************************************^^^^^
						Only change the word CLOSE in the line ABOVE
*****************************************************************************/
//
// 2.0.8
//
$lang['no_arcadehash_data'] = 'Error Reading Game Data from URL';
$lang['amod_update_error'] = 'Error updating database, unable to connect';
$lang['arcade_install_issue'] = 'You have not followed the install instructions correctly';
$lang['admin_arcade_reward_error'] = 'You can ONLY set-up ONE reward system at a time<br><br>';
$land['no_rate_data'] = 'Couldn\'t obtain rate data';
$lang['no_rate_update'] = 'Couldn\'t update rate data';
$lang['no_comment_data'] = 'Couldn\'t obtain comment data';
$lang['no_comment_edit_data'] = 'Couldn\'t obtain comment editing data';
$lang['no_comment_update'] = 'Couldn\'t update comment data';
$lang['no_comment_found'] = 'This comment does not exist';
$lang['does_not_exist'] = 'Does not exist.';
$lang['no_activity'] = 'No Activity specified';
$lang['bad_submitted_value'] = 'Bad submited value';
$lang['error_game_info_data'] = 'Unexpected Error - Bad GAME??';
$lang['games_group_rank_limit'] = 'Sorry, your Access level does not allow access to this area.';
//
//  2.1.0
//
$lang['arcade_file_not_found'] = '<font color="#FFFFFF">Unable to find the file:> %s</font>';
//
//  2.1.2
//
$lang['newscore_close_first'] = '<br /><a href="javascript:self.close();opener.location.reload(true);"><img src="images/close.gif" alt="[Close]" border="0" /></a>';
/*****************************************************************************^^^^^
						Only change the word CLOSE in the line ABOVE
*****************************************************************************/
$lang['arcade_incorrect_install'] = 'Incorrect Installation, Check the install.txt and update your includes/constants.php';
$lang['arcade_incorrect_version'] = 'Incorrect Installation, Version Missmatch %s should be %s Use the ACP DB Update routine';
$lang['arcade_user_held'] = 'Sorry, Your access to the Arcade/Activity section is ON-HOLD';
$lang['arcade_user_banned'] = 'Sorry, Your access to the Arcade/Activity section has been REMOVED';

// Monthly Highscore Mod
$lang['Highscore'] = 'Highscores';
$lang['highscore_table_error'] = 'Wasn\'t able to obtain data from the Highscore Table';
$lang['highscore_jan'] = 'January';
$lang['highscore_feb'] = 'February';
$lang['highscore_mar'] = 'March';
$lang['highscore_apr'] = 'April';
$lang['highscore_may'] = 'May';
$lang['highscore_jun'] = 'June';
$lang['highscore_jul'] = 'July';
$lang['highscore_aug'] = 'August';
$lang['highscore_sep'] = 'September';
$lang['highscore_oct'] = 'October';
$lang['highscore_nov'] = 'November';
$lang['highscore_dec'] = 'December';
$lang['highscore_table_header'] = 'Highscore Ranking List For';
$lang['highscore_submit'] = 'Show';
$lang['highscore_no_score'] = 'No Score added';
$lang['highscore_for'] = 'Highscores for';
$lang['highscore_count_err'] = "Wasn\'t able to count Highscores";
$lang['highscore_other_score'] = "Highscores for the month";
$lang['highscore_new_mon_score'] = '<b>New Highscore for the month added</b>';
$lang['highscore_no_new_mon_score'] = 'No new Highscore for the month reached';
//
// TFFT - The End...!
//

?>
