﻿if (timer>350) {
	enemies++;
	randomenemygenerator = random(_root.enemy_db.length);
	_root.attachMovie("enemycrapship", "enemyship"+enemies, _root.depth);
	_root.depth++;
	_root.toolbar.radar.attachMovie("radarbad", ["baddie"+enemies], _root.depth);
	_root.depth++;
	_root["enemyship"+(enemies-1)].gotoAndStop(_root.enemy_db[randomenemygenerator].ship);
	_root["enemyship"+(enemies-1)]._x = 0
	_root["enemyship"+(enemies-1)]._y = 0
	_root["enemytype"+(enemies-1)] = randomenemygenerator;
	_root["enemylife"+(enemies-1)] = _root.enemy_db[randomenemygenerator].life;
	_root["aVelocity"+(enemies-1)] = 0;
	_root["bVelocity"+(enemies-1)] = 0;
	_root["enemyoldlaser"+(enemies-1)] = 0;
	_root["enemynewlaser"+(enemies-1)] = 0;
	_root["enemystatus"+(enemies-1)] = "alive";
	timer = 00;
	_root.level++;
}
timer++;
