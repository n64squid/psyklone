<?
/*
   getscores.php: Retrieves score data from highscores table and returns 
                  data and status to Flash
   
   errorcode:
      0: successful select
      1: can't connect to server
      2: can't connect to database
      3: can't run query
*/

//  fill this in with the right data for your server/database config
$server = "localhost";
$username = "psyklone_space";
$password = "catshmuklord";
$database = "psyklone_spacebattle";

//  mysql_connect: Open a connection to a MySQL Server
//  Returns a MySQL link identifier on success, or FALSE on failure.
if (!mysql_connect($server, $username, $password)) {
   $r_string = '&errorcode=1&';	
	
//  mysql_select_db: Sets the current active database on the server that's associated 
//    with the specified link identifier. Every subsequent call to mysql_query() 
//    will be made on the active database.
//  Returns TRUE on success or FALSE on failure.
} elseif (!mysql_select_db($database)) {
   $r_string = '&errorcode=2&';
	
//  mysql_query: Sends a query (to the currently active database
//  For SELECT, SHOW, DESCRIBE or EXPLAIN statements, mysql_query() returns a 
//     resource on success, or FALSE on error.  
//  For other type of SQL statements, UPDATE, DELETE, DROP, etc, mysql_query() 
//     returns TRUE on success or FALSE on error. 
} else {
   $qr = mysql_query("SELECT * from spacescore");
   if (!qr || mysql_num_rows($qr)==0) {
      $r_string = '&errorcode=3&msg='.mysql_error().'&';
   } else {
	
      $r_string = '&errorcode=0&n='.mysql_num_rows ($qr);
      $i = 0;
      while ($row = mysql_fetch_assoc ($qr)) {
         while (list ($key, $val) = each ($row)) {
            $r_string .= '&' . $key . $i . '=' . stripslashes($val);
         }
         $i++;
      }
      // add extra & to prevent returning extra chars at the end
      $r_string .='&';
   }
}
echo $r_string;

?>