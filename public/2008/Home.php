<? 
$title=array("The Start Of It All","&quot;The Prophecy of Kendo: Prologue&quot; Released!!","Preview of 'Space Battle'","Artwork","Psyklone - A New Era","Geographical Simulation","New Game!! - Space Battle","Kendo Preview","Maths Stuff","Prime factors","Back Again");
$date=array("");
$author=array("Shmuk","Shmuk","Shmuk","Shmuk","Shmuk","Shmuk","Shmuk","Shmuk","Shmuk","Shmuk","Shmuk");
$desc=array();
array_push($desc, "Well, this is the day this website has started, and I hope that everyone will enjoy watching this site and playing the games as much as I did making them! Join the forum and meet new people!");
array_push($desc, "Here it is, fresh and shiny! Our first game, finally released. Its not as good as the upcoming games becauseit was a sort of 'practice'. However, I'm already working on the next game. I hope you like it!!");
array_push($desc, "I added a preview for 'Space Battle'. ... Oh, well. Its just to give you a preview on an upcoming minigame. Check it out in the 'games' section. Enjoy it!");
array_push($desc, "There is a new section of some of my artwork on flash. I hope you enjoy it!");
array_push($desc, "Welcome to Psyklone! This website was opened back in febuary '06, but it now has been re-modeled to look more modern, and generally better. First of all, I would like to thank Hayate for the design, as well as Moriarti for support. Well, as a short summary: You can play some games in the 'games' section, Discuss things on our forum, see some of my (un)impressive artwork, see some of our other flash projects in the 'other' section, or just contact us. Whatever it is you do, there is sure to me some way in which we can entertain you!<br /><br />Oh, and by the way, There is also a new forum, which is new and improved.<br /><br />Best wishes to everyone who has bothered reading this! ");
array_push($desc, "Well, I had to do a project for my IB ITGS class, so why not do it in Flash? It took me about two months to complete (that is, without working on it every day), and I hope it helps you to revise for exams, or just to learn a bit. It's purpose is to be used on a projector to help the teacher teach al of the physical structures mentioned in the simulation. The simulations provided are: Longshore drift, Groynes, Wave formation, River transportation and River erosion/deposition. You can find it under the 'Other' section of the website (look up on the buttons) or for a quick link, just click here. Well, I hope that that can help you!<br /><br />Anyways, according to the Psyklone games, I am trying to figure out a good way to implement the system of Space Battle into being a decent game. If you have any suggestions, mail me at shmuklidooha(@)psyklone.net. (Remove the brackets/parenthesis around the @.) ");
array_push($desc, "Find the game here! I remember starting this game back in November of 2005. Naturally, this game is inspired on Ambrosia's (www.ambrosiasw.com) classic game 'Escape Velocity', and hopefully Space Battle will end up being such a great game as EV. (Yes, I did love EV that much. I'm still impatient for its next release). Keep checking for new updates, sure to come!");
array_push($desc, "Yey, it's been almost a year since I first started making this game. Phew. I'm working on it quite a bit lately, and since there hasn't been much evidence of progress, I feel obliged to give you a preview of the battle system. Personally, I like it better than the previous one: it's more 3-dimensional, the characters are drawn better, better animated, and the the battles are 2 vs 2. For now, you can only do normal attacks, but when the full game is released, there will be more attack options. So, there it is. Check it out in the 'games' section, or click here for a direct link to it. ");
array_push($desc, "I added a couple of things in the 'other' section related to maths using functions on flash. I'ts not much, but, then again, it didn't take much to do it. I might add a few more later on.");
array_push($desc, "There ye go, a couple of possible pointless prime number calculators based on the one from before. It's not much, but it's still an update.");
array_push($desc, "Sigh... Still working on Kendo, but the thing never ends... For each thing that I finish on it, three new ideas come to mind.<br /><br /> Anyway, I made a new design for the main site which is better for me, as it is easier to edit, as well as having a few (new-ish) features:<br /><br />1. New layout. It's more friendly for those with lower screen resolution, as well as it's centered. The old PK design was indented to the left, and it wasn't really good to look at.<br />2. Added the old PK designs to the Other section<br />3. Removed the Art section. I never added anything to it since i first released it, and there weren't any prospects of doing so either.<br /><br />So yeah, That should be quite enough till I release a new game.");

$i=sizeof($title)-1;
while($i>=0){
	?>
	<tr>
		<td>
			<font color="#006699">
				<? echo "<font color='#00BBBB' size='+1'><u>" . $title[$i] . "</u></font> - <i> by " . $author[$i] . "</i>";
				echo "<br /><br />" . $desc[$i]; ?>
			</font>
		</td>
	</tr>
	<? 
	$i--;
} ?>
