<? 
$title=array("Prime number calculator","Sine & Cosine","Slow Prime Numbers","Prime Factors");
$link=array("PrimeF","Sine","PrimeS","PrimeFactor");
$image=array("PrimeF.jpg","sine.jpg","PrimeS.jpg","PrimeFactor.jpg");
$desc=array();
array_push($desc, "A prime number calculator which calculates all the prime numbers up to the number you say in one frame.");
array_push($desc, "To show how the sine and cosine functions are related to the degrees of a circle. (Yes, I know it kinda gets out of sync after a short while)");
array_push($desc, "A remake of the prime number calculator which calculates one prime number per frame, thus allowing larger numbers to be calculated, but at a slower rate.");
array_push($desc, "Yet another prime number calculator, but it gives you the prime factors of the number you choose.");

$i=sizeof($title)-1;
while($i>=0){
	?>
	<tr>
		<td colspan="2" width="10%">
			<? echo "<a href='?t=" . $link[$i] . "'><img src='Thumbs/" . $image[$i] . "' /></a>"?>
		</td>
		<td>
			<font color="#006699">
				<? echo "<a href='?t=" . $link[$i] . "'><font color='#00BBBB' size='+1'><u>" . $title[$i] . "</u></font></a>";
				echo "<br /><br />" . $desc[$i]; ?>
			</font>
		</td>
	</tr>
	<? 
	$i--;
} ?>